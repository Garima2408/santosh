package co.savm.teacher;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.savm.R;
import co.savm.activities.SignIn;
import co.savm.models.CollageRoleArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class TeacherRegistrationUpdate extends BaseAppCompactActivity {
    EditText edt_department,edt_degisnation,edt_email_id;
    Button btn_go_dashboard;
    TextView collageName;
    String department,degisnation,email;
    private RegistrationTeacherAuthTask registrationteacherAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_registration_update);
        edt_department = findViewById(R.id.edt_department);
        edt_degisnation = findViewById(R.id.edt_degisnation);
        edt_email_id = findViewById(R.id.edt_email_id);
        btn_go_dashboard = findViewById(R.id.btn_go_dashboard);
        collageName = findViewById(R.id.collageName);
        String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        SpannableString spString = new SpannableString(Topheading);
        AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
        spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        collageName.setText(spString);
        displayUserData();


        btn_go_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptToTeacherRegistration();


            }
        });
    }


    private void displayUserData() {

        if (!co.savm.utils.TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment())) {
            edt_department.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment());
        }
        if (!co.savm.utils.TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation())) {
            edt_degisnation.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation());
        }

        if (!co.savm.utils.TextUtils.isNullOrEmpty(getUser().email)) {
            edt_email_id.setText(getUser().email);
        }


    }



    private void attemptToTeacherRegistration() {
        edt_department.setError(null);
        edt_degisnation.setError(null);
        edt_email_id.setError(null);


        // Store values at the time of the login attempt.
        department= edt_department.getText().toString().trim();
        degisnation = edt_degisnation.getText().toString().trim();
        email = edt_email_id.getText().toString().trim();

        CollageRoleArray userRoleDetail = new CollageRoleArray();
        userRoleDetail.role = "14";
        userRoleDetail.tnid = SessionManager.getInstance(getActivity()).getCollage().getTnid();
        userRoleDetail.title = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        userRoleDetail.field_group_image = SessionManager.getInstance(getActivity()).getCollage().getField_group_image();
        userRoleDetail.CollageLogo =SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo();
        userRoleDetail.CollageRole ="faculty";
        userRoleDetail.CollageEmail = email;
        userRoleDetail.CollageDesignation = degisnation;
        userRoleDetail.Collagedepartment = department;

        SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);


        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            focusView = edt_email_id;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        } else if (!Utils.isValidEmailAddress(email)) {
            focusView = edt_email_id;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_invalid_email));
            // Check for a valid password, if the user entered one.
        }else if (co.savm.utils.TextUtils.isNullOrEmpty(degisnation)) {
            // check for First Name
            focusView = edt_degisnation;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(department)) {
            // check for First Name
            focusView = edt_department;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            registrationteacherAuthTask = new RegistrationTeacherAuthTask();
            registrationteacherAuthTask.execute();



        }
    }


    private class RegistrationTeacherAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("group_type","node")
                    .add("field_i_am_a","faculty")
                    .add("state","1")
                    .add("etid",SessionManager.getInstance(getActivity()).getUser().userprofile_id)
                    .add("gid",SessionManager.getInstance(getActivity()).getCollage().getTnid())
                    .add("field_college_email",email)
                    .add("field_department",department)
                    .add("field_faculty_designation",degisnation)
                    .add("roles[14]","faculty")
                    .build();





            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_STUDENETREGISTRATION+"/"+SessionManager.getInstance(getActivity()).getCollage().getCollageMemberShipId(),body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherRegistrationUpdate.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            registrationteacherAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(TeacherRegistrationUpdate.this);
                        builder.setMessage("Role Change Successful. Login Again")
                                .setCancelable(false)
                                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        Intent i = new Intent(TeacherRegistrationUpdate.this, SignIn.class);
                                        startActivity(i);
                                        dialog.dismiss();
                                        finish();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            registrationteacherAuthTask = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {

        finish();
    }
}

