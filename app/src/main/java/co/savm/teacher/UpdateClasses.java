package co.savm.teacher;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import co.savm.R;
import co.savm.models.SubjectFacultyArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class UpdateClasses extends BaseAppCompactActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener ,View.OnClickListener {
    EditText ClassTittle,Details,roomno,bulding,location;
    TextView StartDate,DateEnds,StartTime,EndTime;
    String Classes_Id,Subject_id,tittle,classDetails,classTeacher,classDatestart,Detailbulding,classDatesend,classtimestart,classtimesend,Buldingno,Location,format,weekmon,weektues,weekwedns,weekthur,weekfri,weeksat,weeksun,classrepeat,time24format,time24formatreapet,time24formarend,TeacherEmail,TeacherUid,Classroom;
    String  DetailClassTeacher,DetailClassDetails,DetailClassroom,DetailLocation,ClassEndDate,
            DetailclassDate,DetailClassStart,DetailClassEndDate,DetailClassEnd,Detailweeknos;

    CheckBox repeat,mon, tue, wed, thur, fri, sat, sun;
    private int CalendarHour, CalendarMinute;
    LinearLayout Weeklayoutlayout;
    StringBuilder Weekselect;
    TimePickerDialog timepickerdialog;
    private Calendar mcalender;
    private int year, month, day, week;
    private int startDay, startMonth, startYear;
    Double Lat,Long;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;
    private ClassCreateTeacherAuthTask classcreateteacherAuthTask = null;
    private ProgressSearchOfTeacherList progresssearchofteacherlist = null;
    public ArrayList<SubjectFacultyArray> mfacultyList;
    private ArrayList<String> facultynamelist;
    //To store longitude and latitude from map
    private double longitude;
    boolean isPressed = false;
    private double latitude;
    JSONObject ClassDetailsJson;
    private ArrayList<String> selectedclassdetails;
    Spinner Teacher;
    private ProgressClassDetails classesdetailsAuthTask = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_classes);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        Bundle b = getActivity().getIntent().getExtras();
        Classes_Id = b.getString("CLASS_ID");
        Subject_id = b.getString("COURSE_ID");
        Teacher =findViewById(R.id.Teacher);
        ClassTittle =findViewById(R.id.ClassTittle);
        Details =findViewById(R.id.Details);
        roomno =findViewById(R.id.roomno);
        bulding =findViewById(R.id.bulding);
        location =findViewById(R.id.location);
        StartDate =findViewById(R.id.StartDate);
        DateEnds =findViewById(R.id.DateEnds);
        StartTime =findViewById(R.id.StartTime);
        Weeklayoutlayout =findViewById(R.id.Weeklayoutlayout);
        EndTime =findViewById(R.id.EndTime);
        mon =findViewById(R.id.mon);
        tue =findViewById(R.id.tue);
        wed =findViewById(R.id.wed);
        thur =findViewById(R.id.thu);
        fri =findViewById(R.id.fri);
        sat =findViewById(R.id.sat);
        sun =findViewById(R.id.sun);
        repeat =findViewById(R.id.repeat);

       /* mcalender = Calendar.getInstance();
        CalendarHour = mcalender.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = mcalender.get(Calendar.MINUTE);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = sdf.format(c.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(mcalender.getTime());


        StartDate.setText(formattedDate);
        DateEnds.setText(formattedDate);


        classDatestart = formattedDate2;
        classDatesend = formattedDate2;

        String time = new SimpleDateFormat("hh:mm a").format(new java.util.Date().getTime());

       *//* SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
        String time = simpleDateFormat.format(mcalender.getTime());
*//*
        StartTime.setText(time);
        EndTime.setText(time);




        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(time);
            time24format =date.toString();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        time24format = displayFormat.format(date);

        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));




        classtimestart =time24format;
        classtimesend =time24format;*/


        classrepeat ="0";

        StartDate.setOnClickListener(this);
        DateEnds.setOnClickListener(this);
        StartTime.setOnClickListener(this);
        EndTime.setOnClickListener(this);
        repeat.setOnClickListener(this);
        mon.setOnClickListener(this);
        tue.setOnClickListener(this);
        wed.setOnClickListener(this);
        thur.setOnClickListener(this);
        fri.setOnClickListener(this);
        sat.setOnClickListener(this);
        sun.setOnClickListener(this);

        Weekselect =new StringBuilder();


        mfacultyList=new ArrayList<>();
        facultynamelist = new ArrayList<String>();
        progresssearchofteacherlist = new ProgressSearchOfTeacherList();
        progresssearchofteacherlist.execute();

        GetAllServicesDetails();

    }
    private void GetAllServicesDetails() {

        classesdetailsAuthTask = new ProgressClassDetails();
        classesdetailsAuthTask.execute();


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.StartDate:
                hideKeyBoard(v);

                StartdateDialog();
                break;

            case R.id.DateEnds:
                hideKeyBoard(v);

                StartEnddateDialog();
                break;

            case R.id.repeat:
                hideKeyBoard(v);

                if (((CheckBox) v).isChecked()) {

                    classrepeat = "1";
                    Weeklayoutlayout.setVisibility(View.VISIBLE);

                }else {
                    classrepeat = "0";
                    Weeklayoutlayout.setVisibility(View.GONE);

                }
                break;

            case R.id.StartTime:
                hideKeyBoard(v);


                timepickerdialog = new TimePickerDialog(UpdateClasses.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }

                                String Slow =(hourOfDay + ":" + minute + format);
                                StartTime.setText(hourOfDay + ":" + minute + format);



                                DateFormat f1 = new SimpleDateFormat("h:mma", Locale.US);

                                try {
                                    Date d = f1.parse(Slow);
                                    DateFormat f2 = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                    String FF = f2.format(d).toLowerCase();
                                    Log.d("TAG", "f2 " + FF.toString());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }




                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String Time = null;
                                try {
                                    Time = outFormatx.format(inFormatx.parse(Slow));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatS : " + Time);

                                classtimestart=Time;





                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();
                break;
            case R.id.EndTime:
                hideKeyBoard(v);
                timepickerdialog = new TimePickerDialog(UpdateClasses.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }

                                String SlowEnd =(hourOfDay + ":" + minute + format);
                                EndTime.setText(hourOfDay + ":" + minute + format);

                                Log.d("TAG", "EndTime " + EndTime);


                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String time24 = null;
                                try {
                                    time24 = outFormatx.format(inFormatx.parse(SlowEnd));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatE : " + time24);

                                classtimesend=time24;





                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();

                break;



        }


    }

    private void StartdateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }


    private void StartEnddateDialog() {
        DatePickerDialog.OnDateSetListener listenerend = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateEnd(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listenerend, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        // dpDialog.show();

        Calendar c = Calendar.getInstance();

        // Change date
        c.set(Calendar.YEAR, startYear);
        c.set(Calendar.MONTH, startMonth);
        c.set(Calendar.DATE, startDay);
        Date newDate = c.getTime();

        dpDialog.getDatePicker().setMinDate(newDate.getTime() - 1000);
        dpDialog.show();

    }


    private void showDateStart(int year, int month, int day) {
        Log.d("TAG", "Start Date: " + day + "-" + month + "-" + year);
        classDatestart = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        StartDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));

    }

    private void showDateEnd(int year, int month, int day) {
        Log.d("TAG", "End Date: " + day + "-" + month + "-" + year);
        classDatesend = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        DateEnds.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));


    }
    private class ProgressClassDetails extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTCLASSESDETAILS+ "/" +Classes_Id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UpdateClasses.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            classesdetailsAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");

                        tittle =data.getString("title");
                        DetailClassTeacher =data.getString("field_instructor_class");
                        DetailClassDetails=data.getString("body");
                        Detailbulding=data.getString("field_building");
                        DetailClassroom =data.getString("field_grades");

                        JSONArray ClassdateArrays = data.getJSONArray("field_class_time");
                        JSONArray ar = data.getJSONArray("field_class_time");


                        if(ClassdateArrays != null && ClassdateArrays.length() > 0 ) {


                            for (int j = 0; j < ClassdateArrays.length(); j++) {
                                DetailclassDate = ClassdateArrays.getJSONObject(0).getString("startDate");
                                DetailClassStart = ClassdateArrays.getJSONObject(j).getString("startTime");
                                //  DetailClassEndDate  = ClassdateArrays.getJSONObject(j).getString("endDate");
                                DetailClassEnd = ClassdateArrays.getJSONObject(j).getString("endTime");
                                Detailweeknos = ClassdateArrays.getJSONObject(j).getString("Week");

                                Log.d("TAG", "recource: " + DetailclassDate + DetailClassStart+Detailweeknos);
                                JSONObject lastObj = ClassdateArrays.getJSONObject(ClassdateArrays.length()-1);
                                ClassEndDate = lastObj.getString("endDate");
                                DetailClassEndDate =ClassEndDate;

                                Log.d("TAG", "ClassEndDatedddd: " + ClassEndDate +" "+ DetailClassEndDate);


                            }


                        }

                        JSONObject Local = data.getJSONObject("field_dept_location");
                        Lat = Double.valueOf(Local.getString("lat"));
                        Long = Double.valueOf(Local.getString("lng"));

                        Log.d("TAG", "recource3: " + Lat + Long);

                        ClassTittle.setText(tittle);
                        Details.setText(DetailClassDetails);
                        StartDate.setText(DetailclassDate);

                        DateFormat inputFormat = new SimpleDateFormat("MMM-dd-yyyy");
                        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");

                        Date datemain;
                        try {
                            datemain = inputFormat.parse(DetailclassDate);
                            String outputDateStr = outputFormat.format(datemain);

                            classDatestart =outputDateStr;

                            System.out.println(classDatestart);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        DateEnds.setText(ClassEndDate);
                        String convert =DateEnds.getText().toString();

                       // String  convert ="Mar-05-2018";

                        DateFormat inputFormat1 = new SimpleDateFormat(" MMM-dd-yyyy");
                        DateFormat outputFormat1 = new SimpleDateFormat("yyyy-MM-dd");

                        Date datemainend;
                        try {
                            datemainend = inputFormat1.parse(convert);
                            String outputDateStr2 = outputFormat1.format(datemainend);
                            classDatesend =outputDateStr2;
                            System.out.println(classDatesend);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        StartTime.setText(DetailClassStart);

                        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss",Locale.US);
                        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a",Locale.US);
                        Date date;
                        try {
                            date = parseFormat.parse(DetailClassStart);
                            time24format = displayFormat.format(date);
                            System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));
                            classtimestart =time24format;


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }





                        EndTime.setText(DetailClassEnd);


                        SimpleDateFormat displayFormat1 = new SimpleDateFormat("HH:mm:ss",Locale.US);
                        SimpleDateFormat parseFormat1 = new SimpleDateFormat("hh:mm a",Locale.US);
                        Date date1;
                        try {
                            date1 = parseFormat1.parse(DetailClassEnd);
                            time24formarend = displayFormat1.format(date1);
                            System.out.println(parseFormat1.format(date1) + " = " + displayFormat1.format(date1));

                            classtimesend =time24formarend;

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        bulding.setText(Detailbulding);
                        roomno.setText(DetailClassroom);
                        // Classlocation.setText("Location");

                        onSearch();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            classesdetailsAuthTask = null;
            hideLoading();


        }
    }

    private class ProgressSearchOfTeacherList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDTEACHERINSUBJECTLIST+"/"+Subject_id+"/faculty");                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UpdateClasses.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofteacherlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i=0; i<data.length();i++) {



                            SubjectFacultyArray TeacherInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SubjectFacultyArray.class);
                            facultynamelist.add(TeacherInfo.getName());
                            mfacultyList.add(TeacherInfo);

                            Teacher.setAdapter(new ArrayAdapter<String>(UpdateClasses.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    facultynamelist));


                            Teacher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    TeacherUid = mfacultyList.get(position).getUid();
                                    TeacherEmail = mfacultyList.get(position).getMail();

                                    Log.d("TAG", "edt_selectsub: " + TeacherUid +TeacherEmail);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {}

                            });


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofteacherlist = null;
            hideLoading();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:

                AddClassesIncourceProcess();
                return true;
            case android.R.id.home:
                finish();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void selectdays() {

        int totalselect = 0;
        Weekselect.append("");

        if (mon.isChecked()) {
                weekmon ="MO";
                Weekselect.append(weekmon+","+" " );
                totalselect += 1;
            }else {


            }

            if (tue.isChecked()) {
                weektues ="TU";
                Weekselect.append(weektues+","+" " );
                totalselect += 1;
            }
            else {

            }
            if (wed.isChecked()) {
                weekwedns ="WE";
                Weekselect.append(weekwedns+","+" " );
                totalselect += 1;
            }
            else {

            }

            if (thur.isChecked()) {
                weekthur ="TH";
                Weekselect.append(weekthur+","+" ");
                totalselect += 1;
            }
            else {

            }
            if (fri.isChecked()) {
                weekfri ="FR";
                Weekselect.append(weekfri+","+" ");
                totalselect += 1;
            }
            else {

            }

            if (sat.isChecked()) {
                weeksat ="SA";
                Weekselect.append(weeksat+","+" ");
                totalselect += 1;
            }
            else {

            }
            if (sun.isChecked()) {
                weeksun ="SU";

                sun.setTextColor(Color.parseColor("#FFFFFF"));
                Weekselect.append(weeksun+","+" ");
                totalselect += 1;
            }else {

            }






    }


    private void AddClassesIncourceProcess() {
        selectdays();
        ClassTittle.setError(null);
        Details.setError(null);
        roomno.setError(null);
        bulding.setError(null);



        // Store values at the time of the login attempt.
        tittle = ClassTittle.getText().toString().trim();
        classDetails = Details.getText().toString().trim();
        Buldingno = bulding.getText().toString().trim();
        Location = location.getText().toString().trim();
        Classroom  = roomno.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(tittle)) {
            focusView = ClassTittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        } else if (co.savm.utils.TextUtils.isNullOrEmpty(classDetails)) {
            // check for First Name
            focusView = Details;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (co.savm.utils.TextUtils.isNullOrEmpty(Buldingno)) {
            // check for First Name
            focusView = bulding;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

        } else if (co.savm.utils.TextUtils.isNullOrEmpty(Classroom)) {
            // check for First Name
            focusView = roomno;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));


        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            classcreateteacherAuthTask = new ClassCreateTeacherAuthTask();
            classcreateteacherAuthTask.execute();
        }
        ClassDetailsJson = new JSONObject();

        try {
            ClassDetailsJson.put("start_date", classDatestart);
            ClassDetailsJson.put("end_date", classDatesend);
            ClassDetailsJson.put("start_time", classtimestart);
            ClassDetailsJson.put("end_time", classtimesend);
            ClassDetailsJson.put("week", Weekselect);
            ClassDetailsJson.put("repeat", classrepeat);


            Log.d("TAG", "classdetails: " + ClassDetailsJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }








    private class ClassCreateTeacherAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();



            RequestBody body = new FormBody.Builder()
                    .add("nid",Classes_Id)
                    .add("title",tittle)
                    .add("body",classDetails)
                    .add("field_instructor_class", TeacherEmail+" [uid:"+TeacherUid+"]")
                    .add("field_dept_location[lat]", String.valueOf(Lat))
                    .add("field_dept_location[lng]", String.valueOf(Long))
                    .add("field_building",Buldingno)
                    .add("field_grades",Classroom)
                    .add("field_class_time",ClassDetailsJson.toString())
                    .build();
            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_TEACHERCREATECLASS+"/"+Classes_Id,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                UpdateClasses.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            classcreateteacherAuthTask = null;
            try {
                if (responce != null) {

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                      //  showAlertDialog(msg);
                        hideLoading();
                        TeacherClassList.CalledFromAddClass();
                        finish();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);
                        finish();


                    }
                }
            } catch (JSONException e) {
                hideLoading();


            }
        }

        @Override
        protected void onCancelled() {
            classcreateteacherAuthTask = null;
            hideLoading();



        }
    }






    public void onSearch() {
       /* Mainlocation = (EditText) findViewById(R.id.Editserch);
        String locan = location.getText().toString();*/
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }





        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null) {
            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
        }
        if (SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());
        }

        Log.d("TAG", "recource3: " + Lat + Long);

        LatLng latLng = new LatLng(Lat, Long);
        mMap.addMarker(new MarkerOptions().position(latLng).title(SessionManager.getInstance(getActivity()).getCollage().getTitle()));
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        //  Toast.makeText(this, "", Toast.LENGTH_LONG).show();


    }


    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);


            //moving the map to location
            moveMap();
            onSearch();
        }
    }

    //Function to move the map
    private void moveMap() {
        //String to display current latitude and longitude
       /* String msg = latitude + ", "+longitude;
        Log.d("TAG", "msg: " + msg);
        //Creating a LatLng Object to store Coordinates
        LatLng latLng = new LatLng(latitude, longitude);
*/

        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null) {
            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
        }
        if (SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());
        }

        Log.d("TAG", "recource3: " + Lat + Long);

        LatLng latLng = new LatLng(Lat, Long);



        //Adding marker to map
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
                .title(SessionManager.getInstance(getActivity()).getCollage().getTitle())); //Adding a title

        //Moving the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        //Animating the camera
        //  mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        //Displaying current coordinates in toast
        //  Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        getCurrentLocation();
        moveMap();
        onSearch();
    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //Clearing all the markers
        mMap.clear();

        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //Moving the map
        moveMap();
        onSearch();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
