package co.savm.teacher;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.savm.R;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import co.savm.utils.TextUtils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CreateMemberInCollage extends BaseAppCompactActivity {
    String Course_id,CourseTittle;
    EditText edt_first_name,edt_last_name,edt_email_address,edt_enrollment_address;
    Button btn_create_student;
    private ProgressCreateMemberInCollage creatememberTask = null;
    private SearchedInviteStudent sendinvitetostudent = null;
    String   NewUser_id,New_User;
    String  email,enrollment,firstName,LastName;
    JSONArray studentselected;
    JSONObject shareAttandanceJson;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_member_in_collage);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();





        edt_first_name = findViewById(R.id.edt_first_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_email_address = findViewById(R.id.edt_email_address);
        edt_enrollment_address = findViewById(R.id.edt_enrollment_address);
        btn_create_student = findViewById(R.id.btn_create_student);

        if(extras.containsKey("CameFrominvite")){
            if(extras.getString("CameFrominvite").equals("CameFrominvite")) {
                Course_id = extras.getString("COURSE_ID");
                CourseTittle = extras.getString("TITTLE");


            }
        }
        btn_create_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptToRegister();

            }
        });



    }







    private void attemptToRegister() {

        // Reset errors.
        edt_first_name.setError(null);
        edt_last_name.setError(null);
        edt_enrollment_address.setError(null);


        // Store values at the time of the login attempt.
        email = edt_email_address.getText().toString().trim();
        enrollment = edt_enrollment_address.getText().toString().trim();
        firstName = edt_first_name.getText().toString().trim();
        LastName = edt_last_name.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isNullOrEmpty(enrollment)) {
            // Check for a valid password, if the user entered one.
            focusView = edt_enrollment_address;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }  else if (TextUtils.isNullOrEmpty(firstName)) {
            // check for First Name
            focusView = edt_first_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (TextUtils.isNullOrEmpty(LastName)) {
            // check for Contact No
            focusView = edt_last_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));         }
        if (cancel) {
            // There was an error; don't attempt Registration and focus the first
            // form field with an error.
            if (focusView != null) {
                focusView.requestFocus();
            }
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.

            studentselected = new JSONArray();

            shareAttandanceJson = new JSONObject();
            try {
                shareAttandanceJson.put("first_name", firstName);
                shareAttandanceJson.put("last_name", LastName);
                shareAttandanceJson.put("enroll", enrollment);
                shareAttandanceJson.put("cid", SessionManager.getInstance(getActivity()).getCollage().getTnid());
                shareAttandanceJson.put("parent_email", email);
                studentselected.put(shareAttandanceJson);


                Log.d("TAG", "studentselected: " + studentselected);
                Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

            } catch (JSONException e) {
                e.printStackTrace();
            }



            creatememberTask = new ProgressCreateMemberInCollage();
            creatememberTask.execute(studentselected.toString());


        }
    }









    private class ProgressCreateMemberInCollage extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("records", args[0])
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_CREATE_STUDENTINCOLLAGE,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CreateMemberInCollage.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            creatememberTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject newData = responce.getJSONObject("data");
                        if (newData != null && newData.length() > 0) {

                            New_User = newData.getString("is_new");
                            NewUser_id = newData.getString("uid");

                            sendinvitetostudent = new SearchedInviteStudent();
                            sendinvitetostudent.execute(Course_id,NewUser_id);


                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                showAlertDialog(getString(R.string.error_responce));
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            showAlertDialog(getString(R.string.error_responce));
            creatememberTask = null;
            hideLoading();


        }
    }

    private class SearchedInviteStudent extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type","subject")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDSTUDENTINSUBJECT+"/"+args[0]+"/"+args[1], body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                showAlertDialog(getString(R.string.error_responce));
                hideLoading();



            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetostudent = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // showAlertDialog(msg);
                        Invite_Students.RefreshedThepage();
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);
                        hideLoading();

                    }
                }
            } catch (JSONException e) {
                showAlertDialog(getString(R.string.error_responce));
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetostudent = null;
            hideLoading();


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.createstudent_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.Upload_Csv:




                return true;



            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
