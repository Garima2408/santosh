package co.savm.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.SubjectExamsArray;
import co.savm.models.SubjectsAssignmentsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class TeacherResultUpload extends BaseFragment {
    static Activity activity;
    static private  ProgressBar spinner;
    static  String Subject_id,Adminuser,CourseTittle;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4,floatingActionButton5;
    static TextView ErrorText;
    static ListView ResultDisplaylist;
    static ArrayList<SubjectExamsArray> listofexams;
    public static ArrayList<SubjectsAssignmentsArray> mAssignmentList;
    private static ProgressSearchOfExamList progresssearchofexamlist = null;
    private static SubjectsAssignmentsDisplay assignmentdisplay = null;
    private static ArrayList<String> examlist;
    private static ArrayList<String> assignmentlist;
    RelativeLayout MainLayout;
    Spinner TypeList;
    FrameLayout top;
    static String ResultTpyeSelected;
    View view1;
    static ImageView ReloadProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_teacher_result_upload, container, false);
        activity = (Activity) view.getContext();
        Subject_id = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        CourseTittle = getArguments().getString("TITTLE");
        if (Subject_id == null) {
        }
        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);
        ErrorText =view.findViewById(R.id.ErrorText);
        ResultDisplaylist =view.findViewById(R.id.ResultDisplaylist);
        spinner= view.findViewById(R.id.progressBar);
        TypeList =view.findViewById(R.id.TypeList);
        top =view.findViewById(R.id.top);
        view1 =view.findViewById(R.id.view1);
        MainLayout = view.findViewById(R.id.MainLayout);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);

        examlist = new ArrayList<String>();
        listofexams = new ArrayList<SubjectExamsArray>();
        assignmentlist =new ArrayList<>();
        mAssignmentList=new ArrayList<>();
        final String[] ResultType = { "Exam", "Assignment",  };

        if (Adminuser.equals("TRUE")) {
            top.setVisibility(View.VISIBLE);
            materialDesignFAM.setVisibility(View.VISIBLE);

        } else if (Adminuser.equals("FALSE")) {
            top.setVisibility(View.GONE);
            setHasOptionsMenu(false);
            materialDesignFAM.setVisibility(View.INVISIBLE);
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");
        }


        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        ArrayAdapter classadapter = new ArrayAdapter(activity,android.R.layout.simple_spinner_item,ResultType);
        classadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        TypeList.setAdapter(classadapter);


        TypeList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ResultTpyeSelected = ResultType[position];

                if (ResultType !=null){


                    if (ResultTpyeSelected.matches("Exam")){
                        mAssignmentList.clear();
                        assignmentlist.clear();
                        examlist.clear();
                        listofexams.clear();
                        progresssearchofexamlist = new ProgressSearchOfExamList();
                        progresssearchofexamlist.execute(Subject_id);

                    }else if (ResultTpyeSelected.matches("Assignment")){

                        mAssignmentList.clear();
                        assignmentlist.clear();
                        examlist.clear();
                        listofexams.clear();
                        assignmentdisplay = new SubjectsAssignmentsDisplay();
                        assignmentdisplay.execute(Subject_id);

                    }


                    }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Subject_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Subject_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        return view;
    }





    public static void CalledFromAddResult() {


        if (ResultTpyeSelected.matches("Exam")){
            examlist.clear();
            listofexams.clear();
            progresssearchofexamlist = new ProgressSearchOfExamList();
            progresssearchofexamlist.execute(Subject_id);


        }else if (ResultTpyeSelected.matches("Assignment")){
            assignmentlist.clear();
            mAssignmentList.clear();
            assignmentdisplay = new SubjectsAssignmentsDisplay();
            assignmentdisplay.execute(Subject_id);

        }




    }







    private static class ProgressSearchOfExamList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTEXAMS+"?"+"sid="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });
            }
            return jsonObject;
        }






        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofexamlist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if(data != null && data.length() > 0 ) {
                            for (int i=0; i<data.length();i++) {
                                SubjectExamsArray ExamInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SubjectExamsArray.class);
                                examlist.add(ExamInfo.getTitle());
                                listofexams.add(ExamInfo);

                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_list_item_1, android.R.id.text1,
                                    examlist);
                            ResultDisplaylist.setAdapter(adapter);
                            // Bind array strings into an adapter


                            // Capture ListView item click
                            ResultDisplaylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    // Capture the click position and set it into a string
                                    String Classname = (String) ResultDisplaylist.getItemAtPosition(position);
                                    String ExamId =listofexams.get(position).getTnid();
                                    String ExamName =listofexams.get(position).getTitle();

                                    Intent intent=new Intent(activity,StudentsResultDisplay.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("COURSE_ID",Subject_id);
                                    bundle.putString("EXAM_ID",ExamId);
                                    bundle.putString("EXAMNAME",ExamName);
                                    intent.putExtras(bundle);
                                    activity.startActivity(intent);



                                }
                            });


                            }else {


                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Exams Are Added");


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);



            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofexamlist = null;
            spinner.setVisibility(View.INVISIBLE);



        }
    }

    /*LIST OF ASSIGNMENTS*/


    private static class SubjectsAssignmentsDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTASSIGNMENTS+"?"+"sid="+args[0]+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentdisplay = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    ErrorText.setVisibility(View.GONE);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i=0; i<jsonArrayData.length();i++) {
                                SubjectsAssignmentsArray assignmentInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectsAssignmentsArray.class);
                                mAssignmentList.add(assignmentInfo);
                                assignmentlist.add(assignmentInfo.getTitle());

                            }

                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_list_item_1, android.R.id.text1,
                                    assignmentlist);
                            ResultDisplaylist.setAdapter(adapter);
                            // Bind array strings into an adapter


                            // Capture ListView item click
                            ResultDisplaylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    // Capture the click position and set it into a string
                                    String Classname = (String) ResultDisplaylist.getItemAtPosition(position);
                                    String ExamId =mAssignmentList.get(position).getTnid();
                                    String ExamName =mAssignmentList.get(position).getTitle();

                                    Intent intent=new Intent(activity,StudentsResultDisplay.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("COURSE_ID",Subject_id);
                                    bundle.putString("EXAM_ID",ExamId);
                                    bundle.putString("EXAMNAME",ExamName);
                                    intent.putExtras(bundle);
                                    activity.startActivity(intent);



                                }
                            });


                            }
                            else {


                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Assignments");

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);
                ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            assignmentdisplay = null;
            spinner.setVisibility(View.GONE);


        }
    }



}
