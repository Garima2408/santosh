package co.savm.teacher;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.StudentAttendanceAdapter;
import co.savm.models.StudentListsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import okhttp3.OkHttpClient;

public class AttendanceStudentList extends BaseAppCompactActivity {

    RecyclerView rv_studentlist;
    public ArrayList<StudentListsArray> mStudentList;
    private StudentAttendanceAdapter studentadapter;
    private RecyclerView.LayoutManager layoutManager;
    FloatingActionButton fab;
    private StudentAttendanceListDisplay studentattendancelist = null;
    String Course_id,Module,CourseTittle;
    TextView SubjectSelect,ModuleSelect;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_list);
        Bundle b = getIntent().getExtras();

        Course_id = b.getString("COURSE_ID");
        CourseTittle = b.getString("TITTLE");
        Module =b.getString("ModuleItem");

        Log.d("TAG", "Strings: " + Course_id+CourseTittle+Module);


        SubjectSelect =findViewById(R.id.SubjectSelect);
        ModuleSelect =findViewById(R.id.ModuleSelect);

        SubjectSelect.setText(CourseTittle);
        ModuleSelect.setText(Module);
        mStudentList=new ArrayList<>();


        layoutManager = new LinearLayoutManager(this);

        rv_studentlist = findViewById(R.id.rv_studentlist);
        rv_studentlist.setHasFixedSize(true);
        rv_studentlist.setLayoutManager(layoutManager);


        studentattendancelist = new StudentAttendanceListDisplay();
        studentattendancelist.execute();

        fab = findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  startActivity (new Intent(AttendanceStudentList.this, FAQ.class));

            }
        });


        SubjectSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(AttendanceStudentList.this, SubjectSelect);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.adddata, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        return true;
                    }
                });

                popup.show();//showing popup menu


            }
        });

        ModuleSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(AttendanceStudentList.this, ModuleSelect);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.adddata, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {


                        ModuleSelect.setText(item.getTitle().toString());


                        return true;
                    }
                });

                popup.show();//showing popup menu



            }
        });


    }

    private class StudentAttendanceListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_STUDENTATTENDANCELIST+"/"+Course_id+"/"+"og");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AttendanceStudentList.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentattendancelist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            StudentListsArray StudentInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), StudentListsArray.class);
                            mStudentList.add(StudentInfo);
                            studentadapter = new StudentAttendanceAdapter(AttendanceStudentList.this, mStudentList);
                            rv_studentlist.setAdapter(studentadapter);


                        }






                        }else{
                            showAlertDialog(getString(R.string.No_Student_subject));

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            studentattendancelist = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
