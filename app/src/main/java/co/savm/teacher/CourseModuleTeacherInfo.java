package co.savm.teacher;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.savm.R;
import co.savm.adapters.FacultyListInSubjectAdapter;
import co.savm.adapters.SubjectInfoResourceAdapter;
import co.savm.models.InfoArray;
import co.savm.models.SubjectFacultyArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static co.savm.utils.BaseAppCompactActivity.encodeImageTobase64;


public class CourseModuleTeacherInfo extends BaseFragment {
    static String Course_id, Adminuser,CourseTittle,CourseImage,Tittle,body,field_job_title,field_deparment,field_academic_year,field_semester,field_teacher,field_credit,subjectphoto,CourseValue;
    RelativeLayout MainLayout;
    static String Course_idMain;
    private static ProgressSubjectInfo subjectinfoAuthTask = null;
    ImageView AddTeacher;
    private static ArrayList<String> selectedResourceSend;
    static ImageView subjectlogo;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3, floatingActionButton4,floatingActionButton5;
    static TextView subjectCode, Acadamic, Samester, subjectCredit, tittle, about, Teacher, Department, questionpaper, attachment, resource, syllabus, vedio,Addresource;
    static RecyclerView resourceList;
    static RecyclerView TeacherList;
    private RecyclerView.LayoutManager layoutManager,mlayoutManager;
    public static ArrayList<InfoArray> mInfoList;
    private static SubjectInfoResourceAdapter infoadapter;
    public static ArrayList<SubjectFacultyArray> mfacultyList;
    private static FacultyListInSubjectAdapter facultyadapter;
    private static SubjectTeacherDisplay facultylistdisplay = null;
    private ProgressDialog pDialog;
    ImageView my_image;
    static Activity activity;
    private static ProgressBar spinner;
    private DeleteCollageSubject deletecollagesubject = null;
    Bitmap thumbnail = null;
    String Fileimagename;
    MenuItem item;
    private String mImageFileLocation = "";
    Boolean CallingCamera,CallingGallary;
    public static final int RequestPermissionCode = 1;
    Uri imageUri;
    /*new cam/gallary*/
    private static final int ACTIVITY_START_CAMERA_APP = 0;
    private int SELECT_FILE = 1;
    private ProgressUpdateUserProfile updateProfileAuthTask = null;
    public CourseModuleTeacherInfo() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_course_module_teacher_info, container, false);

        activity = (Activity) view.getContext();


        Course_id = getArguments().getString("COURSE_ID");
        Course_idMain = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        CourseTittle = getArguments().getString("TITTLE");
        CourseImage  = getArguments().getString("COURSE_IMAGE");
        CourseValue = getArguments().getString("VALUECHANGED");


        if (Course_id == null) {
        }


        mInfoList = new ArrayList<>();
        mfacultyList = new ArrayList<>();
        selectedResourceSend =new ArrayList<>();

        subjectCode = view.findViewById(R.id.subjectCode);
        subjectlogo = view.findViewById(R.id.subjectlogo);
        MainLayout = view.findViewById(R.id.MainLayout);
        about = view.findViewById(R.id.about);
        Acadamic = view.findViewById(R.id.Acadamic);
        Samester = view.findViewById(R.id.Samester);
        subjectCredit = view.findViewById(R.id.subjectCredit);
        tittle = view.findViewById(R.id.tittle);

        AddTeacher =view.findViewById(R.id.AddTeacher);
        layoutManager = new LinearLayoutManager(activity);
        mlayoutManager = new LinearLayoutManager(activity);
        spinner= view.findViewById(R.id.progressBar);
        resourceList = view.findViewById(R.id.resourceList);
        resourceList.setHasFixedSize(true);
        resourceList.setLayoutManager(layoutManager);
        TeacherList = view.findViewById(R.id.TeacherList);
        TeacherList.setHasFixedSize(true);
        TeacherList.setLayoutManager(mlayoutManager);

        spinner = view.findViewById(R.id.progressBar);
        Addresource = view.findViewById(R.id.Addresource);
        GetAllSubjectDetails();
        GetAllSubjectFaculty();

        if (CourseImage!=null) {
            Glide.with(activity).load(CourseImage)
                    .placeholder(R.mipmap.clasrrominfo).dontAnimate()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .fitCenter().into(subjectlogo);



        }

        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);


        if (Adminuser.equals("TRUE")) {

            materialDesignFAM.setVisibility(View.VISIBLE);
            setHasOptionsMenu(true);
            Addresource.setVisibility(View.VISIBLE);
            AddLISner();
            Animation makeInAnimation = AnimationUtils.makeInAnimation(activity,false);
            makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) { }

                public void onAnimationRepeat(Animation animation) { }

                public void onAnimationStart(Animation animation) {
                    materialDesignFAM.setVisibility(View.VISIBLE);
                }
            });

            Animation makeOutAnimation = AnimationUtils.makeOutAnimation(activity,true);
            makeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationEnd(Animation animation) {
                    materialDesignFAM.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) { }

                @Override
                public void onAnimationStart(Animation animation) { }
            });


            if (materialDesignFAM.isShown()) {
                materialDesignFAM.startAnimation(makeOutAnimation);
            }

            if (!materialDesignFAM.isShown()) {
                materialDesignFAM.startAnimation(makeInAnimation);
            }

            resourceList.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {
                        materialDesignFAM.setVisibility(View.GONE);
                    } else if (dy < 0) {
                        materialDesignFAM.setVisibility(View.VISIBLE);
                    }
                }
            });


            TeacherList.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {
                        materialDesignFAM.setVisibility(View.GONE);
                    } else if (dy < 0) {
                        materialDesignFAM.setVisibility(View.VISIBLE);
                    }
                }
            });


            NestedScrollView nsv =  view.findViewById(R.id.nestedScroll);
            nsv.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (scrollY > oldScrollY) {

                        materialDesignFAM.setVisibility(View.GONE);
                    } else {
                        materialDesignFAM.setVisibility(View.VISIBLE);
                    }
                }
            });

        


        } else if (Adminuser.equals("FALSE")) {
            materialDesignFAM.setVisibility(View.INVISIBLE);
            setHasOptionsMenu(false);
            Addresource.setVisibility(View.INVISIBLE);
            AddTeacher.setVisibility(View.INVISIBLE);


        }


        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }

                return false;
            }
        });

        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);





        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

                }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                 bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });

        AddTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent newIntent = new Intent(activity, InviteTeacher.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                newIntent.putExtras(bundle);
                startActivity(newIntent);

            }
        });



        return view;
    }






    private void AddLISner() {

        subjectlogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showImageDialog();

            }
        });


    }


    private void showImageDialog() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    CallingCamera =true;
                    CallingGallary =false;
                    if(checkPermission()){
                        CallCamera();
                    }
                } else if (items[item].equals("Choose from Gallery")) {
                    CallingGallary =true;
                    CallingCamera =false;
                    if(checkPermission()){
                        galleryIntent();
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void CallCamera() {
        Intent callCameraApplicationIntent = new Intent();
        callCameraApplicationIntent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }
        imageUri = FileProvider.getUriForFile(activity, "co.savm.fileprovider",photoFile);
        callCameraApplicationIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(callCameraApplicationIntent, ACTIVITY_START_CAMERA_APP);
    }






    private void galleryIntent()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(Intent.createChooser(galleryIntent, "Select File"),SELECT_FILE);
    }




    private void GetAllSubjectFaculty() {
        facultylistdisplay = new SubjectTeacherDisplay();
        facultylistdisplay.execute();

    }


    private void GetAllSubjectDetails() {
        subjectinfoAuthTask = new ProgressSubjectInfo();
        subjectinfoAuthTask.execute();


    }


    public static void RefreshWorkedFaculty() {

        mfacultyList.clear();
        facultylistdisplay = new SubjectTeacherDisplay();
        facultylistdisplay.execute();
    }

    public static void RefreshWorkedsubjectresource() {

        mInfoList.clear();
        selectedResourceSend.clear();
        subjectinfoAuthTask = new ProgressSubjectInfo();
        subjectinfoAuthTask.execute();

    }



    private static class SubjectTeacherDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDTEACHERINSUBJECTLIST+"/"+Course_idMain+"/faculty");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);

                    }
                });


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            facultylistdisplay = null;
            try {
                if (responce != null) {

                    spinner.setVisibility(View.INVISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectFacultyArray TeacherInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectFacultyArray.class);
                                mfacultyList.add(TeacherInfo);
                                facultyadapter = new FacultyListInSubjectAdapter(activity, mfacultyList,Course_idMain,Adminuser);
                                TeacherList.setAdapter(facultyadapter);
                                TeacherList.setNestedScrollingEnabled(false);




                            }

                        }else{
                            try{

                                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                                builder.setMessage(R.string.nouserteacher)
                                        .setCancelable(false)
                                        .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                dialog.dismiss();

                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();

                            }catch (Exception e){

                            }
                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            facultylistdisplay = null;
            spinner.setVisibility(View.INVISIBLE);

        }
    }




    private static class ProgressSubjectInfo extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGESUBJECTINFO+"/"+Course_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            subjectinfoAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");
                        Tittle = data.getString("title");
                        body = data.getString("body");
                        field_job_title = data.getString("field_job_title");
                        field_deparment = data.getString("field_deparment");
                        field_academic_year = data.getString("field_academic_year");
                        field_semester = data.getString("field_semester");
                        field_teacher = data.getString("field_teacher");
                        field_credit = data.getString("field_credit");

                        JSONArray picture = data.getJSONArray("field_logo");
                        if(picture != null && picture.length() > 0 ){
                            String strings[] = new String[picture.length()];
                            for(int i=0;i<strings.length;i++) {
                                strings[i] = picture.getString(i);
                                Glide.with(activity).load(strings[i])
                                        .placeholder(R.mipmap.clasrrominfo).dontAnimate()
                                        .fitCenter().into(subjectlogo);

                                Log.d("TAG", "photo: " +  strings[i]);
                                //  Log.d("TAG", "photo: " +  strings[i]);
                            }
                        }else
                        {
                            subjectlogo.setImageResource(R.mipmap.clasrrominfo);


                        }



                        JSONArray syllabus = data.getJSONArray("field_sub_syllabus");
                        if (syllabus != null && syllabus.length() > 0) {



                            for (int j = 0; j < syllabus.length(); j++) {
                                String t = syllabus.getJSONObject(j).getString("value");
                                String p = syllabus.getJSONObject(j).getString("title");
                                //  Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = syllabus.getJSONObject(j).getString("title");
                                LinkInfo.value = syllabus.getJSONObject(j).getString("value");
                                LinkInfo.pos = syllabus.getJSONObject(j).getString("pos");
                                mInfoList.add(LinkInfo);


                            }
                        }

                        JSONArray QuestionPaperArray = data.getJSONArray("field_question_paper");
                        if (QuestionPaperArray != null && QuestionPaperArray.length() > 0) {



                            for (int j = 0; j < QuestionPaperArray.length(); j++) {
                                String t = QuestionPaperArray.getJSONObject(j).getString("value");
                                String p = QuestionPaperArray.getJSONObject(j).getString("title");
                                //  Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = QuestionPaperArray.getJSONObject(j).getString("title");
                                LinkInfo.value = QuestionPaperArray.getJSONObject(j).getString("value");
                                LinkInfo.pos = QuestionPaperArray.getJSONObject(j).getString("pos");
                                mInfoList.add(LinkInfo);


                            }
                        }





                        JSONArray AttachmentsArray = data.getJSONArray("field_attachments");
                        if (AttachmentsArray != null && AttachmentsArray.length() > 0) {



                            for (int j = 0; j < AttachmentsArray.length(); j++) {
                                String t = AttachmentsArray.getJSONObject(j).getString("value");
                                String p = AttachmentsArray.getJSONObject(j).getString("title");
                                //  Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = AttachmentsArray.getJSONObject(j).getString("title");
                                LinkInfo.value = AttachmentsArray.getJSONObject(j).getString("value");
                                LinkInfo.pos = AttachmentsArray.getJSONObject(j).getString("pos");
                                mInfoList.add(LinkInfo);


                            }
                        }




                        JSONArray resourceArrays = data.getJSONArray("field_course_resource");
                        if (resourceArrays != null && resourceArrays.length() > 0) {



                            for (int j = 0; j < resourceArrays.length(); j++) {
                                String t = resourceArrays.getJSONObject(j).getString("value");
                                String p = resourceArrays.getJSONObject(j).getString("title");
                                Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = resourceArrays.getJSONObject(j).getString("title");
                                LinkInfo.value = resourceArrays.getJSONObject(j).getString("value");
                                LinkInfo.pos = resourceArrays.getJSONObject(j).getString("pos");
                                LinkInfo.resource ="True";
                                mInfoList.add(LinkInfo);
                                selectedResourceSend.add(p+"$"+t);


                            }


                        }
                        infoadapter = new SubjectInfoResourceAdapter(activity, mInfoList,Course_id,Adminuser);
                        resourceList.setAdapter(infoadapter);
                        resourceList.setNestedScrollingEnabled(false);
                        calltheresourcemethod(selectedResourceSend);

                        subjectCode.setText(field_job_title);
                        Acadamic.setText(field_academic_year);
                        Samester.setText(field_semester);
                        subjectCredit.setText(field_credit);
                        tittle.setText(Tittle);
                        about.setText(body);


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        private void calltheresourcemethod(final ArrayList<String> selectedResourceSend) {


            Addresource.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent newIntent = new Intent(activity, AddResourceByTeacher.class);

                    newIntent.putExtra("COURSE_ID",Course_id);
                    newIntent.putExtra("array_list", selectedResourceSend);
                    Log.e("array_list", String.valueOf(selectedResourceSend));

                    activity.startActivity(newIntent);




                }
            });






        }

        @Override
        protected void onCancelled() {
            subjectinfoAuthTask = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }






    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        if(requestCode == ACTIVITY_START_CAMERA_APP && resultCode == RESULT_OK) {

            beginCrop(imageUri);

        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)


                onSelectFromGalleryResult(result);
        }
        if (requestCode == Crop.REQUEST_CROP) {

            handleCrop(resultCode, result);

        }

        }


    //Choose From Gallery
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm=null;
        if (data != null) {

            try {
                beginCrop(data.getData());
                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};

                Cursor cursor = activity.getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                Log.d("TAG", DatabaseUtils.dumpCursorToString(cursor));

                int columnIndex = cursor.getColumnIndex(projection[0]);
                Fileimagename = cursor.getString(columnIndex); // full path of image

                cursor.close();

            }catch (Exception e)
            {
                e.printStackTrace();
            }
            }
        }


    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(activity.getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(getContext(), CourseModuleTeacherInfo.this, Crop.REQUEST_CROP);


    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {

            try {
                thumbnail = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), Crop.getOutput(result));
                thumbnail = getResizedBitmap(thumbnail,110);

                subjectlogo.setImageBitmap(thumbnail);

                updateProfileAuthTask = new ProgressUpdateUserProfile();
                updateProfileAuthTask.execute();

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(getActivity(), Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMAGE_" + timeStamp + "_";
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,".jpg", storageDirectory);
        mImageFileLocation = image.getAbsolutePath();
        Fileimagename = image.getName();
        Log.d("TAG", "createImageFile: "+Fileimagename);

        return image;

    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.optionpopupmenu, menu);
        item = menu.findItem(R.id.deleteoption);
        super.onCreateOptionsMenu(menu, menuInflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Editoption:

                Intent i = new Intent(getActivity(), UpdateCourse.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("COURSE_TITTLE", Tittle);
                bundle.putString("COURSE_BODY", body);
                bundle.putString("COURSE_CODE", field_job_title);
                bundle.putString("COURSE_DEPARTMENT", field_deparment);
                bundle.putString("COURSE_YEAR", field_academic_year);
                bundle.putString("COURSE_SEM", field_semester);
                bundle.putString("COURSE_TEACHER", field_teacher);
                bundle.putString("COURSE_CREDIT", field_credit);
                i.putExtras(bundle);
                startActivity(i);


                return true;

            case R.id.deleteoption:

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), android.app.AlertDialog.THEME_HOLO_DARK)
                        .setTitle("Delete User")
                        .setMessage(R.string.Delete)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                deletecollagesubject = new DeleteCollageSubject();
                                deletecollagesubject.execute(Course_id);
                            }
                        })
                        .setNegativeButton("No", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();


                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }



    /* DELETE  SUBJECTS*/


    private class DeleteCollageSubject extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETESUBJECTS+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletecollagesubject = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        spinner.setVisibility(View.INVISIBLE);

                        if(CourseValue.matches("TeacherCource")){
                            TeacherCourseModule.CalledFromAddCourse();
                            activity.finish();
                        }else if (CourseValue.matches("TeacherMyCource")){
                            TeacherMyCource.CalledFromAddCourse();
                            activity.finish();

                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);


                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);


            }
        }

        @Override
        protected void onCancelled() {
            deletecollagesubject = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }


    private class ProgressUpdateUserProfile extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody  body = new FormBody.Builder()
                    .add("file", encodeImageTobase64(thumbnail))
                    .add("filename",Fileimagename)
                    .build();





            try {
                String responseData = ApiCall.POSTHEADER(client,URLS.URL_TEACHERADDLOGO+"/"+ Course_id,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);

                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            updateProfileAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject picture = responce.getJSONObject("data");

                        if (picture != null && picture.length() > 0) {
                            subjectphoto = picture.getString("icon_url");

                        }

                        } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);



                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            updateProfileAuthTask = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }




    private  boolean checkPermission() {
        int camerapermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(activity,Manifest.permission.READ_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), RequestPermissionCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case RequestPermissionCode: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d("TAG", "sms & location services permission granted");
                        if (CallingCamera ==true){
                            CallingGallary=false;
                            CallCamera();

                        }else if (CallingGallary ==true) {
                            CallingCamera=false;

                            galleryIntent();


                        }
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                                || ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkPermission();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    activity. finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new android.app.AlertDialog.Builder(activity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(activity);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);
                        startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:com.exampledemo.parsaniahardik.marshmallowpermission")));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        activity. finish();
                    }
                });
        dialog.show();
    }


}