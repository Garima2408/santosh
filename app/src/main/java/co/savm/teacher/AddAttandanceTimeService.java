package co.savm.teacher;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import co.savm.database.QuestinSQLiteHelper;
import co.savm.models.AddAttendanceOfflineArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class AddAttandanceTimeService extends Service {
    JSONArray studentselected;
    JSONObject sharefriendJson;
    // constant
    public static final long NOTIFY_INTERVAL = 60 * 1000; // 10 seconds
   //  public static final long NOTIFY_INTERVAL = 60*5 * 1000; // 10 seconds

    private static QuestinSQLiteHelper questinSQLiteHelper;
    // run on another Thread to avoid crash
    private Handler mHandler = new Handler();
    // timer handling
    private Timer mTimer = null;
    private AddAttendenceAsync attendenceAsync = null;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        // cancel if already existed
        if (mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        // schedule task
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask(), 0, NOTIFY_INTERVAL);
    }

    class TimeDisplayTimerTask extends TimerTask {

        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                  /*  // display toast
                    Toast.makeText(getApplicationContext(), "StoreTheData",
                            Toast.LENGTH_SHORT).show();
*/
                    CallAttandanceAllValue();


                }

            });
        }


        private void CallAttandanceAllValue() {



            studentselected = new JSONArray();
            List<AddAttendanceOfflineArray> val = questinSQLiteHelper.getAllAttendance();


            for (AddAttendanceOfflineArray cn : val) {
                String log = "Id: " + cn.getUid() + " ,values: "
                        + cn.getAttendance() + "..." + cn.getClasssId() + "..." + cn.getCourse_id() + "..." + cn.getDate() + "..." + cn.getType();


                Log.d("value: ", log);


                sharefriendJson = new JSONObject();
                try {
                    sharefriendJson.put("uid", cn.getUid());
                    sharefriendJson.put("present", cn.getAttendance());
                    sharefriendJson.put("subject", cn.getCourse_id());
                    sharefriendJson.put("class", cn.getClasssId());
                    sharefriendJson.put("date", cn.getDate());
                    sharefriendJson.put("type", "class");
                    studentselected.put(sharefriendJson);

                   // Log.d("TAG", "studentselected: " + studentselected);
                    Log.d("TAG", "sharefriendJson: " + sharefriendJson);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Log.d("TAG", "studentselected: " + studentselected);
            attendenceAsync = new AddAttendenceAsync();
            attendenceAsync.execute(studentselected.toString());


        }


    }




    class AddAttendenceAsync extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("records", args[0])
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDSTUDENTATTENDANCE, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            try {
                if (responce != null) {

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                       // Toast.makeText(AddAttandanceTimeService.this, "AttandanceAddedd", Toast.LENGTH_SHORT).show();
                        StopTimer();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                       // Toast.makeText(AddAttandanceTimeService.this, "AttandanceAddedd", Toast.LENGTH_SHORT).show();
                        StopTimer();



                    }
                }
            } catch (JSONException e) {

            }
        }

        @Override
        protected void onCancelled() {


        }

    }

    private void StopTimer() {


        if (mTimer != null) {

            mTimer.cancel();

            mTimer = null;

        }

        Toast.makeText(this, "Stop Service", Toast.LENGTH_SHORT).show();



    }


}

