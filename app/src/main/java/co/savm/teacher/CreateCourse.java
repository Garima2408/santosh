package co.savm.teacher;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.ResourceAddAdapter;
import co.savm.models.ResourceAddList;
import co.savm.models.SemesterArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CreateCourse extends BaseAppCompactActivity {
    EditText CourseTittle,Details,field_deparment,subjectCode,subjectCredit,Acadamicyear;
    String course_tittle,courseDetails,teacherdepartment,coursecode,coursecradit,coursesem,courseadamic,jsonResource,SemID;
    TextView Addresource;
    Spinner Semester;
    ListView resourcelist;
    private int RC_NEW_LIST = 1;
    Bundle b;
    ArrayList<ResourceAddList> dataList;
    ResourceAddAdapter adapter;
    private CourseCreateTeacherAuthTask coursecreateteacherAuthTask = null;
    private CourseSemesterAuthTask coursesemsterAuthTask = null;
    ArrayList<SemesterArray> listofSem;
    private ArrayList<String> semlist;
    Bundle extras;
    private int year, month, day, week;
    private int startDay, startMonth, startYear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_course);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        extras = intent.getExtras();
        String action = intent.getAction();

        CourseTittle  = findViewById(R.id.CourseTittle);
        /* Teacher  = findViewById(R.id.Teacher);*/
        Details  = findViewById(R.id.Details);
        field_deparment  = findViewById(R.id.field_deparment);
        subjectCode  = findViewById(R.id.subjectCode);
        subjectCredit  = findViewById(R.id.subjectCredit);
        Semester  = findViewById(R.id.Semester);
        Acadamicyear  = findViewById(R.id.Acadamicyear);
        Addresource  = findViewById(R.id.Addresource);
        resourcelist  = findViewById(R.id.resourcelist);
        dataList = new ArrayList<ResourceAddList>();

        semlist = new ArrayList<String>();
        listofSem = new ArrayList<SemesterArray>();


        if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
            if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")){

                Semester.setVisibility(View.GONE);
                SemID = "846";
                coursesem=SemID;

            }else {

                coursesemsterAuthTask = new CourseSemesterAuthTask();
                coursesemsterAuthTask.execute();




            }

        }


        Addresource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent newChatIntent = new Intent(CreateCourse.this, AddResourceByTeacher.class);
                startActivityForResult(newChatIntent, RC_NEW_LIST);

            }
        });

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_NEW_LIST) {
            if (resultCode == RESULT_OK) {

                dataList = (ArrayList<ResourceAddList>) data.getSerializableExtra("Arraylist");
                Log.d("TAG", "resultArr: " +   dataList);
                AddDataToDisplay();



            }
        }
    }
    private void AddDataToDisplay() {
        adapter = new ResourceAddAdapter(CreateCourse.this, R.layout.list_of_addresource,dataList);
        resourcelist.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        Gson  gson = new Gson();
        jsonResource = gson.toJson(dataList);
        System.out.println("jsonStudents = " + jsonResource);

    }


    private class CourseSemesterAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("vid","46")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_SEMSTERID,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CreateCourse.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            coursesemsterAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i=0; i<data.length();i++) {
                            SemesterArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SemesterArray.class);

                            semlist.add(GoalInfo.getLabel());
                            listofSem.add(GoalInfo);

                            Semester.setAdapter(new ArrayAdapter<String>(CreateCourse.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    semlist));


                            Semester.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                    if (position==0)
                                        position++;

                                    SemID = listofSem.get(position).getId();
                                    Log.d("TAG", "edt_selectsub: " + SemID);
                                    coursesem=SemID;

                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {}

                            });


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            coursesemsterAuthTask = null;
            hideLoading();


        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:
                AddCourseInCollageProcess();

                return true;
            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void AddCourseInCollageProcess() {


        CourseTittle.setError(null);
        Details.setError(null);
        field_deparment.setError(null);
        subjectCode.setError(null);
        subjectCredit.setError(null);
        Acadamicyear.setError(null);



        // Store values at the time of the login attempt.
        course_tittle = CourseTittle.getText().toString().trim();
        courseDetails = Details.getText().toString().trim();
        teacherdepartment = field_deparment.getText().toString().trim();
        coursecode = subjectCode.getText().toString().trim();
        coursecradit = subjectCredit.getText().toString().trim();
        coursesem = SemID;
        courseadamic = Acadamicyear.getText().toString().trim();


        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(course_tittle)) {
            focusView = CourseTittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }

        else if (co.savm.utils.TextUtils.isNullOrEmpty(courseDetails)) {
            // check for First Name
            focusView = Details;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
//        else if (co.questin.utils.TextUtils.isNullOrEmpty(teacherdepartment)) {
//            // check for First Name
//            focusView = field_deparment;
//            cancel = true;
//            showToast(getString(R.string.error_field_required));
//        }
//
//        if (android.text.TextUtils.isEmpty(coursecode)) {
//            focusView = subjectCode;
//            cancel = true;
//            showToast(getString(R.string.error_field_required));
//
//            // Check for a valid email address.
//        }
//        else if (co.questin.utils.TextUtils.isNullOrEmpty(coursecradit)) {
//            // check for First Name
//            focusView = subjectCredit;
//            cancel = true;
//            showToast(getString(R.string.error_field_required));
//        }
//        else if (co.questin.utils.TextUtils.isNullOrEmpty(coursesem)) {
//            // check for First Name
//            focusView = Semester;
//            cancel = true;
//            showToast(getString(R.string.error_field_required));
//        }
//        else if (co.questin.utils.TextUtils.isNullOrEmpty(courseadamic)) {
//            // check for First Name
//            focusView = Acadamicyear;
//            cancel = true;
//            showToast(getString(R.string.error_field_required));
//        }



        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            coursecreateteacherAuthTask = new CourseCreateTeacherAuthTask();
            coursecreateteacherAuthTask.execute();



        }
    }








    private class CourseCreateTeacherAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type","courses")
                    .add("og_group_ref", SessionManager.getInstance(getActivity()).getCollage().getTnid())
                    .add("title",course_tittle)
                    .add("body",courseDetails)
                    .add("field_teacher",SessionManager.getInstance(getActivity()).getUser().email+" [uid:"+SessionManager.getInstance(getActivity()).getUser().userprofile_id+"]")
                    .add("field_deparment",teacherdepartment)
                    .add("field_job_title",coursecode)
                    .add("field_credit",coursecradit)
                    .add("field_academic_year",courseadamic)
                    .add("field_semester",coursesem)
                    /* .add("field_course_resource",jsonResource)
                     .add("field_video_resource",jsonResource)*/
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_TEACHERCREATECOURSE,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CreateCourse.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            coursecreateteacherAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        // showAlertDialog(msg);
                        Check();
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {


                        finish();


                    }
                }
            } catch (JSONException e) {
                hideLoading();


            }
        }

        @Override
        protected void onCancelled() {
            coursecreateteacherAuthTask = null;
            hideLoading();



        }
    }

    private void Check() {

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFromMyCourseTeacher")) {

                TeacherMyCource.CalledFromAddCourse();


            } else if (extras.getString("open").equals("CameFromCourseModuleTeacher")) {

                TeacherCourseModule.CalledFromAddCourse();


            }

            else if (extras.getString("open").equals("CameFromFacultyPage")) {

                Intent backIntent = new Intent(CreateCourse.this, CreateStudentInClass.class);
                startActivity(backIntent);
                finish();

            }


        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }



}
