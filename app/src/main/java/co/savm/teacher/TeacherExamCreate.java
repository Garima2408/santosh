package co.savm.teacher;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import co.savm.R;
import co.savm.models.SubjectFacultyArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class TeacherExamCreate extends BaseAppCompactActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener {
    EditText SubjectTittle,Details,EndTime,roomno,bulding,MaxMarks,examduration;
    TextView StartDate,Classlocation,StartTime;
    String Subject_id,tittle,examDetails,examTeacher,examDate,startfrom,classDatestart,classtimestart,examroom,Buldingno,Location,examMaxmark,duration,format,time24format,TeacherEmail,TeacherUid;
    private ExamCreateTeacherAuthTask examcreateteacherAuthTask = null;
    private int year, month, day, week;
    private int startDay, startMonth, startYear;
    private int CalendarHour, CalendarMinute ,CalendarSecon;
    Double Lat,Long;
    private Calendar mcalender;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;
    TimePickerDialog timepickerdialog;
    //To store longitude and latitude from map
    private double longitude;
    Spinner Teacher;
    private double latitude;
    private ProgressSearchOfTeacherList progresssearchofteacherlist = null;
    public ArrayList<SubjectFacultyArray> mfacultyList;
    private ArrayList<String> facultynamelist;
    int FragmentPosition;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_exam_create);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getIntent().getExtras();
        Subject_id = b.getString("COURSE_ID");
        FragmentPosition = Integer.parseInt(b.getString("SAME"));

        SubjectTittle = findViewById(R.id.SubjectTittle);
        Teacher = findViewById(R.id.Teacher);
        Details = findViewById(R.id.Details);
        StartDate = findViewById(R.id.StartDate);
        StartTime = findViewById(R.id.StartTime);
        roomno = findViewById(R.id.roomno);
        bulding = findViewById(R.id.bulding);
        Classlocation = findViewById(R.id.location);
        MaxMarks = findViewById(R.id.MaxMarks);
        examduration = findViewById(R.id.examduration);

        mcalender = Calendar.getInstance();
        CalendarHour = mcalender.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = mcalender.get(Calendar.MINUTE);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = sdf.format(c.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(mcalender.getTime());


        StartDate.setText(formattedDate);
        startfrom =formattedDate;


        classDatestart = formattedDate2;

        String time = new SimpleDateFormat("hh:mm a").format(new java.util.Date().getTime());

       /* SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
        String time = simpleDateFormat.format(mcalender.getTime());
*/
        StartTime.setText(time);





        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(time);
            time24format =date.toString();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        time24format = displayFormat.format(date);

        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));

        classtimestart =time24format;

        mfacultyList=new ArrayList<>();
        facultynamelist = new ArrayList<String>();
        progresssearchofteacherlist = new ProgressSearchOfTeacherList();
        progresssearchofteacherlist.execute();

        StartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartdateDialog();
            }
        });

        StartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyBoard(view);


                timepickerdialog = new TimePickerDialog(TeacherExamCreate.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = " am";
                                }
                                else if (hourOfDay == 12) {

                                    format = " pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = " pm";

                                }
                                else {

                                    format = " am";
                                }

                                String Slow =(hourOfDay + ":" + minute + format);
                                StartTime.setText(hourOfDay + ":" + minute + format);

                                Log.d("TAG", "StartTime " + StartTime);

                                SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
                                SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a",Locale.US);
                                Date date = null;
                                try {
                                    date = parseFormat.parse(Slow);
                                    time24format = displayFormat.format(date);
                                    System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));

                                    classtimestart =time24format;

                                    Log.d("TAG", "classtimestart " + time24format);


                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }


                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();
            }
        });

    }




    private void StartdateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {

        startfrom = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        StartDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));




    }


    private class ProgressSearchOfTeacherList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDTEACHERINSUBJECTLIST+"/"+Subject_id+"/faculty");                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherExamCreate.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofteacherlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i=0; i<data.length();i++) {



                            SubjectFacultyArray TeacherInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SubjectFacultyArray.class);
                            facultynamelist.add(TeacherInfo.getName());
                            mfacultyList.add(TeacherInfo);

                            Teacher.setAdapter(new ArrayAdapter<String>(TeacherExamCreate.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    facultynamelist));


                            Teacher.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    TeacherUid = mfacultyList.get(position).getUid();
                                    TeacherEmail = mfacultyList.get(position).getMail();

                                    Log.d("TAG", "edt_selectsub: " + TeacherUid +TeacherEmail);


                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {}

                            });


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofteacherlist = null;
            hideLoading();


        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:

                AddExamsInClassProcess();
                return true;
            case android.R.id.home:
                finish();




                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void AddExamsInClassProcess() {


        SubjectTittle.setError(null);
        Details.setError(null);
        StartDate.setError(null);
        roomno.setError(null);
        bulding.setError(null);
        Classlocation.setError(null);
        MaxMarks.setError(null);
        examduration.setError(null);



        // Store values at the time of the login attempt.
        tittle = SubjectTittle.getText().toString().trim();
        examDetails = Details.getText().toString().trim();
        examDate = startfrom+" "+classtimestart;
        examroom = roomno.getText().toString().trim();
        Buldingno = bulding.getText().toString().trim();
        Location = Classlocation.getText().toString().trim();
        examMaxmark = MaxMarks.getText().toString().trim();
        duration = examduration.getText().toString().trim();
        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(tittle)) {
            focusView = SubjectTittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }

      /*  else if (co.questin.utils.TextUtils.isNullOrEmpty(examDetails)) {
            // check for First Name
            focusView = Details;
            cancel = true;
            showToast(getString(R.string.error_field_required));
        }*/
        else if (co.savm.utils.TextUtils.isNullOrEmpty(examDate)) {
            // check for First Name
            focusView = StartDate;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(examroom)) {
            // check for First Name
            focusView = roomno;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(Buldingno)) {
            // check for First Name
            focusView = bulding;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
      /*  else if (co.questin.utils.TextUtils.isNullOrEmpty(Location)) {
            // check for First Name
            focusView = Classlocation;
            cancel = true;
            showToast(getString(R.string.error_field_required));
        }*/
        else if (co.savm.utils.TextUtils.isNullOrEmpty(examMaxmark)) {
            // check for First Name
            focusView = MaxMarks;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(duration)) {
            // check for First Name
            focusView = examduration;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            examcreateteacherAuthTask = new ExamCreateTeacherAuthTask();
            examcreateteacherAuthTask.execute();



        }
    }








    private class ExamCreateTeacherAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();



            RequestBody body = new FormBody.Builder()
                    .add("type","exam")
                    .add("title",tittle)
                    .add("body",examDetails)
                    .add("field_exam_time[value]",examDate)
                    .add("field_og_subject",Subject_id)
                    .add("field_instructor_exam",TeacherEmail+" [uid:"+TeacherUid+"]")
                    .add("field_max_marks",examMaxmark)
                    .add("field_location_exam[lat]", String.valueOf(Lat))
                    .add("field_location_exam[lng]", String.valueOf(Long))
                    .add("field_order",duration)
                    .add("field_building",Buldingno)
                    .add("field_grades",examroom)
                    .build();
            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_TEACHERCREATEEXAM,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherExamCreate.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            examcreateteacherAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        if (FragmentPosition ==4){
                            TeacherCourseModuleDetails.getCurrentPosition(FragmentPosition);
                            TeacherExamList.CalledFromAddExam();
                            finish();

                        }

                        } else if (errorCode.equalsIgnoreCase("0")) {

                        showAlertDialog(msg);
                        finish();


                    }
                }
            } catch (JSONException e) {
                hideLoading();


            }
        }

        @Override
        protected void onCancelled() {
            examcreateteacherAuthTask = null;
            hideLoading();



        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
    public void onSearch() {
       /* Mainlocation = (EditText) findViewById(R.id.Editserch);
        String locan = location.getText().toString();*/
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }



        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());
            LatLng latLng = new LatLng(Lat, Long);
            mMap.addMarker(new MarkerOptions().position(latLng).title(SessionManager.getInstance(getActivity()).getCollage().getTitle()));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

        }







    }

    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);


            //moving the map to location
            moveMap();
            onSearch();
        }
    }

    //Function to move the map
    private void moveMap() {
        //String to display current latitude and longitude
        String msg = latitude + ", " + longitude;
        Log.d("TAG", "msg: " + msg);


        //Creating a LatLng Object to store Coordinates


        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());
            LatLng latLng = new LatLng(Lat, Long);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title(SessionManager.getInstance(getActivity()).getCollage().getTitle())); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

            //Displaying current coordinates in toast
            //  Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }else {

            LatLng latLng = new LatLng(latitude, longitude);

            Lat=latitude;
            Long =longitude;
            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title(SessionManager.getInstance(getActivity()).getCollage().getTitle())); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.getUiSettings().setAllGesturesEnabled(false);
        getCurrentLocation();
        moveMap();
        onSearch();
    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }

}




