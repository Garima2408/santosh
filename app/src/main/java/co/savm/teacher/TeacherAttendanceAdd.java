package co.savm.teacher;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import co.savm.R;
import co.savm.adapters.AddAttendanceAdapter;
import co.savm.database.QuestinSQLiteHelper;
import co.savm.models.AddAttendanceArray;
import co.savm.models.AddAttendanceOfflineArray;
import co.savm.models.AttendanceClassArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class TeacherAttendanceAdd extends BaseAppCompactActivity {
    public ArrayList<AddAttendanceArray> mAttendanceList;
    public ArrayList<AddAttendanceOfflineArray> mAttendanceofflineList;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int startDay, startMonth, startYear;
    ArrayList<AttendanceClassArray> listofcourse;
    ArrayAdapter<String> adapter;
    private ArrayList<String> courselist;
    Spinner classess;
    private AddAttendanceAdapter addattendanceadapter;
    ListView list_of_Students;
    private Calendar mcalender;
    private AttendanceStudentList studentlist = null;
    String Course_id,Module,CourseTittle,Presents , startDate,startfrom,ClasssId,Classname,classid;
    TextView selectdate;
    JSONArray studentselected;
    JSONObject sharefriendJson;
    TextView ErrorText;
    private AddAttendenceAsync attendenceAsync = null;
    private ProgressSearchOfClassList progresssearchofclasslist = null;
    private ProgressSearchOfClassListCollage progresssearchofclasslistcollage = null;
    int FragmentPosition;

    private static QuestinSQLiteHelper questinSQLiteHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_attendance_add);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getIntent().getExtras();
        Course_id = b.getString("COURSE_ID");
        CourseTittle = b.getString("TITTLE");
        FragmentPosition = Integer.parseInt(b.getString("SAME"));
       /* ClasssId =b.getString("CLASSID");
        Classname =b.getString("CLASSNAME");*/
        //  GetTheListOfCourse();
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        mAttendanceList=new ArrayList<>();
        courselist = new ArrayList<String>();
        listofcourse = new ArrayList<AttendanceClassArray>();
        mAttendanceofflineList = new ArrayList<>();
        //calender intialize
        mcalender = Calendar.getInstance();

        mYear = mcalender.get(Calendar.YEAR);
        mMonth = mcalender.get(Calendar.MONTH);
        mDay = mcalender.get(Calendar.DAY_OF_MONTH);




        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String  formattedDate = sdf.format(mcalender.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        String  formattedDate2 = sdf2.format(mcalender.getTime());



        Log.d("TAG", "Strings: " + ClasssId);

        list_of_Students =findViewById(R.id.list_of_Students);
        ErrorText =findViewById(R.id.ErrorText);
        selectdate =findViewById(R.id.selectdate);
        classess =findViewById(R.id.classess);
        selectdate.setText(formattedDate2);
        startfrom =formattedDate;


        if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
            if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")) {
                progresssearchofclasslist = new ProgressSearchOfClassList();
                progresssearchofclasslist.execute();

            }else {


                progresssearchofclasslistcollage = new ProgressSearchOfClassListCollage();
                progresssearchofclasslistcollage.execute();

            }
        }

        selectdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartdateDialog();
            }
        });



    }

    private void StartdateDialog() {


        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {



                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), android.app.AlertDialog.THEME_HOLO_LIGHT, listener, mYear, mMonth, mDay);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis() +1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {

        startfrom = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        selectdate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));

        if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
            if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")) {
                progresssearchofclasslist = new ProgressSearchOfClassList();
                progresssearchofclasslist.execute();

            }else {


                progresssearchofclasslistcollage = new ProgressSearchOfClassListCollage();
                progresssearchofclasslistcollage.execute();

            }
        }

    }






    private class ProgressSearchOfClassList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ATTENDANCECLASSES+"?"+"sid="+Course_id+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherAttendanceAdd.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofclasslist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        courselist.clear();
                        listofcourse.clear();


                        JSONArray data = responce.getJSONArray("data");
                        if (data != null && data.length() > 0) {
                            for (int i=0; i<data.length();i++) {
                                AttendanceClassArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), AttendanceClassArray.class);
                                courselist.add(GoalInfo.getTitle());
                                listofcourse.add(GoalInfo);

                                classess.setAdapter(new ArrayAdapter<String>(TeacherAttendanceAdd.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        courselist));


                                classess.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        ClasssId = listofcourse.get(position).getTnid();

                                        Log.d("TAG", "edt_selectsub: " + ClasssId);
                                        mAttendanceList.clear();
                                        studentlist = new AttendanceStudentList();
                                        studentlist.execute(ClasssId);


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }

                                });
                            }

                        }else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherAttendanceAdd.this);
                            builder.setMessage("No Class Added. Add Class")
                                    .setCancelable(false)
                                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofclasslist = null;
            hideLoading();


        }
    }




    private class ProgressSearchOfClassListCollage extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ATTENDANCECLASSES+"?"+"sid="+Course_id+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherAttendanceAdd.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofclasslistcollage = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        courselist.clear();
                        listofcourse.clear();


                        JSONArray data = responce.getJSONArray("data");
                        if (data != null && data.length() > 0) {
                            for (int i=0; i<data.length()-1;i++) {
                                AttendanceClassArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), AttendanceClassArray.class);
                                courselist.add(GoalInfo.getTitle());
                                listofcourse.add(GoalInfo);

                                classess.setAdapter(new ArrayAdapter<String>(TeacherAttendanceAdd.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        courselist));


                                classess.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        ClasssId = listofcourse.get(position).getTnid();

                                        Log.d("TAG", "edt_selectsub: " + ClasssId);
                                        mAttendanceList.clear();
                                        studentlist = new AttendanceStudentList();
                                        studentlist.execute(ClasssId);


                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }

                                });
                            }


                        }else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherAttendanceAdd.this);
                            // builder.setTitle("Info");
                            builder.setMessage("No Class Added. Add Class")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofclasslistcollage = null;
            hideLoading();


        }
    }





    @Override
    public void onBackPressed() {
        finish();
    }


    private class AttendanceStudentList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("date",startfrom)
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_STUDENTADDATTANDANCE+"/"+args[0]+"&offset="+"0"+"&limit=150",body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherAttendanceAdd.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        final ArrayList<String> allstudent = new ArrayList<String>();
                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                allstudent.add(jsonArrayData.getJSONObject(i).getString("uid"));

                                AddAttendanceArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AddAttendanceArray.class);
                                mAttendanceList.add(CourseInfo);
                                addattendanceadapter = new AddAttendanceAdapter(TeacherAttendanceAdd.this, mAttendanceList);
                                list_of_Students.setAdapter(addattendanceadapter);

                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherAttendanceAdd.this);
                            // builder.setTitle("Info");
                            builder.setMessage("No Student found")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //  finish();
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            studentlist = null;
            hideLoading();


        }
    }








    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.submit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.Submit:

              if(listofcourse !=null && listofcourse.size() >0) {

                  if(Utils.isInternetConnected(this)){
                      Showlistdata(mAttendanceList);


                  }else {
                      questinSQLiteHelper.deleteAllAttendance();
                      AddAttandanceInDataBase(mAttendanceList);

                      }


              }else {


                  AlertDialog.Builder builder = new AlertDialog.Builder(TeacherAttendanceAdd.this);
                  builder.setMessage("No Class Added. Add Class")
                          .setCancelable(false)
                          .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                              public void onClick(DialogInterface dialog, int id) {
                                  finish();
                                  dialog.dismiss();

                              }
                          });
                  AlertDialog alert = builder.create();
                  alert.show();

              }


                return true;



            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void AddAttandanceInDataBase(ArrayList<AddAttendanceArray> mAttendanceList) {

        for (int i = 0; i < this.mAttendanceList.size(); i++) {


            questinSQLiteHelper.AddAttandanceOffline(new AddAttendanceOfflineArray(i,mAttendanceList.get(i).getUid(),mAttendanceList.get(i).getAttendance(),Course_id,ClasssId,startfrom,"class"));

            Log.d("TAG", "insertAttendance: "+(mAttendanceList.get(i).getUid()+".."+mAttendanceList.get(i).getAttendance()+".."+Course_id+".."+ClasssId+".."+startfrom));


        }




        startService(new Intent(this, AddAttandanceTimeService.class));





    }



    private void Showlistdata(ArrayList<AddAttendanceArray> mAttendanceList) {
        studentselected = new JSONArray();
        for (int i = 0; i < this.mAttendanceList.size(); i++) {
            System.out.println(this.mAttendanceList.get(i).toString()); //prints element i
            System.out.println(this.mAttendanceList.get(i).getAttendance() + "," + this.mAttendanceList.get(i).getUid());


            sharefriendJson = new JSONObject();
            try {
                sharefriendJson.put("uid", mAttendanceList.get(i).getUid());
                sharefriendJson.put("present", mAttendanceList.get(i).getAttendance());
                sharefriendJson.put("subject", Course_id);
                sharefriendJson.put("class", ClasssId);
                sharefriendJson.put("date", startfrom);
                sharefriendJson.put("type", "class");//subject
                studentselected.put(sharefriendJson);

                Log.d("TAG", "studentselected: " + studentselected);
                Log.d("TAG", "sharefriendJson: " + sharefriendJson);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        attendenceAsync = new AddAttendenceAsync();
        attendenceAsync.execute();








    }



    private class AddAttendenceAsync extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("records",studentselected.toString())
                    .build();





            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDSTUDENTATTENDANCE,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherAttendanceAdd.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            attendenceAsync = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(TeacherAttendanceAdd.this);
                        // builder.setTitle("Info");
                        builder.setMessage("Attendance Record Successfully")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        RefreshTheList();
                                        dialog.dismiss();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            attendenceAsync = null;
            hideLoading();


        }
    }

    private void RefreshTheList() {
        mAttendanceList.clear();
        studentlist = new AttendanceStudentList();
        studentlist.execute(ClasssId);




    }




}