package co.savm.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.SubjectExamAdapter;
import co.savm.models.SubjectExamsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import okhttp3.OkHttpClient;

public class TeacherExamList extends BaseFragment {
    public static ArrayList<SubjectExamsArray> mExamList;
    private static SubjectExamAdapter examAdapters;
    static RecyclerView rv_subject_exams;
    private RecyclerView.LayoutManager layoutManager;
    private static CourseModuleExamDisplay courseexamdisplay = null;
    static ImageView ReloadProgress;
    String Subject_id,Adminuser;
    static String MainSubject_id,CourseTittle;
    static Activity activity;
    static TextView ErrorText;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4,floatingActionButton5;
    RelativeLayout MainLayout;
    static SwipeRefreshLayout swipeContainer;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_teacher_exam_list, container, false);
        activity = (Activity) view.getContext();
        Subject_id = getArguments().getString("COURSE_ID");
        MainSubject_id= getArguments().getString("COURSE_ID");
        CourseTittle = getArguments().getString("TITTLE");
        Adminuser = getArguments().getString("ADMIN");
        if (Subject_id == null) {
        }




        mExamList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(activity);
        rv_subject_exams = view.findViewById(R.id.rv_subject_exams);
        rv_subject_exams.setHasFixedSize(true);
        rv_subject_exams.setLayoutManager(layoutManager);
        ErrorText =view.findViewById(R.id.ErrorText);
        MainLayout = view.findViewById(R.id.MainLayout);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);
        swipeContainer =view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));


        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalledFromAddExam();
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        CalledFromAddExam();

                    }
                }, 1000);
            }
        });

        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        if(Adminuser.equals("TRUE")){
            setHasOptionsMenu(true);
            materialDesignFAM.setVisibility(View.VISIBLE);
            rv_subject_exams.setVisibility(View.VISIBLE);
            courseexamdisplay = new CourseModuleExamDisplay();
            courseexamdisplay.execute(MainSubject_id);


        }else if (Adminuser.equals("FALSE")) {
            setHasOptionsMenu(false);
            materialDesignFAM.setVisibility(View.INVISIBLE);
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");

        }

        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Subject_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Subject_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
        return view;
    }


    public static void CalledFromAddExam() {
        rv_subject_exams.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(false);
        mExamList.clear();
        /* CALLING LISTS OF COMMENTS*/
        courseexamdisplay = new CourseModuleExamDisplay();
        courseexamdisplay.execute(MainSubject_id);


    }



    private static class CourseModuleExamDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTEXAMS+"?"+"sid="+args[0]+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            courseexamdisplay = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            SubjectExamsArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectExamsArray.class);
                            mExamList.add(CourseInfo);

                            examAdapters = new SubjectExamAdapter(activity, mExamList,MainSubject_id);
                            rv_subject_exams.setAdapter(examAdapters);
                            ErrorText.setVisibility(View.GONE);

                        }
                    }else {
                            ErrorText.setVisibility(View.VISIBLE);
                            rv_subject_exams.setVisibility(View.GONE);
                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            courseexamdisplay = null;
            swipeContainer.setRefreshing(false);


        }
    }


}


