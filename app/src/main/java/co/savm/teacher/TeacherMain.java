package co.savm.teacher;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.iid.FirebaseInstanceId;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.savm.R;
import co.savm.activities.AllAppNotification;
import co.savm.activities.ChangeCollage;
import co.savm.activities.ForceUpdateChecker;
import co.savm.activities.ProfileEdit;
import co.savm.activities.QuestinSite;
import co.savm.activities.SignIn;
import co.savm.activities.StartupGuide;
import co.savm.activities.SwitchYourType;
import co.savm.adapters.NavigationItemsAdapter;
import co.savm.calendersection.CalenderMain;
import co.savm.campusfeedsection.AllCampusFeeds;
import co.savm.chat.ChatTabbedActivity;
import co.savm.chat.FriendsListService;
import co.savm.models.NavItems;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.settings.AboutUs;
import co.savm.settings.EmailUs;
import co.savm.settings.FAQ;
import co.savm.settings.PrivacyPolicy;
import co.savm.settings.Settings;
import co.savm.settings.TermsAndConditions;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.TextUtils;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.savm.fcm.MessagingService.All_App_Notification;
import static co.savm.fcm.MessagingService.college_classroom;
import static co.savm.fcm.MessagingService.college_feeds;
import static co.savm.fcm.MessagingService.singlechat;

public class TeacherMain extends BaseAppCompactActivity implements  NavigationView.OnNavigationItemSelectedListener,ForceUpdateChecker.OnUpdateNeededListener {
    Toolbar toolbar;
    TextView mToolbarTitle,toolbar_title;
    ExpandableListView expandableListView;
    DrawerLayout mDrawerlayout;
    TextView mEmail;
    FrameLayout parent_content;
    private NavigationItemsAdapter mNavigationItemsAdapter;
    private List<NavItems> listNavigationItem;
    private HashMap<String, List<NavItems>> listDataChild;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private Fragment mFragment = null;
    private Class mFragmentClass = null;
    private ProgressLogout logouttAuthTask = null;
    BottomNavigationView mBottomNavigationView;
    TextView tv_username, tv_useremail,tv_userbatch,badge_notificationCalender,badge_notificationcampus,badge_notificationMsg;
    ImageButton imgbtn_edit_profile,imgbtn_account;
    ImageView mainheader,profileImage;
    Dialog imageDialog;
    private SharedPreferences preferences;
    private int Runfirst;
    private DeleteFireBaseToken deleteFireBaseToken = null;
    private UpdateFirbaseID updateFirbaseID = null;
    private TextView badge_notification_icon,bell_icon;
    private SessionManager prefer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_main);
        Utils.setBadge(this, 0);

        preferences = Utils.getSharedPreference(TeacherMain.this);
        prefer = new SessionManager(this);
        Runfirst = preferences.getInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE);
        FirstOneThis();

        //  ForceUpdateChecker.with(this).onUpdateNeeded( this).check();

        badge_notification_icon = findViewById(R.id.badge_notification_icon);
        bell_icon = findViewById(R.id.bell_icon);



        pushFragment(new TeacherDashBoard());
        toolbar_title = findViewById(R.id.toolbar_title);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(SessionManager.getInstance(getActivity()).getCollage().getTitle().toString() !=null){

            toolbar_title.setText(SessionManager.getInstance(getApplicationContext()).getCollage().getTitle().toString());
            toolbar_title.setSelected(true);
            getSupportActionBar().setTitle(null);
            Log.d("TAG", "secciontokenmainacctivity: " + SessionManager.getInstance(getApplicationContext()).getCollage().getTitle().toString());

        }


        mDrawerlayout = findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawerlayout.setDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        expandableListView = findViewById(R.id.nav_expand_listview);


        //  ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
        prepareNavigationList ();
        Setuponbottombar();
        getFCMToken();

        //  CallTheCalenderONE();
        // setupNavigationView();
        Utils.init(TeacherMain.this);

      /*  if (Utils.getFCMId()==null){

        }*/


        startService(new Intent(this, FriendsListService.class));

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.navigation_header, null, false);
        expandableListView.addHeaderView(listHeaderView);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        profileImage = findViewById(R.id.imageProfile);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        mainheader = findViewById(R.id.mainheader);
        imgbtn_account=findViewById(R.id.imgbtn_account);
        imgbtn_account.setVisibility(View.GONE);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        parent_content =findViewById(R.id.parent_content);

        displayUserData();



        if(All_App_Notification >0){
            badge_notification_icon.setVisibility(View.VISIBLE);
            badge_notification_icon.setText(String.valueOf(All_App_Notification));

        }else if (All_App_Notification ==0){
            badge_notification_icon.setVisibility(View.GONE);
        }


        bell_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                All_App_Notification=0;
                badge_notification_icon.setVisibility(View.GONE);
                Intent backIntent = new Intent(TeacherMain.this, AllAppNotification.class);
                startActivity(backIntent);


            }
        });

        badge_notification_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                badge_notification_icon.setVisibility(View.GONE);
                Intent backIntent = new Intent(TeacherMain.this, AllAppNotification.class);
                startActivity(backIntent);
            }
        });





        imgbtn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent n = new Intent(TeacherMain.this,ProfileEdit.class);
                startActivity(n);
            }
        });


        imgbtn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent n = new Intent(TeacherMain.this,SwitchYourType.class);
                startActivity(n);
            }
        });



        mNavigationItemsAdapter = new NavigationItemsAdapter (this,
                listNavigationItem,listDataChild);

        expandableListView.setAdapter (mNavigationItemsAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                int len = mNavigationItemsAdapter.getGroupCount();
                for (int i = 0; i < len; i++) {
                    if (i != groupPosition) {
                        expandableListView.collapseGroup(i);
                    }
                }


            }
        });
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener () {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long rowId) {
                selectFragments(groupPosition,10);
                return false;
            }
        });

        expandableListView.setOnChildClickListener (new ExpandableListView.OnChildClickListener () {
            @Override public boolean onChildClick (ExpandableListView parent, View v, int groupPosition,
                                                   int childPosition, long id) {
                selectFragments(groupPosition,childPosition);
                v.setSelected (true);
                return false;
            }
        });




    }

    private void getFCMToken() {
        updateFirbaseID=new UpdateFirbaseID();
        updateFirbaseID.execute();

    }

    private void FirstOneThis() {
        switch (Runfirst){
            case Constants.ROLE_RUNNING_TRUE :
                imageDialog = new Dialog(TeacherMain.this);
                imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                imageDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                imageDialog.setContentView(R.layout.image_overlay_screen);
                // dialogLogin.setCancelable(false);
                imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT);
                imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

                imageDialog.show();
                ImageView showimage = imageDialog.findViewById(R.id.DisplayOnbondingImage);
                showimage.setImageResource(R.mipmap.onboarding_dashboard);

                showimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageDialog.dismiss();

                        Utils.getSharedPreference(TeacherMain.this).edit()
                                .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE).apply();
                        Utils.removeStringPreferences(TeacherMain.this,"0");

                    }
                });



                break;
            case Constants.ROLE_RUNNING_FALSE :
                break;


        }




    }


    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);
            }
            if (SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole()!=null) {

                if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole()) && !TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation())) {
                    tv_useremail.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole() + " " + SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation());
                    Log.d("TAG", "secciontokenmainacctivity: " + SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole().toString()+SessionManager.getInstance(getActivity()).getUserClgRole().getCollageDesignation().toString());

                }
            }

            if (SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment()!=null) {
                if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment())) {
                    tv_userbatch.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollagedepartment());
                }
            }
            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(profileImage);

            }
            if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(mainheader);




            }
        }
    }

    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private class DeleteFireBaseToken extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }
        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {


                String responseData = ApiCall.DELETE_HEADER(client, URLS.REG_TOKEN+"/" + Utils.getFCMId());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            deleteFireBaseToken = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        logouttAuthTask = new ProgressLogout();
                        logouttAuthTask.execute();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            deleteFireBaseToken = null;
            hideLoading();


        }
    }
    private class UpdateFirbaseID extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type", "android")
                    .add("token", FirebaseInstanceId.getInstance().getToken())
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.REG_TOKEN,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataprint: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Utils.showAlertDialog(TeacherMain.this,"Error Info",getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            updateFirbaseID = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.init(TeacherMain.this);
                        Utils.saveFCMId(TeacherMain.this,FirebaseInstanceId.getInstance().getToken());
                        Log.e("token",Utils.getFCMId());

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(TeacherMain.this,"Error Info",msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            updateFirbaseID = null;
            hideLoading();

        }
    }

    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);
        badge_notificationCalender =findViewById(R.id.badge_notificationCalender);
        badge_notificationcampus =findViewById(R.id.badge_notificationcampus);
        badge_notificationMsg =findViewById(R.id.badge_notificationMsg);


        if(singlechat >0){
            badge_notificationMsg.setVisibility(View.VISIBLE);
            badge_notificationMsg.setText(String.valueOf(singlechat));

        }else if (singlechat ==0){
            badge_notificationMsg.setVisibility(View.GONE);
        }

        if(college_classroom >0){
            badge_notificationCalender.setVisibility(View.VISIBLE);
            badge_notificationCalender.setText(String.valueOf(college_classroom));

        }else if (college_classroom ==0){
            badge_notificationCalender.setVisibility(View.GONE);
        }


        if(college_feeds >0){
            badge_notificationcampus.setVisibility(View.VISIBLE);
            badge_notificationcampus.setText(String.valueOf(college_feeds));

        }else if (college_feeds ==0){
            badge_notificationcampus.setVisibility(View.GONE);
        }





        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                college_classroom=0;
                badge_notificationCalender.setVisibility(View.GONE);
                Intent p = new Intent(TeacherMain.this,CalenderMain.class);
                startActivity(p);
                /* finish();*/


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                college_feeds=0;
                badge_notificationcampus.setVisibility(View.GONE);
                Intent y = new Intent(TeacherMain.this,AllCampusFeeds.class);
                startActivity(y);


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                singlechat=0;
                badge_notificationMsg.setVisibility(View.GONE);
                Intent z = new Intent(TeacherMain.this,ChatTabbedActivity.class);
                startActivity(z);



            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent j = new Intent(TeacherMain.this,TeacherProfile.class);
                startActivity(j);


            }
        });


    }


    protected void pushFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.parent_content, fragment);
            ft.commit();
        }
    }






    private void prepareNavigationList () {

        listNavigationItem = new ArrayList<NavItems>();
        listDataChild = new HashMap<String, List<NavItems>> ();

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.community),
                getResources ().getString (R.string.questin)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.colleges),
                getResources ().getString (R.string.colleges)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.learning),
                getResources ().getString (R.string.learning)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.need_help),
                getResources ().getString (R.string.need_help)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.read_more)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.setting),
                getResources ().getString (R.string.setting)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.log_out),
                getResources ().getString (R.string.log_out)));

        List<NavItems> needHelp = new ArrayList<> ();
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.email_us)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq1)));

        List<NavItems> readMore = new ArrayList<> ();
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.about_us)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.term_condition)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.privacy_policy)));

        List<NavItems> empty = new ArrayList<NavItems> ();

        listDataChild.put (listNavigationItem.get (0).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (1).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (2).getNavItemName (), empty);

        listDataChild.put (listNavigationItem.get (3).getNavItemName (), needHelp);
        listDataChild.put (listNavigationItem.get (4).getNavItemName (), readMore);
        listDataChild.put (listNavigationItem.get (5).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (6).getNavItemName (), empty);
    }

    private void selectFragments(int groupPosition,int childPosition){


        switch (groupPosition){
            case 0:
                Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

              /*  Intent i = new Intent(TeacherMain.this, QuestinSite.class);
                startActivity(i);*/
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 1:
                //  Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();
                Intent j = new Intent(TeacherMain.this, ChangeCollage.class);
                startActivity(j);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 2:
                //  Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                Intent k = new Intent(TeacherMain.this, QuestinSite.class);
                startActivity(k);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 3:

                switch (childPosition){
                    case 0:

                        Intent backIntent = new Intent(TeacherMain.this, EmailUs.class)
                                .putExtra("TITLE"," Email Us");
                        startActivity(backIntent);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        startActivity (new Intent (this, FAQ.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, StartupGuide.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 4:
                switch (childPosition){
                    case 0:

                        startActivity (new Intent (this, AboutUs.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        Intent o = new Intent(TeacherMain.this, TermsAndConditions.class);
                        startActivity(o);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, PrivacyPolicy.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 5:

                startActivity (new Intent (this, Settings.class));
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

            case 6:
                showNoticeDialog ();
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

        }


        try {
            mFragment = (Fragment) mFragmentClass.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager ();
            fragmentManager.beginTransaction ()
                    .replace (R.id.parent_content,mFragment)
                    .commit ();
        } catch (Exception e) {
            e.printStackTrace ();
        }

    }

    private void showNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(TeacherMain.this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Logout ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application

                deleteFireBaseToken=new DeleteFireBaseToken();
                deleteFireBaseToken.execute();
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }


    public int getPixelFromDips (float pixels) {
        // Get the screen's density scale
        final float scale = getResources ().getDisplayMetrics ().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }









    @Override
    protected void onResume() {
        super.onResume();
        displayUserData();

    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to close this application ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                Utils.getSharedPreference(TeacherMain.this).edit()
                        .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE).apply();
                Utils.removeStringPreferences(TeacherMain.this,"0");
                finishAffinity();
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }


    private class ProgressLogout extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LOGOUT,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataprint: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Utils.showAlertDialog(TeacherMain.this,"Error Info",getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            logouttAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        Utils.clearData();

                        try{
                            //user chat delete
                            Utils.deleteUserChatAndTable(TeacherMain.this);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        //use for clear snappy db
                        DB snappyDB = null;
                        try {
                            snappyDB = DBFactory.open(TeacherMain.this);
                            if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {

                                snappyDB.destroy();

                            }
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }
                        prefer.removeAllAppnotification(TeacherMain.this);
                        prefer.removenotification(TeacherMain.this);
                        prefer.removeCampusnotification(TeacherMain.this);
                        SessionManager.getInstance(getApplicationContext()).deleteUser();
                        Utils.getSharedPreference(TeacherMain.this).edit()
                                .putInt(Constants.USER_ROLE, Constants.ROLE_NULL).apply();
                        Utils.removeStringPreferences(TeacherMain.this,"0");
                        Intent upanel = new Intent(TeacherMain.this,SignIn.class);
                        upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(upanel);
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        // Utils.showAlertDialog(TeacherMain.this,"Error Info",msg);
                        showAlertDialog("Error Info",msg);

                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            logouttAuthTask = null;
            hideLoading();

        }
    }


}




