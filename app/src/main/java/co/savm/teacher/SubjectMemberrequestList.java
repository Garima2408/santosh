package co.savm.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.SubjectRequestMemberAdapter;
import co.savm.models.SubjectRequestArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import okhttp3.OkHttpClient;


public class SubjectMemberrequestList extends Fragment {
    static Activity activity;
    static String MainCourse_id;
    String Course_id, Adminuser, CourseTittle;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3, floatingActionButton4, floatingActionButton5;
    static TextView ErrorText;
    static ImageView ReloadProgress;
    private static SubjectRequestedStudentDisplay subjectrequestlist = null;
    public static ArrayList<SubjectRequestArray> mRequestList;
    private static SubjectRequestMemberAdapter subjectrequestadapter;
    static RecyclerView StudentPendingList;
    private RecyclerView.LayoutManager layoutManager;
    RelativeLayout MainLayout;
    static SwipeRefreshLayout swipeContainer;
    public SubjectMemberrequestList() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subject_memberrequest_list, container, false);
        activity = (Activity) view.getContext();
        Course_id = getArguments().getString("COURSE_ID");
        MainCourse_id = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        CourseTittle = getArguments().getString("TITTLE");
        if (Course_id == null) {
        }



        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);
        ErrorText = view.findViewById(R.id.ErrorText);
        layoutManager = new LinearLayoutManager(getContext());
        StudentPendingList = view.findViewById(R.id.StudentPendingList);
        StudentPendingList.setHasFixedSize(true);
        StudentPendingList.setLayoutManager(layoutManager);
        mRequestList = new ArrayList<SubjectRequestArray>();
        MainLayout = view.findViewById(R.id.MainLayout);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        swipeContainer =view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        if (Adminuser.equals("TRUE")) {

            materialDesignFAM.setVisibility(View.VISIBLE);
            subjectrequestlist = new SubjectRequestedStudentDisplay();
            subjectrequestlist.execute(MainCourse_id);
            StudentPendingList.setVisibility(View.VISIBLE);

        } else if (Adminuser.equals("FALSE")) {
            materialDesignFAM.setVisibility(View.INVISIBLE);
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");

        }

        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalledFromAcceptReject();
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        CalledFromAcceptReject();

                    }
                }, 1000);
            }
        });

        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
        return view;
    }


    public static void CalledFromAcceptReject() {

        mRequestList.clear();
        subjectrequestlist = new SubjectRequestedStudentDisplay();
        subjectrequestlist.execute(MainCourse_id);

    }




    private static class SubjectRequestedStudentDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_STUDENTSUBJECTREQUESTED+"/"+args[0]+"/"+"users"+"?"+"fields"+"="+"id,etid,gid,state&parameters=state=2");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            subjectrequestlist = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);
                    mRequestList.clear();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectRequestArray Request = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectRequestArray.class);
                                mRequestList.add(Request);
                                subjectrequestadapter = new SubjectRequestMemberAdapter(activity, mRequestList);
                                StudentPendingList.setAdapter(subjectrequestadapter);



                            }
                        }else{


                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No member is pending");
                            swipeContainer.setRefreshing(false);
                            StudentPendingList.setVisibility(View.GONE);
                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {

                        ReloadProgress.setVisibility(View.VISIBLE);
                        swipeContainer.setRefreshing(false);


                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            subjectrequestlist = null;
            swipeContainer.setRefreshing(false);

        }
    }

}