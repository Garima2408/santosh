package co.savm.teacher;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.MyCourseItem;
import co.savm.models.SubjectExamsArray;
import co.savm.models.SubjectsAssignmentsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class TeacherMyStudentResults extends BaseAppCompactActivity {
    ListView list_of_Exams;
    Spinner SubjectsList,TypeList;
    public ArrayList<MyCourseItem> mcourse;
    private ArrayList<String> courselist;
    private ProgressSearchOfCourceList progresssearchofcourcelist = null;
    String SubjectId,SubjectTittle,ResultTpyeSelected;
    ArrayList<SubjectExamsArray> listofexams;
    private ProgressSearchOfExamList progresssearchofexamlist = null;
    private  SubjectsAssignmentsDisplay assignmentdisplay = null;
    private ArrayList<String> examlist;
    private ArrayList<String> assignmentlist;
    public  ArrayList<SubjectsAssignmentsArray> mAssignmentList;
    public FloatingActionButton fab;
    FrameLayout top;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_my_student_results);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        SubjectsList =findViewById(R.id.SubjectsList);
        list_of_Exams =findViewById(R.id.list_of_Exams);
        fab = findViewById(R.id.fab);
        top =findViewById(R.id.top);
        TypeList =findViewById(R.id.TypeList);
        mcourse=new ArrayList<>();
        courselist=new ArrayList<>();
        examlist = new ArrayList<String>();
        listofexams = new ArrayList<SubjectExamsArray>();
        assignmentlist=new ArrayList<String>();
        mAssignmentList =new ArrayList<SubjectsAssignmentsArray>();
        progresssearchofcourcelist = new ProgressSearchOfCourceList();
        progresssearchofcourcelist.execute();

        final String[] ResultType = { "Exam", "Assignment",  };

        ArrayAdapter classadapter = new ArrayAdapter(TeacherMyStudentResults.this,android.R.layout.simple_spinner_item,ResultType);
        classadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        TypeList.setAdapter(classadapter);

        TypeList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                ResultTpyeSelected = ResultType[position];

                if (ResultType !=null && SubjectId!=null ){

                    if (ResultTpyeSelected.matches("Exam")){
                        examlist.clear();
                        listofexams.clear();
                        progresssearchofexamlist = new ProgressSearchOfExamList();
                        progresssearchofexamlist.execute(SubjectId);

                    }else if (ResultTpyeSelected.matches("Assignment")){
                        mAssignmentList.clear();
                        assignmentlist.clear();
                        assignmentdisplay = new SubjectsAssignmentsDisplay();
                        assignmentdisplay.execute(SubjectId);
                    }


                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }


    private class ProgressSearchOfCourceList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEFACULTYSUBJECT+"/"+ SessionManager.getInstance(getActivity()).getUser().userprofile_id+"/"+"subject"+"?type=faculty");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherMyStudentResults.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofcourcelist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        mcourse.clear();
                        courselist.clear();
                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                MyCourseItem CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), MyCourseItem.class);
                                mcourse.add(CourseInfo);
                                courselist.add(CourseInfo.getTitle());

                                SubjectsList.setAdapter(new ArrayAdapter<String>(TeacherMyStudentResults.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        courselist));
                                SubjectsList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                        SubjectId = mcourse.get(position).getNid();
                                        SubjectTittle = mcourse.get(position).getTitle();
                                        Log.d("TAG", "edt_selectsub: " + SubjectId);

                                        if (ResultTpyeSelected.matches("Exam")){
                                            examlist.clear();
                                            listofexams.clear();
                                            progresssearchofexamlist = new ProgressSearchOfExamList();
                                            progresssearchofexamlist.execute(SubjectId);

                                        }else if (ResultTpyeSelected.matches("Assignment")){
                                            mAssignmentList.clear();
                                            assignmentlist.clear();
                                            assignmentdisplay = new SubjectsAssignmentsDisplay();
                                            assignmentdisplay.execute(SubjectId);
                                        }




                                        CallFabValue(SubjectId,SubjectTittle);



                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                            }



                        }else{
                            top.setVisibility(View.GONE);
                            showAlertDialog(getString(co.savm.R.string.No_course));

                        }











                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofcourcelist = null;
            hideLoading();


        }
    }

    private void CallFabValue(final String subjectId, final String subjectTittle) {

        fab.setVisibility(View.VISIBLE);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(TeacherMyStudentResults.this, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",subjectId);
                bundle.putString("TITTLE",subjectTittle);
                bundle.putString("SAME","0");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private class ProgressSearchOfExamList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTEXAMS+"?"+"sid="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherMyStudentResults.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofexamlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        examlist.clear();
                        listofexams.clear();
                        JSONArray data = responce.getJSONArray("data");
                        if(data != null && data.length() > 0 ) {
                            for (int i=0; i<data.length();i++) {
                                SubjectExamsArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), SubjectExamsArray.class);
                                examlist.add(GoalInfo.getTitle());
                                listofexams.add(GoalInfo);

                            }


                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(TeacherMyStudentResults.this,
                                    android.R.layout.simple_list_item_1, android.R.id.text1,
                                    examlist);
                            list_of_Exams.setAdapter(adapter);
                            // Bind array strings into an adapter
                            list_of_Exams.setVisibility(View.VISIBLE);

                            // Capture ListView item click
                            list_of_Exams.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    // Capture the click position and set it into a string
                                    String Examname = listofexams.get(position).getTitle();
                                    String ExamId =listofexams.get(position).getTnid();

                                    Intent intent=new Intent(TeacherMyStudentResults.this,StudentsResultDisplay.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("COURSE_ID",SubjectId);
                                    bundle.putString("EXAM_ID",ExamId);
                                    bundle.putString("EXAMNAME",Examname);
                                    intent.putExtras(bundle);
                                    startActivity(intent);



                                }
                            });




                        }else {

                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherMyStudentResults.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(co.savm.R.string.NoExams))
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            list_of_Exams.setVisibility(View.GONE);
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();


                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);




                    }
                }
            } catch (JSONException e) {
                hideLoading();



            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofexamlist = null;
            hideLoading();



        }
    }




    /*LIST OF ASSIGNMENTS*/


    private  class SubjectsAssignmentsDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTASSIGNMENTS+"?"+"sid="+args[0]+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                TeacherMyStudentResults.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentdisplay = null;
            try {
                String errorCode = null;
                if (responce != null) {
                    hideLoading();
                    errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                SubjectsAssignmentsArray assignmentInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectsAssignmentsArray.class);
                                mAssignmentList.add(assignmentInfo);
                                assignmentlist.add(assignmentInfo.getTitle());

                            }


                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(TeacherMyStudentResults.this,
                                    android.R.layout.simple_list_item_1, android.R.id.text1,
                                    assignmentlist);
                            list_of_Exams.setAdapter(adapter);
                            // Bind array strings into an adapter
                            list_of_Exams.setVisibility(View.VISIBLE);

                            // Capture ListView item click
                            list_of_Exams.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    // Capture the click position and set it into a string
                                    String Examname = mAssignmentList.get(position).getTitle();
                                    String ExamId =mAssignmentList.get(position).getTnid();

                                    Intent intent=new Intent(TeacherMyStudentResults.this,StudentsResultDisplay.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("COURSE_ID",SubjectId);
                                    bundle.putString("EXAM_ID",ExamId);
                                    bundle.putString("EXAMNAME",Examname);
                                    intent.putExtras(bundle);
                                    startActivity(intent);



                                }
                            });




                        }else {

                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherMyStudentResults.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(co.savm.R.string.NoAssignment))
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            list_of_Exams.setVisibility(View.GONE);
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();


                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }

                }
            }catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            assignmentdisplay = null;
            hideLoading();

        }
    }














    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
