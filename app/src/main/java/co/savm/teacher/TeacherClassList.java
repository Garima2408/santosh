package co.savm.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.SubjectsClassesAdapters;
import co.savm.models.SubjectsClassesArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import okhttp3.OkHttpClient;

public class TeacherClassList extends BaseFragment {

    public static ArrayList<SubjectsClassesArray> mClassesList;
    private static SubjectsClassesAdapters classesAdapters;
    static RecyclerView rv_subject_classes;
    private RecyclerView.LayoutManager layoutManager;
    private static CourseModuleClassessDisplay courseclassdisplay = null;
    String Subject_id,Adminuser,CourseTittle;
    static String Subject_idMain,classID, classtittle,classlocation,startdate,endate,statrttime,endtime,week,classroom,classType;
    static Activity activity;
    private static ProgressBar spinner;
    static TextView ErrorText;
    static ImageView ReloadProgress;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4,floatingActionButton5;
   // private DeleteSubjectClasses deletesubjectclasses = null;
    RelativeLayout MainLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_teacher_class_list, container, false);
        activity = (Activity) view.getContext();
        Subject_id = getArguments().getString("COURSE_ID");
        Subject_idMain = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        CourseTittle = getArguments().getString("TITTLE");
        if (Subject_id == null) {
        }

        mClassesList = new ArrayList<>();
        layoutManager = new LinearLayoutManager(activity);
        MainLayout = view.findViewById(R.id.MainLayout);
        rv_subject_classes = view.findViewById(R.id.rv_subject_classes);
        rv_subject_classes.setHasFixedSize(true);
        rv_subject_classes.setLayoutManager(layoutManager);
        spinner= view.findViewById(R.id.progressBar);
        ErrorText =view.findViewById(R.id.ErrorText);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);

        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);


        if(Adminuser.equals("TRUE")){
            setHasOptionsMenu(true);
            rv_subject_classes.setVisibility(View.VISIBLE);

            materialDesignFAM.setVisibility(View.VISIBLE);
            courseclassdisplay = new CourseModuleClassessDisplay();
            courseclassdisplay.execute(Subject_idMain);

        }else if (Adminuser.equals("FALSE")) {
            setHasOptionsMenu(false);
            materialDesignFAM.setVisibility(View.INVISIBLE);
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");
        }

        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalledFromAddClass();
            }
        });




        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Subject_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Subject_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });
        return view;
    }


    public static void CalledFromAddClass() {

        mClassesList.clear();
        courseclassdisplay = new CourseModuleClassessDisplay();
        courseclassdisplay.execute(Subject_idMain);

    }








    private static class CourseModuleClassessDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGESUBCLASSES+ "?"+"sid="+args[0]+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            courseclassdisplay = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);
                    rv_subject_classes.setVisibility(View.VISIBLE);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length()-1; i++) {

                                SubjectsClassesArray CourseInfo = new SubjectsClassesArray();

                                classID = jsonArrayData.getJSONObject(i).getString("tnid");
                                classtittle = jsonArrayData.getJSONObject(i).getString("title");
                                classlocation = jsonArrayData.getJSONObject(i).getString("location");
                                classroom = jsonArrayData.getJSONObject(i).getString("room");
                                classType =jsonArrayData.getJSONObject(i).getString("type");
                                JSONArray classdates = jsonArrayData.getJSONObject(i).getJSONArray("date");
                                if (classdates != null && classdates.length() > 0) {

                                    for (int j = 0; j < classdates.length(); j++) {
                                        startdate = classdates.getJSONObject(j).getString("start_date");
                                        endate = classdates.getJSONObject(j).getString("end_date");
                                        statrttime = classdates.getJSONObject(j).getString("start_time");
                                        endtime = classdates.getJSONObject(j).getString("end_time");
                                        week = classdates.getJSONObject(j).getString("week");


                                        CourseInfo.tnid = classID;
                                        CourseInfo.title = classtittle;
                                        CourseInfo.location = classlocation;
                                        CourseInfo.start_date = startdate;
                                        CourseInfo.end_date = endate;
                                        CourseInfo.start_time = statrttime;
                                        CourseInfo.end_time = endtime;
                                        CourseInfo.week = week;
                                        CourseInfo.room =classroom;
                                        CourseInfo.classType =classType;
                                        mClassesList.add(CourseInfo);

                                        classesAdapters = new SubjectsClassesAdapters(activity, mClassesList,Subject_idMain);
                                        rv_subject_classes.setAdapter(classesAdapters);

                                        ErrorText.setVisibility(View.INVISIBLE);
                                        ReloadProgress.setVisibility(View.GONE);

                                    }

                                }

                            }
                        }else {
                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Classes");
                            rv_subject_classes.setVisibility(View.GONE);
                            ReloadProgress.setVisibility(View.GONE);
                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);
               // ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            courseclassdisplay = null;
            spinner.setVisibility(View.GONE);

        }
    }



}


