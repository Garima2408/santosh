package co.savm.teacher;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.InviteTeacherAdapter;
import co.savm.models.InviteTeacherArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

import static co.savm.teacher.CourseModuleTeacherInfo.RefreshWorkedFaculty;

public class InviteTeacher extends BaseAppCompactActivity {
    private SendInviteTeacherDisplay sendinvitelist = null;
    public static ArrayList<InviteTeacherArray> mTeacherList;
    private InviteTeacherAdapter searchedadapter;
    RecyclerView SearchedTeacherList;
    private RecyclerView.LayoutManager layoutManager;
    static EditText searchEmail;
    String SearchedName,Course_id;
    static Activity activity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_teacher);
        activity = this;
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getActivity().getIntent().getExtras();
        Course_id = b.getString("COURSE_ID");



        searchEmail = findViewById(R.id.searchEmail);
        layoutManager = new LinearLayoutManager(InviteTeacher.this);
        SearchedTeacherList = findViewById(R.id.SearchedTeacherList);
        SearchedTeacherList.setHasFixedSize(true);
        SearchedTeacherList.setLayoutManager(layoutManager);


        searchEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {

                    addnewTeacherinsubject();


                    return true;
                }
                return false;
            }
        });


    }
    public static void hideKeyboard(Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(((Activity) mContext).getWindow()
                .getCurrentFocus().getWindowToken(), 0);
    }

    private void addnewTeacherinsubject() {

        searchEmail.setError(null);

        // Store values at the time of the login attempt.
        SearchedName = searchEmail.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(SearchedName)) {
            focusView = searchEmail;
            cancel = true;
            //showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            /* ADD COMMENTS ON SUBJECTS*/
            sendinvitelist = new SendInviteTeacherDisplay();
            sendinvitelist.execute(SearchedName);
        }
    }





      private class SendInviteTeacherDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
             showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEFACULTYLIST+"?"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&"+"search="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                 InviteTeacher.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitelist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            mTeacherList = new ArrayList<>();
                            for (int i = 0; i < jsonArrayData.length(); i++) {


                                InviteTeacherArray TeacherInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), InviteTeacherArray.class);
                                mTeacherList.add(TeacherInfo);
                                searchedadapter = new InviteTeacherAdapter(InviteTeacher.this, mTeacherList, Course_id);
                                SearchedTeacherList.setAdapter(searchedadapter);


                            }

                        } else {
                            showAlertDialog(getString(R.string.noteacher));

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                      showAlertDialog(msg);
                         hideLoading();



                    }
                }
            } catch (JSONException e) {
                 hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            sendinvitelist = null;
            hideLoading();


        }
    }



  /*  private class SendInviteTeacherDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEFACULTYLIST+"?"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&"+"search="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                InviteTeacher.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitelist = null;
            try {
                if (responce != null) {

                    hideLoading();

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                mTeacherList = new ArrayList<>();
                                InviteTeacherArray TeacherInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), InviteTeacherArray.class);
                                mTeacherList.add(TeacherInfo);
                                searchedadapter = new InviteTeacherAdapter(InviteTeacher.this, mTeacherList,Course_id);
                                SearchedTeacherList.setAdapter(searchedadapter);




                            }

                        }else{

                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                           // builder.setTitle("Info");
                            builder.setMessage(getString(co.questin.R.string.nouser))
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {


                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            sendinvitelist = null;
            hideLoading();

        }
    }*/

    public static void RefreshedThepage() {
        searchEmail.clearFocus();
        searchEmail.getText().clear();
        mTeacherList.clear();
        hideKeyboard(activity);




    }

    @Override
    public void onBackPressed() {

        RefreshWorkedFaculty();
        finish();

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case android.R.id.home:
                RefreshWorkedFaculty();
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
