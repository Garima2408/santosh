package co.savm.teacher;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.AttendanceClassArray;
import co.savm.models.MyCourseItem;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class TeacherMyStudentAttandance extends BaseAppCompactActivity {
    ListView list_of_Classess;
    Spinner SubjectsList;
    private ProgressSearchOfClassList progresssearchofclasslist = null;
    private ProgressSearchOfClassListCollage progresssearchofclasslistcollage = null;
    public ArrayList<MyCourseItem> mcourse;
    private ArrayList<String> courselist;
      private ProgressSearchOfCourceList progresssearchofcourcelist = null;
    String SubjectId,SubjectTittle;
    ArrayList<AttendanceClassArray> listofclasses;
    private ArrayList<String> Classlist;
    public FloatingActionButton fab;
    FrameLayout top;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_my_student_attandance);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        SubjectsList =findViewById(R.id.SubjectsList);
        list_of_Classess =findViewById(R.id.list_of_Classess);
        fab = findViewById(R.id.fab);
        top =findViewById(R.id.top);
        mcourse=new ArrayList<>();
        courselist=new ArrayList<>();
        Classlist = new ArrayList<String>();
        listofclasses = new ArrayList<AttendanceClassArray>();
        progresssearchofcourcelist = new ProgressSearchOfCourceList();
        progresssearchofcourcelist.execute();



    }


    private class ProgressSearchOfCourceList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEFACULTYSUBJECT+"/"+ SessionManager.getInstance(getActivity()).getUser().userprofile_id+"/"+"subject"+"?type=faculty");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherMyStudentAttandance.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofcourcelist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                MyCourseItem CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), MyCourseItem.class);
                                mcourse.add(CourseInfo);
                                courselist.add(CourseInfo.getTitle());

                                SubjectsList.setAdapter(new ArrayAdapter<String>(TeacherMyStudentAttandance.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        courselist));
                                SubjectsList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                        SubjectId = mcourse.get(position).getNid();
                                        SubjectTittle = mcourse.get(position).getTitle();

                                        Log.d("TAG", "edt_selectsub: " + SubjectId);
                                        Classlist.clear();
                                        listofclasses.clear();

                                        if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
                                            if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")) {
                                                progresssearchofclasslist = new ProgressSearchOfClassList();
                                                progresssearchofclasslist.execute(SubjectId);

                                            }else {


                                                progresssearchofclasslistcollage = new ProgressSearchOfClassListCollage();
                                                progresssearchofclasslistcollage.execute(SubjectId);

                                            }
                                            }


                                       CallFabValue(SubjectId,SubjectTittle);

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                            }



                        }else{
                            top.setVisibility(View.GONE);
                            showAlertDialog(getString(co.savm.R.string.No_course));

                        }











                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofcourcelist = null;
            hideLoading();


        }
    }

    private void CallFabValue(final String subjectId, final String subjectTittle) {

        fab.setVisibility(View.VISIBLE);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(TeacherMyStudentAttandance.this, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",subjectId);
                bundle.putString("TITTLE",subjectTittle);
                bundle.putString("SAME","0");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }


    private class ProgressSearchOfClassList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ATTENDANCECLASSES+"?"+"sid="+args[0]+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherMyStudentAttandance.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofclasslist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if(data != null && data.length() > 0 ) {
                            for (int i=0; i<data.length();i++) {
                                AttendanceClassArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), AttendanceClassArray.class);
                                Classlist.add(GoalInfo.getTitle());
                                listofclasses.add(GoalInfo);

                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(TeacherMyStudentAttandance.this,
                                    android.R.layout.simple_list_item_1, android.R.id.text1,
                                    Classlist);
                            list_of_Classess.setAdapter(adapter);
                            // Bind array strings into an adapter
                            list_of_Classess.setVisibility(View.VISIBLE);

                            // Capture ListView item click
                            list_of_Classess.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    // Capture the click position and set it into a string
                                    String Classname = (String) list_of_Classess.getItemAtPosition(position);
                                    String ClassId =listofclasses.get(position).getTnid();

                                    Intent intent=new Intent(TeacherMyStudentAttandance.this,AttandanceViewStudent.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("COURSE_ID",SubjectId);
                                    bundle.putString("CLASSID",ClassId);
                                    bundle.putString("CLASSNAME",Classname);
                                    bundle.putString("TITTLE",SubjectTittle);
                                    intent.putExtras(bundle);
                                    startActivity(intent);



                                }
                            });




                        }else {
                            list_of_Classess.setVisibility(View.GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherMyStudentAttandance.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(co.savm.R.string.NoClass))
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);




                    }
                }
            } catch (JSONException e) {
                hideLoading();



            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofclasslist = null;
            hideLoading();



        }
    }

    /*For Collage List*/

    private class ProgressSearchOfClassListCollage extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ATTENDANCECLASSES+"?"+"sid="+args[0]+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                TeacherMyStudentAttandance.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofclasslistcollage = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if(data != null && data.length() > 0 ) {
                            for (int i=0; i<data.length()-1;i++) {
                                AttendanceClassArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), AttendanceClassArray.class);
                                Classlist.add(GoalInfo.getTitle());
                                listofclasses.add(GoalInfo);

                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(TeacherMyStudentAttandance.this,
                                    android.R.layout.simple_list_item_1, android.R.id.text1,
                                    Classlist);
                            list_of_Classess.setAdapter(adapter);
                            // Bind array strings into an adapter
                            list_of_Classess.setVisibility(View.VISIBLE);

                            // Capture ListView item click
                            list_of_Classess.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    // Capture the click position and set it into a string
                                    String Classname = (String) list_of_Classess.getItemAtPosition(position);
                                    String ClassId =listofclasses.get(position).getTnid();

                                    Intent intent=new Intent(TeacherMyStudentAttandance.this,AttandanceViewStudent.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("COURSE_ID",SubjectId);
                                    bundle.putString("CLASSID",ClassId);
                                    bundle.putString("CLASSNAME",Classname);
                                    bundle.putString("TITTLE",SubjectTittle);
                                    intent.putExtras(bundle);
                                    startActivity(intent);



                                }
                            });




                        }else {
                            list_of_Classess.setVisibility(View.GONE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(TeacherMyStudentAttandance.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(co.savm.R.string.NoClass))
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);




                    }
                }
            } catch (JSONException e) {
                hideLoading();



            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofclasslistcollage = null;
            hideLoading();



        }
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
