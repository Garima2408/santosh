package co.savm.teacher;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.AddStudentInSubjectAdapter;
import co.savm.adapters.StudentGroupMemberAdapter;
import co.savm.models.CollageStudentArray;
import co.savm.models.CourseMemberLists;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;


public class Invite_Students extends BaseFragment {
    static Activity activity;
    static String Course_id;
    String Adminuser;
    String CourseTittle;
    String SearchedName;
    static Button Create_Student;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3, floatingActionButton4,floatingActionButton5;
    static TextView ErrorText;
    static EditText searchEmail;
    private static ProgressBar spinner;
    private SendInviteStudentDisplay sendinvitelist = null;
    public static ArrayList<CollageStudentArray> mSearchList;
    private AddStudentInSubjectAdapter searchedadapter;
    static RecyclerView SearchedStudentList;
    static RecyclerView CourseMemberList;
    private RecyclerView.LayoutManager layoutManager ,mlayoutManager;
    static StudentGroupMemberAdapter mGroupMemberAdapter;
    public static ArrayList<CourseMemberLists> memberList;
    private static GroupMemberListDisplay groupmemberList = null;
    RelativeLayout MainLayout;
    static SwipeRefreshLayout swipeContainer;
    static ImageView ReloadProgress;
    public Invite_Students() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invite__students, container, false);
        activity = (Activity) view.getContext();
        Course_id = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        CourseTittle = getArguments().getString("TITTLE");
        if (Course_id == null) {
        }



        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);
        ErrorText =view.findViewById(R.id.ErrorText);
        searchEmail = view.findViewById(R.id.searchEmail);
        spinner= view.findViewById(R.id.progressBar);
        layoutManager = new LinearLayoutManager(getContext());
        mlayoutManager  = new LinearLayoutManager(getContext());
        SearchedStudentList = view.findViewById(R.id.SearchedStudentList);
        SearchedStudentList.setHasFixedSize(true);
        SearchedStudentList.setLayoutManager(layoutManager);
        CourseMemberList = view.findViewById(R.id.CourseMemberList);
        CourseMemberList.setHasFixedSize(true);
        CourseMemberList.setLayoutManager(mlayoutManager);
        MainLayout = view.findViewById(R.id.MainLayout);
        Create_Student =view.findViewById(R.id.Create_Student);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        memberList=new ArrayList<>();
        swipeContainer =view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        if (Adminuser.equals("TRUE")) {

            materialDesignFAM.setVisibility(View.VISIBLE);
            searchEmail.setVisibility(View.VISIBLE);
            CourseMemberList.setVisibility(View.VISIBLE);
            memberList.clear();
            groupmemberList = new GroupMemberListDisplay();
            groupmemberList.execute(Course_id);




        } else if (Adminuser.equals("FALSE")) {
            materialDesignFAM.setVisibility(View.GONE);
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");

        }


        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        memberList.clear();
                        groupmemberList = new GroupMemberListDisplay();
                        groupmemberList.execute(Course_id);

                    }
                }, 3000);
            }
        });

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RefreshedThepagetheAdapter();
            }
        });


        searchEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {

                    addnewStudentinsubject();


                    return true;
                }
                return false;
            }
        });



        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        Create_Student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, CreateMemberInCollage.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("CameFrominvite","CameFrominvite");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });


        return view;
    }

    public static void hideKeyboard(Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(((Activity) mContext).getWindow()
                .getCurrentFocus().getWindowToken(), 0);
    }

    private void addnewStudentinsubject() {

        searchEmail.setError(null);

        // Store values at the time of the login attempt.
        SearchedName = searchEmail.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(SearchedName)) {
            focusView = searchEmail;
            cancel = true;
            //showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            /* ADD COMMENTS ON SUBJECTS*/
            sendinvitelist = new SendInviteStudentDisplay();
            sendinvitelist.execute(SearchedName);

        }
    }






    private class SendInviteStudentDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDSTUDENTINSUBJECTBYEMAIL+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&"+"search="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitelist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        mSearchList = new ArrayList<>();

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CollageStudentArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CollageStudentArray.class);
                                mSearchList.add(CourseInfo);

                                searchedadapter = new AddStudentInSubjectAdapter(activity, mSearchList,Course_id);
                                SearchedStudentList.setAdapter(searchedadapter);
                                SearchedStudentList.setVisibility(View.VISIBLE);
                                spinner.setVisibility(View.GONE);
                                CourseMemberList.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.GONE);

                                if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
                                    if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")){
                                        Create_Student.setVisibility(View.VISIBLE);
                                    }else {
                                        Create_Student.setVisibility(View.GONE);
                                    }
                                }else {

                                }


                            }

                        }else{
                            spinner.setVisibility(View.GONE);
                            CourseMemberList.setVisibility(View.GONE);
                            ErrorText.setText(R.string.noStudent);
                            ErrorText.setVisibility(View.VISIBLE);
                            hideKeyboard(activity);
                            if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")){
                                Create_Student.setVisibility(View.VISIBLE);
                            }else {
                                Create_Student.setVisibility(View.GONE);
                            }



                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertFragmentDialog(activity, "Error", msg);
                        spinner.setVisibility(View.GONE);



                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);

            }
        }

        @Override
        protected void onCancelled() {
            sendinvitelist = null;
            spinner.setVisibility(View.GONE);


        }
    }




    public static void RefreshedThepage() {
        searchEmail.clearFocus();
        searchEmail.getText().clear();
        mSearchList.clear();
        memberList.clear();
        hideKeyboard(activity);
        SearchedStudentList.setVisibility(View.GONE);
        CourseMemberList.setVisibility(View.VISIBLE);
        groupmemberList = new GroupMemberListDisplay();
        groupmemberList.execute(Course_id);
        Create_Student.setVisibility(View.GONE);
        ErrorText.setVisibility(View.GONE);


    }


    public static void  RefreshedThepagetheAdapter(){
        memberList.clear();
        groupmemberList = new GroupMemberListDisplay();
        groupmemberList.execute(Course_id);


    }



    private static class GroupMemberListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMEMBERLISTS+"/"+args[0]+"/users?fields=id, etid, gid, state&parameters=state=1&page=0&pagesize=100");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        swipeContainer.setRefreshing(false);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        memberList.clear();
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);


                            }
                            mGroupMemberAdapter = new StudentGroupMemberAdapter(activity, memberList,Course_id);
                            CourseMemberList.setAdapter(mGroupMemberAdapter);

                        }else {


                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }else {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.VISIBLE);

                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                ReloadProgress.setVisibility(View.VISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            swipeContainer.setRefreshing(false);



        }
    }



}
