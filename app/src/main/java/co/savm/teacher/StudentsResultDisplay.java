package co.savm.teacher;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.ResultDetailDisplayAdapter;
import co.savm.models.ResultDetailArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class StudentsResultDisplay extends BaseAppCompactActivity {

    private ProgressExamDetailDisplay progresssearchofexamdetail = null;
    String ExamId,ExamName,Course_id;
    RecyclerView Rv_resultDetail;
    public ArrayList<ResultDetailArray> myresultList;
    private ResultDetailDisplayAdapter resultadapter;
    private RecyclerView.LayoutManager layoutManager;
    static TextView ErrorText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_student_display);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getIntent().getExtras();
        Course_id = b.getString("COURSE_ID");
        ExamName = b.getString("EXAMNAME");
        ExamId = b.getString("EXAM_ID");

        myresultList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(this);
        Rv_resultDetail = findViewById(co.savm.R.id.Rv_resultDetail);
        Rv_resultDetail.setHasFixedSize(true);
        Rv_resultDetail.setLayoutManager(layoutManager);
        ErrorText =findViewById(R.id.ErrorText);

        progresssearchofexamdetail = new ProgressExamDetailDisplay();
        progresssearchofexamdetail.execute();


    }

    private class ProgressExamDetailDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_STUDENTDETAILRESULTDISPLAY+"/"+ExamId,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                StudentsResultDisplay.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofexamdetail = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                ResultDetailArray resultList = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ResultDetailArray.class);
                                myresultList.add(resultList);


                                resultadapter = new ResultDetailDisplayAdapter(StudentsResultDisplay.this, myresultList);
                                Rv_resultDetail.setAdapter(resultadapter);

                                }

                        }else {
                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText(msg);
                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofexamdetail = null;
            hideLoading();


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;

                default:
                return super.onOptionsItemSelected(item);
        }

    }


}
