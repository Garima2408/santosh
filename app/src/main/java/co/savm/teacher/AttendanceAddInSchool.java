package co.savm.teacher;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import co.savm.R;
import co.savm.adapters.AddAttendanceAdapter;
import co.savm.database.QuestinSQLiteHelper;
import co.savm.models.AddAttendanceArray;
import co.savm.models.AddAttendanceOfflineArray;
import co.savm.models.AttendanceClassArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class AttendanceAddInSchool extends BaseAppCompactActivity {
    public ArrayList<AddAttendanceArray> mAttendanceList;
    public ArrayList<AddAttendanceOfflineArray> mAttendanceofflineList;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private int startDay, startMonth, startYear;
    ArrayList<AttendanceClassArray> listofcourse;
    ArrayAdapter<String> adapter;
    private ArrayList<String> courselist;
    TextView classess;
    private AddAttendanceAdapter addattendanceadapter;
    ListView list_of_Students;
    private Calendar mcalender;
    private AttendanceStudentList studentlist = null;
    String Course_id, Module, CourseTittle, Presents, startDate, startfrom, ClasssId, Classname, classid;
    TextView selectdate;
    JSONArray studentselected;
    JSONObject sharefriendJson;
    TextView ErrorText;
    private AddAttendenceAsync attendenceAsync = null;

    private static QuestinSQLiteHelper questinSQLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_add_in_school);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Bundle b = getIntent().getExtras();
        ClasssId = b.getString("COURSE_ID");
        CourseTittle = b.getString("TITTLE");
       /* ClasssId =b.getString("CLASSID");
        Classname =b.getString("CLASSNAME");*/
        //  GetTheListOfCourse();
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        mAttendanceList = new ArrayList<>();
        courselist = new ArrayList<String>();
        listofcourse = new ArrayList<AttendanceClassArray>();
        mAttendanceofflineList = new ArrayList<>();
        //calender intialize
        mcalender = Calendar.getInstance();

        mYear = mcalender.get(Calendar.YEAR);
        mMonth = mcalender.get(Calendar.MONTH);
        mDay = mcalender.get(Calendar.DAY_OF_MONTH);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = sdf.format(mcalender.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate2 = sdf2.format(mcalender.getTime());


        Log.d("TAG", "Strings: " + ClasssId);

        list_of_Students = findViewById(R.id.list_of_Students);
        ErrorText = findViewById(R.id.ErrorText);
        selectdate = findViewById(R.id.selectdate);
        classess = findViewById(R.id.classess);
        selectdate.setText(formattedDate2);
        startfrom = formattedDate;
        classess.setText(CourseTittle);

        if(startfrom != null){
            studentlist = new AttendanceStudentList();
            studentlist.execute(ClasssId,startfrom);


        }

        selectdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartdateDialog();
            }
        });


    }

    private void StartdateDialog() {


        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {


                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), android.app.AlertDialog.THEME_HOLO_LIGHT, listener, mYear, mMonth, mDay);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis() + 1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {

        startfrom = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        selectdate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));


        mAttendanceList.clear();
        studentlist = new AttendanceStudentList();
        studentlist.execute(ClasssId,startfrom);


    }




    @Override
    public void onBackPressed() {
        finish();
    }


    private class AttendanceStudentList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("date", args[1])
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_STUDENTATTENDANCELIST + "/" + args[0] +"/og", body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AttendanceAddInSchool.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            studentlist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        final ArrayList<String> allstudent = new ArrayList<String>();
                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                allstudent.add(jsonArrayData.getJSONObject(i).getString("uid"));

                                AddAttendanceArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AddAttendanceArray.class);
                                mAttendanceList.add(CourseInfo);
                                addattendanceadapter = new AddAttendanceAdapter(AttendanceAddInSchool.this, mAttendanceList);
                                list_of_Students.setAdapter(addattendanceadapter);

                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AttendanceAddInSchool.this);
                            // builder.setTitle("Info");
                            builder.setMessage("No Student found")
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            finish();
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            studentlist = null;
            hideLoading();


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.submit_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.Submit:

                if (Utils.isInternetConnected(this)) {
                  /*  Toast.makeText(this, "Connected",
                            Toast.LENGTH_SHORT).show();*/
                    Showlistdata(mAttendanceList);

                    // startService(new Intent(this, AddAttandanceTimeService.class));


                } else {
                   /* Toast.makeText(this,
                            "not Connected",
                            Toast.LENGTH_SHORT).show();*/

                    questinSQLiteHelper.deleteAllAttendance();

                    AddAttandanceInDataBase(mAttendanceList);

                    //  startService(new Intent(this, AddAttandanceTimeService.class));


                }

                //

                //


                return true;


            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void AddAttandanceInDataBase(ArrayList<AddAttendanceArray> mAttendanceList) {

        for (int i = 0; i < this.mAttendanceList.size(); i++) {


            questinSQLiteHelper.AddAttandanceOffline(new AddAttendanceOfflineArray(i, mAttendanceList.get(i).getUid(), mAttendanceList.get(i).getAttendance(), Course_id, ClasssId, startfrom, "class"));

            Log.d("TAG", "insertAttendance: " + (mAttendanceList.get(i).getUid() + ".." + mAttendanceList.get(i).getAttendance() + ".." + Course_id + ".." + ClasssId + ".." + startfrom));


        }


        startService(new Intent(this, AddAttandanceTimeService.class));


    }


    private void Showlistdata(ArrayList<AddAttendanceArray> mAttendanceList) {


        studentselected = new JSONArray();
        for (int i = 0; i < this.mAttendanceList.size(); i++) {
            System.out.println(this.mAttendanceList.get(i).toString()); //prints element i
            System.out.println(this.mAttendanceList.get(i).getAttendance() + "," + this.mAttendanceList.get(i).getUid());


            sharefriendJson = new JSONObject();
            try {
                sharefriendJson.put("uid", mAttendanceList.get(i).getUid());
                sharefriendJson.put("present", mAttendanceList.get(i).getAttendance());
                sharefriendJson.put("subject", ClasssId);
                sharefriendJson.put("class", ClasssId);
                sharefriendJson.put("date", startfrom);
                sharefriendJson.put("type", "subject");//subject
                studentselected.put(sharefriendJson);

                Log.d("TAG", "studentselected: " + studentselected);
                Log.d("TAG", "sharefriendJson: " + sharefriendJson);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        attendenceAsync = new AddAttendenceAsync();
        attendenceAsync.execute();




    }



    private class AddAttendenceAsync extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("records",studentselected.toString())
                    .build();





            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDSTUDENTATTENDANCE,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AttendanceAddInSchool.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            attendenceAsync = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(AttendanceAddInSchool.this);
                        // builder.setTitle("Info");
                        builder.setMessage("Attendance Record Successfully")
                                .setCancelable(false)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        RefreshTheList();
                                        dialog.dismiss();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            attendenceAsync = null;
            hideLoading();


        }
    }

    private void RefreshTheList() {
        mAttendanceList.clear();
        studentlist = new AttendanceStudentList();
        studentlist.execute(ClasssId);




    }




}