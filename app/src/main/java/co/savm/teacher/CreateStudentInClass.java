package co.savm.teacher;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.MyCourseItem;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import co.savm.utils.TextUtils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CreateStudentInClass extends BaseAppCompactActivity {
    String Course_id,CourseTittle,SubjectId,SubjectTittle;
    EditText edt_first_name,edt_last_name,edt_email_address,edt_enrollment_address;
    Button btn_create_student_fromFaculty;
    Spinner SubjectsList;
    private ProgressCreateMemberInCollageFromFaculty creatememberFacultyTask = null;
    private SearchedInviteStudent sendinvitetostudent = null;
    public ArrayList<MyCourseItem> mcourse;
    private ArrayList<String> courselist;
    private ProgressSearchOfCourceList progresssearchofcourcelist = null;
    // private ProgressSearchOfClassList progresssearchofclasslist = null;
    String  email,enrollment,firstName,LastName;
    JSONArray studentselected;
    JSONObject shareAttandanceJson;
    String   NewUser_id,New_User;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_student_in_class);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        edt_first_name = findViewById(R.id.edt_first_name);
        edt_last_name = findViewById(R.id.edt_last_name);
        edt_email_address = findViewById(R.id.edt_email_address);
        edt_enrollment_address = findViewById(R.id.edt_enrollment_address);
        btn_create_student_fromFaculty =findViewById(R.id.btn_create_student_fromFaculty);
        SubjectsList =findViewById(R.id.SubjectsList);
        mcourse=new ArrayList<>();
        courselist=new ArrayList<>();
        progresssearchofcourcelist = new ProgressSearchOfCourceList();
        progresssearchofcourcelist.execute();




        btn_create_student_fromFaculty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptToRegisterFromFaculty();

            }
        });

    }



    private class ProgressSearchOfCourceList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEFACULTYSUBJECT+"/"+ SessionManager.getInstance(getActivity()).getUser().userprofile_id+"/"+"subject"+"?type=faculty");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CreateStudentInClass.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofcourcelist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                MyCourseItem CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), MyCourseItem.class);
                                mcourse.add(CourseInfo);
                                courselist.add(CourseInfo.getTitle());

                                SubjectsList.setAdapter(new ArrayAdapter<String>(CreateStudentInClass.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        courselist));
                                SubjectsList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                                        SubjectId = mcourse.get(position).getNid();
                                        SubjectTittle = mcourse.get(position).getTitle();

                                        Log.d("TAG", "edt_selectsub: " + SubjectId);
                                       /* Classlist.clear();
                                        listofclasses.clear();
                                        progresssearchofclasslist = new ProgressSearchOfClassList();
                                        progresssearchofclasslist.execute(SubjectId);
*/

                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });

                            }



                        }else{

                            AlertDialog.Builder builder = new AlertDialog.Builder(CreateStudentInClass.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(co.savm.R.string.No_course))
                                    .setCancelable(false)
                                    .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Intent backIntent = new Intent(CreateStudentInClass.this, CreateCourse.class)
                                                    .putExtra("open", "CameFromFacultyPage");
                                            startActivity(backIntent);
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }











                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofcourcelist = null;
            hideLoading();


        }
    }






    private void attemptToRegisterFromFaculty() {

        // Reset errors.
        edt_first_name.setError(null);
        edt_last_name.setError(null);
        edt_enrollment_address.setError(null);


        // Store values at the time of the login attempt.
        email = edt_email_address.getText().toString().trim();
        enrollment = edt_enrollment_address.getText().toString().trim();
        firstName = edt_first_name.getText().toString().trim();
        LastName = edt_last_name.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isNullOrEmpty(enrollment)) {
            // Check for a valid password, if the user entered one.
            focusView = edt_enrollment_address;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }  else if (TextUtils.isNullOrEmpty(firstName)) {
            // check for First Name
            focusView = edt_first_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        } else if (TextUtils.isNullOrEmpty(LastName)) {
            // check for Contact No
            focusView = edt_last_name;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt Registration and focus the first
            // form field with an error.
            if (focusView != null) {
                focusView.requestFocus();
            }
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.

            studentselected = new JSONArray();

            shareAttandanceJson = new JSONObject();
            try {
                shareAttandanceJson.put("first_name", firstName);
                shareAttandanceJson.put("last_name", LastName);
                shareAttandanceJson.put("enroll", enrollment);
                shareAttandanceJson.put("cid", SessionManager.getInstance(getActivity()).getCollage().getTnid());
                shareAttandanceJson.put("parent_email", email);
                studentselected.put(shareAttandanceJson);


                Log.d("TAG", "studentselected: " + studentselected);
                Log.d("TAG", "sharefriendJson: " + shareAttandanceJson);

            } catch (JSONException e) {
                e.printStackTrace();
            }



            creatememberFacultyTask = new ProgressCreateMemberInCollageFromFaculty();
            creatememberFacultyTask.execute(studentselected.toString());


        }
    }








    /*METHOD TO CREATE THE MEMBER IN CLG FROM FACULTY PAGE*/



    private class ProgressCreateMemberInCollageFromFaculty extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("records", args[0])
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_CREATE_STUDENTINCOLLAGE,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CreateStudentInClass.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            creatememberFacultyTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {




                        JSONObject newData = responce.getJSONObject("data");
                        if (newData != null && newData.length() > 0) {

                            New_User = newData.getString("is_new");
                            NewUser_id = newData.getString("uid");

                            sendinvitetostudent = new SearchedInviteStudent();
                            sendinvitetostudent.execute(SubjectId,NewUser_id);


                        }














                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                showAlertDialog(getString(R.string.error_responce));
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            showAlertDialog(getString(R.string.error_responce));
            creatememberFacultyTask = null;
            hideLoading();


        }
    }








    private class SearchedInviteStudent extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type","subject")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDSTUDENTINSUBJECT+"/"+args[0]+"/"+args[1], body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                showAlertDialog(getString(R.string.error_responce));
                hideLoading();



            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetostudent = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        // showAlertDialog(msg);
                        // Invite_Students.RefreshedThepage();
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);
                        hideLoading();

                    }
                }
            } catch (JSONException e) {
                showAlertDialog(getString(R.string.error_responce));
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetostudent = null;
            hideLoading();


        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.createstudent_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.Upload_Csv:




                return true;



            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public void onBackPressed() {
        finish();
    }

}
