package co.savm.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.AttendanceClassArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class TeacherSelectAttendance extends BaseFragment {
    TextView TV_theory,TV_practical,TV_tutorial;
    String Course_id,CourseTittle,Adminuser;
    static Activity activity;
    private static ProgressBar spinner;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4,floatingActionButton5;
    RelativeLayout Laymain;
    TextView ErrorText;
    ListView Classeslist;
    static ImageView ReloadProgress;
    ArrayList<AttendanceClassArray> listofclasses;
    private ProgressSearchOfClassList progresssearchofclasslist = null;
    private ArrayList<String> courselist;
    RelativeLayout MainLayout;
    private ProgressSearchOfClassListCollage progresssearchofclasslistcollage = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_teacher_select_attendance, container, false);
        activity = (Activity) view.getContext();

        Course_id = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        Laymain = view.findViewById(R.id.Laymain);
        ErrorText =view.findViewById(R.id.ErrorText);
        Classeslist =view.findViewById(R.id.Classeslist);
        spinner= view.findViewById(R.id.progressBar);
        courselist = new ArrayList<String>();
        listofclasses = new ArrayList<AttendanceClassArray>();
        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);
        MainLayout = view.findViewById(R.id.MainLayout);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        if (Course_id == null) {
        }

        if (Adminuser.equals("TRUE")) {

            Laymain.setVisibility(View.VISIBLE);
            materialDesignFAM.setVisibility(View.VISIBLE);

            if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
                if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")) {
                    progresssearchofclasslist = new ProgressSearchOfClassList();
                    progresssearchofclasslist.execute();

                }else {


                    progresssearchofclasslistcollage = new ProgressSearchOfClassListCollage();
                    progresssearchofclasslistcollage.execute();

                }
            }



        } else if (Adminuser.equals("FALSE")) {
            materialDesignFAM.setVisibility(View.INVISIBLE);

            Laymain.setVisibility(View.INVISIBLE);
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");


        }

        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
                    if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")) {
                        progresssearchofclasslist = new ProgressSearchOfClassList();
                        progresssearchofclasslist.execute();

                    }else {


                        progresssearchofclasslistcollage = new ProgressSearchOfClassListCollage();
                        progresssearchofclasslistcollage.execute();

                    }
                }
            }
        });


        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Course_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Course_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        return view;
    }


    private class ProgressSearchOfClassList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ATTENDANCECLASSES+"?"+"sid="+Course_id+"&offset="+"0"+"&limit=100");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }






        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofclasslist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);
                    ReloadProgress.setVisibility(View.GONE);
                    courselist.clear();
                    listofclasses.clear();

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if(data != null && data.length() > 0 ) {
                        for (int i=0; i<data.length();i++) {
                            AttendanceClassArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), AttendanceClassArray.class);
                            courselist.add(GoalInfo.getTitle());
                            listofclasses.add(GoalInfo);

                        }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_list_item_1, android.R.id.text1,
                                    courselist);
                            Classeslist.setAdapter(adapter);
                            // Bind array strings into an adapter


                            // Capture ListView item click
                            Classeslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    // Capture the click position and set it into a string
                                    String Classname = (String) Classeslist.getItemAtPosition(position);
                                    String ClassId =listofclasses.get(position).getTnid();

                                    Intent intent=new Intent(activity,AttandanceViewStudent.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("COURSE_ID",Course_id);
                                    bundle.putString("CLASSID",ClassId);
                                    bundle.putString("CLASSNAME",Classname);
                                    bundle.putString("TITTLE",CourseTittle);

                                    intent.putExtras(bundle);
                                    startActivity(intent);



                                }
                            });




                        }else {


                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Classes Are Added");


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);
                ReloadProgress.setVisibility(View.VISIBLE);


            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofclasslist = null;
            spinner.setVisibility(View.GONE);



        }
    }


    private class ProgressSearchOfClassListCollage extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }




        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ATTENDANCECLASSES+"?"+"sid="+Course_id+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }






        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofclasslistcollage = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);
                    ReloadProgress.setVisibility(View.GONE);
                    courselist.clear();
                    listofclasses.clear();

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");
                        if(data != null && data.length() > 0 ) {
                            for (int i=0; i<data.length()-1;i++) {
                                AttendanceClassArray GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), AttendanceClassArray.class);
                                courselist.add(GoalInfo.getTitle());
                                listofclasses.add(GoalInfo);

                            }
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity,
                                    android.R.layout.simple_list_item_1, android.R.id.text1,
                                    courselist);
                            Classeslist.setAdapter(adapter);
                            // Bind array strings into an adapter


                            // Capture ListView item click
                            Classeslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                public void onItemClick(AdapterView<?> parent, View view,
                                                        int position, long id) {

                                    // Capture the click position and set it into a string
                                    String Classname = (String) Classeslist.getItemAtPosition(position);
                                    String ClassId =listofclasses.get(position).getTnid();

                                    Intent intent=new Intent(activity,AttandanceViewStudent.class);
                                    Bundle bundle=new Bundle();
                                    bundle.putString("COURSE_ID",Course_id);
                                    bundle.putString("CLASSID",ClassId);
                                    bundle.putString("CLASSNAME",Classname);
                                    bundle.putString("TITTLE",CourseTittle);

                                    intent.putExtras(bundle);
                                    startActivity(intent);



                                }
                            });




                        }else {


                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Classes Are Added");


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);
                ReloadProgress.setVisibility(View.VISIBLE);


            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofclasslistcollage = null;
            spinner.setVisibility(View.GONE);



        }
    }


}
