package co.savm.teacher;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.SubjectsAssignmentsAdapter;
import co.savm.models.SubjectsAssignmentsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import okhttp3.OkHttpClient;

public class TeacherAssignmentList extends BaseFragment {
    public static ArrayList<SubjectsAssignmentsArray> mAssignmentList;
    private static SubjectsAssignmentsAdapter assignmentAdapters;
    static RecyclerView rv_subject_assignments;
    private RecyclerView.LayoutManager layoutManager;
    private static SubjectsAssignmentsDisplay assignmentdisplay = null;
    String Subject_id,Adminuser,CourseTittle;
    static String MainSubject_id;
    static Activity activity;
    static TextView ErrorText;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2, floatingActionButton3,floatingActionButton4,floatingActionButton5;
    RelativeLayout MainLayout;
    static ImageView ReloadProgress;
    private static int PAGE_SIZE = 0;
    private static boolean isLoading= false;
    private boolean _hasLoadedOnce= false; // your boolean field
    static SwipeRefreshLayout swipeContainer;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_teacher_assignment_list, container, false);
        activity = (Activity) view.getContext();
        Subject_id = getArguments().getString("COURSE_ID");
        MainSubject_id= getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        CourseTittle = getArguments().getString("TITTLE");
        if (Subject_id == null) {
        }
        mAssignmentList=new ArrayList<>();


        layoutManager = new LinearLayoutManager(activity);
        rv_subject_assignments = view.findViewById(R.id.rv_subject_assignments);
        rv_subject_assignments.setHasFixedSize(true);
        rv_subject_assignments.setLayoutManager(layoutManager);
        ErrorText =view.findViewById(R.id.ErrorText);
        MainLayout = view.findViewById(R.id.MainLayout);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);
        swipeContainer =view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));


        if(Adminuser.equals("TRUE")){
            setHasOptionsMenu(true);

            materialDesignFAM.setVisibility(View.VISIBLE);
            rv_subject_assignments.setVisibility(View.VISIBLE);
            assignmentdisplay = new SubjectsAssignmentsDisplay();
            assignmentdisplay.execute(MainSubject_id);



        }else if (Adminuser.equals("FALSE")) {
            materialDesignFAM.setVisibility(View.INVISIBLE);
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");
            setHasOptionsMenu(false);

        }

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalledFromAddAssignMent();
            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        CalledFromAddAssignMent();

                    }
                }, 1000);
            }
        });

        MainLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });

        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = view.findViewById(R.id.material_design_floating_action_menu_item2);
        floatingActionButton3 = view.findViewById(R.id.material_design_floating_action_menu_item3);
        floatingActionButton4 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        floatingActionButton5 = view.findViewById(R.id.material_design_floating_action_menu_item5);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(activity, TeacherAddClasses.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","2");
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamCreate.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","4");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, AssignmentUpload.class);
                Bundle bundle = new Bundle();
                bundle.putString("COURSE_ID", Subject_id);
                bundle.putString("SAME","5");
                intent.putExtras(bundle);
                startActivity(intent);


            }
        });
        floatingActionButton4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherAttendanceAdd.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Subject_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","7");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        floatingActionButton5.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, TeacherExamResultUpload.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURSE_ID",Subject_id);
                bundle.putString("TITTLE",CourseTittle);
                bundle.putString("SAME","6");
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });

        return view;
    }


    public static void CalledFromAddAssignMent() {
        swipeContainer.setRefreshing(false);
        rv_subject_assignments.setVisibility(View.VISIBLE);
        mAssignmentList.clear();
        /* CALLING LISTS OF COMMENTS*/
        assignmentdisplay = new SubjectsAssignmentsDisplay();
        assignmentdisplay.execute(MainSubject_id);


    }








    private static class SubjectsAssignmentsDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTASSIGNMENTS+"?"+"sid="+args[0]+"&offset="+"0"+"&limit=50");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentdisplay = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ErrorText.setVisibility(View.GONE);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectsAssignmentsArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectsAssignmentsArray.class);
                                mAssignmentList.add(CourseInfo);

                                assignmentAdapters = new SubjectsAssignmentsAdapter(activity, mAssignmentList,MainSubject_id);
                                rv_subject_assignments.setAdapter(assignmentAdapters);


                            }
                        }else {


                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Assignments");
                            rv_subject_assignments.setVisibility(View.GONE);
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            assignmentdisplay = null;
            swipeContainer.setRefreshing(false);


        }
    }

}


