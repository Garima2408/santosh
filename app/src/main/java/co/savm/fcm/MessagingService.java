package co.savm.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import co.savm.R;
import co.savm.activities.NewsAnonocment;
import co.savm.activities.SpalashScreen;
import co.savm.admin.AdminRouteActivity;
import co.savm.campusfeedsection.AllCampusFeeds;
import co.savm.college.CoursesModules;
import co.savm.models.AllNotificationArray;
import co.savm.models.CampusNotificationArray;
import co.savm.models.chat.SendMessageModel;
import co.savm.models.fcm.FcmChatModel;
import co.savm.models.fcm.FcmGroupChatModel;
import co.savm.teacher.TeacherCourseModule;
import co.savm.tracker.AllRoutesDisplay;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;

public class MessagingService extends FirebaseMessagingService {

    private Gson gson;
    private static final String TAG = "MessagingService";
    List<SendMessageModel> adminNotificationList;
    List<CampusNotificationArray> notificationlist;
    private ArrayList<AllNotificationArray> allAppnotification;
    SessionManager sharedPreference;
    Intent intent;
    String  QUESTIN_NOTIFICATION= "co.questin";
    static int counter = 0;
    public static int college_news = 0;
    public static int singlechat = 0;
    public static int college_class = 0;
    public static int college_exam = 0;
    public static int college_assignment = 0;
    public static int college_feeds = 0;
    public static int college_campusfeeds = 0;
    public static int college_Allfeeds = 0;
    public static int college_classroom = 0;
    String notificationType,notificationId,campuscategory;
    public static int All_App_Notification = 0;




    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        gson = new Gson();

        if (SessionManager.getInstance(this).getUserClgRole().getRole().matches("27")) {

        }else {


            String message = remoteMessage.getData().get("message");
            Log.e(TAG, "onMessageReceived1: " + remoteMessage.getData().toString());
            if (counter >= 0) {
                counter++;
                int badge = counter;
                Utils.setBadge(getApplicationContext(), badge);
            }
            if (message.contains("type")) {
                try {
                    int messageType = new JSONObject(message).getInt("type");
                    if (singlechat >= 0) {
                        singlechat++;

                        switch (messageType) {
                            case Constants.FCM_TYPE_CHAT:

                                handleChatMessage(message);
                                break;
                            case Constants.FCM_TYPE_GROUPCHAT:

                                handleGroupChatMessage(message);
                                break;


                            default:
                                break;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                notificationType = remoteMessage.getData().get("type");
                notificationId = remoteMessage.getData().get("id");
                Log.e(TAG, "onMessageReceived2: " + message + notificationType + notificationId);



                if (notificationType != null) {
                    if (notificationType.matches("college_news")) {
                        if (college_news >= 0) {
                            college_news++;

                            All_App_Notification =college_news;
                           /* SharedPreferences settings = getSharedPreferences("college_news", 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putInt("COLLEGE_NEWS",college_news);
                            editor.commit();*/
                            Log.e(TAG, "onMessageReceivednewsannounce: " + message + notificationType + notificationId +campuscategory);

                            handleNotification(message);

                        }
                    }else if (notificationType.matches("classes")){

                        if (college_class >= 0) {
                            college_class++;

                            All_App_Notification=college_class;
                            college_classroom =college_class;
                            handleNotification(message);
                        }

                    }else if (notificationType.matches("exam")){
                        if (college_exam >= 0) {
                            college_exam++;

                            All_App_Notification=college_exam;

                            college_classroom =college_exam;
                            handleNotification(message);

                        }
                    }else if (notificationType.matches("assignment")){
                        if (college_assignment >= 0) {
                            college_assignment++;

                            All_App_Notification=college_assignment;

                            college_classroom =college_assignment;
                            handleNotification(message);

                        }
                    }else if (notificationType.matches("campus_feed")){
                        if (college_feeds >= 0) {
                            college_feeds++;
                            college_Allfeeds =college_feeds;
                            campuscategory  =remoteMessage.getData().get("category");

                            Log.e(TAG, "onMessageReceivedcampusfeed: " + message + notificationType + notificationId +campuscategory);

                            handleNotification(message);

                        }

                    } else if (notificationType.matches("Bus_Route")) {

                        handleAdminNotification(message);

                        Log.e(TAG, "onMessageReceived3: " + message + notificationType + notificationId);

                    } else if (notificationType.matches("Bus_Route_Attendance")) {
                        if (All_App_Notification >=0){
                            All_App_Notification++;
                            handleNotification(message);
                        }
                    }

                    else if (notificationType.matches("courses")) {
                        if (All_App_Notification >=0){
                            All_App_Notification++;
                            handleNotification(message);
                        }
                    }


                }else{

                    if (All_App_Notification >=0){
                        All_App_Notification++;

                    }

                    handleNotification(message);
                }

            }
        }

    }



    private void handleNotification(String messageBody) {
        if (notificationType !=null){
            if (notificationType.matches("college_news")){

                SaveAllAppNotification(messageBody);

                intent = new Intent(this,NewsAnonocment.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);


            }
            if (notificationType.matches("courses")){

                SaveAllAppNotification(messageBody);

                if (SessionManager.getInstance(this).getUserClgRole().getRole().matches("14")) {
                    intent= new Intent(this,TeacherCourseModule.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }else {
                    intent = new Intent(this,CoursesModules.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }


            }
            else  if (notificationType.matches("campus_feed")){

                SaveAllCampusFeedNotification(messageBody);

                intent = new Intent(this,AllCampusFeeds.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);


            } else if (notificationType.matches("classes")){

                SaveAllAppNotification(messageBody);

                if (SessionManager.getInstance(this).getUserClgRole().getRole().matches("14")) {
                    intent= new Intent(this,TeacherCourseModule.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }else {
                    intent = new Intent(this,CoursesModules.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }


            }
            else if (notificationType.matches("exam")){
                SaveAllAppNotification(messageBody);
                if (SessionManager.getInstance(this).getUserClgRole().getRole().matches("14")) {
                    intent = new Intent(this,TeacherCourseModule.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }else {
                    intent = new Intent(this,CoursesModules.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }
            }
            else if (notificationType.matches("assignment")){
                SaveAllAppNotification(messageBody);
                if (SessionManager.getInstance(this).getUserClgRole().getRole().matches("14")) {
                    intent = new Intent(this,TeacherCourseModule.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }else {
                    intent = new Intent(this,CoursesModules.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }
            }
            else if (notificationType.matches("Bus_Route")){
                if (SessionManager.getInstance(this).getUserClgRole().getRole().matches("3")) {
                    intent = new Intent(this,AdminRouteActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }else {
                    intent = new Intent(this,AllRoutesDisplay.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }
            }else if (notificationType.matches("Bus_Route_Attendance")){
                if (SessionManager.getInstance(this).getUserClgRole().getRole().matches("3")) {
                    intent = new Intent(this,AdminRouteActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

                }else {
                    intent = new Intent(this,AllRoutesDisplay.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                }
            }


        }else {
            SaveAllAppNotification(messageBody);
            intent = new Intent(this,SpalashScreen.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        }

        int uniqueId = (int) System.currentTimeMillis();

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Creates the PendingIntent
            PendingIntent notifyPendingIntent =
                    PendingIntent.getActivity(this, 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            String channelID = "co.questin";// The id of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelID, "MyApp", importance);


            // Create a notification and set the notification channel.
            Notification notification = getNotificationBuilder(messageBody, notifyPendingIntent, defaultSoundUri)
                    .setChannelId(channelID)
                    .build();

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
                notificationManager.notify(uniqueId, notification);
            }
        } else if (notificationManager != null) {

            PendingIntent pendingIntent =
                    PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notificationBuilder = getNotificationBuilder(messageBody, pendingIntent, defaultSoundUri);
            notificationManager.notify(uniqueId /* ID of notification */,
                    notificationBuilder.build());
        }
    }


    private NotificationCompat.Builder getNotificationBuilder(String messageBody, PendingIntent pendingIntent, Uri defaultSoundUri) {
        return new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.appicon)
                .setContentTitle("Santosh Riyan Badi")
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);




    }




    void handleChatMessage(String message){
        boolean screenOpen = LocalBroadcastManager.getInstance(this).sendBroadcast(
                new Intent(Constants.ACTION_CHAT_MESSAGE_RECEIVED)
                        .putExtra(Constants.EXTRA_MESSAGE, message));

        if(!screenOpen){

            FcmChatModel fcmChatModel = gson.fromJson(message, FcmChatModel.class);
            Utils.saveChatMessage(this, fcmChatModel);
        }
    }




    private void handleGroupChatMessage(String message){
        boolean screenOpen = LocalBroadcastManager.getInstance(this).sendBroadcast(
                new Intent(Constants.ACTION_GROUPCHAT_MESSAGE_RECEIVED)
                        .putExtra(Constants.EXTRA_MESSAGE, message));

        if(!screenOpen){



            FcmGroupChatModel fcmChatModel = gson.fromJson(message, FcmGroupChatModel.class);
            Utils.saveGroupChatMessage(this, fcmChatModel);
        }
    }



    private void handleAdminNotification(String messageBody) {

        if (SessionManager.getInstance(this).getUserClgRole().getRole().matches("3")){
            intent = new Intent(this,AdminRouteActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            SaveThatNotification(messageBody);

        }else {
            intent = new Intent(this,AllRoutesDisplay.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                    | Intent.FLAG_ACTIVITY_SINGLE_TOP);


        }



        int uniqueId = (int) System.currentTimeMillis();

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Creates the PendingIntent
            PendingIntent notifyPendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            String channelID = "co.questin";// The id of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelID, "MyApp", importance);
            // Create a notification and set the notification channel.
            Notification notification = getNotificationBuilder(messageBody, notifyPendingIntent)
                    .setChannelId(channelID)
                    .build();

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
                notificationManager.notify(uniqueId, notification);
            }
        } else if (notificationManager != null) {

            PendingIntent pendingIntent =
                    PendingIntent.getActivity(
                            this,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            NotificationCompat.Builder notificationBuilder = getNotificationBuilder(messageBody, pendingIntent);
            notificationManager.notify(uniqueId /* ID of notification */,
                    notificationBuilder.build());
        }
    }



    private NotificationCompat.Builder getNotificationBuilder(String messageBody, PendingIntent pendingIntent) {
        return new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.appicon)
                .setContentTitle("Santosh Riyan Badi")
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);


    }


    private void SaveThatNotification(String messageBody) {
        sharedPreference = new SessionManager(this);
        SendMessageModel msg = new SendMessageModel("Admin",  messageBody);
        adminNotificationList = new ArrayList<SendMessageModel>();
        adminNotificationList.add(msg);
        sharedPreference.addAdminNotification(this, adminNotificationList.get(0));




    }

    private void SaveAllCampusFeedNotification(String messageBody) {

        sharedPreference = new SessionManager(this);
        CampusNotificationArray msg = new CampusNotificationArray("id",  messageBody);
        notificationlist = new ArrayList<CampusNotificationArray>();
        notificationlist.add(msg);
        sharedPreference.addCampusNotification(this, notificationlist.get(0));




    }


    private void SaveAllAppNotification(String messageBody) {
        sharedPreference = new SessionManager(this);
        AllNotificationArray msg = new AllNotificationArray("id",  messageBody);
        allAppnotification = new ArrayList<AllNotificationArray>();
        allAppnotification.add(msg);
        sharedPreference.addAllAppNotification(this, allAppnotification.get(0));


    }


}
