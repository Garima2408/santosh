package co.savm.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import co.savm.utils.Constants;
import co.savm.utils.Utils;

public class InstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = "InstanceIdService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.getSharedPreference(this).edit().putString(Constants.FCM_REG_TOKEN, refreshedToken)
                .apply();
//        Utils.init(this);
//        Utils.saveFCMId(this,refreshedToken);
        Log.e(TAG, "onTokenRefresh: " + refreshedToken +"ye save h " );

    }
}
