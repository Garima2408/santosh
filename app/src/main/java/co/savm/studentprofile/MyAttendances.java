package co.savm.studentprofile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import co.savm.R;
import co.savm.adapters.MyAttendanceAdapter;
import co.savm.models.AttendanceArray;
import co.savm.models.TaskChildListArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class MyAttendances extends BaseAppCompactActivity {
    private ExpandableListView mExpandableListView;
    private MyAttendanceAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private Map<String, List<String>> mExpandableListData;
    private ArrayList<TaskChildListArray> goalList;

    private List<AttendanceArray> expandableList;
    private ProgressoftaskList progressoftasklistAuthTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.savm.R.layout.activity_my_attendances);
        mExpandableListView = findViewById(R.id.itemExpandableListView);

        progressoftasklistAuthTask = new ProgressoftaskList();
        progressoftasklistAuthTask.execute();


        expandableList = new ArrayList<AttendanceArray>();
        ArrayList<TaskChildListArray> goalList = null;

    }


    private class ProgressoftaskList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_MYATTENDANCE +"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id, body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                MyAttendances.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.savm.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progressoftasklistAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                JSONObject perResult = jsonArrayData.getJSONObject(i);

                                JSONArray ClassArray = perResult.getJSONArray("class");
                                ArrayList<TaskChildListArray> goalList = new ArrayList<>();
                                for (int j = 0; j < ClassArray.length(); j++) {

                                    TaskChildListArray task = new Gson().fromJson(ClassArray.getJSONObject(j).toString(), TaskChildListArray.class);
                                    goalList.add(task);

                                }


                                AttendanceArray habitParentBean = new AttendanceArray();
                                habitParentBean.parentTitle = perResult.getString("subject");
                                habitParentBean.numbers = perResult.getString("total");
                                habitParentBean.childsTaskList.addAll(goalList);
                                expandableList.add(habitParentBean);


                                mExpandableListAdapter = new MyAttendanceAdapter(getActivity(), expandableList);
                                mExpandableListView.setIndicatorBounds(0, 20);
                                mExpandableListView.setAdapter(mExpandableListAdapter);
                                mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                    @Override
                                    public void onGroupExpand(int groupPosition) {
                                        int len = mExpandableListAdapter.getGroupCount();
                                        for (int i = 0; i < len; i++) {
                                            if (i != groupPosition) {
                                                mExpandableListView.collapseGroup(i);
                                            }
                                        }
                                    }
                                });

                                mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

                                    @Override
                                    public boolean onChildClick(ExpandableListView parent, View v,
                                                                int groupPosition, int childPosition, long id) {

                                        Intent intent=new Intent(MyAttendances.this,DetailDisplayMyattandance.class);
                                        Bundle bundle=new Bundle();
                                        bundle.putString("TITTLE",expandableList.get(groupPosition).childsTaskList.get(childPosition).getTitle());
                                        bundle.putString("PERCENT", expandableList.get(groupPosition).childsTaskList.get(childPosition).getPerc());
                                        bundle.putString("CLASS_ID", expandableList.get(groupPosition).childsTaskList.get(childPosition).getNid());
                                        intent.putExtras(bundle);
                                        startActivity(intent);

                                        return false;

                                    }
                                });


                            }
                        } else {
                            showAlertDialog(getString(R.string.MyAttandance));

                        }
                    }
                    else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progressoftasklistAuthTask = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}