package co.savm.studentprofile;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import co.savm.R;
import co.savm.adapters.MyResultAdapter;
import co.savm.models.MyResultsItem;
import co.savm.models.ResultChlidListArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class MyResults extends BaseAppCompactActivity {
    private ExpandableListView mExpandableListView;
    private MyResultAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private Map<String, List<String>> mExpandableListData;
    private ArrayList<ResultChlidListArray> parentList;
    private ArrayList<ResultChlidListArray> parentassignmentList;

    private List<MyResultsItem> expandableList;
    private ProgressoftaskList progressoftasklistAuthTask = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_results);
        mExpandableListView = findViewById(R.id.itemExpandableListView);

        progressoftasklistAuthTask = new ProgressoftaskList();
        progressoftasklistAuthTask.execute();

        expandableList = new ArrayList<MyResultsItem>();
        ArrayList<ResultChlidListArray> parentList = null;
        ArrayList<ResultChlidListArray> parentassignmentList = null;

    }


    private class ProgressoftaskList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();



            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYRESULT +"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                MyResults.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progressoftasklistAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                JSONObject perResult = jsonArrayData.getJSONObject(i);

                                JSONArray ClassArray = perResult.getJSONArray("exam");

                                if (ClassArray != null && ClassArray.length() > 0) {
                                    ArrayList<ResultChlidListArray> parentList = new ArrayList<>();
                                    for (int j = 0; j < ClassArray.length(); j++) {

                                        ResultChlidListArray task = new Gson().fromJson(ClassArray.getJSONObject(j).toString(), ResultChlidListArray.class);
                                        parentList.add(task);

                                    }
                                    MyResultsItem ParentBean = new MyResultsItem();
                                    ParentBean.className = perResult.getString("subject");
                                    ParentBean.classType ="Exams";
                                    ParentBean.childsTaskList.addAll(parentList);
                                    expandableList.add(ParentBean);

                                }

                                JSONArray AssignmentArray = perResult.getJSONArray("assignment");

                                if (AssignmentArray != null && AssignmentArray.length() > 0) {

                                    ArrayList<ResultChlidListArray> parentassignmentList = new ArrayList<>();
                                    for (int j = 0; j < AssignmentArray.length(); j++) {

                                        ResultChlidListArray task = new Gson().fromJson(AssignmentArray.getJSONObject(j).toString(), ResultChlidListArray.class);
                                        parentassignmentList.add(task);


                                    }
                                    MyResultsItem ParentBean = new MyResultsItem();
                                    ParentBean.className = perResult.getString("subject");
                                    ParentBean.classType ="Assignments";
                                    ParentBean.childsTaskList.addAll(parentassignmentList);
                                    expandableList.add(ParentBean);

                                }


                                mExpandableListAdapter = new MyResultAdapter(getActivity(), expandableList);
                                mExpandableListView.setIndicatorBounds(0, 20);
                                mExpandableListView.setAdapter(mExpandableListAdapter);
                                mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                    @Override
                                    public void onGroupExpand(int groupPosition) {
                                        int len = mExpandableListAdapter.getGroupCount();
                                        for (int i = 0; i < len; i++) {
                                            if (i != groupPosition) {
                                                mExpandableListView.collapseGroup(i);
                                            }
                                        }
                                    }
                                });
                            }

                        }else{
                            showAlertDialog(getString(R.string.MyResult));

                        }
                    }

                    else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progressoftasklistAuthTask = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}