package co.savm.studentprofile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.savm.R;

public class MyTransport extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_transport);
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
