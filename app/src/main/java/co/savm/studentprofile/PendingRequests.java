package co.savm.studentprofile;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.PendindRequestAdapter;
import co.savm.models.RequestPendingArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class PendingRequests extends Fragment {
    static RecyclerView PendingFriends;
    static TextView ErrorText;
    public static ArrayList<RequestPendingArray> mRequestpendingList;
    private static PendindRequestAdapter requestadapter;
    private RecyclerView.LayoutManager layoutManager;
    private static FriendRequestPendingListDisplay friendrequestpendinglist = null;
    static Activity activity;

    static boolean isLoading= false;
    static int PAGE_SIZE = 0;
    private int currentPage = 1;
    static SwipeRefreshLayout swipeContainer;
    private static ProgressBar spinner;
    public PendingRequests() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_pending_requests, container, false);
        activity = (Activity) view.getContext();

        mRequestpendingList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        PendingFriends = view.findViewById(R.id.PendingFriends);
        PendingFriends.setHasFixedSize(true);
        PendingFriends.setLayoutManager(layoutManager);
        spinner= view.findViewById(R.id.progressBar);
        swipeContainer=view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        ErrorText =view.findViewById(R.id.ErrorText);
        requestadapter = new PendindRequestAdapter(PendingFriends, mRequestpendingList,activity);
        PendingFriends.setAdapter(requestadapter);

        friendrequestpendinglist = new FriendRequestPendingListDisplay();
        friendrequestpendinglist.execute();



        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        CalledFromPendingAdapter();


                    }
                }, 3000);
            }
        });

        inItView();
        return view;
    }

    private void inItView() {

        swipeContainer.setRefreshing(true);

        PendingFriends.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    requestadapter.addProgress();


                    friendrequestpendinglist = new FriendRequestPendingListDisplay();
                    friendrequestpendinglist.execute();


                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });


    }


    public static void CalledFromPendingAdapter() {
        PAGE_SIZE=0;
        isLoading=false;
        swipeContainer.setRefreshing(true);
        mRequestpendingList.clear();
        friendrequestpendinglist = new FriendRequestPendingListDisplay();
        friendrequestpendinglist.execute();





    }



    private static class FriendRequestPendingListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //  Utils.p_dialog(activity);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_GETPENDINGREQUEST+"?offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);

                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

                requestadapter.removeProgress();
                isLoading=false;
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            friendrequestpendinglist = null;
            try {
                if (responce != null) {

                    PendingFriends.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);
                    mRequestpendingList.clear();


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<RequestPendingArray> arrayList = new ArrayList<>();
                        arrayList.clear();
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                RequestPendingArray Request = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), RequestPendingArray.class);
                                arrayList.add(Request);



                            }


                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                PendingFriends.setVisibility(View.VISIBLE);
                                requestadapter.setInitialData(arrayList);

                            } else {
                                isLoading = false;
                                requestadapter.removeProgress();
                                swipeContainer.setRefreshing(false);
                                requestadapter.addData(arrayList);
                                ErrorText.setVisibility(View.GONE);

                            }
                        }else{
                            if (PAGE_SIZE==0){
                                PendingFriends.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);


                            }else
                                requestadapter.removeProgress();

                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                        swipeContainer.setRefreshing(false);

                    }
                }
            } catch (JSONException e) {

                swipeContainer.setRefreshing(false);
            }
        }

        @Override
        protected void onCancelled() {
            friendrequestpendinglist = null;
            swipeContainer.setRefreshing(false);

        }
    }




    @Override
    public void onDestroy() {
        isLoading=false;
        PAGE_SIZE=0;
        super.onDestroy();
    }

}
