package co.savm.studentprofile;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.MyFriendsAdapter;
import co.savm.models.AllFriends;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class MyFriends extends Fragment {
    RecyclerView rv_myfriendslist;
    public ArrayList<AllFriends> mFriendsList;
    public String MainRole,CollageId,  CollageName,CollageRole,CollageBranch,CollageEnrollment,Collagedepartment,CollageStudentName;


    private  boolean isLoading= false;
    private  int PAGE_SIZE = 0;
    private int currentPage = 1;
    SwipeRefreshLayout swipeContainer;

    private MyFriendsAdapter mMyfriendadapter;
    private RecyclerView.LayoutManager layoutManager;
    private MyFriendsListDisplay myfriendslist = null;
    static Activity activity;
    private ProgressBar spinner;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;


    public MyFriends() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_my_friends, container, false);
        activity = (Activity) view.getContext();
        mFriendsList = new ArrayList<>();


        layoutManager = new LinearLayoutManager(getContext());
        rv_myfriendslist = view.findViewById(R.id.rv_myfriendslist);
        spinner= view.findViewById(R.id.progressBar);
        rv_myfriendslist.setHasFixedSize(true);
        swipeContainer =view.findViewById(R.id.swipeContainer);
        rv_myfriendslist.setLayoutManager(layoutManager);
        ErrorText =view.findViewById(R.id.ErrorText);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        mMyfriendadapter = new MyFriendsAdapter(rv_myfriendslist,mFriendsList,activity);
        rv_myfriendslist.setAdapter(mMyfriendadapter);


        myfriendslist = new MyFriendsListDisplay();
        myfriendslist.execute();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        myfriendslist = new MyFriendsListDisplay();
                        myfriendslist.execute();

                    }
                }, 3000);
            }
        });

        inItView();
        return view;
    }

    private void inItView() {
        swipeContainer.setRefreshing(false);

        rv_myfriendslist.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mMyfriendadapter.addProgress();

                    myfriendslist = new MyFriendsListDisplay();
                    myfriendslist.execute();
                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }



    private class MyFriendsListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYFRIENDS+"?"+"email="+ SessionManager.getInstance(activity).getUser().getEmail()+"&offset="+PAGE_SIZE+"&limit=150");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

                mMyfriendadapter.removeProgress();
                isLoading=false;


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
               myfriendslist = null;
            try {
                if (responce != null) {

                    rv_myfriendslist.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);



                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<AllFriends> arrayList = new ArrayList<>();

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                AllFriends FriendInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);


                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                if (jsonObject.has("college")) {
                                    JSONObject CampusData = jsonObject.getJSONObject("college");

                                    if (CampusData != null && CampusData.length() > 0 ){
                                        String MainRole = CampusData.getString("role");

                                        if (MainRole.matches("13")) {
                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageBranch = CampusData.getString("field_branch");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.CollageBranch =CollageBranch;
                                            FriendInfo.MainRole =MainRole;


                                        } else if (MainRole.matches("14")) {


                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            Collagedepartment = CampusData.getString("field_department");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.Collagedepartment =Collagedepartment;
                                            FriendInfo.MainRole =MainRole;


                                        } else if (MainRole.matches("15")) {


                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageBranch = CampusData.getString("field_branch");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.CollageBranch =CollageBranch;
                                            FriendInfo.MainRole =MainRole;



                                        } else if (MainRole.matches("16")) {

                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            CollageStudentName = CampusData.getString("field_student_name");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.CollageStudentName =CollageStudentName;
                                            FriendInfo.MainRole =MainRole;


                                        }else if (MainRole.matches("3")) {


                                            CollageName = CampusData.getString("title");
                                            CollageRole = CampusData.getString("field_i_am_a");
                                            Collagedepartment = CampusData.getString("field_department");
                                            CollageId = CampusData.getString("tnid");

                                            FriendInfo.CollageId =CollageId;
                                            FriendInfo.CollageName =CollageName;
                                            FriendInfo.CollageRole =CollageRole;
                                            FriendInfo.Collagedepartment =Collagedepartment;
                                            FriendInfo.MainRole =MainRole;


                                        }



                                    }else {

                                    }



                                }else {

                                }





                                arrayList.add(FriendInfo);


                            }







                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_myfriendslist.setVisibility(View.VISIBLE);
                                mMyfriendadapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                isLoading = false;
                                mMyfriendadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                mMyfriendadapter.addData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);




                            }
                        }else{

                            if (PAGE_SIZE==0){
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("you have no friends");

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                mMyfriendadapter.removeProgress();

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");
                        swipeContainer.setRefreshing(false);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);



                    }
                }
            } catch (JSONException e) {

                swipeContainer.setRefreshing(false);

            }
        }

        @Override
        protected void onCancelled() {
            myfriendslist = null;
            swipeContainer.setRefreshing(false);




        }
    }





    @Override
    public void onDestroy() {
        isLoading=false;
        PAGE_SIZE=0;
        super.onDestroy();
    }
}
