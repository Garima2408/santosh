package co.savm.studentprofile;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.MyEventsAdapters;
import co.savm.college.CollageEvent;
import co.savm.models.MyEventsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class MyEvents extends BaseAppCompactActivity {

   static Activity activity;
    FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton floatingActionButton1;
   static TextView text_view,text_view2;
  static   RelativeLayout relative,Displaytext;
    public static ArrayList<MyEventsArray> mEvent;
   static RecyclerView rv_myEvents;
    private RecyclerView.LayoutManager layoutManager;
    private static MyEventsAdapters eventadapter;
    private static MyEventsListDisplay myeventlistdisplay = null;
   static String eventid,eventtittle,eventlogo,eventstartdate,eventstarttime,eventlocation,endate,endtime,week;

   static TextView ErrorText;
   static ImageView ReloadProgress;

    private static ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.savm.R.layout.activity_my_events);

        activity = getActivity();
        Displaytext =findViewById(R.id.Displaytext);
        text_view = findViewById(R.id.text_view);
        text_view2 = findViewById(R.id.text_view2);
        ErrorText = findViewById(R.id.ErrorText);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);
        relative = findViewById(R.id.relative);
        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);
        mEvent=new ArrayList<>();
        ReloadProgress = findViewById(R.id.ReloadProgress);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        rv_myEvents = findViewById(co.savm.R.id.rv_myEvents);
        rv_myEvents.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_myEvents.setLayoutManager(layoutManager);


        if(SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {

            myeventlistdisplay = new MyEventsListDisplay();
            myeventlistdisplay.execute(SessionManager.getInstance(getActivity()).getUser().getParentUid());


        }else {
            myeventlistdisplay = new MyEventsListDisplay();
            myeventlistdisplay.execute(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());

        }



        relative.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MyEvents.this, CollageEvent.class);
                startActivity(intent);


            }
        });


        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RefershMyEventList();
            }
        });



    }

    public static void RefershMyEventList() {

        mEvent.clear();

        if(SessionManager.getInstance(activity).getUserClgRole().getRole().matches("16")) {

            myeventlistdisplay = new MyEventsListDisplay();
            myeventlistdisplay.execute(SessionManager.getInstance(activity).getUser().getParentUid());


        }else {
            myeventlistdisplay = new MyEventsListDisplay();
            myeventlistdisplay.execute(SessionManager.getInstance(activity).getUser().getUserprofile_id());

        }

    }


    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }


    private static class MyEventsListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          // showLoading();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .build();
            try {

                String responseData = ApiCall.POSTHEADER(client, URLS.URL_MYEVENTS+"?"+"uid="+ args[0]+"&cid=250905&offset="+"0"+"&limit=100",body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        ReloadProgress.setVisibility(View.VISIBLE);

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            myeventlistdisplay = null;
            try {
                if (responce != null) {

                    ErrorText.setVisibility(View.GONE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                MyEventsArray EventInfo = new MyEventsArray();

                                eventid = jsonArrayData.getJSONObject(i).getString("nid");
                                eventtittle = jsonArrayData.getJSONObject(i).getString("title");

                                JSONObject jsonObject1 = jsonArrayData.optJSONObject(i);
                                JSONArray logo = jsonObject1.getJSONArray("field_groups_logo");

                                if(logo != null && logo.length() > 0 ) {
                                    eventlogo =logo.getString(0);
                                    EventInfo.field_groups_logo =eventlogo;
                                    Log.e("logo", logo.getString(0));


                                }


                                JSONArray eventdates = jsonArrayData.getJSONObject(i).getJSONArray("field_time");
                                if (eventdates != null && eventdates.length() > 0) {

                                    for (int j = 0; j < eventdates.length(); j++) {
                                        eventstartdate = eventdates.getJSONObject(j).getString("startDate");
                                          endate = eventdates.getJSONObject(j).getString("endDate");
                                        eventstarttime = eventdates.getJSONObject(j).getString("startTime");
                                         endtime = eventdates.getJSONObject(j).getString("endTime");
                                           week = eventdates.getJSONObject(j).getString("Week");

                                        EventInfo.nid = eventid;
                                        EventInfo.title = eventtittle;
                                        EventInfo.field_dept_location = eventlocation;
                                        EventInfo.startDate = eventstartdate;
                                        EventInfo.endDate = endate;
                                        EventInfo.startTime = eventstarttime;
                                        EventInfo.endTime = endtime;

                                        EventInfo.Week = week;

                                        mEvent.add(EventInfo);

                                        eventadapter = new MyEventsAdapters(activity, mEvent);
                                        rv_myEvents.setAdapter(eventadapter);


                                        Displaytext.setVisibility(View.INVISIBLE);



                                    }




                                    mShimmerViewContainer.stopShimmerAnimation();
                                    mShimmerViewContainer.setVisibility(View.GONE);


                                }



                                }




                        }else{
                           Displaytext.setVisibility(View.VISIBLE);
                            rv_myEvents.setVisibility(View.GONE);
                            text_view.setVisibility(View.VISIBLE);


                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);

                            text_view2.setVisibility(View.VISIBLE);

                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {

                        ErrorText.setVisibility(View.VISIBLE);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);



                    }
                }
            } catch (JSONException e) {


                mShimmerViewContainer.stopShimmerAnimation();
                mShimmerViewContainer.setVisibility(View.GONE);
                ReloadProgress.setVisibility(View.VISIBLE);


            }
        }

        @Override
        protected void onCancelled() {
            myeventlistdisplay = null;
            Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


        }
    }




    @Override
    public void onBackPressed() {
        finish();
    }
}
