package co.savm.studentprofile;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.MyCourserAdapter;
import co.savm.college.CoursesModules;
import co.savm.models.MyCourseItem;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class MyCourses extends BaseAppCompactActivity {
    public ArrayList<MyCourseItem> mcourse;
    RelativeLayout relative;
    private  boolean isLoading= false;
    private  int PAGE_SIZE = 0;
    private int currentPage = 1;
    SwipeRefreshLayout swipeContainer;
    FloatingActionMenu materialDesignFAM;
    com.github.clans.fab.FloatingActionButton floatingActionButton1;
    TextView text_view,text_view2;
    private static ShimmerFrameLayout mShimmerViewContainer;

    private RecyclerView.LayoutManager layoutManager;
    private MyCourserAdapter courseadapter;
    private MyCoursesListDisplay mycourseslistdisplay = null;

    RecyclerView mRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.savm.R.layout.activity_my_courses);
        mcourse=new ArrayList<>();
        swipeContainer =findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        relative = findViewById(R.id.relative);
        text_view = findViewById(R.id.text_view);
        text_view2 = findViewById(R.id.text_view2);
        materialDesignFAM = findViewById(R.id.material_design_android_floating_action_menu);

        floatingActionButton1 = findViewById(R.id.material_design_floating_action_menu_item1);

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        mRecyclerView = findViewById(co.savm.R.id.rv_myGroups);
        mRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);


        courseadapter = new MyCourserAdapter(MyCourses.this, mcourse);
        mRecyclerView.setAdapter(courseadapter);


        mycourseslistdisplay = new MyCoursesListDisplay();
        mycourseslistdisplay.execute();

        relative.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.d("TAG", "onTouch  " + "");
                if (materialDesignFAM.isOpened()) {
                    materialDesignFAM.close(true);
                    return true;
                }
                return false;
            }
        });


        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MyCourses.this, CoursesModules.class);
                startActivity(intent);


            }
        });



        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        if (!isLoading){
                            CalledFromAddCourse();

                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }

                    }
                }, 3000);
            }
        });

      /*  ArrayList<MyGroupItem> mgroups = null;*/

        inItView();


    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }



    public  void CalledFromAddCourse() {
        PAGE_SIZE=0;
        isLoading=true;
        mShimmerViewContainer.startShimmerAnimation();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(true);
        mycourseslistdisplay = new MyCoursesListDisplay();
        mycourseslistdisplay.execute();



    }
    private void inItView() {


        swipeContainer.setRefreshing(false);

        mRecyclerView.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){

                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    courseadapter.addProgress();

                    mycourseslistdisplay = new MyCoursesListDisplay();
                    mycourseslistdisplay.execute();
                }else{
                    Log.i("loadinghua", "im else now");

                }

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

    }


    private class MyCoursesListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYCOURSES+"?"+"uid"+"="+SessionManager.getInstance(getActivity()).getUser().userprofile_id+"&"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                MyCourses.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.savm.R.string.error_something_wrong));
                    }
                });

                courseadapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            mycourseslistdisplay = null;
            try {
                if (responce != null) {

                    mRecyclerView.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<MyCourseItem> arrayList = new ArrayList<>();

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                MyCourseItem CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), MyCourseItem.class);
                                arrayList.add(CourseInfo);


                            }



                        }


                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                mRecyclerView.setVisibility(View.VISIBLE);
                                courseadapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                courseadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                courseadapter.addData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }

                            isLoading = false;

                        }else{
                            if (PAGE_SIZE==0){

                                text_view2.setVisibility(View.VISIBLE);
                                text_view.setVisibility(View.VISIBLE);
                                materialDesignFAM.setVisibility(View.VISIBLE);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }else
                                courseadapter.removeProgress();

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);




                    }
                }
            } catch (JSONException e) {
                isLoading=false;
                swipeContainer.setRefreshing(false);


            }
        }

        @Override
        protected void onCancelled() {
            mycourseslistdisplay = null;


        }
    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }
}
