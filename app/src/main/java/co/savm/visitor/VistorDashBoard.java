package co.savm.visitor;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import co.savm.R;
import co.savm.activities.EmergencyContacts;
import co.savm.activities.NewsAnonocment;
import co.savm.studentprofile.Adapterdashboard;
import co.savm.utils.BaseFragment;
import co.savm.utils.SessionManager;
import co.savm.weathersection.Function;

/**
 * Created by HP on 5/2/2018.
 */

public class VistorDashBoard extends BaseFragment{

    public static int[] CollageInformationImages = {
            R.mipmap.aboutusmain,
            //  R.mipmap.courses_programmes,
            R.mipmap.campus_map,
            R.mipmap.important_links,
            R.mipmap.campus_services,/*no new pic*/
            // R.mipmap.facilities,
            R.mipmap.academic_division,/*no new pic*/
            R.mipmap.accomodation,};




    public static int[] moreResourcesImages = {
            R.mipmap.deals_discounts,
    };

    RecyclerView mRvCollegeInformation,mmoreResource;
    RelativeLayout layoutrelay;
    ImageView mainheader,imageView3,wetherIcon;
    ImageButton news,emergencyicon;
    TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, weatherIcon, updatedField;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_visitor_new, container, false);

    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initLayout(view);

    }


    private void initLayout(View view) {
        mRvCollegeInformation = view.findViewById(R.id.rv_college_information);

        mmoreResource = view.findViewById(R.id.rv_more_resource);
        mainheader =view. findViewById(R.id.mainheader);
        layoutrelay = view.findViewById(R.id.layoutrelay);
        imageView3  = view.findViewById(R.id.imageView3);
        news =view. findViewById(R.id.news);
        emergencyicon = view.findViewById(R.id.emergencyicon);
        cityField = view.findViewById(R.id.city_field);
        updatedField = view.findViewById(R.id.updated_field);
        currentTemperatureField = view.findViewById(R.id.current_temperature_field);
        humidity_field =view. findViewById(R.id.humidity_field);
        pressure_field = view.findViewById(R.id.pressure_field);
        wetherIcon = view.findViewById(R.id.wetherIcon);

        Function.placeIdTask asyncTask = new Function.placeIdTask(new Function.AsyncResponse() {
            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity, String weather_pressure, String weather_updatedOn, String weather_iconText, String sun_rise, String MainIcon) {
                String iconurl ="http://openweathermap.org/img/w/"+MainIcon+ ".png";
                cityField.setText(weather_city);
                updatedField.setText(weather_updatedOn);
                String numWihoutDecimal = String.valueOf(weather_temperature).split("\\.")[0];
                currentTemperatureField.setText(numWihoutDecimal +"°" +" "+ "C");
                humidity_field.setText("Humidity: " + weather_humidity);
                pressure_field.setText("Pressure: " + weather_pressure);
                try {

                    Glide.with(getActivity()).load(iconurl)
                            .placeholder(R.mipmap.clouds).dontAnimate()
                            .fitCenter().into(wetherIcon);
                }catch (Exception e){

                }


            }
        });

        news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent (getActivity(), NewsAnonocment.class));

            }
        });

        emergencyicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity (new Intent (getActivity(), EmergencyContacts.class));

            }
        });


        if (SessionManager.getInstance(getActivity()).getCollage().getField_group_image() !=null) {
            Log.e("TAG", "getField_group_image: " +SessionManager.getInstance(getActivity()).getCollage().getField_group_image());
            Glide.with(getActivity()).load(SessionManager.getInstance(getActivity()).getCollage().getField_group_image())
                    .placeholder(R.mipmap.header_image).dontAnimate()
                    .fitCenter().into(mainheader);



        }


        if (SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo() !=null) {
            Log.e("TAG", "logo2: " +SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo());
            Glide.with(getActivity()).load(SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo())
                    .placeholder(R.mipmap.appicon).dontAnimate()
                    .fitCenter().into(imageView3);



        }
        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null) {
            String Lat = String.valueOf(Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat()));
            String   Long = String.valueOf(Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng()));

            asyncTask.execute(Lat, Long); //  asyncTask.execute("Latitude", "Longitude")
            Log.e("TAG", "LatLong " +Lat + Long);


        }
        else {
            asyncTask.execute("28.7041", "77.1025");


        }
        mRvCollegeInformation.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        mRvCollegeInformation.setLayoutManager(layoutManager);

        mRvCollegeInformation.setAdapter(new Adapterdashboard(getActivity(),"collegeInfo" ,CollageInformationImages));


        mmoreResource.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager3 = new GridLayoutManager(getActivity(),2);
        mmoreResource.setLayoutManager(layoutManager3);
        mmoreResource.setAdapter(new Adapterdashboard(getActivity(),"mResource",  moreResourcesImages));



    }

}
