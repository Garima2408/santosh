package co.savm.visitor;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.iid.FirebaseInstanceId;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.savm.R;
import co.savm.activities.ChangeCollage;
import co.savm.activities.ForceUpdateChecker;
import co.savm.activities.ProfileEdit;
import co.savm.activities.QuestinSite;
import co.savm.activities.SignIn;
import co.savm.activities.StartupGuide;
import co.savm.activities.SwitchYourType;
import co.savm.adapters.NavigationItemsAdapter;
import co.savm.chat.FriendsListService;
import co.savm.models.NavItems;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.settings.AboutUs;
import co.savm.settings.EmailUs;
import co.savm.settings.FAQ;
import co.savm.settings.PrivacyPolicy;
import co.savm.settings.Settings;
import co.savm.settings.TermsAndConditions;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.TextUtils;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class VisitorMain extends BaseAppCompactActivity implements  NavigationView.OnNavigationItemSelectedListener,ForceUpdateChecker.OnUpdateNeededListener {
    Toolbar toolbar;
    TextView mToolbarTitle,toolbar_title;
    ExpandableListView expandableListView;
    DrawerLayout mDrawerlayout;
    TextView mEmail;
    private NavigationItemsAdapter mNavigationItemsAdapter;
    private List<NavItems> listNavigationItem;
    private HashMap<String, List<NavItems>> listDataChild;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private Fragment mFragment = null;
    private Class mFragmentClass = null;
    private ProgressLogout logouttAuthTask = null;

    TextView tv_username, tv_useremail,tv_userbatch;
    ImageView profileImage;
    ImageButton imgbtn_edit_profile,imgbtn_account;
   // GridView mRvCollegeInformation, mmoreResource;
   // TextView cityField, detailsField, currentTemperatureField, humidity_field, pressure_field, weatherIcon, updatedField;
    private DeleteFireBaseToken deleteFireBaseToken = null;
    private UpdateFirbaseID updateFirbaseID = null;
    TextView badge_notification;
//    RelativeLayout layoutrelay;
    //  ImageView mainheader,imageView3;
    //ImageButton news,emergencyicon;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_main);
        Utils.setBadge(this, 0);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar_title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);

        if(SessionManager.getInstance(getActivity()).getCollage().getTitle() !=null){

            toolbar_title.setText(SessionManager.getInstance(getApplicationContext()).getCollage().getTitle());
            toolbar_title.setSelected(true);
            getSupportActionBar().setTitle(null);



        }
        ForceUpdateChecker.with(this).onUpdateNeeded( this).check();

        pushFragment(new VistorDashBoard());

        mDrawerlayout = findViewById(R.id.drawer_layout);
        mActionBarDrawerToggle= new ActionBarDrawerToggle(
                this, mDrawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        mDrawerlayout.setDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();

        expandableListView = findViewById(R.id.nav_expand_listview);

        prepareNavigationList ();
        initLayout();

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.navigation_header, null, false);
        expandableListView.addHeaderView(listHeaderView);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        profileImage = findViewById(R.id.imageProfile);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        tv_userbatch.setVisibility(View.INVISIBLE);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        imgbtn_account=findViewById(R.id.imgbtn_account);
        imgbtn_account.setVisibility(View.GONE);



        displayUserData();
        getFCMToken();


        imgbtn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(VisitorMain.this,SwitchYourType.class);
                startActivity(n);
            }
        });






        imgbtn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n = new Intent(VisitorMain.this,ProfileEdit.class);
                startActivity(n);
            }
        });





        mNavigationItemsAdapter = new NavigationItemsAdapter (this,
                listNavigationItem,listDataChild);

        expandableListView.setAdapter (mNavigationItemsAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                int len = mNavigationItemsAdapter.getGroupCount();
                for (int i = 0; i < len; i++) {
                    if (i != groupPosition) {
                        expandableListView.collapseGroup(i);
                    }
                }


            }
        });
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener () {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long rowId) {
                selectFragments(groupPosition,10);
                return false;
            }
        });

        expandableListView.setOnChildClickListener (new ExpandableListView.OnChildClickListener () {
            @Override public boolean onChildClick (ExpandableListView parent, View v, int groupPosition,
                                                   int childPosition, long id) {
                selectFragments(groupPosition,childPosition);
                v.setSelected (true);
                return false;
            }
        });




    }

    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void getFCMToken() {
        UpdateFirbaseID updateFirbaseID=new UpdateFirbaseID();
        updateFirbaseID.execute();
    }

    private void initLayout() {
        Utils.init(VisitorMain
                .this);

        startService(new Intent(this, FriendsListService.class));



    }





    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);
            }
            if (!TextUtils.isNullOrEmpty(getUser().email)) {
                tv_useremail.setText(getUser().email);
            }
            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .fitCenter().into(profileImage);


            }
//            if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
//                Glide.with(this).load(getUser().Userwallpic)
//                        .placeholder(R.mipmap.header_image).dontAnimate()
//                        .skipMemoryCache(true)
//                        .diskCacheStrategy(DiskCacheStrategy.NONE)
//                        .fitCenter().into(mainheader);
//
//
//            }
        }
    }
    protected void pushFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.parent_content, fragment);
            ft.commit();
        }
    }

    private void prepareNavigationList () {

        listNavigationItem = new ArrayList<NavItems>();
        listDataChild = new HashMap<String, List<NavItems>> ();

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.community),
                getResources ().getString (R.string.questin)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.colleges),
                getResources ().getString (R.string.colleges)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.learning),
                getResources ().getString (R.string.learning)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.need_help),
                getResources ().getString (R.string.need_help)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.read_more)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.setting),
                getResources ().getString (R.string.setting)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.log_out),
                getResources ().getString (R.string.log_out)));

        List<NavItems> needHelp = new ArrayList<> ();
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.email_us)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq)));

        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq1)));

        List<NavItems> readMore = new ArrayList<> ();
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.about_us)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.term_condition)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.privacy_policy)));

        List<NavItems> empty = new ArrayList<NavItems> ();

        listDataChild.put (listNavigationItem.get (0).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (1).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (2).getNavItemName (), empty);

        listDataChild.put (listNavigationItem.get (3).getNavItemName (), needHelp);
        listDataChild.put (listNavigationItem.get (4).getNavItemName (), readMore);
        listDataChild.put (listNavigationItem.get (5).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (6).getNavItemName (), empty);
    }

    private void selectFragments(int groupPosition,int childPosition){


        switch (groupPosition){
            case 0:
             //  Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(VisitorMain.this, QuestinSite.class);
                startActivity(i);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 1:
               // Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                Intent j = new Intent(VisitorMain.this, ChangeCollage.class);
                startActivity(j);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 2:
               // Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                Intent k = new Intent(VisitorMain.this, QuestinSite.class);
                startActivity(k);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;


            case 3:

                switch (childPosition){
                    case 0:


                        Intent backIntent = new Intent(VisitorMain.this, EmailUs.class)
                                .putExtra("TITLE"," Email Us");
                        startActivity(backIntent);

                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        startActivity (new Intent (VisitorMain.this, FAQ.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, StartupGuide.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 4:
                switch (childPosition){
                    case 0:

                        startActivity (new Intent (VisitorMain.this, AboutUs.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        Intent o = new Intent(VisitorMain.this, TermsAndConditions.class);
                        startActivity(o);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (VisitorMain.this, PrivacyPolicy.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 5:

                startActivity (new Intent (VisitorMain.this, Settings.class));
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

            case 6:
                showNoticeDialog ();
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

        }


//        try {
//            mFragment = (Fragment) mFragmentClass.newInstance();
//            FragmentManager fragmentManager = getSupportFragmentManager ();
//            fragmentManager.beginTransaction ()
//                    .replace (R.id.parent_content,mFragment)
//                    .commit ();
//        } catch (Exception e) {
//            e.printStackTrace ();
//        }


    }

    private void showNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(VisitorMain.this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Logout ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application

//                logouttAuthTask = new ProgressLogout();
//                logouttAuthTask.execute();

                 deleteFireBaseToken=new DeleteFireBaseToken();
                 deleteFireBaseToken.execute();


            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }



    public int getPixelFromDips (float pixels) {
        // Get the screen's density scale
        final float scale = getResources ().getDisplayMetrics ().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }


    private class DeleteFireBaseToken extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }
        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {


                String responseData = ApiCall.DELETE_HEADER(client, URLS.REG_TOKEN+"/"+ Utils.getFCMId());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                VisitorMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            deleteFireBaseToken = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        logouttAuthTask = new ProgressLogout();
                        logouttAuthTask.execute();


                        Utils.clearData();

                         try {
                             //user chat delete
                             Utils.deleteUserChatAndTable(VisitorMain.this);
                         }catch (Exception e){
                             e.printStackTrace();
                         }


//                        //group chat delete
//                        Utils.deleteGroupChatAndTable(MainActivity.this);


                        //use for clear snappy db
                        DB snappyDB = null;
                        try {
                            snappyDB = DBFactory.open(VisitorMain.this);
                            if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {

                                snappyDB.destroy();

                            }
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            deleteFireBaseToken = null;
            hideLoading();


        }
    }
    private class UpdateFirbaseID extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type", "android")
                    .add("token", FirebaseInstanceId.getInstance().getToken())
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.REG_TOKEN,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataprint: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                VisitorMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Utils.showAlertDialog(VisitorMain.this,"Error Info",getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            updateFirbaseID = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.init(VisitorMain.this);
                        Utils.saveFCMId(VisitorMain.this,FirebaseInstanceId.getInstance().getToken());
                        Log.e("token",Utils.getFCMId());

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(VisitorMain.this,"Error Info",msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            updateFirbaseID = null;
            hideLoading();

        }
    }







    @Override
    protected void onResume() {
        super.onResume();

    }
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to close this application ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finishAffinity();

            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }


    private class ProgressLogout extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LOGOUT,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataprint: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                VisitorMain.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Utils.showAlertDialog(VisitorMain.this,"Error Info",getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            logouttAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.clearData();

                        try{
                            //user chat delete
                            Utils.deleteUserChatAndTable(VisitorMain.this);
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                        //use for clear snappy db
                        DB snappyDB = null;
                        try {
                            snappyDB = DBFactory.open(VisitorMain.this);
                            if (snappyDB.exists(Constants.CART_FRIENDS_LIST)) {

                                snappyDB.destroy();

                            }
                        } catch (SnappydbException e) {
                            e.printStackTrace();
                        }

                        SessionManager.getInstance(getApplicationContext()).deleteUser();
                        Utils.removeStringPreferences(VisitorMain.this,"0");
                        Utils.getSharedPreference(VisitorMain.this).edit()
                                .putInt(Constants.USER_ROLE, Constants.ROLE_NULL).apply();
                        Intent upanel = new Intent(VisitorMain.this,SignIn.class);
                        upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(upanel);
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(VisitorMain.this,"Error Info",msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            logouttAuthTask = null;
            hideLoading();

        }
    }


}
