package co.savm.driver;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.RoutelistAdapter;
import co.savm.models.tracker.RoutelistArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class DriverDashFragment extends Fragment {
    public ArrayList<RoutelistArray> mrouteList;
    private RecyclerView.LayoutManager layoutManager;
    private RoutelistAdapter routeadapter;
    RecyclerView rv_Route_list;
    SwipeRefreshLayout swipeContainer;
     Activity activity;
  //  private  ProgressBar spinner;
    private CollageRoutesDisplay collageroutedisplay = null;



    public DriverDashFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_driver_dash, container, false);

        activity = (Activity)view.getContext();

        mrouteList=new ArrayList<>();
        routeadapter=new RoutelistAdapter(activity,mrouteList);
        layoutManager = new LinearLayoutManager(activity);
        swipeContainer =view.findViewById(R.id.swipeContainer);


        rv_Route_list = view.findViewById(co.savm.R.id.rv_Route_list);
        rv_Route_list.setHasFixedSize(true);
        rv_Route_list.setLayoutManager(layoutManager);
        rv_Route_list.setAdapter(routeadapter);

        collageroutedisplay = new CollageRoutesDisplay();
        collageroutedisplay.execute();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        swipeContainer.setRefreshing(false);
                        mrouteList.clear();
                        collageroutedisplay = new CollageRoutesDisplay();
                        collageroutedisplay.execute();

                    }
                }, 3000);
            }
        });




        return view;
    }


    private class CollageRoutesDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.ROUTES_LIST+"?"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
               activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server.Reload the page.");
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            collageroutedisplay = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        for (int i = 0; i < jsonArrayData.length(); i++) {



                            RoutelistArray RouteInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), RoutelistArray.class);
                            mrouteList.add(RouteInfo);
                            routeadapter = new RoutelistAdapter(activity, mrouteList);
                            rv_Route_list.setAdapter(routeadapter);


                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        showAlertFragmentDialog(activity,"Information", msg);



                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);

            }
        }

        @Override
        protected void onCancelled() {
            collageroutedisplay = null;
            swipeContainer.setRefreshing(false);

        }
    }

    public static void showAlertFragmentDialog(Context context, String title, String message) {
        AlertDialog.Builder builder  = new AlertDialog.Builder(context);

        builder.setTitle(title);

        builder.setMessage(message).setCancelable(false);

        builder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

}
