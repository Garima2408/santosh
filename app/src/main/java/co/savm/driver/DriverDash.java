package co.savm.driver;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.savm.R;
import co.savm.activities.ForceUpdateChecker;
import co.savm.activities.QuestinSite;
import co.savm.activities.SignIn;
import co.savm.activities.StartupGuide;
import co.savm.adapters.NavigationItemsAdapter;
import co.savm.database.QuestinSQLiteHelper;
import co.savm.models.NavItems;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.settings.AboutUs;
import co.savm.settings.EmailUs;
import co.savm.settings.FAQ;
import co.savm.settings.PrivacyPolicy;
import co.savm.settings.Settings;
import co.savm.settings.TermsAndConditions;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.TextUtils;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class DriverDash extends BaseAppCompactActivity implements  NavigationView.OnNavigationItemSelectedListener ,ForceUpdateChecker.OnUpdateNeededListener {
    Toolbar toolbar;
    TextView mToolbarTitle;
    ExpandableListView expandableListView;
    DrawerLayout mDrawerlayout;
    TextView mEmail;
    private NavigationItemsAdapter mNavigationItemsAdapter;
    private List<NavItems> listNavigationItem;
    private HashMap<String, List<NavItems>> listDataChild;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private Fragment mFragment = null;
    private Class mFragmentClass = null;
    private ProgressLogout logouttAuthTask = null;
    BottomNavigationView mBottomNavigationView;
    TextView tv_username, tv_useremail,tv_userbatch;

    ImageButton imgbtn_edit_profile,imgbtn_account;
    ImageView mainheader,profileImage;
    QuestinSQLiteHelper questinSQLiteHelper ;
    Dialog imageDialog;
    private SharedPreferences preferences;
    private int Runfirst;
    private DeleteFireBaseToken deleteFireBaseToken = null;
    private UpdateFirbaseID updateFirbaseID = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_dash);
        preferences = Utils.getSharedPreference(DriverDash.this);
        Runfirst = preferences.getInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE);
        FirstOneThis();
        //ForceUpdateChecker.with(this).onUpdateNeeded( this).check();

        // pushFragment(new DashboardFragment());
        pushFragment(new DriverDashFragment());
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(SessionManager.getInstance(getActivity()).getCollage().getTitle().toString() !=null){
            getSupportActionBar().setTitle(SessionManager.getInstance(getApplicationContext()).getCollage().getTitle().toString());


        }



        mDrawerlayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerlayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);



        mDrawerlayout.setDrawerListener(toggle);
        toggle.syncState();

        expandableListView = findViewById(R.id.nav_expand_listview);

        prepareNavigationList ();
      //  Setuponbottombar();
        // setupNavigationView();

        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.navigation_header, null, false);
        expandableListView.addHeaderView(listHeaderView);
        tv_username = findViewById(R.id.tv_username);
        tv_useremail = findViewById(R.id.tv_useremail);
        tv_userbatch = findViewById(R.id.tv_userbatch);
        profileImage = findViewById(R.id.imageProfile);
        imgbtn_account=findViewById(R.id.imgbtn_account);
        imgbtn_account.setVisibility(View.VISIBLE);
        mainheader = findViewById(R.id.mainheader);
        imgbtn_edit_profile = findViewById(R.id.imgbtn_edit_profile);
        displayUserData();
        getFCMToken();


        imgbtn_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Your are unauthorized", Toast.LENGTH_SHORT).show();

              /*  Intent n = new Intent(DriverDash.this,ProfileEdit.class);
                startActivity(n);*/
            }
        });

        imgbtn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), "Your are unauthorized", Toast.LENGTH_SHORT).show();

                /*Intent n = new Intent(DriverDash.this,SwitchYourType.class);
                startActivity(n);*/
            }
        });




        mNavigationItemsAdapter = new NavigationItemsAdapter (this,
                listNavigationItem,listDataChild);

        expandableListView.setAdapter (mNavigationItemsAdapter);

        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {

                int len = mNavigationItemsAdapter.getGroupCount();
                for (int i = 0; i < len; i++) {
                    if (i != groupPosition) {
                        expandableListView.collapseGroup(i);
                    }
                }


            }
        });
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener () {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View clickedView, int groupPosition, long rowId) {
                selectFragments(groupPosition,10);
                return false;
            }
        });

        expandableListView.setOnChildClickListener (new ExpandableListView.OnChildClickListener () {
            @Override public boolean onChildClick (ExpandableListView parent, View v, int groupPosition,
                                                   int childPosition, long id) {
                selectFragments(groupPosition,childPosition);
                v.setSelected (true);
                return false;
            }
        });

        Utils.init(this);
       /* if (Utils.getFCMId()==null){

            updateFirbaseID=new UpdateFirbaseID();
            updateFirbaseID.execute();
        }*/


       // startService(new Intent(this, FriendsListService.class));

    }


    public void onUpdateNeeded(final String updateUrl) {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("New version available")
                .setMessage("Please, update app to new version to continue reposting.")
                .setPositiveButton("Update",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                redirectStore(updateUrl);
                            }
                        }).setNegativeButton("No, thanks",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        }).create();
        dialog.show();
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void getFCMToken() {
        updateFirbaseID=new UpdateFirbaseID();
        updateFirbaseID.execute();

    }


    private void FirstOneThis() {

        switch (Runfirst){
            case Constants.ROLE_RUNNING_TRUE :
                imageDialog = new Dialog(DriverDash.this);
                imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                imageDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                imageDialog.setContentView(R.layout.image_overlay_screen);
                // dialogLogin.setCancelable(false);
                imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT);
                imageDialog.show();
                ImageView showimage = imageDialog.findViewById(R.id.DisplayOnbondingImage);
                showimage.setImageResource(R.mipmap.onboarding_dashboard);

                showimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        imageDialog.dismiss();

                        Utils.getSharedPreference(DriverDash.this).edit()
                                .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE).apply();
                        Utils.removeStringPreferences(DriverDash.this,"0");

                    }
                });



                break;
            case Constants.ROLE_RUNNING_FALSE :
                break;


        }





    }


    private class DeleteFireBaseToken extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }
        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {


                String responseData = ApiCall.DELETE_HEADER(client, URLS.REG_TOKEN+"/"+ Utils.getFCMId());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                DriverDash.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            deleteFireBaseToken = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.clearData();



                            logouttAuthTask = new ProgressLogout();
                            logouttAuthTask.execute();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            deleteFireBaseToken = null;
            hideLoading();


        }
    }
    private class UpdateFirbaseID extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type", "android")
                    .add("token", FirebaseInstanceId.getInstance().getToken())
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.REG_TOKEN,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataprint: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                DriverDash.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Utils.showAlertDialog(DriverDash.this,"Error Info",getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            updateFirbaseID = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.init(DriverDash.this);
                        Utils.saveFCMId(DriverDash.this,FirebaseInstanceId.getInstance().getToken());
                        Log.e("token",Utils.getFCMId());

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(DriverDash.this,"Error Info",msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            updateFirbaseID = null;
            hideLoading();

        }
    }





    private class ProgressLogout extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LOGOUT,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataprint: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                DriverDash.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        Utils.showAlertDialog(DriverDash.this,"Error Info",getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            logouttAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        //Utils.showAlertDialog(MainActivity.this,"Logout",msg);

                        SessionManager.getInstance(getApplicationContext()).deleteUser();
                        Utils.getSharedPreference(DriverDash.this).edit()
                                .putInt(Constants.USER_ROLE, Constants.ROLE_NULL).apply();
                        Utils.removeStringPreferences(DriverDash.this,"0");
                        Intent upanel = new Intent(DriverDash.this,SignIn.class);
                        upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(upanel);
                        finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(DriverDash.this,"Error Info",msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();
            }
        }

        @Override
        protected void onCancelled() {
            logouttAuthTask = null;
            hideLoading();

        }
    }


    private void displayUserData() {
        if (getUser()!=null){
            if (!TextUtils.isNullOrEmpty(getUser().username)) {
                tv_username.setText(getUser().username);
            }
            if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole())) {
                tv_useremail.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageRole()+" "+SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBatch());
            }
            if (!TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBranch())) {
                tv_userbatch.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBranch());
            }

            if (!TextUtils.isNullOrEmpty(getUser().photo)) {
                Glide.with(this).load(getUser().photo)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(profileImage);

            }

            if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
                Glide.with(this).load(getUser().Userwallpic)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(mainheader);

            }
        }
    }

    protected void pushFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.parent_content, fragment);
            ft.commit();
        }
    }
   /* private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();


            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();


            }
        });


    }

    protected void pushFragment(Fragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.parent_content, fragment);
            ft.commit();
        }
    }
*/





    private void prepareNavigationList () {

        listNavigationItem = new ArrayList<NavItems>();
        listDataChild = new HashMap<String, List<NavItems>> ();

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.community),
                getResources ().getString (R.string.questin)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.colleges),
                getResources ().getString (R.string.colleges)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.learning),
                getResources ().getString (R.string.learning)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.need_help),
                getResources ().getString (R.string.need_help)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.read_more)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.setting),
                getResources ().getString (R.string.setting)));

        listNavigationItem.add (new NavItems (getResources ().getDrawable (R.mipmap.log_out),
                getResources ().getString (R.string.log_out)));

        List<NavItems> needHelp = new ArrayList<> ();
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.email_us)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq)));
        needHelp.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.faq1)));
        List<NavItems> readMore = new ArrayList<> ();
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.read_more),
                getResources ().getString (R.string.about_us)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.term_condition)));
        readMore.add (new NavItems (getResources ().getDrawable (R.mipmap.profile),
                getResources ().getString (R.string.privacy_policy)));

        List<NavItems> empty = new ArrayList<NavItems> ();

        listDataChild.put (listNavigationItem.get (0).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (1).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (2).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (3).getNavItemName (), needHelp);
        listDataChild.put (listNavigationItem.get (4).getNavItemName (), readMore);
        listDataChild.put (listNavigationItem.get (5).getNavItemName (), empty);
        listDataChild.put (listNavigationItem.get (6).getNavItemName (), empty);
    }

    private void selectFragments(int groupPosition,int childPosition){


        switch (groupPosition){
            case 0:
                 Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

               /* Intent i = new Intent(DriverDash.this, QuestinSite.class);
                startActivity(i);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);*/
                break;
            case 1:
                  Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

               /* Intent j = new Intent(DriverDash.this, ChangeCollage.class);
                startActivity(j);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);*/

                break;
            case 2:
                //   Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                Intent k = new Intent(DriverDash.this, QuestinSite.class);
                startActivity(k);
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;
            case 3:

                switch (childPosition){
                    case 0:

                        Intent backIntent = new Intent(DriverDash.this, EmailUs.class)
                                .putExtra("TITLE"," Email Us");
                        startActivity(backIntent);

                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        startActivity (new Intent (this, FAQ.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, StartupGuide.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 4:
                switch (childPosition){
                    case 0:

                        startActivity (new Intent (this, AboutUs.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                    case 1:

                        Intent o = new Intent(DriverDash.this, TermsAndConditions.class);
                        startActivity(o);
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;

                    case 2:

                        startActivity (new Intent (this, PrivacyPolicy.class));
                        mDrawerlayout.closeDrawer (Gravity.LEFT);
                        mDrawerlayout.setStatusBarBackground(R.color.white);

                        break;
                }
                break;

            case 5:

                startActivity (new Intent (this, Settings.class));
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

            case 6:
                showNoticeDialog ();
                mDrawerlayout.closeDrawer (Gravity.LEFT);
                mDrawerlayout.setStatusBarBackground(R.color.white);

                break;

        }


        try {
            mFragment = (Fragment) mFragmentClass.newInstance();
            FragmentManager fragmentManager = getSupportFragmentManager ();
            fragmentManager.beginTransaction ()
                    .replace (R.id.parent_content,mFragment)
                    .commit ();
        } catch (Exception e) {
            e.printStackTrace ();
        }

    }

    private void showNoticeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DriverDash.this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
//                logouttAuthTask = new ProgressLogout();
//                logouttAuthTask.execute();


                deleteFireBaseToken=new DeleteFireBaseToken();
                deleteFireBaseToken.execute();



            }
        });
        builder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();
    }


    public int getPixelFromDips (float pixels) {
        // Get the screen's density scale
        final float scale = getResources ().getDisplayMetrics ().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }









    @Override
    protected void onResume() {
        super.onResume();

        if (!TextUtils.isNullOrEmpty(getUser().Userwallpic)) {
            Glide.with(this).load(getUser().Userwallpic)
                    .placeholder(R.mipmap.header_image).dontAnimate()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .fitCenter().into(mainheader);




        }
        if (!TextUtils.isNullOrEmpty(getUser().photo)) {
            Glide.with(this).load(getUser().photo)
                    .placeholder(R.mipmap.header_image).dontAnimate()
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .fitCenter().into(profileImage);

        }

    }








    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Utils.getSharedPreference(DriverDash.this).edit()
                        .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_FALSE).apply();
                Utils.removeStringPreferences(DriverDash.this,"0");
                finishAffinity();



            }
        });
        builder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }







}




