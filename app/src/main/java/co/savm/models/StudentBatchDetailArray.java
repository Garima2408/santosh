package co.savm.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StudentBatchDetailArray implements Serializable {
    public String batch;
    public String id;
    public String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List<StudentClassDetailArray> StudentInfo = new ArrayList<StudentClassDetailArray>();



    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<StudentClassDetailArray> getStudentInfo() {
        return StudentInfo;
    }

    public void setStudentInfo(List<StudentClassDetailArray> studentInfo) {
        StudentInfo = studentInfo;
    }



}
