package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 29-01-2018.
 */

public class AnouncementArray implements Serializable {
    public String tnid;
    public String description;
    public String date;
    public String title;
    public String file;
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;
    public String uid;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public AnouncementArray(int type) {
        this.type = type;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
