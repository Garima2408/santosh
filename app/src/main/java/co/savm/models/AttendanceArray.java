package co.savm.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 25-08-2017.
 */

public class AttendanceArray implements Serializable {
    public String parentTitle;
    public String numbers;

    public boolean isSelected = false;
    public List<TaskChildListArray> childsTaskList = new ArrayList<TaskChildListArray>();
}