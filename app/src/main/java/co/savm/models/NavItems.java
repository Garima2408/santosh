package co.savm.models;

import android.graphics.drawable.Drawable;


public class NavItems {
  private Drawable icon;
  private String navItemName;

  public Drawable getIcon () {
    return icon;
  }

  public NavItems(Drawable icon, String navItemName) {
    this.icon = icon;
    this.navItemName = navItemName;
  }

  public void setIcon (Drawable icon) {
    this.icon = icon;
  }

  public String getNavItemName () {
    return navItemName;
  }

  public void setNavItemName (String navItemName) {
    this.navItemName = navItemName;
  }
}
