package co.savm.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StudentClassDetailArray implements Serializable {

    public String uid;
    public String name;
    public String member_class;
    public String BatchId;
    public boolean IsSelected;

    public List<StudentBatchDetailArray> BatchInfo = new ArrayList<StudentBatchDetailArray>();

    public List<StudentBatchDetailArray> getBatchInfo() {
        return BatchInfo;
    }

    public void setBatchInfo(List<StudentBatchDetailArray> batchInfo) {
        BatchInfo = batchInfo;
    }

    public boolean isSelected() {
        return IsSelected;
    }

    public void setSelected(boolean selected) {
        IsSelected = selected;
    }

    public String getBatchId() {
        return BatchId;
    }

    public void setBatchId(String batchId) {
        BatchId = batchId;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMember_class() {
        return member_class;
    }

    public void setMember_class(String member_class) {
        this.member_class = member_class;
    }
}
