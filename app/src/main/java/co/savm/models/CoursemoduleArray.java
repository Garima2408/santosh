package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 17-08-2017.
 */

public class CoursemoduleArray implements Serializable {
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;
    public String tnid;
    public String teacher;
    public String code;
    public String title;
    public String logo;
    public String is_faculty;
    public String is_student;
    public String is_member;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public CoursemoduleArray(int type) {
        this.type = type;
    }

    public String getIs_member() {
        return is_member;
    }

    public void setIs_member(String is_member) {
        this.is_member = is_member;
    }

    public String getIs_student() {
        return is_student;
    }

    public void setIs_student(String is_student) {
        this.is_student = is_student;
    }

    public String getIs_faculty() {
        return is_faculty;
    }

    public void setIs_faculty(String is_faculty) {
        this.is_faculty = is_faculty;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
