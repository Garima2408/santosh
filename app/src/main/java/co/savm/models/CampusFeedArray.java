package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 23-08-2017.
 */

public class CampusFeedArray implements Serializable {
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;
    public  String id;
    public  String  date;
    public  String  post;
    public  String  userName;
    public  String  picture;
    public  String pid;
    public  String uid;
    public  String replies_count;
    public  String likes;
    public  String is_liked;
    public  String title;
    public  String url;
    public  String comment;
    public  String categoryId;
    public  String categoryName;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    /*"comment":"<p>zvdbehrjeheheh</p>\n",
         "uid":"4847",
         "id":"3",
         "depth":"",
         "pid":"",
         "date":"05 Oct 02:20pm",
         "category":[  ],
         "is_liked":"False",
         "is_inappropriate":"False",
         "field_comment_attachments":{  },
         "subject":"zvdbehrjeheheh",
         "userName":"Garima Shukla",
         "picture":"https://mes.questin.co/sites/mes.questin.co/files/storage/extSdCard/Pictures/20180422_141357_0.jpg",
         "replies_count":"0",
         "likes":0,
         "inappropriate":0*/




    public CampusFeedArray(int type) {
        this.type = type;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReplies_count() {
        return replies_count;
    }

    public void setReplies_count(String replies_count) {
        this.replies_count = replies_count;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getIs_liked() {
        return is_liked;
    }

    public void setIs_liked(String is_liked) {
        this.is_liked = is_liked;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

}
