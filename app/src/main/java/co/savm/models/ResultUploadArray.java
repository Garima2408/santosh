package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 05-01-2018.
 */

public class ResultUploadArray implements Serializable {
   public String uid;
    public String name;
    public String picture;
    public String Marks;

    public String getMarks() {
        return Marks;
    }

    public void setMarks(String marks) {
        Marks = marks;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
