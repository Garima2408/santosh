package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 21-08-2017.
 */

public class AllFriends implements Serializable {

    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;


    public String picture;
    public String field_lastname;
    public String field_firstname;
    public String  mail;
    public String  name;
    public String  uid;
    public String CollageName;
    public String CollageRole;
    public String CollageBranch;
    public String CollageEnrollment;
    public String Collagedepartment;
    public String CollageStudentName;
    public  String MainRole;
    public String CollageId;

    public AllFriends(int type) {
        this.type = type;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCollageId() {
        return CollageId;
    }

    public void setCollageId(String collageId) {
        CollageId = collageId;
    }

    public String getMainRole() {
        return MainRole;
    }

    public void setMainRole(String mainRole) {
        MainRole = mainRole;
    }

    public String getCollageName() {
        return CollageName;
    }

    public void setCollageName(String collageName) {
        CollageName = collageName;
    }

    public String getCollageRole() {
        return CollageRole;
    }

    public void setCollageRole(String collageRole) {
        CollageRole = collageRole;
    }

    public String getCollageBranch() {
        return CollageBranch;
    }

    public void setCollageBranch(String collageBranch) {
        CollageBranch = collageBranch;
    }

    public String getCollageEnrollment() {
        return CollageEnrollment;
    }

    public void setCollageEnrollment(String collageEnrollment) {
        CollageEnrollment = collageEnrollment;
    }

    public String getCollagedepartment() {
        return Collagedepartment;
    }

    public void setCollagedepartment(String collagedepartment) {
        Collagedepartment = collagedepartment;
    }

    public String getCollageStudentName() {
        return CollageStudentName;
    }

    public void setCollageStudentName(String collageStudentName) {
        CollageStudentName = collageStudentName;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }




    public String getField_lastname() {
        return field_lastname;
    }

    public void setField_lastname(String field_lastname) {
        this.field_lastname = field_lastname;
    }

    public String getField_firstname() {
        return field_firstname;
    }

    public void setField_firstname(String field_firstname) {
        this.field_firstname = field_firstname;
    }


}
