package co.savm.models;

import java.io.Serializable;

/**
 * Created by developer on 27/10/16.
 */
public class AccessDeviceToken implements Serializable {
    public String accesstoken;
    public String sessionID;
    public String sessionName;
    public String DomainUrl;

    public String getDomainUrl() {
        return DomainUrl;
    }

    public void setDomainUrl(String domainUrl) {
        DomainUrl = domainUrl;
    }

    public String getAccesstoken() {
        return accesstoken;
    }

    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken;
    }
    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }





}
