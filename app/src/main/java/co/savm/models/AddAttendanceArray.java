package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 04-10-2017.
 */

public class AddAttendanceArray implements Serializable{
    public String uid;
    public String name;
    public String picture;
    public String attendance;
    public String markedValue;
    public String enrollment_id;


    public String getEnrollment_id() {
        return enrollment_id;
    }

    public void setEnrollment_id(String enrollment_id) {
        this.enrollment_id = enrollment_id;
    }

    public String getMarkedValue() {
        return markedValue;
    }

    public void setMarkedValue(String markedValue) {
        this.markedValue = markedValue;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }


}
