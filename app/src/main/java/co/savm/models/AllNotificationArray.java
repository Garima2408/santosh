package co.savm.models;

import java.io.Serializable;

public class AllNotificationArray implements Serializable {

    String id;
    String Message;

    public AllNotificationArray(String id, String message) {
        this.id = id;
        Message = message;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
