package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 17-08-2017.
 */

public class CampusServiceArray implements Serializable {


    public String  tnid;
    public String description;
    public String title;
    public String logo;



    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public CampusServiceArray(int type) {
        this.type = type;

    }


    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }




    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
