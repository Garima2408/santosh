package co.savm.models;

import java.io.Serializable;

/**
 * Created by developer on 13/1/17.
 */
public class TaskChildListArray  implements Serializable {
    public  String nid;
    public String title;
    public String perc;

    public String getTitle() {
        return title;

    }
    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getPerc() {
        return perc;
    }

    public void setPerc(String perc) {
        this.perc = perc;
    }



}
