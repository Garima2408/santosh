package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 29-08-2017.
 */

public class ResultChlidListArray implements Serializable {
    public String title;
    public String marks;
    public String max_marks;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getMax_marks() {
        return max_marks;
    }

    public void setMax_marks(String max_marks) {
        this.max_marks = max_marks;
    }
}
