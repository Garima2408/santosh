
package co.savm.models.userFriendsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum2 {

    @SerializedName("uid")
    @Expose
    private String uid;

    @SerializedName("name")
    @Expose
    private String name;


    @SerializedName("picture")
    @Expose
    private String picture;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("thread_id")
    @Expose
    private String threadid;




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getThreadID()
    {
        return threadid;
    }

    public void setThreadid(String threadid)
    {
        this.threadid = threadid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }



    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
