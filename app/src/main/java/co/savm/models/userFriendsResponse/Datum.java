
package co.savm.models.userFriendsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("field_lastname")
    @Expose
    private String fieldLastname;
    @SerializedName("field_firstname")
    @Expose
    private String fieldFirstname;
    @SerializedName("picture")
    @Expose
    private String picture;





    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getFieldLastname() {
        return fieldLastname;
    }

    public void setFieldLastname(String fieldLastname) {
        this.fieldLastname = fieldLastname;
    }

    public String getFieldFirstname() {
        return fieldFirstname;
    }

    public void setFieldFirstname(String fieldFirstname) {
        this.fieldFirstname = fieldFirstname;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

}
