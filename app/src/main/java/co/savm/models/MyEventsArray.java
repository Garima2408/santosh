package co.savm.models;

import java.io.Serializable;

/**
 * Created by AKASH on 16-Apr-18.
 */

public class MyEventsArray implements Serializable {

    public String title;
    public String   nid;
    public String  type;
    public String  body;
    public String  field_discipline;
    public String  field_time;
    public String  startDate;
    public String  startTime;
    public String  endDate;
    public String  endTime;
    public String  Week;
    public String  field_groups_logo;
    public String  field_dept_location;
    public String  lat;
    public String  lng;
    public String  field_is_subscribe;

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getField_discipline() {
        return field_discipline;
    }

    public void setField_discipline(String field_discipline) {
        this.field_discipline = field_discipline;
    }

    public String getField_time() {
        return field_time;
    }

    public void setField_time(String field_time) {
        this.field_time = field_time;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getWeek() {
        return Week;
    }

    public void setWeek(String week) {
        Week = week;
    }

    public String getField_groups_logo() {
        return field_groups_logo;
    }

    public void setField_groups_logo(String field_groups_logo) {
        this.field_groups_logo = field_groups_logo;
    }

    public String getField_dept_location() {
        return field_dept_location;
    }

    public void setField_dept_location(String field_dept_location) {
        this.field_dept_location = field_dept_location;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getField_is_subscribe() {
        return field_is_subscribe;
    }

    public void setField_is_subscribe(String field_is_subscribe) {
        this.field_is_subscribe = field_is_subscribe;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
