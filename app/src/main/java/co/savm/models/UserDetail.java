package co.savm.models;

import org.json.JSONArray;

import java.io.Serializable;


public class UserDetail implements Serializable {


    public String userprofile_id;
    public String username;
    public String email;
    public String password;
    public String contact;
    public String fullname ;
    public String FirstName;
    public String LastName;
    public String Userwallpic;
    public String ParentUid;
    public static String childImage;


    public String getChildImage() {
        return childImage;
    }

    public void setChildImage(String childImage) {
        UserDetail.childImage = childImage;
    }




    public String getParentUid() {
        return ParentUid;
    }

    public void setParentUid(String parentUid) {
        ParentUid = parentUid;
    }

    public String getUserwallpic() {
        return Userwallpic;
    }

    public void setUserwallpic(String userwallpic) {
        Userwallpic = userwallpic;
    }

    public JSONArray getColleges() {
        return colleges;
    }

    public void setColleges(JSONArray colleges) {
        this.colleges = colleges;
    }

    public JSONArray colleges;


    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String photo;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public  String getREGISTRATIONID() {
        return REGISTRATIONID;
    }

    public  void setREGISTRATIONID(String REGISTRATIONID) {
        UserDetail.REGISTRATIONID = REGISTRATIONID;
    }

    public static String REGISTRATIONID = "registrationId";

    public String getUserprofile_id() {
        return userprofile_id;
    }

    public void setUserprofile_id(String userprofile_id) {
        this.userprofile_id = userprofile_id;
    }








}
