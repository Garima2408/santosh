
package co.savm.models.routedetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Destination {

    @SerializedName("lat")
    @Expose
    private String lat;
    @SerializedName("lng")
    @Expose
    private String lng;
    @SerializedName("field_start_time")
    @Expose
    private String fieldStartTime;
    @SerializedName("field_departure_time")
    @Expose
    private String fieldDepartureTime;
    @SerializedName("field_photo")
    @Expose
    private String fieldPhoto;
    private String  field_place_name;

    public String getField_place_name() {
        return field_place_name;
    }

    public void setField_place_name(String field_place_name) {
        this.field_place_name = field_place_name;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getFieldStartTime() {
        return fieldStartTime;
    }

    public void setFieldStartTime(String fieldStartTime) {
        this.fieldStartTime = fieldStartTime;
    }

    public String getFieldDepartureTime() {
        return fieldDepartureTime;
    }

    public void setFieldDepartureTime(String fieldDepartureTime) {
        this.fieldDepartureTime = fieldDepartureTime;
    }

    public String getFieldPhoto() {
        return fieldPhoto;
    }

    public void setFieldPhoto(String fieldPhoto) {
        this.fieldPhoto = fieldPhoto;
    }

}
