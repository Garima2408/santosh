
package co.savm.models.routedetailResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("nid")
    @Expose
    private String nid;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("field_device_id")
    @Expose
    private String fieldDeviceId;
    @SerializedName("field_route_number")
    @Expose
    private String fieldRouteNumber;
    @SerializedName("field_collection_way_points")
    @Expose
    private FieldCollectionWayPoints fieldCollectionWayPoints;
    private PickDropLatLng pickup_point;
    private PickDropLatLng  drop_point;

    public PickDropLatLng getPickup_point() {
        return pickup_point;
    }

    public void setPickup_point(PickDropLatLng pickup_point) {
        this.pickup_point = pickup_point;
    }

    public PickDropLatLng getDrop_point() {
        return drop_point;
    }

    public void setDrop_point(PickDropLatLng drop_point) {
        this.drop_point = drop_point;
    }


    //     "pickup_point": {
//        "lat": "23.406798107278433",
//                "lng": "79.94529962539673"
//    },
//            "drop_point": {
//        "lat": "23.406798107278433",
//                "lng": "79.94529962539673"
//    }






    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getFieldDeviceId() {
        return fieldDeviceId;
    }

    public void setFieldDeviceId(String fieldDeviceId) {
        this.fieldDeviceId = fieldDeviceId;
    }

    public String getFieldRouteNumber() {
        return fieldRouteNumber;
    }

    public void setFieldRouteNumber(String fieldRouteNumber) {
        this.fieldRouteNumber = fieldRouteNumber;
    }

    public FieldCollectionWayPoints getFieldCollectionWayPoints() {
        return fieldCollectionWayPoints;
    }

    public void setFieldCollectionWayPoints(FieldCollectionWayPoints fieldCollectionWayPoints) {
        this.fieldCollectionWayPoints = fieldCollectionWayPoints;
    }

}
