package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 23-10-2017.
 */

public class RequestPendingArray implements Serializable {
    public String field_firstname;
    public String field_lastname;
    public String rid;
    public static final int COARSE_TYPE = 0;            //default yeh hai
    public static final int PROGRESS_ = 1;
    private int type= 0;

    public RequestPendingArray(int type) {
        this.type = type;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getField_firstname() {
        return field_firstname;
    }

    public void setField_firstname(String field_firstname) {
        this.field_firstname = field_firstname;
    }

    public String getField_lastname() {
        return field_lastname;
    }

    public void setField_lastname(String field_lastname) {
        this.field_lastname = field_lastname;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String picture;


}
