package co.savm.models.tracker;

import java.io.Serializable;

public class WayPointsDrawArray implements Serializable {

    public String field_place_name;
    public String field_start_time;
    public String field_departure_time;
    public String field_way_points;
    public Double lat;
    public Double lng;
    public String field_photo;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getField_place_name() {
        return field_place_name;
    }

    public void setField_place_name(String field_place_name) {
        this.field_place_name = field_place_name;
    }

    public String getField_start_time() {
        return field_start_time;
    }

    public void setField_start_time(String field_start_time) {
        this.field_start_time = field_start_time;
    }

    public String getField_departure_time() {
        return field_departure_time;
    }

    public void setField_departure_time(String field_departure_time) {
        this.field_departure_time = field_departure_time;
    }

    public String getField_way_points() {
        return field_way_points;
    }

    public void setField_way_points(String field_way_points) {
        this.field_way_points = field_way_points;
    }



    public String getField_photo() {
        return field_photo;
    }

    public void setField_photo(String field_photo) {
        this.field_photo = field_photo;
    }
}
