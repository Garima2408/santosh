package co.savm.models.tracker;

import java.util.ArrayList;

/**
 * Created by HP on 6/1/2018.
 */

public class WayPointResponse {

    private String membership_id;

    private String child_student;

    public String getMembership_id() {
        return membership_id;
    }
    public String getChild_student() {
        return child_student;
    }

    public void setMembership_id(String membership_id) {
        this.membership_id = membership_id;
    }


    public void setChild_student(String child_student) {
        this.child_student = child_student;
    }

    private ArrayList<WayPointsArray> wayPointsArrays;

    public ArrayList<WayPointsArray> getWayPointsArrays() {
        return wayPointsArrays;
    }

    public void setWayPointsArrays(ArrayList<WayPointsArray> wayPointsArrays) {
        this.wayPointsArrays = wayPointsArrays;
    }
}

