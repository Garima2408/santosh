package co.savm.models.tracker;

/**
 * Created by farheen on 5/10/17
 */

public class StopsModel {

    private String name;
    private double lat;
    private double lng;
    private String planceName;
    private String startTime;
    private String departTime;
    private String stopPic;




    public String getStopPic() {
        return stopPic;
    }

    public void setStopPic(String stopPic) {
        this.stopPic = stopPic;
    }

    public String getPlanceName() {
        return planceName;
    }

    public void setPlanceName(String planceName) {
        this.planceName = planceName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public StopsModel() {

    }

    public StopsModel(String name, double lat, double lng) {
        this.name = name;
        this.lat = lat;
        this.lng = lng;
    }

    public StopsModel(double lat, double lng, String planceName, String startTime, String departTime, String stopPic) {
        this.lat = lat;
        this.lng = lng;
        this.planceName = planceName;
        this.startTime = startTime;
        this.departTime = departTime;
        this.stopPic = stopPic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
