package co.savm.models.tracker;

import java.io.Serializable;

public class BusAttandanceArray implements Serializable {
    public String uid;
    public String name;
    public String picture;
    public String attendance;
    public String MorningStatus;
    public String EveningStatus;
    public String enrollment_id;

    public String getEnrollment_id() {
        return enrollment_id;
    }

    public void setEnrollment_id(String enrollment_id) {
        this.enrollment_id = enrollment_id;
    }

    public String getMorningStatus() {
        return MorningStatus;
    }

    public void setMorningStatus(String morningStatus) {
        MorningStatus = morningStatus;
    }

    public String getEveningStatus() {
        return EveningStatus;
    }

    public void setEveningStatus(String eveningStatus) {
        EveningStatus = eveningStatus;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }


}
