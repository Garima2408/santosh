package co.savm.models.tracker;

/**
 * Created by farheen on 13/10/17
 */

public class RouteListAdminModel {
    private String title;
    private String id;
    private boolean isSelected;
    public  String is_faculty;
    public  String is_student;
    public  String is_member;
    public  String is_driver;
    public  String node_uid;
    public  String is_creator;
    private String sponsered;

    public String getIs_faculty() {
        return is_faculty;
    }

    public void setIs_faculty(String is_faculty) {
        this.is_faculty = is_faculty;
    }

    public String getIs_student() {
        return is_student;
    }

    public void setIs_student(String is_student) {
        this.is_student = is_student;
    }

    public String getIs_member() {
        return is_member;
    }

    public void setIs_member(String is_member) {
        this.is_member = is_member;
    }

    public String getIs_driver() {
        return is_driver;
    }

    public void setIs_driver(String is_driver) {
        this.is_driver = is_driver;
    }

    public String getNode_uid() {
        return node_uid;
    }

    public void setNode_uid(String node_uid) {
        this.node_uid = node_uid;
    }

    public String getIs_creator() {
        return is_creator;
    }

    public void setIs_creator(String is_creator) {
        this.is_creator = is_creator;
    }

    public String getSponsered() {
        return sponsered;
    }

    public void setSponsered(String sponsered) {
        this.sponsered = sponsered;
    }

    public RouteListAdminModel() {

    }

    public RouteListAdminModel(String title, String id, boolean isSelected,String is_faculty, String is_student,String is_member, String is_driver,String node_uid,String is_creator,String sponsered) {
        this.title = title;
        this.id = id;
        this.isSelected = isSelected;
        this.is_faculty = is_faculty;
        this.is_student = is_student;
        this.is_member = is_member;
        this.is_driver = is_driver;
        this.node_uid = node_uid;
        this.is_creator = is_creator;
        this.sponsered = sponsered;




    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
