package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 09-08-2017.
 */

public class CourseMainList implements Serializable {
    public String tid ;
    public String name ;


    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
