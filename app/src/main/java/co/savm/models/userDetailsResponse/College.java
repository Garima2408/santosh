
package co.savm.models.userDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class College {

    @SerializedName("2")
    @Expose
    private String _2;
    @SerializedName("college_nid")
    @Expose
    private String collegeNid;
    @SerializedName("rid")
    @Expose
    private String rid;
    @SerializedName("13")
    @Expose
    private String _13;
    @SerializedName("14")
    @Expose
    private String _14;

    public String get2() {
        return _2;
    }

    public void set2(String _2) {
        this._2 = _2;
    }

    public String getCollegeNid() {
        return collegeNid;
    }

    public void setCollegeNid(String collegeNid) {
        this.collegeNid = collegeNid;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String get13() {
        return _13;
    }

    public void set13(String _13) {
        this._13 = _13;
    }

    public String get14() {
        return _14;
    }

    public void set14(String _14) {
        this._14 = _14;
    }

}
