
package co.savm.models.userDetailsResponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {

    @SerializedName("uid")
    @Expose
    private String uid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("picture")
    @Expose
    private Picture picture;
    @SerializedName("field_first_name")
    @Expose
    private String fieldFirstName;
    @SerializedName("field_last_name")
    @Expose
    private String fieldLastName;
    @SerializedName("colleges")
    @Expose
    private List<College> colleges = null;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getFieldFirstName() {
        return fieldFirstName;
    }

    public void setFieldFirstName(String fieldFirstName) {
        this.fieldFirstName = fieldFirstName;
    }

    public String getFieldLastName() {
        return fieldLastName;
    }

    public void setFieldLastName(String fieldLastName) {
        this.fieldLastName = fieldLastName;
    }

    public List<College> getColleges() {
        return colleges;
    }

    public void setColleges(List<College> colleges) {
        this.colleges = colleges;
    }

}
