package co.savm.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 03-08-2017.
 */

public class CollageLists implements Serializable {

    public String tnid;
    public String title;
    public String field_group_image;
    public String field_groups_logo;
    public String CollageMemberShipId;
    public String lat;
    public String lng;
    public String affiliation;
    public String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }



    public List<MultipleLocn> multiple = new ArrayList<MultipleLocn>();

    public List<MultipleLocn> getMultiple() {
        return multiple;
    }

    public void setMultiple(List<MultipleLocn> multiple) {
        this.multiple = multiple;
    }

    private  LatLng location;

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getCollageMemberShipId() {
        return CollageMemberShipId;
    }

    public void setCollageMemberShipId(String collageMemberShipId) {
        CollageMemberShipId = collageMemberShipId;
    }

    public String getField_groups_logo() {
        return field_groups_logo;
    }

    public void setField_groups_logo(String field_groups_logo) {
        this.field_groups_logo = field_groups_logo;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getField_group_image() {
        return field_group_image;
    }

    public void setField_group_image(String field_group_image) {
        this.field_group_image = field_group_image;
    }



}
