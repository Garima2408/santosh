package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 31-10-2017.
 */

public class InfoVedioArray implements Serializable {
    public String value;
    public String title;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
