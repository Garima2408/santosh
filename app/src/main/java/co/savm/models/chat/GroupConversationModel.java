package co.savm.models.chat;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by farheen on 15/9/17
 */

public class GroupConversationModel implements Parcelable{
    private String senderId;
    private String name;
    private String dpUrl;
    private int isMine;
    private int lastMsgCategory;
    private String lastMsgText;
    private String lastMsgTime;
    private int lastMsgStatus;
    private int unreadCount;
    private String groupId;

    public GroupConversationModel() {

    }

    public GroupConversationModel(String senderId, String name, String dpUrl,
                                  int isMine, int lastMsgCategory, String lastMsgText,
                                  String lastMsgTime, int lastMsgStatus, int unreadCount, String groupId) {
        this.senderId = senderId;
        this.name = name;
        this.dpUrl = dpUrl;
        this.isMine = isMine;
        this.lastMsgCategory = lastMsgCategory;
        this.lastMsgText = lastMsgText;
        this.lastMsgTime = lastMsgTime;
        this.lastMsgStatus = lastMsgStatus;
        this.unreadCount = unreadCount;
        this.groupId = groupId;
    }

    protected GroupConversationModel(Parcel in) {
        senderId = in.readString();
        name = in.readString();
        dpUrl = in.readString();
        isMine = in.readInt();
        lastMsgCategory = in.readInt();
        lastMsgText = in.readString();
        lastMsgTime = in.readString();
        lastMsgStatus = in.readInt();
        unreadCount = in.readInt();
        groupId = in.readString();
    }

    public static final Creator<GroupConversationModel> CREATOR = new Creator<GroupConversationModel>() {
        @Override
        public GroupConversationModel createFromParcel(Parcel in) {
            return new GroupConversationModel(in);
        }

        @Override
        public GroupConversationModel[] newArray(int size) {
            return new GroupConversationModel[size];
        }
    };

    public String getSenderId() {
        return senderId;
    }

    public GroupConversationModel setSenderId(String senderId) {
        this.senderId = senderId;
        return this;
    }

    public String getName() {
        return name;
    }

    public GroupConversationModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getDpUrl() {
        return dpUrl;
    }

    public GroupConversationModel setDpUrl(String dpUrl) {
        this.dpUrl = dpUrl;
        return this;
    }

    public int isMine() {
        return isMine;
    }

    public GroupConversationModel setMine(int mine) {
        isMine = mine;
        return this;
    }

    public int getLastMsgCategory() {
        return lastMsgCategory;
    }

    public GroupConversationModel setLastMsgCategory(int lastMsgCategory) {
        this.lastMsgCategory = lastMsgCategory;
        return this;
    }

    public String getLastMsgText() {
        return lastMsgText;
    }

    public GroupConversationModel setLastMsgText(String lastMsgText) {
        this.lastMsgText = lastMsgText;
        return this;
    }

    public String getLastMsgTime() {
        return lastMsgTime;
    }

    public GroupConversationModel setLastMsgTime(String lastMsgTime) {
        this.lastMsgTime = lastMsgTime;
        return this;
    }

    public int getLastMsgStatus() {
        return lastMsgStatus;
    }

    public GroupConversationModel setLastMsgStatus(int lastMsgStatus) {
        this.lastMsgStatus = lastMsgStatus;
        return this;
    }

    public int getUnreadCount() {
        return unreadCount;
    }

    public GroupConversationModel setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
        return this;
    }

    public String getGroupId() {
        return groupId;
    }

    public GroupConversationModel setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(senderId);
        parcel.writeString(name);
        parcel.writeString(dpUrl);
        parcel.writeInt(isMine);
        parcel.writeInt(lastMsgCategory);
        parcel.writeString(lastMsgText);
        parcel.writeString(lastMsgTime);
        parcel.writeInt(lastMsgStatus);
        parcel.writeInt(unreadCount);
        parcel.writeString(groupId);
    }
}
