package co.savm.models.chat;

import java.util.List;

/**
 * Created by HP on 4/12/2018.
 */

public class FriendsData {

    List<FriendsList> friendsList;

    public List<FriendsList> getFriendsList() {
        return friendsList;
    }

    public void setFriendsList(List<FriendsList> friendsList) {
        this.friendsList = friendsList;
    }
}
