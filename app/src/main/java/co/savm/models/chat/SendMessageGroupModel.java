package co.savm.models.chat;

public class SendMessageGroupModel {

    private String gid;
    private String trigger;

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public SendMessageGroupModel() {

    }

    public SendMessageGroupModel(String gid, String trigger) {
        this.gid = gid;
        this.trigger = trigger;
    }

    public String getgid() {
        return gid;
    }

    public SendMessageGroupModel setgid(String gid) {
        this.gid = gid;
        return this;
    }

    public String gettrigger() {
        return trigger;
    }

    public SendMessageGroupModel settrigger(String trigger) {
        this.trigger = trigger;
        return this;


    }
}
