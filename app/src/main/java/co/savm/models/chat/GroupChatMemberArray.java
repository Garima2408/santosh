package co.savm.models.chat;

import java.io.Serializable;

/**
 * Created by Dell on 08-11-2017.
 */

public class GroupChatMemberArray implements Serializable {
  public String  id;
    public String  etid;
    public String   gid;
    public String  uri;
    public String  field_first_name;
    public String   field_last_name;
    public String  picture;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEtid() {
        return etid;
    }

    public void setEtid(String etid) {
        this.etid = etid;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getField_first_name() {
        return field_first_name;
    }

    public void setField_first_name(String field_first_name) {
        this.field_first_name = field_first_name;
    }

    public String getField_last_name() {
        return field_last_name;
    }

    public void setField_last_name(String field_last_name) {
        this.field_last_name = field_last_name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
