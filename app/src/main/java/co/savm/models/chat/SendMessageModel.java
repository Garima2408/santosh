package co.savm.models.chat;

/**
 * Created by farheen on 17/10/17
 */

public class SendMessageModel {

    private String uids;
    private String message;

    public SendMessageModel() {

    }

    public SendMessageModel(String uids, String message) {
        this.uids = uids;
        this.message = message;
    }

    public String getUids() {
        return uids;
    }

    public SendMessageModel setUids(String uids) {
        this.uids = uids;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public SendMessageModel setMessage(String message) {
        this.message = message;
        return this;
    }
}
