package co.savm.models.chat;

import java.io.Serializable;

/**
 * Created by AKASH on 11-Mar-18.
 */

public class ServerChatListArray implements Serializable {

    public String thread_id;
    public String  subject;
    public String  timestamp;
    public String  author;
    public String  body;
    public String  recipient;
    public String  mid;
    public String  author_name;
    public String  author_picture;
    public String  recipient_name;
    public String  recipient_picture;

    public String getThread_id() {
        return thread_id;
    }

    public void setThread_id(String thread_id) {
        this.thread_id = thread_id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getAuthor_picture() {
        return author_picture;
    }

    public void setAuthor_picture(String author_picture) {
        this.author_picture = author_picture;
    }

    public String getRecipient_name() {
        return recipient_name;
    }

    public void setRecipient_name(String recipient_name) {
        this.recipient_name = recipient_name;
    }

    public String getRecipient_picture() {
        return recipient_picture;
    }

    public void setRecipient_picture(String recipient_picture) {
        this.recipient_picture = recipient_picture;
    }
}
