package co.savm.models.chat;

import java.io.Serializable;

/**
 * Created by Dell on 03-11-2017.
 */

public class ChatSelectedArray implements Serializable {
    public String picture;
    public String field_lastname;
    public String field_firstname;

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getField_lastname() {
        return field_lastname;
    }

    public void setField_lastname(String field_lastname) {
        this.field_lastname = field_lastname;
    }

    public String getField_firstname() {
        return field_firstname;
    }

    public void setField_firstname(String field_firstname) {
        this.field_firstname = field_firstname;
    }
}
