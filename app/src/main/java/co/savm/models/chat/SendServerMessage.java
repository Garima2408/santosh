package co.savm.models.chat;

/**
 * Created by farheen on 17/10/17
 */

public class SendServerMessage {

    public String recipients;
    public String subject;
    public String body;

    public SendServerMessage() {

    }

    public String getRecipients() {
        return recipients;
    }

    public SendServerMessage setRecipients(String recipients) {
        this.recipients = recipients;
        return this;
    }

    public String getSubject() {
        return subject;
    }

    public SendServerMessage setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getBody() {
        return body;
    }

    public SendServerMessage setBody(String body) {
        this.body = body;
        return this;
    }

    public SendServerMessage(String recipients, String subject, String body) {
        this.recipients = recipients;
        this.subject = subject;
        this.body = body;

    }


}
