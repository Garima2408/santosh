package co.savm.models.chat;

import android.os.Parcel;
import android.os.Parcelable;



public class ChatModel implements Parcelable{
    private int isMine;
    private int category;
    private String text;
    private String link;
    private String time;
    private int status;
    private boolean isDownloading = false;

    public ChatModel() {

    }

    public ChatModel(int isMine, int category, String text, String link, String time, int status, boolean isDownloading) {
        this.isMine = isMine;
        this.category = category;
        this.text = text;
        this.link = link;
        this.time = time;
        this.status = status;
        this.isDownloading = isDownloading;
    }

    protected ChatModel(Parcel in) {
        isMine = in.readInt();
        category = in.readInt();
        text = in.readString();
        link = in.readString();
        time = in.readString();
        status = in.readInt();
        isDownloading = in.readByte() != 0;
    }

    public static final Creator<ChatModel> CREATOR = new Creator<ChatModel>() {
        @Override
        public ChatModel createFromParcel(Parcel in) {
            return new ChatModel(in);
        }

        @Override
        public ChatModel[] newArray(int size) {
            return new ChatModel[size];
        }
    };

    public boolean isDownloading() {
        return isDownloading;
    }

    public ChatModel setDownloading(boolean downloading) {
        isDownloading = downloading;
        return this;
    }

    public int getIsMine() {
        return isMine;
    }

    public ChatModel setIsMine(int isMine) {
        this.isMine = isMine;
        return this;
    }

    public int getCategory() {
        return category;
    }

    public ChatModel setCategory(int category) {
        this.category = category;
        return this;
    }

    public String getText() {
        return text;
    }

    public ChatModel setText(String text) {
        this.text = text;
        return this;
    }

    public String getLink() {
        return link;
    }

    public ChatModel setLink(String link) {
        this.link = link;
        return this;
    }

    public String getTime() {
        return time;
    }

    public ChatModel setTime(String time) {
        this.time = time;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public ChatModel setStatus(int status) {
        this.status = status;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(isMine);
        parcel.writeInt(category);
        parcel.writeString(text);
        parcel.writeString(link);
        parcel.writeString(time);
        parcel.writeInt(status);
        parcel.writeByte((byte) (isDownloading ? 1 : 0));
    }
}
