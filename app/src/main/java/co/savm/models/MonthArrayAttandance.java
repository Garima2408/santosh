package co.savm.models;

import java.io.Serializable;

/**
 * Created by AKASH on 12-Apr-18.
 */

public class MonthArrayAttandance implements Serializable{
    public String month;
    public String perc;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getPerc() {
        return perc;
    }

    public void setPerc(String perc) {
        this.perc = perc;
    }
}
