package co.savm.models;

import java.io.Serializable;

/**
 * Created by Dell on 22-09-2017.
 */

public class SubjectExamsArray implements Serializable {
    public String tnid;
    public String date;
    public String title;
    public String room;

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
