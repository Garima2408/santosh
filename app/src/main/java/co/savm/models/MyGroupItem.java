package co.savm.models;

/**
 * Created by nmn on 22/6/17.
 */

public class MyGroupItem {


    String tnid ;
    String title ;
    String image ;

    public String getTnid() {
        return tnid;
    }

    public void setTnid(String tnid) {
        this.tnid = tnid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
