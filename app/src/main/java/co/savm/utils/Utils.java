package co.savm.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.media.MediaScannerConnection;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import co.savm.Event.ConversationFire;
import co.savm.R;
import co.savm.chat.ChatActivity;
import co.savm.chat.ChatTabbedActivity;
import co.savm.database.QuestinContract;
import co.savm.database.QuestinSQLiteHelper;
import co.savm.imagezoom.ImageViewTouch;
import co.savm.imagezoom.ImageViewTouchBase;
import co.savm.models.chat.ConversationModel;
import co.savm.models.chat.GroupConversationModel;
import co.savm.models.fcm.FcmChatModel;
import co.savm.models.fcm.FcmGroupChatModel;
import co.savm.models.tracker.WayPointResponse;
import co.savm.models.tracker.WayPointsArray;
import co.savm.models.userDetailsResponse.UserDetailsResponseModel;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;

import static co.savm.database.QuestinContract.Conversation.TABLE_NAME;



public class Utils {
    public static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";
    private static Toast myToast;
    private static String PREFERENCES = "preferences";
    public static SharedPreferences sharedPreferences,pickDropLocation,joinGroup;
    private static ProgressDialog progressDoalog;
    public static final String PICK_DROP = "pickDrop";
    public static final String PICK_DROP_ = "DISCOUNT_TEST";
    public static final String JOINGROUPPREF="join_group";
    static Bitmap thumbnail = null;
    static String Extension;
    static Dialog imageDialog;
    @SuppressLint("NewApi")
    public static void initThreadPolicy() {
        // TODO Auto-generated method stub
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD) {
            StrictMode.ThreadPolicy tp = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(tp);
        }
    }

    public static void init(Context context) {
        if (context!=null)
            sharedPreferences = context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        if (context!=null)
            joinGroup=context.getSharedPreferences(JOINGROUPPREF,Context.MODE_PRIVATE);
        if (context!=null)
            pickDropLocation = context.getSharedPreferences(PICK_DROP_,
                    Context.MODE_PRIVATE);

    }

    public static void clearData() {

        sharedPreferences.edit().clear().apply();
    }

    public static void clearDataJoin() {

        joinGroup.edit().clear().apply();
    }
    public static boolean isValidEmailAddress(String emailAddress) {
        String expression = "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})";
        CharSequence inputStr = emailAddress;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        return matcher.matches();
    }

    // validating password with retype password
    public static boolean isValidPassword(String pass) {
        return pass != null && pass.length() >= 4;
    }

    public static boolean isJoinGroup(Activity context) {

        joinGroup = context.getSharedPreferences(JOINGROUPPREF,
                Context.MODE_PRIVATE);

        boolean  joinGrp = joinGroup.getBoolean(Constants.JOIN_GROUP, false);
        return   joinGrp;
    }
    // This  is use for user home page discount array.
    public static void savePickDrop(Context context, List<WayPointsArray> favorites) {

        SharedPreferences.Editor editor;


        editor = pickDropLocation.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString(PICK_DROP, jsonFavorites);

        editor.commit();
    }

    public  static void savePickAndDrop(Activity activity, WayPointResponse wayPointResponse){
        //wayPointResponse=  new Gson().fromJson("waypoint", WayPointResponse.class);

        SharedPreferences.Editor editor;
        editor = pickDropLocation.edit();

        Gson gson = new Gson();

        String jsonFavorites = gson.toJson(wayPointResponse);
        editor.putString(PICK_DROP, jsonFavorites);
        editor.commit();


    }

    public static WayPointResponse getsavePickAndDrop(Activity activity){
        WayPointResponse wayPointRes=new WayPointResponse();
        if (activity.getSharedPreferences(PICK_DROP_,Context.MODE_PRIVATE)!=null)
            pickDropLocation = activity.getSharedPreferences(PICK_DROP_,
                    Context.MODE_PRIVATE);

        if (pickDropLocation.contains(PICK_DROP)) {
            String jsonDis = pickDropLocation.getString(PICK_DROP, null);
            Gson gson = new Gson();
            wayPointRes = gson.fromJson(jsonDis,
                    WayPointResponse.class);


        } else
            return null;
        return wayPointRes;
    }

    public static void clearPickDropData() {
        pickDropLocation.edit().clear().apply();
    }

    public static List<WayPointsArray> getDiscount(Context context) {
        List<WayPointsArray> favorites;

        pickDropLocation = context.getSharedPreferences(PICK_DROP_,
                Context.MODE_PRIVATE);

        if (pickDropLocation.contains(PICK_DROP)) {
            String jsonDis = pickDropLocation.getString(PICK_DROP_, null);
            Gson gson = new Gson();
            WayPointsArray[] favoriteItems = gson.fromJson(jsonDis,
                    WayPointsArray[].class);

            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<WayPointsArray>(favorites);
        } else
            return null;

        return favorites;
    }
    public static long getTimeDifference(Date date) {
        Calendar c = Calendar.getInstance();
        // SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        // String currentTime = sdf.format(c.getTime());
        Date currentDate = c.getTime();
        return currentDate.getTime() - date.getTime();
    }

    public static long getTimeDifference(Date currentDate, Date date) {
        //Calendar c = Calendar.getInstance();
        // SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
        // String currentTime = sdf.format(c.getTime());
        //Date currentDate = c.getTime();
        return currentDate.getTime() - date.getTime();
    }

    public static boolean isInternetConnected(Context mContext) {

        try {
            ConnectivityManager connect = null;
            connect = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connect != null) {
                NetworkInfo resultMobile = connect.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                NetworkInfo resultWifi = connect.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                return (resultMobile != null && resultMobile.isConnectedOrConnecting()) || (resultWifi != null && resultWifi.isConnectedOrConnecting());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static int getDeviceWidth(Context context) {
        try {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            return metrics.widthPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 480;
    }

    public static int getDeviceHeight(Context context) {
        try {
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            return metrics.heightPixels;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 480;
    }


    public static void deleteUserChatAndTable(Activity activity){
        QuestinSQLiteHelper questinSQLiteHelper =new QuestinSQLiteHelper(activity);
        List<ConversationModel> conversationModels=new ArrayList<>();
        conversationModels.addAll(questinSQLiteHelper.getAllCotacts());

        if (conversationModels!=null && conversationModels.size()>0){

            for (int i=0;i<conversationModels.size();i++){
                questinSQLiteHelper.deleteChildTableMessage(activity,conversationModels.get(i).getSenderId(),
                        QuestinContract.Chat.TABLE_NAME);
            }

        }

        questinSQLiteHelper.deletaTable(activity,TABLE_NAME);

    }
    public static void deleteUserChat(Context mcontext,String senderId){

        QuestinSQLiteHelper questinSQLiteHelper =new QuestinSQLiteHelper(mcontext);

        questinSQLiteHelper.deleteChildTableMessage(mcontext,  senderId,QuestinContract.Chat.TABLE_NAME);

        questinSQLiteHelper.deleteSingleChatList(senderId);
    }
    public static void deleteGroupChatAndTable(Context activity,String groupId){
        QuestinSQLiteHelper questinSQLiteHelper =new QuestinSQLiteHelper(activity);



        questinSQLiteHelper.deleteGroupChildTableMessage(activity,groupId,
                QuestinContract.GroupChat.TABLE_NAME);


        //questinSQLiteHelper.deletaTable(activity, QuestinContract.GroupConversationList.TABLE_NAME);

    }

    public static void hideKeyBoard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    public static void showAlertDialog(Context context, String title, String message) {
        try{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setMessage(message);
            alertDialog.setPositiveButton("Proceed",
                     new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

        }catch (Exception e){

        }


    }


    public static void showAlertFragmentDialog(Context context, String title, String message) {

        try {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setMessage(message).setCancelable(false);
            alertDialog.setPositiveButton("Proceed",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();

        } catch (Exception e){

        }

    }


    public static void showDataToast(String data, Context context) {
        String toastMsg = data;
        if (myToast != null) {
            myToast.cancel();
            myToast = null;
        }
        if (myToast == null) {
            myToast = Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT);
        }
        if (myToast != null) {
            myToast.setText(toastMsg);
        }
        myToast.show();
    }

    public static void viewVisible(View... view) {
        if (view == null) return;
        for (View v : view) {
            v.setVisibility(View.VISIBLE);
        }
    }

    public static void viewInvisible(View... view) {
        if (view == null) return;
        for (View v : view) {
            v.setVisibility(View.INVISIBLE);
        }
    }

    public static void viewGone(View... view) {
        if (view == null) return;
        for (View v : view) {
            v.setVisibility(View.GONE);
        }
    }

    public static String printKeyHash(Context context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    public static Typeface getGothamBook(Context c) {
        try {
            return Typeface.createFromAsset(c.getAssets(), "fonts/ufonts.com_gotham-book.ttf");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Typeface getNormal(Context c) {
        try {
            return Typeface.createFromAsset(c.getAssets(), "TrajanPro-Regular.ttf");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }



    public static String getBase64String(String imagePath) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static void dialog(Context context, String title, String msg)
    {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public static void setStringPreferences(Context context, String key,
                                            String value) {
        SharedPreferences setting = context
                .getSharedPreferences(PREFERENCES, 0);

        SharedPreferences.Editor editor = setting.edit();

        editor.putString(key, value);
        editor.commit();

    }



    public static String getStringPreferences(Context context, String key) {

        SharedPreferences setting = context
                .getSharedPreferences(PREFERENCES, 0);
        return setting.getString(key, null);

    }
    public static void removeStringPreferences(Context context, String key)
    {
        SharedPreferences setting = context
                .getSharedPreferences(PREFERENCES, 0);

        SharedPreferences.Editor editor = setting.edit();

        editor.remove(key);
        editor.commit();

    }

    public static  void saveJoinGroup(boolean join){
        SharedPreferences.Editor editor = joinGroup.edit();

        editor.putBoolean(Constants.JOIN_GROUP,join);

        editor.apply();
    }
    public static  void saveFCMId(Context context,String fireBaseId){
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(Constants.FCM_REG_TOKEN,fireBaseId);

        editor.apply();
    }



    public static String getFCMId(){
        return    sharedPreferences.getString(Constants.FCM_REG_TOKEN, null);
    }

    public static SharedPreferences getSharedPreference(Context context){
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
    }






    public static LocationRequest createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;



    }





    public static String getETAUrl(LatLng origin, LatLng dest){
        return URLS.MAPS_DIST_MATRIX + "traffic_model=best_guess" +
                "&origins=" + origin.latitude + "," + origin.longitude +
                "&destinations=" + dest.latitude + "," + dest.longitude +
                "&departure_time=" + System.currentTimeMillis() +
                "&key=" + Constants.MAPS_API_KEY;
    }

    public static String getDirectionsUrl(LatLng start, LatLng end){
        String str_origin = "origin=" + start.latitude + "," + start.longitude;

        // Destination of route
        String str_dest = "destination=" + end.latitude + "," + end.longitude
                ;


        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;


        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters+"&key="+Constants.MAPS_API_KEY;


        return url;
    }

    private static void showChatMessageNotification(Context context, String title, String text) {
        Intent intent = new Intent(context, ChatTabbedActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        int uniqueId = 100;

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Creates the PendingIntent
            PendingIntent notifyPendingIntent =
                    PendingIntent.getActivity(
                            context,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            String channelID = "co.questin";// The id of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelID, "Questin", importance);
            // Create a notification and set the notification channel.
            Notification notification = getNotificationBuilder(title,text, notifyPendingIntent, defaultSoundUri, context)
                    .setChannelId(channelID)
                    .build();

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
                notificationManager.notify(uniqueId, notification);
            }
        } else if (notificationManager != null) {

            PendingIntent pendingIntent =
                    PendingIntent.getActivity(
                            context,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            NotificationCompat.Builder notificationBuilder = getNotificationBuilder(title, text, pendingIntent, defaultSoundUri,context);
            notificationManager.notify(uniqueId /* ID of notification */,
                    notificationBuilder.build());
        }
    }

    private static NotificationCompat.Builder getNotificationBuilder(String Tittle, String messageBody, PendingIntent pendingIntent, Uri defaultSoundUri, Context context) {
        return new NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.appicon)
                .setContentTitle(Tittle)
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setOnlyAlertOnce(true)
                .setContentIntent(pendingIntent);


    }









    public static ConversationModel getConversationModelFromFcmChatModel(FcmChatModel fcmChatModel, int unreadCount){
        return new ConversationModel()
                .setSenderId(fcmChatModel.getSenderId())
                .setMine(QuestinContract.MINE_NO)
                .setLastMsgCategory(fcmChatModel.getCategory())
                .setLastMsgText(fcmChatModel.getText())
                .setLastMsgTime(fcmChatModel.getTime())
                .setLastMsgStatus(QuestinContract.STATUS_DELIVERED)
                .setUnreadCount(unreadCount);
    }


    public static GroupConversationModel getGroupConversationModelFromFcmGroupChatModel(FcmGroupChatModel fcmChatModel, int unreadCount){
        return new GroupConversationModel()
                .setGroupId(fcmChatModel.getGroupId())
                .setSenderId(fcmChatModel.getSenderId())
                .setMine(QuestinContract.MINE_NO)
                .setLastMsgCategory(fcmChatModel.getCategory())
                .setLastMsgText(fcmChatModel.getText())
                .setLastMsgTime(fcmChatModel.getTime())
                .setLastMsgStatus(QuestinContract.STATUS_DELIVERED)
                .setUnreadCount(unreadCount);
    }



    public static File createImageFile(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "Questin_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//        File storageDir = new File(Environment
//                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath());
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    public static String createNewImageFileName(){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        return "Questin_" + timeStamp + "_.jpg";
    }

    public static void addPicToGallery(Context context, Uri photoUri) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        File file = new File(photoPath);
//        Uri contentUri = Uri.fromFile(file);
        mediaScanIntent.setData(photoUri);
        context.sendBroadcast(mediaScanIntent);
    }

    /**
     * Checks if the external storage is available to write
     * @return true if available, false otherwise
     */
    public static boolean isExternalStorageWritable(){
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    /**
     * Method to get user details from userId on the calling thread.
     * Calling function will have to manage background threading.
     * @param userId Unique questin id of the user
     * @return {@link UserDetailsResponseModel} containing the user details
     * @throws IOException if call is unsuccessful
     */
    public static UserDetailsResponseModel getUserDetails(String userId) throws IOException {
        String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                URLS.USER_DETAILS + userId);
        return new Gson().fromJson(response, UserDetailsResponseModel.class);
    }

    /**
     * Method to save chat message, add/update conversation table and show notification
     * when the user is not in the specific activity
     * @param context {@link Context} for opening db and showing notification
     * @param fcmChatModel {@link FcmChatModel} containing the details of the message
     */
    public static void saveChatMessage(Context context, FcmChatModel fcmChatModel){
        QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(context);

        if (ChatActivity.userId!=null && ChatActivity.userId.equals(fcmChatModel.getSenderId())){
            questinSQLiteHelper.addChatMessage(fcmChatModel, QuestinContract.STATUS_DELIVERED);
            Log.e("show","normal msg save");

        }else{
            EventBus.getDefault().post(new ConversationFire());

            questinSQLiteHelper.addChatMessage(fcmChatModel, QuestinContract.STATUS_DELIVERED);

            Utils.showChatMessageNotification(context, fcmChatModel.getUserName(), fcmChatModel.getText());
            Log.e("show","Notification");

            //use for
            questinSQLiteHelper.updateConversation(
                    Utils.getConversationModelFromFcmChatModel(fcmChatModel,
                            questinSQLiteHelper.getConversationUnreadCount(fcmChatModel.getSenderId()) + 1));


        }

        if(!questinSQLiteHelper.doesConversationExist(fcmChatModel.getSenderId())){
            ConversationModel conversationModel =
                    Utils.getConversationModelFromFcmChatModel(fcmChatModel, 0);
            try {
                UserDetailsResponseModel userDetailsResponseModel =
                        Utils.getUserDetails(fcmChatModel.getSenderId());
                conversationModel.setName(userDetailsResponseModel.getData().getFieldFirstName()
                        + " " + userDetailsResponseModel.getData().getFieldLastName());
                questinSQLiteHelper.addConversation(conversationModel);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
//            questinSQLiteHelper.updateConversation(
//                    Utils.getConversationModelFromFcmChatModel(fcmChatModel,
//                            questinSQLiteHelper.getConversationUnreadCount(fcmChatModel.getSenderId()) + 1));
        }
    }















    public static void saveGroupChatMessage(Context context, FcmGroupChatModel fcmChatModel){
        QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(context);


        if (ChatActivity.userId!=null && ChatActivity.userId.equals(fcmChatModel.getGroupId())){
            questinSQLiteHelper.addGroupChatMessage(fcmChatModel, QuestinContract.STATUS_DELIVERED);
        }else{
            EventBus.getDefault().post(new ConversationFire());

            questinSQLiteHelper.addGroupChatMessage(fcmChatModel, QuestinContract.STATUS_DELIVERED);
            Utils.showChatMessageNotification(context, fcmChatModel.getMyName(), fcmChatModel.getText());

            questinSQLiteHelper.updateGroupConversation(
                    Utils.getGroupConversationModelFromFcmGroupChatModel(fcmChatModel,
                            questinSQLiteHelper.getGroupConversationUnreadCount(fcmChatModel.getGroupId()) + 1));

        }

        //changes here group id at the place of senderId
        if(!questinSQLiteHelper.doesConversationExist(fcmChatModel.getGroupId())){
            GroupConversationModel conversationModel =
                    Utils.getGroupConversationModelFromFcmGroupChatModel(fcmChatModel, 0);
            /* UserDetailsResponseModel userDetailsResponseModel =
                     Utils.getUserDetails(fcmChatModel.getGroupId());
             GroupConversationModel.setName(userDetailsResponseModel.getData().getFieldFirstName()
                     + " " + userDetailsResponseModel.getData().getFieldLastName());*/
            questinSQLiteHelper.addGroupConversation(conversationModel);
        }
        else {
//            questinSQLiteHelper.updateGroupConversation(
//                    Utils.getGroupConversationModelFromFcmGroupChatModel(fcmChatModel,
//                            questinSQLiteHelper.getGroupConversationUnreadCount(fcmChatModel.getGroupId()) + 1));
        }
    }
















    public static  void hideKeyBorad(Context context, View view)
    {
        if(view!=null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager)  context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }}

    public static void p_dialog(Context context)    {
        progressDoalog = new ProgressDialog(context);
        progressDoalog.setMessage("Loading....");
        progressDoalog.show();
    }


    public static void p_dialog_dismiss(Context context)    {
        progressDoalog.dismiss();

    }


    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    public  static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    /**
     * Returns the {@code location} object as a human readable string.
     * @param location  The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public  static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }


    /*OPEN IMAGE DIALOGS*/



    public static void ImageDialogOpen(final Activity activity , String url) {

        imageDialog = new Dialog(activity);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.imagepreview_chat);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
        final ImageViewTouch showimage = imageDialog.findViewById(R.id.showimage);
        showimage.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);

        getBitmapFromURL(url);

        Glide.with(activity).load(url)
                .placeholder(R.color.black).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(showimage);

        showimage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                OpenSaveDialogBox(activity,thumbnail);
                return false;
            }
        });


        showimage.setSingleTapListener(
                new ImageViewTouch.OnImageViewTouchSingleTapListener() {

                    @Override
                    public void onSingleTapConfirmed() {
                        Log.d("LOG_TAG", "onSingleTapConfirmed");

                        OpenSaveDialogBox(activity,thumbnail);

                    }
                }
        );

        showimage.setDoubleTapListener(
                new ImageViewTouch.OnImageViewTouchDoubleTapListener() {

                    @Override
                    public void onDoubleTap() {
                        Log.d("LOG_TAG", "onDoubleTap");
                    }
                }
        );

        showimage.setOnDrawableChangedListener(
                new ImageViewTouchBase.OnDrawableChangeListener() {

                    @Override
                    public void onDrawableChanged(Drawable drawable) {
                        Log.i("LOG_TAG", "onBitmapChanged: " + drawable);
                    }
                }
        );



    }

    public static void OpenSaveDialogBox(final Activity activity, final Bitmap thumbnail) {
        final CharSequence[] items = { "Save Image",  "Cancel" };
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Save Image")) {

                    //saveImage(s);
                    saveImageToExternalStorage(activity,thumbnail);

                    Toast.makeText(activity, "Download Image Successfully ", Toast.LENGTH_SHORT).show();


                } else if (items[item].equals("Cancel")) {

                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    public static void getBitmapFromURL(final String src) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL url = new URL(src);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    thumbnail = BitmapFactory.decodeStream(input);
                    //return thumbnail;
                } catch (IOException e) {
                    e.printStackTrace();
                    // return null;
                }
            }
        }).start();


    }





    /*SAVE IMAGE IN STORAGE*/

    public static void saveImageToExternalStorage(Activity activity ,Bitmap myBitmap) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Santosh Images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(activity, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });

    }

    /*DOWNLOAD FILE AND SAVE IN STORAGE*/

    public static void downloadFile(Activity activity,String file) {
        String DownloadUrl = file;
        try {
            String filename = DownloadUrl.substring(DownloadUrl.lastIndexOf("/" ));
           String documentname= filename.substring(1);
            Extension = file.substring(file.lastIndexOf("."));
            Log.d("TAG", "Extension: " + Extension);

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));
            request.setDescription(documentname);   //appears the same in Notification bar while downloading
            request.setTitle(documentname);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            }

            request.setDestinationInExternalFilesDir(activity, null, documentname + Extension);

            // get download service and enqueue file
            DownloadManager manager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
            manager.enqueue(request);

        }catch (IndexOutOfBoundsException i){

        }


    }


    public static  void viewDocument(Activity activity, String url){

        Intent viewIntent =
                new Intent("android.intent.action.VIEW", Uri.parse(url));
        activity.startActivity(viewIntent);

    }



    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            Log.e("classname","null");
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }



    //File Full Path from internal storage,external and Downloads

    public static String getPath(Context context, Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {

                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                final String folder = split[1];

                final String finalPath = "/storage/"+type+"/"+folder;

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }else
                {
                    return finalPath;
                }


                // TODO handle non-primary volumes
            }


            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                File  selectedFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);

                /* File  selectedFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);*/
                String Filepath = selectedFile.toString();

                return Filepath;


               /* final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);*/
            }
            // MediaProvider
            else
            if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }

        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }

        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = { column };
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }



}