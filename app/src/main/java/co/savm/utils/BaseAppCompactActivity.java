package co.savm.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.io.ByteArrayOutputStream;

import co.savm.R;
import co.savm.activities.SignIn;
import co.savm.database.QuestinSQLiteHelper;
import co.savm.models.UserDetail;


public class BaseAppCompactActivity extends BaseActivity {
    String EventStartDate, EventTimeStart,  EventtimeEnd,EventendDate,EventBody, Eventtittle,AssignmentStartDate,AssignmentENDDate, AssignmentTimeStart,  AssignmenttimeEnd,  Assignmenttittle,ExamStartDate, ExamTimeStart, ExamtimeEnd,  Examtittle;
    RelativeLayout loading_view;
    private ProgressDialog progressDialog;
    // Progress Dialog
    private static ProgressDialog pDialog;
    public static final int progress_bar_type = 0;

    private QuestinSQLiteHelper questinSQLiteHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_base);
        ViewGroup contentView = findViewById(R.id.content_view);
        contentView.addView(getLayoutInflater().inflate(layoutResID, null));
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        loading_view = findViewById(R.id.loading_view);

        progressDialog = new ProgressDialog(BaseAppCompactActivity.this, R.style.MyTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progress_style));
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
    }

    public void showLoading() {
        if (progressDialog != null)
            progressDialog.show();
    }

    public void hideLoading() {
        try {
            if (progressDialog != null && progressDialog.isShowing())
                progressDialog.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyBoard(View v) {
        Utils.hideKeyBoard(BaseAppCompactActivity.this, v);
    }

    public Boolean isInternetConnected() {
        return Utils.isInternetConnected(BaseAppCompactActivity.this);
    }

    public void showAlertDialog(String message) {
        showAlertDialog(null, message);
    }




    public void showAlertDialog(String title, String message) {
        Utils.showAlertDialog(BaseAppCompactActivity.this, title, message);
    }

    public void showToast(String message) {
        Utils.showDataToast(message, BaseAppCompactActivity.this);
    }

    public UserDetail getUser() {
        return SessionManager.getInstance(getActivity()).getUser();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }


    public void showSnackbarUnautorized(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(this.getResources().getString(R.string.app_name));
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        finish();
                        startActivity(new Intent(BaseAppCompactActivity.this, SignIn.class));
                    }
                });
        alertDialog.show();
    }



    public  void showAlertDialogFinish(Context context,String title) {
        try{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setMessage(title);
            alertDialog.setPositiveButton("Proceed",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            finish();
                        }
                    });
            alertDialog.show();

        }catch (Exception e){

        }


    }







    /*
    *   Show Snackbar
     */
    public void showSnackbarMessage(String message) {

        final Snackbar snackBar = Snackbar.make(loading_view, message, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();

            }
        });
        snackBar.show();
    }



    // method for encode bitmap image to string
    public static String encodeImageTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.JPEG, 20, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        //  Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }

    // method for decode String to bitmap image
    public Bitmap decodeImageBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory
                .decodeByteArray(decodedByte, 0, decodedByte.length);
    }









}

















