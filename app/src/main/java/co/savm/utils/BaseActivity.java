package co.savm.utils;

import android.support.v7.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    protected AppCompatActivity getActivity() {
        return this;
    }
}
