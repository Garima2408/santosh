package co.savm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import co.savm.calendersection.EventModel;
import co.savm.models.AddAttendanceOfflineArray;
import co.savm.models.chat.ChatModel;
import co.savm.models.chat.ConversationModel;
import co.savm.models.chat.GroupChatModel;
import co.savm.models.chat.GroupConversationModel;
import co.savm.models.fcm.FcmChatModel;
import co.savm.models.fcm.FcmGroupChatModel;

import static co.savm.database.QuestinContract.Conversation.COL_LAST_MSG_TIME;
import static co.savm.database.QuestinContract.Conversation.COL_SENDER_ID;
import static co.savm.database.QuestinContract.Conversation.COL_UNREAD_COUNT;
import static co.savm.database.QuestinContract.Conversation.TABLE_NAME;

/**
 * Created by farheen on 13/9/17
 */

public class QuestinSQLiteHelper extends SQLiteOpenHelper {

    private static final String TAG = "QuestinSQLiteHelper";

    private static final String CREATE_CHAT_TABLE = "CREATE TABLE IF NOT EXISTS "
            + QuestinContract.Chat.TABLE_NAME + "( "
            + QuestinContract.Chat._ID + " INTEGER PRIMARY KEY autoincrement, "
            + QuestinContract.Chat.COL_IS_MINE + " INTEGER, "
            + QuestinContract.Chat.COL_SENDER_ID + " TEXT, "
            + QuestinContract.Chat.COL_CATEGORY + " INTEGER, "
            + QuestinContract.Chat.COL_TEXT + " TEXT, "
            + QuestinContract.Chat.COL_LINK + " TEXT, "
            + QuestinContract.Chat.COL_TIME + " TEXT, "
            + QuestinContract.Chat.COL_STATUS + " INTEGER);";

    private static final String CREATE_CONVERSATION_TABLE = "CREATE TABLE "
            + TABLE_NAME + "( "
            + QuestinContract.Conversation._ID + " INTEGER PRIMARY KEY, "
            + COL_SENDER_ID + " TEXT, "
            + QuestinContract.Conversation.COL_NAME + " TEXT, "
            + QuestinContract.Conversation.COL_DP_URL + " TEXT, "
            + QuestinContract.Conversation.COL_IS_MINE + " INTEGER, "
            + QuestinContract.Conversation.COL_LAST_MSG_CATEGORY + " INTEGER, "
            + QuestinContract.Conversation.COL_LAST_MSG_TEXT + " TEXT, "
            + COL_LAST_MSG_TIME + " TEXT, "
            + QuestinContract.Conversation.COL_LAST_MSG_STATUS + " INTEGER, "
            + COL_UNREAD_COUNT + " INTEGER, "
            + QuestinContract.Conversation.COL_GROUP_ID + " TEXT);";
//            + QuestinContract.Conversation.COL_REG_TOKEN + " TEXT);";


    private static final String CREATE_GROUPCONVERSATION_TABLE = "CREATE TABLE "
            + QuestinContract.GroupConversationList.TABLE_NAME + "( "
            + QuestinContract.GroupConversationList._ID + " INTEGER PRIMARY KEY, "
            + QuestinContract.GroupConversationList.GROUP_SENDER_ID + " TEXT, "
            + QuestinContract.GroupConversationList.GROUP_NAME + " TEXT, "
            + QuestinContract.GroupConversationList.GROUP_DP_URL + " TEXT, "
            + QuestinContract.GroupConversationList.GROUP_IS_MINE + " INTEGER, "
            + QuestinContract.GroupConversationList.GROUP_LAST_MSG_CATEGORY + " INTEGER, "
            + QuestinContract.GroupConversationList.GROUP_LAST_MSG_TEXT + " TEXT, "
            + QuestinContract.GroupConversationList.GROUP_LAST_MSG_TIME + " TEXT, "
            + QuestinContract.GroupConversationList.GROUP_LAST_MSG_STATUS + " INTEGER, "
            + QuestinContract.GroupConversationList.GROUP_UNREAD_COUNT + " INTEGER, "
            + QuestinContract.GroupConversationList.GROUP_GROUP_ID + " TEXT);";


    private static final String CREATE_GROUP_CHAT_TABLE = "CREATE TABLE "
            + QuestinContract.GroupChat.TABLE_NAME + "( "
            + QuestinContract.GroupChat._ID + " INTEGER PRIMARY KEY, "
            + QuestinContract.GroupChat.GROUP_GROUP_ID + " TEXT, "
            + QuestinContract.GroupChat.GROUP_SENDER_ID + " TEXT, "
            + QuestinContract.GroupChat.GROUP_SENDER_NAME + " TEXT, "
            + QuestinContract.GroupChat.GROUP_IS_MINE + " INTEGER, "
            + QuestinContract.GroupChat.GROUP_CATEGORY + " INTEGER, "
            + QuestinContract.GroupChat.GROUP_TEXT + " TEXT, "
            + QuestinContract.GroupChat.GROUP_LINK + " TEXT, "
            + QuestinContract.GroupChat.GROUP_TIME + " TEXT, "
            + QuestinContract.GroupChat.GROUP_STATUS + " INTEGER);";


    private static final String CREATE_CALENDER_EVENT_TABLE = "CREATE TABLE "
            + QuestinContract.CalenderEvents.TABLE_NAME + "( "
            + QuestinContract.CalenderEvents.EVENT_ID + " INTEGER PRIMARY KEY, "
            + QuestinContract.CalenderEvents.EVENT_START_DATE + " TEXT, "
            + QuestinContract.CalenderEvents.EVENT_END_DATE + " TEXT, "
            + QuestinContract.CalenderEvents.EVENT_START_TIME + " TEXT, "
            + QuestinContract.CalenderEvents.EVENT_END_TIME + " TEXT, "
            + QuestinContract.CalenderEvents.EVENT_TITTLE + " TEXT, "
            + QuestinContract.CalenderEvents.EVENT_DESCRIPTION + " TEXT, "
            + QuestinContract.CalenderEvents.EVENT_MAINID + " TEXT);";


    private static final String CREATE_CALENDER_COLLAGE_EVENT_TABLE = "CREATE TABLE "
            + QuestinContract.CalenderCollageEvents.TABLE_NAME + "( "
            + QuestinContract.CalenderCollageEvents.EVENT_ID + " INTEGER PRIMARY KEY, "
            + QuestinContract.CalenderCollageEvents.EVENT_START_DATE + " TEXT, "
            + QuestinContract.CalenderCollageEvents.EVENT_END_DATE + " TEXT, "
            + QuestinContract.CalenderCollageEvents.EVENT_START_TIME + " TEXT, "
            + QuestinContract.CalenderCollageEvents.EVENT_END_TIME + " TEXT, "
            + QuestinContract.CalenderCollageEvents.EVENT_TITTLE + " TEXT, "
            + QuestinContract.CalenderCollageEvents.EVENT_DESCRIPTION + " TEXT, "
            + QuestinContract.CalenderCollageEvents.EVENT_MAINID + " TEXT);";




    private static final String CREATE_CALENDER_ASSIGNMENT_TABLE = "CREATE TABLE "
            + QuestinContract.CalenderAssignment.TABLE_NAME + "( "
            + QuestinContract.CalenderAssignment.EVENT_ID + " INTEGER PRIMARY KEY, "
            + QuestinContract.CalenderAssignment.EVENT_START_DATE + " TEXT, "
            + QuestinContract.CalenderAssignment.EVENT_END_DATE + " TEXT, "
            + QuestinContract.CalenderAssignment.EVENT_START_TIME + " TEXT, "
            + QuestinContract.CalenderAssignment.EVENT_END_TIME + " TEXT, "
            + QuestinContract.CalenderAssignment.EVENT_TITTLE + " TEXT, "
            + QuestinContract.CalenderAssignment.EVENT_DESCRIPTION + " TEXT, "
            + QuestinContract.CalenderAssignment.EVENT_MAINID + " TEXT);";




    private static final String CREATE_CALENDER_EXAM_TABLE = "CREATE TABLE "
            + QuestinContract.CalenderExams.TABLE_NAME + "( "
            + QuestinContract.CalenderExams.EVENT_ID + " INTEGER PRIMARY KEY, "
            + QuestinContract.CalenderExams.EVENT_START_DATE + " TEXT, "
            + QuestinContract.CalenderExams.EVENT_END_DATE + " TEXT, "
            + QuestinContract.CalenderExams.EVENT_START_TIME + " TEXT, "
            + QuestinContract.CalenderExams.EVENT_END_TIME + " TEXT, "
            + QuestinContract.CalenderExams.EVENT_TITTLE + " TEXT, "
            + QuestinContract.CalenderExams.EVENT_DESCRIPTION + " TEXT, "
            + QuestinContract.CalenderExams.EVENT_MAINID + " TEXT);";


 private static final String CREATE_ATTENDANCE_TABLE = "CREATE TABLE "
            + QuestinContract.AddAttendanceOffline.TABLE_NAME + "( "
         + QuestinContract.AddAttendanceOffline.ATTENDANCE_ID + " INTEGER PRIMARY KEY autoincrement, "
            + QuestinContract.AddAttendanceOffline.ATTENDANCE_UID + " TEXT, "
            + QuestinContract.AddAttendanceOffline.ATTENDANCE_VALUE + " TEXT, "
            + QuestinContract.AddAttendanceOffline.ATTENDANCE_COURSE_ID + " TEXT, "
            + QuestinContract.AddAttendanceOffline.ATTENDANCE_CLASS_ID + " TEXT, "
            + QuestinContract.AddAttendanceOffline.ATTENDANCE_DATE + " TEXT, "
            + QuestinContract.AddAttendanceOffline.ATTENDANCE_TYPE + " TEXT);";




    public QuestinSQLiteHelper(Context context) {
        super(context, QuestinContract.DB_NAME, null, QuestinContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.beginTransaction();
        try {

            sqLiteDatabase.execSQL(CREATE_CHAT_TABLE);
            sqLiteDatabase.execSQL(CREATE_CONVERSATION_TABLE);
            sqLiteDatabase.execSQL(CREATE_GROUPCONVERSATION_TABLE);
            sqLiteDatabase.execSQL(CREATE_GROUP_CHAT_TABLE);
            sqLiteDatabase.execSQL(CREATE_CALENDER_EVENT_TABLE);
            sqLiteDatabase.execSQL(CREATE_CALENDER_ASSIGNMENT_TABLE);
            sqLiteDatabase.execSQL(CREATE_CALENDER_EXAM_TABLE);
            sqLiteDatabase.execSQL(CREATE_CALENDER_COLLAGE_EVENT_TABLE);
            sqLiteDatabase.execSQL(CREATE_ATTENDANCE_TABLE);


            sqLiteDatabase.setTransactionSuccessful();
        } finally {
            sqLiteDatabase.endTransaction();
        }



    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        Log.e(TAG, "onUpgrade: upgrading db from version " + i + " to version " + i1);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestinContract.Chat.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestinContract.GroupConversationList.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestinContract.GroupChat.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestinContract.CalenderEvents.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestinContract.CalenderAssignment.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestinContract.CalenderExams.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestinContract.CalenderCollageEvents.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + QuestinContract.AddAttendanceOffline.TABLE_NAME);

        onCreate(sqLiteDatabase);
    }


    public ArrayList<ConversationModel> getAllCotacts() {
        ArrayList<ConversationModel> array_list = new ArrayList<ConversationModel>();
        ArrayList<String> userId = new ArrayList<>();

        // hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME
                + " order by " + COL_LAST_MSG_TIME + " desc", null);
        try {
            if (res.moveToFirst()) {

                do {
                    ConversationModel objPigeonUserListModal = new ConversationModel();
//                    objPigeonUserListModal
//                            .setId(Integer.parseInt(res.getString(res
//                                    .getColumnIndex(PIGEON_USER_KEY_ID))));
//                    objPigeonUserListModal.setmNumber((res.getString(res
//                            .getColumnIndex(PIGEON_USER_KEY_NUMBER))));
//                    objPigeonUserListModal.setmName((res.getString(res
//                            .getColumnIndex(PIGEON_USER_KEY_NAME))));
//                    objPigeonUserListModal.setmTeamTag(res.getString(res.
//                            getColumnIndex(PIGEON_TEAM_KEY_NAME)));
//                    objPigeonUserListModal.setmEmail((res.getString(res
//                            .getColumnIndex(PIGEON_USER_KEY_EMAIL))));
//                    objPigeonUserListModal.setmImageUrl((res.getString(res
//                            .getColumnIndex(PIGEON_USER_KEY_IMAGE_URI))));
//                    objPigeonUserListModal.setmImageThumbURI((res.getString(res
//                            .getColumnIndex(PIGEON_USER_KEY_IMAGE_THUMB_URI))));
//                    objPigeonUserListModal.setmLastChatTime((res.getString(res
//                            .getColumnIndex(PIGEON_USER_LAST_CHAT_TIME))));

                    objPigeonUserListModal  .setSenderId(res.getString(res.getColumnIndex(COL_SENDER_ID)))
                            .setName(res.getString(res.getColumnIndex(QuestinContract.Conversation.COL_NAME)))
                            .setDpUrl(res.getString(res.getColumnIndex(QuestinContract.Conversation.COL_DP_URL)))
                            .setMine(res.getInt(res.getColumnIndex(QuestinContract.Conversation.COL_IS_MINE)))
                            .setLastMsgCategory(res.getInt(res.getColumnIndex(QuestinContract.Conversation.COL_LAST_MSG_CATEGORY)))
                            .setLastMsgText(res.getString(res.getColumnIndex(QuestinContract.Conversation.COL_LAST_MSG_TEXT)))
                            .setLastMsgTime(res.getString(res.getColumnIndex(COL_LAST_MSG_TIME)))
                            .setLastMsgStatus(res.getInt(res.getColumnIndex(QuestinContract.Conversation.COL_LAST_MSG_STATUS)))
                            .setUnreadCount(res.getInt(res.getColumnIndex(COL_UNREAD_COUNT)))
                            .setGroupId(res.getString(res.getColumnIndex(QuestinContract.Conversation.COL_GROUP_ID)));
                    if (!userId.contains(objPigeonUserListModal.getSenderId()) && !objPigeonUserListModal.getSenderId().equals("")){
                        userId.add(objPigeonUserListModal.getSenderId());
                        array_list.add(objPigeonUserListModal);

                    }else if (userId.size()==0){
                        if (!objPigeonUserListModal.getSenderId().equals("")){
                            userId.add(objPigeonUserListModal.getSenderId());
                            array_list.add(objPigeonUserListModal);
                        }

                    }

                } while (res.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
            res.close();
        }
        return array_list;
    }
    /*list of  single chat table*/

    public ArrayList<ConversationModel>  getConversationsList() {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = new String[]{
                COL_SENDER_ID,
                QuestinContract.Conversation.COL_NAME,
                QuestinContract.Conversation.COL_DP_URL,
                QuestinContract.Conversation.COL_IS_MINE,
                QuestinContract.Conversation.COL_LAST_MSG_CATEGORY,
                QuestinContract.Conversation.COL_LAST_MSG_TEXT,
                COL_LAST_MSG_TIME,
                QuestinContract.Conversation.COL_LAST_MSG_STATUS,
                COL_UNREAD_COUNT,
                QuestinContract.Conversation.COL_GROUP_ID
        };
        ArrayList<ConversationModel> conversationList = new ArrayList<>();

        Cursor cursor = db.query(TABLE_NAME, columns,
                null, null, null, null, COL_LAST_MSG_TIME);
        while (cursor.moveToNext()) {
            ConversationModel conversationModel = new ConversationModel()
                    .setSenderId(cursor.getString(cursor.getColumnIndex(COL_SENDER_ID)))
                    .setName(cursor.getString(cursor.getColumnIndex(QuestinContract.Conversation.COL_NAME)))
                    .setDpUrl(cursor.getString(cursor.getColumnIndex(QuestinContract.Conversation.COL_DP_URL)))
                    .setMine(cursor.getInt(cursor.getColumnIndex(QuestinContract.Conversation.COL_IS_MINE)))
                    .setLastMsgCategory(cursor.getInt(cursor.getColumnIndex(QuestinContract.Conversation.COL_LAST_MSG_CATEGORY)))
                    .setLastMsgText(cursor.getString(cursor.getColumnIndex(QuestinContract.Conversation.COL_LAST_MSG_TEXT)))
                    .setLastMsgTime(cursor.getString(cursor.getColumnIndex(COL_LAST_MSG_TIME)))
                    .setLastMsgStatus(cursor.getInt(cursor.getColumnIndex(QuestinContract.Conversation.COL_LAST_MSG_STATUS)))
                    .setUnreadCount(cursor.getInt(cursor.getColumnIndex(COL_UNREAD_COUNT)))
                    .setGroupId(cursor.getString(cursor.getColumnIndex(QuestinContract.Conversation.COL_GROUP_ID)));
            conversationList.add(conversationModel);
        }
        cursor.close();
        db.close();
        return conversationList;
    }

    /**
     * Add a new conversation in the database
     *
     * @param conversationModel New data to add
     */


    //new user add if not exits
    public void addConversation(ConversationModel conversationModel) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_SENDER_ID, conversationModel.getSenderId());
        contentValues.put(QuestinContract.Conversation.COL_NAME, conversationModel.getName());
        contentValues.put(QuestinContract.Conversation.COL_DP_URL, conversationModel.getDpUrl());
        contentValues.put(QuestinContract.Conversation.COL_IS_MINE, conversationModel.isMine());
        contentValues.put(QuestinContract.Conversation.COL_LAST_MSG_CATEGORY, conversationModel.getLastMsgCategory());
        contentValues.put(QuestinContract.Conversation.COL_LAST_MSG_TEXT, conversationModel.getLastMsgText());
        contentValues.put(COL_LAST_MSG_TIME, conversationModel.getLastMsgTime());
        contentValues.put(QuestinContract.Conversation.COL_LAST_MSG_STATUS, conversationModel.getLastMsgStatus());
        contentValues.put(COL_UNREAD_COUNT, conversationModel.getUnreadCount());
        contentValues.put(QuestinContract.Conversation.COL_GROUP_ID, conversationModel.getGroupId());

        db.insert(TABLE_NAME, null, contentValues);
        db.close();
    }

    public void deletaTable(Context context ,String tableName){
        SQLiteDatabase db = getWritableDatabase();

        Log.e("database handlar ", "deleteContactchild()CAll    " + tableName
                + "    " );
        db.delete(tableName, null , null);

        db.close();
    }
    public boolean drapChildTableMessage(Context mContext, String TableName) {
        SQLiteDatabase db = getWritableDatabase();

        db.delete(TableName, null, null);
        // Toast.makeText(mContext, TableName+" Droped ",
        // Toast.LENGTH_SHORT).show();
        db.close();
        return true;
    }

    /**
     * Update an existing conversation
     *
     * @param conversationModel New data to update
     */
    public void updateConversation(ConversationModel conversationModel) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuestinContract.Conversation.COL_IS_MINE, conversationModel.isMine());
        contentValues.put(QuestinContract.Conversation.COL_LAST_MSG_CATEGORY, conversationModel.getLastMsgCategory());
        contentValues.put(QuestinContract.Conversation.COL_LAST_MSG_TEXT, conversationModel.getLastMsgText());
        contentValues.put(COL_LAST_MSG_TIME, conversationModel.getLastMsgTime());
        contentValues.put(QuestinContract.Conversation.COL_LAST_MSG_STATUS, conversationModel.getLastMsgStatus());
        contentValues.put(COL_UNREAD_COUNT, conversationModel.getUnreadCount());

        db.update(TABLE_NAME, contentValues,
                COL_SENDER_ID + " =?", new String[]{conversationModel.getSenderId()});
        db.close();
    }


    /*list of group chat table*/


    public ArrayList<GroupConversationModel> getGroupConversationsList() {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = new String[]{
                QuestinContract.GroupConversationList.GROUP_SENDER_ID,
                QuestinContract.GroupConversationList.GROUP_NAME,
                QuestinContract.GroupConversationList.GROUP_DP_URL,
                QuestinContract.GroupConversationList.GROUP_IS_MINE,
                QuestinContract.GroupConversationList.GROUP_LAST_MSG_CATEGORY,
                QuestinContract.GroupConversationList.GROUP_LAST_MSG_TEXT,
                QuestinContract.GroupConversationList.GROUP_LAST_MSG_TIME,
                QuestinContract.GroupConversationList.GROUP_LAST_MSG_STATUS,
                QuestinContract.GroupConversationList.GROUP_UNREAD_COUNT,
                QuestinContract.GroupConversationList.GROUP_GROUP_ID
        };
        ArrayList<GroupConversationModel> conversationList = new ArrayList<>();

        Cursor cursor = db.query(QuestinContract.GroupConversationList.TABLE_NAME, columns,
                null, null, null, null, QuestinContract.GroupConversationList.GROUP_LAST_MSG_TIME);
        while (cursor.moveToNext()) {
            GroupConversationModel conversationModel = new GroupConversationModel()
                    .setSenderId(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_SENDER_ID)))
                    .setName(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_NAME)))
                    .setDpUrl(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_DP_URL)))
                    .setMine(cursor.getInt(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_IS_MINE)))
                    .setLastMsgCategory(cursor.getInt(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_LAST_MSG_CATEGORY)))
                    .setLastMsgText(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_LAST_MSG_TEXT)))
                    .setLastMsgTime(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_LAST_MSG_TIME)))
                    .setLastMsgStatus(cursor.getInt(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_LAST_MSG_STATUS)))
                    .setUnreadCount(cursor.getInt(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_UNREAD_COUNT)))
                    .setGroupId(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupConversationList.GROUP_GROUP_ID)));
            conversationList.add(conversationModel);
        }
        cursor.close();
        db.close();
        return conversationList;
    }

    /**
     * Add a new conversation in the database
     *
     * @param conversationModel New data to add
     */
    public void addGroupConversation(GroupConversationModel conversationModel) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuestinContract.GroupConversationList.GROUP_SENDER_ID, conversationModel.getSenderId());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_NAME, conversationModel.getName());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_DP_URL, conversationModel.getDpUrl());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_IS_MINE, conversationModel.isMine());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_LAST_MSG_CATEGORY, conversationModel.getLastMsgCategory());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_LAST_MSG_TEXT, conversationModel.getLastMsgText());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_LAST_MSG_TIME, conversationModel.getLastMsgTime());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_LAST_MSG_STATUS, conversationModel.getLastMsgStatus());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_UNREAD_COUNT, conversationModel.getUnreadCount());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_GROUP_ID, conversationModel.getGroupId());

        db.insert(QuestinContract.GroupConversationList.TABLE_NAME, null, contentValues);
        db.close();
    }

    /**
     * Update an existing conversation
     *
     * @param conversationModel New data to update
     */
    public void updateGroupConversation(GroupConversationModel conversationModel) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuestinContract.GroupConversationList.GROUP_IS_MINE, conversationModel.isMine());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_LAST_MSG_CATEGORY, conversationModel.getLastMsgCategory());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_LAST_MSG_TEXT, conversationModel.getLastMsgText());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_LAST_MSG_TIME, conversationModel.getLastMsgTime());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_LAST_MSG_STATUS, conversationModel.getLastMsgStatus());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_UNREAD_COUNT, conversationModel.getUnreadCount());
        contentValues.put(QuestinContract.GroupConversationList.GROUP_SENDER_ID, conversationModel.getSenderId());

        db.update(QuestinContract.GroupConversationList.TABLE_NAME, contentValues,
                QuestinContract.GroupConversationList.GROUP_GROUP_ID + " =?", new String[]{conversationModel.getGroupId()});
        db.close();
    }




    /*single chatting*/


    public ArrayList<ChatModel> getChatList(String senderId) {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = new String[]{
                QuestinContract.Chat.COL_IS_MINE,
                QuestinContract.Chat.COL_CATEGORY,
                QuestinContract.Chat.COL_TEXT,
                QuestinContract.Chat.COL_LINK,
                QuestinContract.Chat.COL_TIME,
                QuestinContract.Chat.COL_STATUS
        };
        ArrayList<ChatModel> chatList = new ArrayList<>();

        Cursor cursor = db.query(QuestinContract.Chat.TABLE_NAME, columns,
                QuestinContract.Chat.COL_SENDER_ID + " =?", new String[]{senderId},
                null, null, QuestinContract.Chat.COL_TIME);
        while (cursor.moveToNext()) {
            ChatModel chatModel = new ChatModel()
                    .setIsMine(cursor.getInt(cursor.getColumnIndex(QuestinContract.Chat.COL_IS_MINE)))
                    .setCategory(cursor.getInt(cursor.getColumnIndex(QuestinContract.Chat.COL_CATEGORY)))
                    .setText(cursor.getString(cursor.getColumnIndex(QuestinContract.Chat.COL_TEXT)))
                    .setLink(cursor.getString(cursor.getColumnIndex(QuestinContract.Chat.COL_LINK)))
                    .setTime(cursor.getString(cursor.getColumnIndex(QuestinContract.Chat.COL_TIME)))
                    .setStatus(cursor.getInt(cursor.getColumnIndex(QuestinContract.Chat.COL_STATUS)));
            chatList.add(chatModel);
        }
        cursor.close();
        db.close();
        return chatList;
    }

    public void addChatData(ArrayList<ChatModel> chatData, String senderId) {
        SQLiteDatabase db = getWritableDatabase();
        for (ChatModel chatModel : chatData) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(QuestinContract.Chat.COL_SENDER_ID, senderId);
            contentValues.put(QuestinContract.Chat.COL_IS_MINE, chatModel.getIsMine());
            contentValues.put(QuestinContract.Chat.COL_CATEGORY, chatModel.getCategory());
            contentValues.put(QuestinContract.Chat.COL_TEXT, chatModel.getText());
            contentValues.put(QuestinContract.Chat.COL_LINK, chatModel.getLink());
            contentValues.put(QuestinContract.Chat.COL_TIME, chatModel.getTime());
            contentValues.put(QuestinContract.Chat.COL_STATUS, chatModel.getStatus());

            long result = db.insert(QuestinContract.Chat.TABLE_NAME, null, contentValues);
        }
        db.close();
    }

    /**
     * Method to store a single message received by the user,
     * hence COL_IS_MINE will be MINE_NO
     *
     * @param fcmChatModel FcmChatModel object of the message received by the user
     */
    public void addChatMessage(FcmChatModel fcmChatModel, int status) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuestinContract.Chat.COL_SENDER_ID, fcmChatModel.getSenderId());
        contentValues.put(QuestinContract.Chat.COL_IS_MINE, QuestinContract.MINE_NO);
        contentValues.put(QuestinContract.Chat.COL_CATEGORY, fcmChatModel.getCategory());
        contentValues.put(QuestinContract.Chat.COL_TEXT, fcmChatModel.getText());
        contentValues.put(QuestinContract.Chat.COL_LINK, fcmChatModel.getLink());
        contentValues.put(QuestinContract.Chat.COL_TIME, fcmChatModel.getTime());
        contentValues.put(QuestinContract.Chat.COL_STATUS, status);

        db.insert(QuestinContract.Chat.TABLE_NAME, null, contentValues);
        db.close();
    }


    /**
     * Method to update the link of a message from server url to a local uri
     *
     * @param chatModel {@link ChatModel} object to update
     * @param senderId  id of the sender to find the right row
     */
    public void updateChatMessageLink(ChatModel chatModel, String senderId) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuestinContract.Chat.COL_LINK, chatModel.getLink());
        Log.e("link update",chatModel.getLink()+" grp"+senderId);

        db.update(QuestinContract.Chat.TABLE_NAME, contentValues,
                QuestinContract.Chat.COL_SENDER_ID + " =? AND " + QuestinContract.Chat.COL_TIME + " =?",
                new String[]{senderId, chatModel.getTime()});
        db.close();
    }


//    public void updateChatGroupMessageLink(ChatModel chatModel, String senderId) {
//        SQLiteDatabase db = getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(QuestinContract.Chat.COL_LINK, chatModel.getLink());
//        db.update(QuestinContract.Chat.TABLE_NAME, contentValues,
//                QuestinContract.Chat.COL_SENDER_ID + " =? AND " + QuestinContract.Chat.COL_TIME + " =?",
//                new String[]{senderId, chatModel.getTime()});
//        db.close();
//    }



    public ArrayList<GroupChatModel> getGroupChatList(String groupId) {
        SQLiteDatabase db = getReadableDatabase();
        String[] columns = new String[]{


                QuestinContract.GroupChat.GROUP_IS_MINE,
                QuestinContract.GroupChat.GROUP_CATEGORY,
                QuestinContract.GroupChat.GROUP_TEXT,
                QuestinContract.GroupChat.GROUP_LINK,
                QuestinContract.GroupChat.GROUP_TIME,
                QuestinContract.GroupChat.GROUP_STATUS,
                QuestinContract.GroupChat.GROUP_SENDER_ID
        };
        ArrayList<GroupChatModel> chatList = new ArrayList<>();

        Cursor cursor = db.query(QuestinContract.GroupChat.TABLE_NAME, columns,
                QuestinContract.GroupChat.GROUP_GROUP_ID + " =?", new String[]{groupId},
                null, null, QuestinContract.GroupChat.GROUP_TIME);
        while (cursor.moveToNext()) {
            GroupChatModel chatModel = new GroupChatModel()

                    .setIsMine(cursor.getInt(cursor.getColumnIndex(QuestinContract.GroupChat.GROUP_IS_MINE)))
                    .setCategory(cursor.getInt(cursor.getColumnIndex(QuestinContract.GroupChat.GROUP_CATEGORY)))
                    .setText(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupChat.GROUP_TEXT)))
                    .setLink(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupChat.GROUP_LINK)))
                    .setTime(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupChat.GROUP_TIME)))
                    .setStatus(cursor.getInt(cursor.getColumnIndex(QuestinContract.GroupChat.GROUP_STATUS)))
                    .setSenderId(cursor.getString(cursor.getColumnIndex(QuestinContract.GroupChat.GROUP_SENDER_ID)));
            chatList.add(chatModel);
        }
        cursor.close();
        db.close();
        return chatList;
    }

    public void addGroupChatData(ArrayList<GroupChatModel> chatData, String groupId) {
        SQLiteDatabase db = getWritableDatabase();
        for (GroupChatModel chatModel : chatData) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(QuestinContract.GroupChat.GROUP_GROUP_ID, groupId);
            contentValues.put(QuestinContract.GroupChat.GROUP_IS_MINE, chatModel.getIsMine());
            contentValues.put(QuestinContract.GroupChat.GROUP_CATEGORY, chatModel.getCategory());
            contentValues.put(QuestinContract.GroupChat.GROUP_TEXT, chatModel.getText());
            contentValues.put(QuestinContract.GroupChat.GROUP_LINK, chatModel.getLink());
            contentValues.put(QuestinContract.GroupChat.GROUP_TIME, chatModel.getTime());
            contentValues.put(QuestinContract.GroupChat.GROUP_STATUS, chatModel.getStatus());
             contentValues.put(QuestinContract.GroupChat.GROUP_SENDER_ID,chatModel.getSenderId());
            long result = db.insert(QuestinContract.GroupChat.TABLE_NAME, null, contentValues);
        }
        db.close();
    }


    public void addGroupChatMessage(FcmGroupChatModel fcmChatModel, int status) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuestinContract.GroupChat.GROUP_SENDER_ID, fcmChatModel.getSenderId());
        contentValues.put(QuestinContract.GroupChat.GROUP_GROUP_ID, fcmChatModel.getGroupId());
        contentValues.put(QuestinContract.GroupChat.GROUP_IS_MINE, QuestinContract.MINE_NO);
        contentValues.put(QuestinContract.GroupChat.GROUP_CATEGORY, fcmChatModel.getCategory());
        contentValues.put(QuestinContract.GroupChat.GROUP_TEXT, fcmChatModel.getText());
        contentValues.put(QuestinContract.GroupChat.GROUP_LINK, fcmChatModel.getLink());
        contentValues.put(QuestinContract.GroupChat.GROUP_TIME, fcmChatModel.getTime());
        contentValues.put(QuestinContract.GroupChat.GROUP_STATUS, status);

        db.insert(QuestinContract.GroupChat.TABLE_NAME, null, contentValues);
        db.close();
    }




    public void updateGroupChatMessageLink(GroupChatModel chatModel, String groupId) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(QuestinContract.GroupChat.GROUP_LINK, chatModel.getLink());

        Log.e("link update",chatModel.getLink()+" grp"+groupId);
        db.update(QuestinContract.GroupChat.TABLE_NAME, contentValues,
                QuestinContract.GroupChat.GROUP_GROUP_ID + " =? AND " + QuestinContract.Chat.COL_TIME + " =?",
                new String[]{groupId, chatModel.getTime()});
        db.close();
    }



    /*need to check*/

    public boolean doesConversationExist(String senderId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME,
                new String[]{COL_SENDER_ID},
                COL_SENDER_ID + " =? ", new String[]{senderId},
                null, null, null);
        if (cursor.moveToNext()) {
            cursor.close();
            db.close();
            return true;
        } else {
            cursor.close();
            db.close();
            return false;
        }
    }

    /*  SQLiteDatabase dataBase = getWritableDatabase();

//    Deleting all records from database table
        dataBase.delete(QuestinContract.CalenderEvents.TABLE_NAME, null, null);

        dataBase.close();*/


    // Deleting single chat table
    public void deleteChildTableMessage(Context mContext, String id,
                                       String TableName) {

        Log.e("database handlar ", "deleteChatchild()CAll    " + TableName
                + "    " + id);
        SQLiteDatabase db = getWritableDatabase();
      /*  int deleteStatus = db.delete(TableName,
                COL_SENDER_ID + " = " +id,
                null );*/
        db.delete(QuestinContract.Chat.TABLE_NAME, null, null);

        db.close();

        // db.delete(
        // TableName,
        // SQLitesStaticData.KEY_ID+ " = ?",
        // new String[] { String.valueOf(mCreateGroupsContents.getID()) });

       /* db.close();
       */


    }



    public void deleteSingleChatList(String senderId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, QuestinContract.Chat.COL_SENDER_ID  + "    = ?", new String[]{String.valueOf(senderId)});
    }


    public void deleteSingleChatConversation(String senderId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, QuestinContract.Chat.COL_SENDER_ID + "    = ?", new String[]{String.valueOf(senderId)});
    }






    // Deleting single chat table
    public int deleteGroupChildTableMessage(Context mContext, String id,
                                       String TableName) {

        Log.e("database handlar ", "deleteChatchild()CAll    " + TableName
                + "    " + id);
        SQLiteDatabase db = getReadableDatabase();
        int deleteStatus = db.delete(TableName,
                QuestinContract.GroupChat.GROUP_GROUP_ID+ " = " +id,
                null );

        // db.delete(
        // TableName,
        // SQLitesStaticData.KEY_ID+ " = ?",
        // new String[] { String.valueOf(mCreateGroupsContents.getID()) });

        db.close();
        return deleteStatus;
    }

    public void deleteSingleGroupChatList(String senderId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, QuestinContract.GroupConversationList.GROUP_SENDER_ID + "    = ?", new String[]{String.valueOf(senderId)});
    }
    public boolean updateUnreadMsgChatMessage(ConversationModel chatModel) {

        SQLiteDatabase db = getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_UNREAD_COUNT, 0);

        Log.e("unread update",chatModel.getUnreadCount()+" grp"+chatModel.getSenderId());
//          db.update(TABLE_NAME, contentValues,
//                QuestinContract.Conversation.COL_SENDER_ID + " = ? ",
//                new String[]{ String.valueOf(chatModel.getSenderId().toString().trim()) });
        int rowreflected = db.update(TABLE_NAME, contentValues,
                COL_SENDER_ID + " = ?",
                new String[] { String.valueOf(chatModel.getSenderId().toString().trim()) });
        System.out.println("reflect number of row " + rowreflected);

        db.close();

        return true;
    }
//    public void updateRecord(ContactModel contact) {
//        database = this.getReadableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put(COLUMN_FIRST_NAME, contact.getFirstName());
//        contentValues.put(COLUMN_LAST_NAME, contact.getLastName());
//        database.update(TABLE_NAME, contentValues, COLUMN_ID + " = ?", new String[]{contact.getID()});
//        database.close();
//    }
    public int getConversationUnreadCount(String senderId) {
        int count;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME,
                new String[]{COL_UNREAD_COUNT},
                COL_SENDER_ID + " =?", new String[]{senderId},
                null, null, null);
        if (cursor.moveToNext()) {
            count = cursor.getInt(cursor.getColumnIndex(COL_UNREAD_COUNT));
        } else {
            count = 0;
        }
        cursor.close();
        db.close();
        return count;

    }
    public int getGroupConversationUnreadCount(String groupId) {
        int count;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(QuestinContract.GroupConversationList.TABLE_NAME,
                new String[]{QuestinContract.GroupConversationList.GROUP_UNREAD_COUNT},
                QuestinContract.GroupConversationList.GROUP_GROUP_ID + " =?", new String[]{groupId},
                null, null, null);
        if (cursor.moveToNext()) {
            count = cursor.getInt(cursor.getColumnIndex(COL_UNREAD_COUNT));
        } else {
            count = 0;
        }
        cursor.close();
        db.close();
        return count;
    }


    public boolean doesGroupConversationExist(String groupId) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(QuestinContract.GroupConversationList.TABLE_NAME,
                new String[]{QuestinContract.GroupConversationList.GROUP_GROUP_ID},
                QuestinContract.GroupConversationList.GROUP_GROUP_ID + " =?", new String[]{groupId},
                null, null, null);
        if (cursor.moveToNext()) {
            cursor.close();
            db.close();
            return true;
        } else {
            cursor.close();
            db.close();
            return false;
        }
    }








   


    /*calender event database*/

    public void insertcalenderevent(EventModel events) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(QuestinContract.CalenderEvents.EVENT_START_DATE, events.getStrDate());
            values.put(QuestinContract.CalenderEvents.EVENT_END_DATE, events.getStrDateEnd());
            values.put(QuestinContract.CalenderEvents.EVENT_START_TIME, events.getStrStartTime());
            values.put(QuestinContract.CalenderEvents.EVENT_END_TIME, events.getStrEndTime());
            values.put(QuestinContract.CalenderEvents.EVENT_TITTLE, events.getStrName());
            values.put(QuestinContract.CalenderEvents.EVENT_DESCRIPTION, events.getStrDetails());
            values.put(QuestinContract.CalenderEvents.EVENT_MAINID, events.getDisplayid());


            db.insert(QuestinContract.CalenderEvents.TABLE_NAME, null, values);
            db.setTransactionSuccessful();
        }catch(Exception e){
            Log.e("Error in data","Error in transaction");
        }finally {
            db.endTransaction();
            db.close();
        }
    }






    // code to get the single contact
    EventModel getEventTittle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(QuestinContract.CalenderEvents.TABLE_NAME, new String[] {QuestinContract.CalenderEvents.EVENT_ID,
                        QuestinContract.CalenderEvents.EVENT_START_DATE,QuestinContract.CalenderEvents.EVENT_END_DATE,QuestinContract.CalenderEvents.EVENT_START_TIME,
                        QuestinContract.CalenderEvents.EVENT_END_TIME, QuestinContract.CalenderEvents.EVENT_TITTLE, QuestinContract.CalenderEvents.EVENT_DESCRIPTION},
                QuestinContract.CalenderEvents.EVENT_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        EventModel events = new EventModel(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7));
        // return contact
        return events;
    }


    // Getting All data
    public List<EventModel> getAllvalues() {
        List<EventModel> modelmain = new ArrayList<EventModel>();
        // Select All Query
       String selectQuery = "SELECT  * FROM " + QuestinContract.CalenderEvents.TABLE_NAME;



        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EventModel model = new EventModel();
                model.setId(Integer.parseInt(cursor.getString(0)));
                model.setStrDate(cursor.getString(1));
                model.setStrDateEnd(cursor.getString(2));
                model.setStrStartTime(cursor.getString(3));
                model.setStrEndTime(cursor.getString(4));
                model.setStrName(cursor.getString(5));
                model.setStrDetails(cursor.getString(6));
                model.setDisplayid(cursor.getString(7));



                modelmain.add(model);
            } while (cursor.moveToNext());
        }

        return modelmain;
    }


    /*collage events in callender*/



    public void insertcollagecalenderevent(EventModel events) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(QuestinContract.CalenderCollageEvents.EVENT_START_DATE, events.getStrDate());
            values.put(QuestinContract.CalenderCollageEvents.EVENT_END_DATE, events.getStrDateEnd());
            values.put(QuestinContract.CalenderCollageEvents.EVENT_START_TIME, events.getStrStartTime());
            values.put(QuestinContract.CalenderCollageEvents.EVENT_END_TIME, events.getStrEndTime());
            values.put(QuestinContract.CalenderCollageEvents.EVENT_TITTLE, events.getStrName());
            values.put(QuestinContract.CalenderCollageEvents.EVENT_DESCRIPTION, events.getStrDetails());
            values.put(QuestinContract.CalenderCollageEvents.EVENT_MAINID, events.getDisplayid());


            db.insert(QuestinContract.CalenderCollageEvents.TABLE_NAME, null, values);
            db.setTransactionSuccessful();
        }catch(Exception e){
            Log.e("Error in data","Error in transaction");
        } finally {
            db.endTransaction();
            db.close();

        }
    }




    // code to get the single contact
    EventModel getCollageEventTittle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(QuestinContract.CalenderCollageEvents.TABLE_NAME, new String[] {QuestinContract.CalenderCollageEvents.EVENT_ID,
                        QuestinContract.CalenderCollageEvents.EVENT_START_DATE,QuestinContract.CalenderCollageEvents.EVENT_END_DATE,QuestinContract.CalenderCollageEvents.EVENT_START_TIME,
                        QuestinContract.CalenderCollageEvents.EVENT_END_TIME, QuestinContract.CalenderCollageEvents.EVENT_TITTLE, QuestinContract.CalenderCollageEvents.EVENT_DESCRIPTION},
                QuestinContract.CalenderCollageEvents.EVENT_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        EventModel events = new EventModel(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7));
        // return contact
        return events;
    }


    // Getting All data
    public List<EventModel> getAllCollageEvent() {
        List<EventModel> modelmain = new ArrayList<EventModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + QuestinContract.CalenderCollageEvents.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EventModel model = new EventModel();
                model.setId(Integer.parseInt(cursor.getString(0)));
                model.setStrDate(cursor.getString(1));
                model.setStrDateEnd(cursor.getString(2));
                model.setStrStartTime(cursor.getString(3));
                model.setStrEndTime(cursor.getString(4));
                model.setStrName(cursor.getString(5));
                model.setStrDetails(cursor.getString(6));
                model.setDisplayid(cursor.getString(7));



                modelmain.add(model);
            } while (cursor.moveToNext());
        }

        return modelmain;
    }

    /*calender Assignment database*/

    public void insertcalenderAssignment(EventModel events) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(QuestinContract.CalenderAssignment.EVENT_START_DATE, events.getStrDate());
            values.put(QuestinContract.CalenderAssignment.EVENT_END_DATE, events.getStrDateEnd());
            values.put(QuestinContract.CalenderAssignment.EVENT_START_TIME, events.getStrStartTime());
            values.put(QuestinContract.CalenderAssignment.EVENT_END_TIME, events.getStrEndTime());
            values.put(QuestinContract.CalenderAssignment.EVENT_TITTLE, events.getStrName());
            values.put(QuestinContract.CalenderAssignment.EVENT_DESCRIPTION, events.getStrDetails());
            values.put(QuestinContract.CalenderAssignment.EVENT_MAINID, events.getDisplayid());


            db.insert(QuestinContract.CalenderAssignment.TABLE_NAME, null, values);
            db.setTransactionSuccessful();
        }catch(Exception e){
            Log.e("Error in data","Error in transaction");
        } finally {
            db.endTransaction();
            db.close();
        }
    }



    // code to get the single contact
    EventModel getAssignmentTittle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(QuestinContract.CalenderAssignment.TABLE_NAME, new String[] {QuestinContract.CalenderAssignment.EVENT_ID,
                        QuestinContract.CalenderAssignment.EVENT_START_DATE,QuestinContract.CalenderAssignment.EVENT_END_DATE,QuestinContract.CalenderAssignment.EVENT_START_TIME,
                        QuestinContract.CalenderAssignment.EVENT_END_TIME, QuestinContract.CalenderAssignment.EVENT_TITTLE, QuestinContract.CalenderAssignment.EVENT_DESCRIPTION},
                QuestinContract.CalenderAssignment.EVENT_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        EventModel events = new EventModel(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7));
        // return contact
        return events;
    }


    // Getting All data
    public List<EventModel> getAllvaluesAssignment() {
        List<EventModel> modelmain = new ArrayList<EventModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + QuestinContract.CalenderAssignment.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EventModel model = new EventModel();
                model.setId(Integer.parseInt(cursor.getString(0)));
                model.setStrDate(cursor.getString(1));
                model.setStrDateEnd(cursor.getString(2));
                model.setStrStartTime(cursor.getString(3));
                model.setStrEndTime(cursor.getString(4));
                model.setStrName(cursor.getString(5));
                model.setStrDetails(cursor.getString(6));
                model.setDisplayid(cursor.getString(7));



                modelmain.add(model);
            } while (cursor.moveToNext());
        }

        return modelmain;
    }




      /*calender Exams database*/

    public void insertcalenderExams(EventModel events) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(QuestinContract.CalenderExams.EVENT_START_DATE, events.getStrDate());
            values.put(QuestinContract.CalenderExams.EVENT_END_DATE, events.getStrDateEnd());
            values.put(QuestinContract.CalenderExams.EVENT_START_TIME, events.getStrStartTime());
            values.put(QuestinContract.CalenderExams.EVENT_END_TIME, events.getStrEndTime());
            values.put(QuestinContract.CalenderExams.EVENT_TITTLE, events.getStrName());
            values.put(QuestinContract.CalenderExams.EVENT_DESCRIPTION, events.getStrDetails());
            values.put(QuestinContract.CalenderExams.EVENT_MAINID, events.getDisplayid());
            db.insert(QuestinContract.CalenderExams.TABLE_NAME, null, values);
            db.setTransactionSuccessful();

        }catch(Exception e){
        Log.e("Error in data","Error in transaction");
    } finally {
            db.endTransaction();
            db.close();
        }
    }



    // code to get the single contact
    EventModel getExamsTittle(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(QuestinContract.CalenderExams.TABLE_NAME, new String[] {QuestinContract.CalenderExams.EVENT_ID,
                        QuestinContract.CalenderExams.EVENT_START_DATE,QuestinContract.CalenderExams.EVENT_END_DATE,QuestinContract.CalenderExams.EVENT_START_TIME,
                        QuestinContract.CalenderExams.EVENT_END_TIME, QuestinContract.CalenderExams.EVENT_TITTLE, QuestinContract.CalenderExams.EVENT_DESCRIPTION},
                QuestinContract.CalenderExams.EVENT_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        EventModel events = new EventModel(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7));
        // return contact
        return events;
    }


    // Getting All data
    public List<EventModel> getAllvaluesExams() {
        List<EventModel> modelmain = new ArrayList<EventModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + QuestinContract.CalenderExams.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EventModel model = new EventModel();
                model.setId(Integer.parseInt(cursor.getString(0)));
                model.setStrDate(cursor.getString(1));
                model.setStrDateEnd(cursor.getString(2));
                model.setStrStartTime(cursor.getString(3));
                model.setStrEndTime(cursor.getString(4));
                model.setStrName(cursor.getString(5));
                model.setStrDetails(cursor.getString(6));
                model.setDisplayid(cursor.getString(7));

                modelmain.add(model);
            } while (cursor.moveToNext());
        }

        return modelmain;
    }







    public void deleteRecord() {

        SQLiteDatabase dataBase = getWritableDatabase();

//    Deleting all records from database table
        dataBase.delete(QuestinContract.CalenderEvents.TABLE_NAME, null, null);

        dataBase.close();
    }



    public void deleteRecordFromMyCollageEvents() {

        SQLiteDatabase dataBase = getWritableDatabase();

//    Deleting all records from database table
        dataBase.delete(QuestinContract.CalenderCollageEvents.TABLE_NAME, null, null);

        dataBase.close();
    }


    public void deleteRecordAssignment() {

        SQLiteDatabase dataBase = getWritableDatabase();

//    Deleting all records from database table
        dataBase.delete(QuestinContract.CalenderAssignment.TABLE_NAME, null, null);

        dataBase.close();
    }



    public void deleteRecordExams() {

        SQLiteDatabase dataBase = getWritableDatabase();

//    Deleting all records from database table
        dataBase.delete(QuestinContract.CalenderExams.TABLE_NAME, null, null);

        dataBase.close();
    }



    public void AddAttandanceOffline(AddAttendanceOfflineArray attandance) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(QuestinContract.AddAttendanceOffline.ATTENDANCE_UID, attandance.getUid());
            values.put(QuestinContract.AddAttendanceOffline.ATTENDANCE_VALUE, attandance.getAttendance());
            values.put(QuestinContract.AddAttendanceOffline.ATTENDANCE_COURSE_ID, attandance.getCourse_id());
            values.put(QuestinContract.AddAttendanceOffline.ATTENDANCE_CLASS_ID, attandance.getClasssId());
            values.put(QuestinContract.AddAttendanceOffline.ATTENDANCE_DATE, attandance.getDate());
            values.put(QuestinContract.AddAttendanceOffline.ATTENDANCE_TYPE, attandance.getType());



            db.insert(QuestinContract.AddAttendanceOffline.TABLE_NAME, null, values);
            db.setTransactionSuccessful();
        }catch(Exception e){
            Log.e("Error in data","Error in transaction");
        } finally {
            db.endTransaction();
            db.close();

        }
    }





    // Getting All data
    public List<AddAttendanceOfflineArray> getAllAttendance() {
        List<AddAttendanceOfflineArray> modelmain = new ArrayList<AddAttendanceOfflineArray>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + QuestinContract.AddAttendanceOffline.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                AddAttendanceOfflineArray model = new AddAttendanceOfflineArray();
                model.setUid(cursor.getString(1));
                model.setAttendance(cursor.getString(2));
                model.setCourse_id(cursor.getString(3));
                model.setClasssId(cursor.getString(4));
                model.setDate(cursor.getString(5));
                model.setType(cursor.getString(6));



                modelmain.add(model);
            } while (cursor.moveToNext());
        }

        return modelmain;
    }



    /*Delete All Attendance*/

    public void deleteAllAttendance() {

        SQLiteDatabase dataBase = getWritableDatabase();

//    Deleting all records from database table
        dataBase.delete(QuestinContract.AddAttendanceOffline.TABLE_NAME, null, null);

        dataBase.close();
    }


}






