package co.savm.calendersection;

import android.graphics.Color;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import co.savm.database.QuestinSQLiteHelper;
import co.savm.weekviewdisplay.WeekViewEvent;

public class WeekViewDisplay  extends BaseWeekActivity {


    int Months, CalenderID,CalenderEventStartDate,CalenderEventEndDate,  CalenderEventTimeStart1,CalenderEventTimeStart2,   CalenderEventtimeEnd1,CalenderEventtimeEnd2;
    int AssignmentMonths, AssignmentCalenderID ,AssignmentStartDate,AssignmentEndDate,  AssignmentTimeStart1,AssignmentTimeStart2,   AssignmenttimeEnd1,AssignmenttimeEnd2;
    int ExamMonths, ExamCalenderID ,ExamStartDate,ExamEndDate,  ExamTimeStart1,ExamTimeStart2,   ExamtimeEnd1,ExamtimeEnd2;

    int CollageEventMonths, CollageEventID ,CollageEventStartDate,CollageEventEndDate,  CollageEventTimeStart1,CollageEventTimeStart2,   CollageEventtimeEnd1,CollageEventtimeEnd2;

    List<EventModel> assignment;
    List<EventModel> classes;
    List<EventModel> exams;

    List<WeekViewEvent> events = new ArrayList<WeekViewEvent>();
    int newYearget, newMonthget;
    private QuestinSQLiteHelper questinSQLiteHelper;

    public int getRandomColor(){
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


    }









    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        // Populate the week view with some events.
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        events.clear();
       /*
        classes.clear();
        exams.clear();
        assignment.clear();
*/

        Log.d("Reading: ", "Reading all data..");

        List<EventModel> classes = questinSQLiteHelper.getAllvalues();

               /*    calll the data for classes in weekview*/
        for (EventModel classData : classes) {

            String log2 = classData.getStrDate()+classData.getStrDateEnd()+classData.getStrStartTime()+classData.getStrEndTime()+classData.getStrName();
            Log.d("value: ", log2);
            CalenderID = Integer.parseInt(classData.getDisplayid());
            String[] splited = classData.getStrDate().split("-");
            String split_one=splited[0];
            String split_month =splited[1];
            CalenderEventStartDate = Integer.parseInt(split_one);
            Months= Integer.parseInt(split_month);

            System.out.println("first: " + CalenderEventStartDate);
            String output = classData.getStrDateEnd().replaceAll("\\s+","");
            String[] splitend = output.split("-");
            String split_two=splitend[0];
            CalenderEventEndDate = Integer.parseInt(split_two);

            System.out.println("output: " + output);
            System.out.println("two: " + CalenderEventEndDate);

            SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mm aa");
            SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm");
            String time24 = null;
            try {
                time24 = outFormatx.format(inFormatx.parse(classData.getStrStartTime()));

                String[] split = time24.split(":");
                String split_time1=split[0];
                String split_time2=split[1];
                CalenderEventTimeStart1 = Integer.parseInt(split_time1);
                CalenderEventTimeStart2 = Integer.parseInt(split_time2);

                System.out.println("time in 24 hour format : " + CalenderEventTimeStart1+"@"+CalenderEventTimeStart2);


            } catch (ParseException e) {
                e.printStackTrace();
            }


            SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
            SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
            String time = null;
            try {
                time = outFormat.format(inFormat.parse(classData.getStrEndTime()));
                String[] split = time.split(":");
                String split_time1=split[0];
                String split_time2=split[1];
                CalenderEventtimeEnd1 = Integer.parseInt(split_time1);
                CalenderEventtimeEnd2 = Integer.parseInt(split_time2);

                System.out.println("time in 24 hour format : " + CalenderEventtimeEnd1+"@"+CalenderEventtimeEnd2);



            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.d("TAG", " Showclasses: " +"$"+classData.getId() +"$"+classData.getStrName()+"$"+CalenderEventStartDate+"$"+CalenderEventTimeStart1+"$"+CalenderEventTimeStart2+"$"+CalenderEventEndDate+"$"+CalenderEventtimeEnd2);


            Log.d("TAG", " DatesOnly: " +"..."+CalenderEventTimeStart1+"..."+CalenderEventTimeStart2+"..."+CalenderEventtimeEnd1+"..."+ CalenderEventtimeEnd2);


            WeekViewEvent evente = new WeekViewEvent(CalenderID,classData.getStrName(),2018,Months,CalenderEventStartDate,CalenderEventTimeStart1,CalenderEventTimeStart2,2018,Months,CalenderEventStartDate,CalenderEventtimeEnd1,CalenderEventtimeEnd2,"class");
            evente.setColor(getRandomColor());
            events.add(evente);


        }


        List<EventModel> exams = questinSQLiteHelper.getAllvaluesExams();



                 /*  calll the data for Assignments in weekview*/
        for (EventModel examsData : exams) {

            String log2 = examsData.getStrDate()+examsData.getStrDateEnd()+examsData.getStrStartTime()+examsData.getStrEndTime()+examsData.getStrName();
            Log.d("value: ", log2);
            ExamCalenderID = Integer.parseInt(examsData.getDisplayid());
            String[] splited = examsData.getStrDate().split("-");
            String split_one=splited[0];
            String split_month =splited[1];
            ExamStartDate = Integer.parseInt(split_one);
            ExamMonths= Integer.parseInt(split_month);
            ExamtimeEnd1 = Integer.parseInt(examsData.getStrEndTime());


            SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mm aa");
            SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm");
            String time24 = null;
            try {
                time24 = outFormatx.format(inFormatx.parse(examsData.getStrStartTime()));


                Date d = outFormatx.parse(time24);
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                cal.add(Calendar.MINUTE, ExamtimeEnd1);
                String newTime = outFormatx.format(cal.getTime());



                String[] split = time24.split(":");
                String split_time1=split[0];
                String split_time2=split[1];
                ExamTimeStart1 = Integer.parseInt(split_time1);
                ExamTimeStart2 = Integer.parseInt(split_time2);


                 String[] split2 = newTime.split(":");
                String split_time3=split2[0];
                String split_time4=split2[1];
                ExamtimeEnd1 = Integer.parseInt(split_time3);
                ExamtimeEnd2 = Integer.parseInt(split_time4);



                System.out.println("time in 24 hour format exam : " + ExamTimeStart1+"@"+ExamTimeStart2 +newTime);
                System.out.println("Exam time" +examsData.getStrStartTime()+"...."+ExamtimeEnd1+"...."+newTime);


            } catch (ParseException e) {
                e.printStackTrace();
            }















            Log.d("TAG", " Showexams: " +"$"+examsData.getId() +"$"+examsData.getStrName()+"$"+ExamStartDate+"$"+ExamEndDate+"$"+ExamTimeStart1+"$"+ExamTimeStart2+"$"+ExamtimeEnd1+"$"+ExamtimeEnd2);


            WeekViewEvent evente = new WeekViewEvent(ExamCalenderID,examsData.getStrName(),2018,ExamMonths,ExamStartDate,ExamTimeStart1,ExamTimeStart2,2018,ExamMonths,ExamStartDate,ExamtimeEnd1,ExamtimeEnd2,"exams");
            evente.setColor(getRandomColor());
            events.add(evente);


        }

        List<EventModel> assignment = questinSQLiteHelper.getAllvaluesAssignment();

       // assignment.clear();
           /*    calll the data for exams in weekview*/

           for (EventModel assignmentData : assignment) {

            String log2 = assignmentData.getStrDate()+assignmentData.getStrDateEnd()+assignmentData.getStrStartTime()+assignmentData.getStrEndTime()+assignmentData.getStrName();
            Log.d("value: ", log2);
            AssignmentCalenderID = Integer.parseInt(assignmentData.getDisplayid());
            String[] splited = assignmentData.getStrDate().split("-");
            String split_one=splited[0];
            String split_month =splited[1];
            AssignmentStartDate = Integer.parseInt(split_one);
            AssignmentMonths= Integer.parseInt(split_month);

            System.out.println("first: " + AssignmentStartDate);


            Log.d("TAG", " AssignmentShow: " +"$"+assignmentData.getId() +"$"+assignmentData.getStrName()+"$"+AssignmentStartDate+"$"+AssignmentEndDate+"$"+AssignmentTimeStart1+"$"+AssignmentTimeStart2+"$"+AssignmenttimeEnd1+"$"+AssignmenttimeEnd2);


            Calendar startTime = Calendar.getInstance();
            Calendar endTime = (Calendar) startTime.clone();


            startTime.set(Calendar.DAY_OF_MONTH, AssignmentStartDate);
            startTime.set(Calendar.HOUR_OF_DAY, 0);
            startTime.set(Calendar.MINUTE, 0);
            startTime.set(Calendar.MONTH,AssignmentMonths-1);
            startTime.set(Calendar.YEAR, newYear);
           // endTime = (Calendar) startTime.clone();
          //  endTime.add(Calendar.HOUR_OF_DAY, 23);
            endTime.set(Calendar.DAY_OF_MONTH, AssignmentStartDate);

           // WeekViewEvent evente = new WeekViewEvent(AssignmentCalenderID,assignmentData.getStrName(),2018,AssignmentMonths,AssignmentStartDate,0,0,2018,AssignmentMonths,AssignmentStartDate,24,0,"assign");
            WeekViewEvent evente  = new WeekViewEvent(AssignmentCalenderID, assignmentData.getStrName(), null, startTime, endTime, true,"assign");
            evente.setColor(getRandomColor());
            events.add(evente);


        }




        /*getAllCollageEvent*/

        List<EventModel> collageEvent = questinSQLiteHelper.getAllCollageEvent();

        //int CollageEventMonths, CollageEventID ,CollageEventStartDate,CollageEventEndDate,  CollageEventTimeStart1,CollageEventTimeStart2,   CollageEventtimeEnd1,CollageEventtimeEnd2;


        for (EventModel collegeevents : collageEvent) {

            String log2 = collegeevents.getStrDate()+collegeevents.getStrDateEnd()+collegeevents.getStrStartTime()+collegeevents.getStrEndTime()+collegeevents.getStrName();
            Log.d("value: ", log2);
            CollageEventID = Integer.parseInt(collegeevents.getDisplayid());
            String[] splited = collegeevents.getStrDate().split("-");
            String split_one=splited[0];
            String split_month =splited[1];
            CollageEventStartDate = Integer.parseInt(split_one);
            CollageEventMonths= Integer.parseInt(split_month);

            System.out.println("first: " + CollageEventStartDate);
            String output = collegeevents.getStrDateEnd().replaceAll("\\s+","");
            String[] splitend = output.split("-");
            String split_two=splitend[0];
            CollageEventEndDate = Integer.parseInt(split_two);

            System.out.println("output: " + output);
            System.out.println("two: " + CollageEventEndDate);

            SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mm aa");
            SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm");
            String time24 = null;
            try {
                time24 = outFormatx.format(inFormatx.parse(collegeevents.getStrStartTime()));

                String[] split = time24.split(":");
                String split_time1=split[0];
                String split_time2=split[1];
                CollageEventTimeStart1 = Integer.parseInt(split_time1);
                CollageEventTimeStart2 = Integer.parseInt(split_time2);

                System.out.println("time in 24 hour format : " + CollageEventTimeStart1+"@"+CollageEventTimeStart2);


            } catch (ParseException e) {
                e.printStackTrace();
            }


            SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
            SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
            String time = null;
            try {
                time = outFormat.format(inFormat.parse(collegeevents.getStrEndTime()));
                String[] split = time.split(":");
                String split_time1=split[0];
                String split_time2=split[1];
                CollageEventtimeEnd1 = Integer.parseInt(split_time1);
                CollageEventtimeEnd2 = Integer.parseInt(split_time2);

                System.out.println("time in 24 hour format : " + CollageEventtimeEnd1+"@"+CollageEventtimeEnd2);



            } catch (ParseException e) {
                e.printStackTrace();
            }

            Log.d("TAG", " Showclasses: " +"$"+collegeevents.getId() +"$"+collegeevents.getStrName()+"$"+CollageEventStartDate+"$"+CollageEventTimeStart1+"$"+CollageEventTimeStart2+"$"+CollageEventtimeEnd1+"$"+CollageEventtimeEnd2);


            Log.d("TAG", " DatesOnlycoll: " +"..."+CollageEventTimeStart1+"..."+CollageEventTimeStart2+"..."+CollageEventtimeEnd1+"..."+ CollageEventtimeEnd2);


            WeekViewEvent evente = new WeekViewEvent(CollageEventID,collegeevents.getStrName(),2018,CollageEventMonths,CollageEventStartDate,CollageEventTimeStart1,CollageEventTimeStart2,2018,Months,CollageEventStartDate,CollageEventtimeEnd1,CollageEventtimeEnd2,"collageEvent");
            evente.setColor(getRandomColor());
            events.add(evente);


        }




        List<WeekViewEvent> matchedEvents = new ArrayList<WeekViewEvent>();
        for (WeekViewEvent event : events) {
            if (eventMatches(event, newYear, newMonth)) {
                matchedEvents.add(event);

            }
        }
        return matchedEvents;

    }

    private boolean eventMatches(WeekViewEvent event, int year, int month) {
        return (event.getStartTime().get(Calendar.YEAR) == year && event.getStartTime().get(Calendar.MONTH) == month-1)
                || (event.getEndTime().get(Calendar.YEAR) == year && event.getEndTime().get(Calendar.MONTH) == month - 1);
    }




}








