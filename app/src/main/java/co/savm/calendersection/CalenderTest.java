package co.savm.calendersection;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import co.savm.R;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CalenderTest extends BaseAppCompactActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener, View.OnClickListener {
    TextView TV_StartDate,TV_StartTime,TV_endDate,TV_EndTime;
    Switch AllDayOn;
    EditText edittittle,editDetails;
    String  tittle,Details, timeEnd, StartDate, TimeStart,EndDate, startDate,  startfrom,format, weekmon, weektues, weekwedns, weekthur, weekfri, weeksat, weeksun, classrepeat;
    private int CalendarHour, CalendarMinute;
    TimePickerDialog timepickerdialog;
    private Calendar mcalender;
    CheckBox repeat,mon, tue, wed, thur, fri, sat, sun;
    LinearLayout Weeklayoutlayout;
    boolean isPressed = false;
    StringBuilder Weekselect;
    private int year, month, day, week;
    private int startDay, startMonth, startYear;
    Double Lat,Long;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;
    JSONObject ClassDetailsJson;
    //To store longitude and latitude from map
    private double longitude;
    private double latitude;
    private CalenderTestCreateAuthTask calendertestcreateAuthTask = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender_test);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        edittittle = findViewById(R.id.edittittle);
        editDetails = findViewById(R.id.Details);
        TV_StartDate = findViewById(R.id.StartDate);
        TV_endDate = findViewById(R.id.endDate);
        TV_StartTime = findViewById(R.id.StartTime);
        TV_EndTime = findViewById(R.id.EndTime);
        mon = findViewById(R.id.mon);
        tue = findViewById(R.id.tue);
        wed = findViewById(R.id.wed);
        thur = findViewById(R.id.thu);
        fri = findViewById(R.id.fri);
        sat = findViewById(R.id.sat);
        sun = findViewById(R.id.sun);
        repeat = findViewById(R.id.repeat);
        Weeklayoutlayout = findViewById(R.id.Weeklayoutlayout);
        mcalender = Calendar.getInstance();
        CalendarHour = mcalender.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = mcalender.get(Calendar.MINUTE);
        TV_StartDate.setOnClickListener(this);
        TV_endDate.setOnClickListener(this);
        TV_StartTime.setOnClickListener(this);
        TV_EndTime.setOnClickListener(this);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String  formattedDate = sdf.format(c.getTime());

        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(mcalender.getTime());


        StartDate = formattedDate2;
        EndDate = formattedDate2;



        TV_StartDate.setText(formattedDate);
        TV_endDate.setText(formattedDate);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String time = simpleDateFormat.format(mcalender.getTime());

        TV_StartTime.setText(time);
        TV_EndTime.setText(time);
        SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
        Date date = null;
        try {
            date = parseFormat.parse(time);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        String time24format = displayFormat.format(date);

        System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));





        TimeStart =time24format;
        timeEnd =time24format;

        repeat.setOnClickListener(this);
        mon.setOnClickListener(this);
        tue.setOnClickListener(this);
        wed.setOnClickListener(this);
        thur.setOnClickListener(this);
        fri.setOnClickListener(this);
        sat.setOnClickListener(this);
        sun.setOnClickListener(this);

        Weekselect =new StringBuilder();


    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.StartDate:
                hideKeyBoard(v);

                StartdateDialog();
                break;

            case R.id.endDate:
                hideKeyBoard(v);

                StartEnddateDialog();
                break;

            case R.id.repeat:
                hideKeyBoard(v);


                if (((CheckBox) v).isChecked()) {

                    classrepeat = "1";
                    Weeklayoutlayout.setVisibility(View.VISIBLE);

                }else {
                    classrepeat = "0";
                    Weeklayoutlayout.setVisibility(View.GONE);

                }
                break;

            case R.id.StartTime:
                hideKeyBoard(v);


                timepickerdialog = new TimePickerDialog(CalenderTest.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }


                                TV_StartTime.setText(hourOfDay + ":" + minute + format);
                                String Slow =(hourOfDay + ":" + minute + format);



                                DateFormat f1 = new SimpleDateFormat("h:mma", Locale.US);

                                try {
                                    Date d = f1.parse(Slow);
                                    DateFormat f2 = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                    String FF = f2.format(d).toLowerCase();
                                    Log.d("TAG", "f2 " + FF.toString());
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }




                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String Time = null;
                                try {
                                    Time = outFormatx.format(inFormatx.parse(Slow));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatS : " + Time);

                                TimeStart=Time;


                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();
                break;
            case R.id.EndTime:
                hideKeyBoard(v);
                timepickerdialog = new TimePickerDialog(CalenderTest.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                if (hourOfDay == 0) {

                                    hourOfDay += 12;

                                    format = "am";
                                }
                                else if (hourOfDay == 12) {

                                    format = "pm";

                                }
                                else if (hourOfDay > 12) {

                                    hourOfDay -= 12;

                                    format = "pm";

                                }
                                else {

                                    format = "am";
                                }


                                String SlowEnd =(hourOfDay + ":" + minute + format);
                                TV_EndTime.setText(hourOfDay + ":" + minute + format);

                                Log.d("TAG", "EndTime " + TV_EndTime);


                                SimpleDateFormat inFormatx = new SimpleDateFormat("hh:mma",Locale.US);
                                SimpleDateFormat outFormatx = new SimpleDateFormat("HH:mm:ss",Locale.US);
                                String time24 = null;
                                try {
                                    time24 = outFormatx.format(inFormatx.parse(SlowEnd));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                System.out.println("time in 24 hour formatE : " + time24);

                                timeEnd=time24;


                            }
                        },
                        CalendarHour, CalendarMinute, false);
                timepickerdialog.show();

                break;



        }
    }

    private void StartdateDialog() {
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }


    private void StartEnddateDialog() {
//        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
//
//            @Override
//            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {
//
//                showDateEnd(mYear, mMonth + 1, mDay);
//
//            }
//        };
//
//        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
//       // dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
//        Calendar c = Calendar.getInstance();
//        // Change date
//        c.set(Calendar.YEAR, startYear);
//        c.set(Calendar.MONTH, startMonth);
//        c.set(Calendar.DATE, startDay);
//        Date newDate = c.getTime();
//
//        dpDialog.getDatePicker().setMinDate(newDate.getTime() - 1000);
//        dpDialog.show();

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {

                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateEnd(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, listener, year, month, day);
        dpDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {
        Log.d("TAG", "Start Date: " + day + "-" + month + "-" + year);
        StartDate = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        TV_StartDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));

        TV_endDate.setText(TV_StartDate.getText().toString());
        EndDate =StartDate;

    }

    private void showDateEnd(int year, int month, int day) {
        Log.d("TAG", "End Date: " + day + "-" + month + "-" + year);
        EndDate = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        TV_endDate.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));


    }








    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.submit_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Submit:

                AddEventinCalenderProcess();

                return true;

            case android.R.id.home:

                Intent i = new Intent(CalenderTest.this,CalenderMain.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void selectdays() {

        int totalselect = 0;

        Weekselect.append("");

        if (mon.isChecked()) {
            weekmon ="MO";
            Weekselect.append(weekmon+","+" " );
            totalselect += 1;
        }

        if (tue.isChecked()) {
            weektues ="TU";
            Weekselect.append(weektues+","+" " );
            totalselect += 1;
        }
        if (wed.isChecked()) {
            weekwedns ="WE";
            Weekselect.append(weekwedns+","+" " );
            totalselect += 1;
        }

        if (thur.isChecked()) {
            weekthur ="TH";
            Weekselect.append(weekthur+","+" ");
            totalselect += 1;
        }
        if (fri.isChecked()) {
            weekfri ="FR";
            Weekselect.append(weekfri+","+" ");
            totalselect += 1;
        }

        if (sat.isChecked()) {
            weeksat ="SA";
            Weekselect.append(weeksat+","+" ");
            totalselect += 1;
        }
        if (sun.isChecked()) {
            weeksun ="SU";
            Weekselect.append(weeksun+","+" ");
            totalselect += 1;
        }

        // Weekselect.append("\nTotal: " + totalselect);
     //   Toast.makeText(getApplicationContext(), Weekselect.toString(), Toast.LENGTH_LONG).show();

    }


    private void AddEventinCalenderProcess() {
        selectdays();

        edittittle.setError(null);
        editDetails.setError(null);



        // Store values at the time of the login attempt.
        tittle = edittittle.getText().toString().trim();
        Details = editDetails.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;


        if (android.text.TextUtils.isEmpty(tittle)) {
            focusView = edittittle;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
            // Check for a valid email address.
        }
       /* else if (co.questin.utils.TextUtils.isNullOrEmpty(Details)) {
            // check for First Name
            focusView = editDetails;
            cancel = true;
            showToast(getString(R.string.error_field_required));
        }*/

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            ClassDetailsJson = new JSONObject();

            try {
                ClassDetailsJson.put("start_date", StartDate);
                ClassDetailsJson.put("end_date", EndDate);
                ClassDetailsJson.put("start_time", TimeStart);
                ClassDetailsJson.put("end_time", timeEnd);
                ClassDetailsJson.put("week", Weekselect);
                ClassDetailsJson.put("repeat", classrepeat);


                Log.d("TAG", "classdetails: " + ClassDetailsJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }


                    calendertestcreateAuthTask = new CalenderTestCreateAuthTask();
            calendertestcreateAuthTask.execute();

        }
    }


    private class CalenderTestCreateAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("type","my_content")
                    .add("title",tittle)
                    .add("field_my_content_type","exam")
                    .add("body",Details)
                    .add("field_dept_location[lat]", String.valueOf(Lat))
                    .add("field_dept_location[lng]", String.valueOf(Long))
                    .add("field_class_time",ClassDetailsJson.toString())
                    .build();
            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_TEACHERCREATECLASS,body);

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CalenderTest.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject responce) {
            calendertestcreateAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        //showAlertDialog(msg);
                        Intent i = new Intent(CalenderTest.this,CalenderMain.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finish();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();


            }
        }

        @Override
        protected void onCancelled() {
            calendertestcreateAuthTask = null;
            hideLoading();



        }
    }












    @Override
    public void onBackPressed() {
        Intent i = new Intent(CalenderTest.this,CalenderMain.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }



    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);


            //moving the map to location
            moveMap();

        }
    }

    //Function to move the map
    //Function to move the map
    //Function to move the map
    private void moveMap() {
        //String to display current latitude and longitude
        String msg = latitude + ", " + longitude;
        Log.d("TAG", "msg: " + msg);


        //Creating a LatLng Object to store Coordinates


        if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());
            LatLng latLng = new LatLng(Lat, Long);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title(SessionManager.getInstance(getActivity()).getCollage().getTitle())); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

            //Displaying current coordinates in toast
            //  Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }else {

            LatLng latLng = new LatLng(latitude, longitude);
            Lat=latitude;
            Long =longitude;
            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(true) //Making the marker draggable
                    .title("My Location")); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
        getCurrentLocation();
        moveMap();

    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //Clearing all the markers
        mMap.clear();

        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
     /*   //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //Moving the map
        moveMap();*/

    }


}
