/*
 * Copyright (c) 2016. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package co.savm.network;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by developer on 30/12/16.
 */
public class OkHttpResponse {

    public OkHttpResponse() {

    }

    public static JSONObject getDataFromWeb(RequestBody body, String MAIN_URL) {
        try {
            final GetOkHttpClient client = new GetOkHttpClient();
            //HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
            HostnameVerifier hostnameVerifier=new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {

                    HostnameVerifier hv= (HostnameVerifier) client.getUnsafeOkHttpClient();
                    return hv.verify("https://questin.co",sslSession);
                }
            };

            Request request = new Request.Builder()
                    .url(MAIN_URL)
                    .post(body)
                    .build();

            Response response = client.getUnsafeOkHttpClient().newCall(request).execute();
            Log.d("OkHttpResponseJson", "RESPONSE: "+response.body().string());
            JSONObject jsonObject = new JSONObject(response.body().string());
            return jsonObject;
        } catch (@NonNull IOException | JSONException e) {
            Log.e("OkHttpResponseJson", e.getLocalizedMessage());
            return null;
        }
    }


}
