/*
 * Copyright (c) 2017. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package co.savm.network;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import co.savm.utils.SessionManager;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;

/**
 * Created by developer on 30/1/17.
 */
public class ApiCall extends Activity {


    private static Activity gContext;
    private static final String TAG = "ApiCall";

    public static void setContext( Activity activity) {
        gContext = activity;
    }

    public static Activity getActivity() {
        return gContext;
    }

    public static Context getContext() {
        return gContext;
    }



    //GET network request
    public static String GET(OkHttpClient client, String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .header("Content-Type", "application/json")
                .header("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ")
                .header("Accept-Language", "application/json")
                .build();

       /* client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();
*/

        Response response = client.newCall(request).execute();
        return response.body().string();
    }



    //POST network request
    public static String POST(OkHttpClient client, String url, RequestBody body) throws IOException {
        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        Request request = new Request.Builder()
                .url(url)
                .header("Content-Type", "application/json")
                .header("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ")
                .header("Accept-Language", "application/json")
                .post(body)
                .build();

       /* client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();*/

        Response response = client.newCall(request).execute();
        String res = response.body().string();
        final Buffer buffer = new Buffer();
        request.newBuilder().build().body().writeTo(buffer);

        Log.d("ApiCall "+new Date().getTime(),"URL: " + url + "\nPARAMS: [" + buffer.readUtf8() + "]\nREPSONSE: " +res);
        return res;
    }




    //POST network request
    public static String POSTHEADER(OkHttpClient client, String url, RequestBody body) throws IOException {
        String Cookies= SessionManager.getInstance(gContext).getaccesstoken().sessionName+"="+SessionManager.getInstance(gContext).getaccesstoken().sessionID;
        Log.d("TAG", "Cookies: " + Cookies);

        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        Request request = new Request.Builder()
                .url(url)
                .header("Accept-Language", "application/json")
                .header("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ")
                .header("X-CSRF-Token", SessionManager.getInstance(gContext).getaccesstoken().accesstoken)
                .header("Cookie",Cookies)
                .header("Content-Type", "application/json")
                .post(body)
                .build();


        Log.d("TAG", "secciontoken: " + SessionManager.getInstance(gContext).getaccesstoken().accesstoken);
        Response responsee = client.newCall(request).execute();
        String res = responsee.body().string();
        final Buffer buffer = new Buffer();
        request.newBuilder().build().body().writeTo(buffer);

        Log.d("ApiCall "+new Date().getTime(),"URL: " + url + "\nPARAMS: [" + buffer.readUtf8() + "]\nREPSONSE: " +res);
        return res;
    }


    //POST network request
    public static String POSTHEADERCREATE(OkHttpClient client, String url, RequestBody body) throws IOException {
        String Cookies= SessionManager.getInstance(gContext).getaccesstoken().sessionName+"="+SessionManager.getInstance(gContext).getaccesstoken().sessionID;
        Log.d("TAG", "Cookies: " + Cookies);

        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        Request request = new Request.Builder()
                .url(url)
                .header("Accept-Language", "application/json")
                .header("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ")
                .header("X-CSRF-Token", SessionManager.getInstance(gContext).getaccesstoken().accesstoken)
                .header("Cookie",Cookies)
                .header("Content-Type", "application/json")
                .post(body)
                .build();

        client = new OkHttpClient.Builder()
                .connectTimeout(50, TimeUnit.SECONDS)
                .writeTimeout(50, TimeUnit.SECONDS)
                .readTimeout(50, TimeUnit.SECONDS)
                .build();


        Log.d("TAG", "secciontoken: " + SessionManager.getInstance(gContext).getaccesstoken().accesstoken);
        Response responsee = client.newCall(request).execute();
        String res = responsee.body().string();
        final Buffer buffer = new Buffer();
        request.newBuilder().build().body().writeTo(buffer);

        Log.d("ApiCall "+new Date().getTime(),"URL: " + url + "\nPARAMS: [" + buffer.readUtf8() + "]\nREPSONSE: " +res);
        return res;
    }






    // Delete request with required headers
    public static String DELETE_HEADER(OkHttpClient client, String url) throws IOException {
        String cookies= SessionManager.getInstance(gContext).getaccesstoken().sessionName+"="+SessionManager.getInstance(gContext).getaccesstoken().sessionID;
        Request request = new Request.Builder()
                .url(url)
                .delete()
                .header("Accept-Language", "application/json")
                .header("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ")
                .header("X-CSRF-Token", SessionManager.getInstance(gContext).getaccesstoken().accesstoken)
                .header("Cookie",cookies)
                .header("Content-Type", "application/json")
                .build();


        Log.e(TAG, "DELETE_HEADER: " + url);
        Response response = client.newCall(request).execute();
        return response.body().string();
    }



    //GET network request
    public static String GETHEADER(OkHttpClient client, String url) throws IOException {
         String Cookies= SessionManager.getInstance(gContext).getaccesstoken().sessionName+"="+SessionManager.getInstance(gContext).getaccesstoken().sessionID;


        Request request = new Request.Builder()
                .url(url)
                .header("Content-Type", "application/json")
                .header("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ")
                .header("Accept-Language", "application/json")
                .header("X-CSRF-Token", SessionManager.getInstance(gContext).getaccesstoken().accesstoken)
                .header("Cookie",Cookies)
                .build();

        Log.e("ApiCall", SessionManager.getInstance(gContext).getaccesstoken().accesstoken + "");
        Log.e("ApiCall", Cookies);
        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    //PUT network request
    public static String PUTHEADER(OkHttpClient client, String url, RequestBody body) throws IOException {
        String Cookies= SessionManager.getInstance(gContext).getaccesstoken().sessionName+"="+SessionManager.getInstance(gContext).getaccesstoken().sessionID;


        MediaType JSON
                = MediaType.parse("application/json; charset=utf-8");
        Request request = new Request.Builder()
                .url(url)
                .header("Accept-Language", "application/json")
                .header("API-KEY", "a5XSE8XCdsY6hAoCNojYBQ")
                .header("X-CSRF-Token", SessionManager.getInstance(gContext).getaccesstoken().accesstoken)
                .header("Cookie",Cookies)
                .header("Content-Type", "application/json")
                .put(body)
                .build();

        client = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        Log.d("TAG", "secciontoken: " + SessionManager.getInstance(gContext).getaccesstoken().accesstoken);
        Response responsee = client.newCall(request).execute();
        String res = responsee.body().string();
        final Buffer buffer = new Buffer();
        request.newBuilder().build().body().writeTo(buffer);

        Log.d("ApiCall "+new Date().getTime(),"URL: " + url + "\nPARAMS: [" + buffer.readUtf8() + "]\nREPSONSE: " +res);
        return res;
    }





    }
