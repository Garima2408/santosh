package co.savm.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.AllFriends;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.studentprofile.AnotherUserDisplay;
import co.savm.teacher.CourseModuleTeacherInfo;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.savm.models.AllFriends.PROGRESS_;
import static co.savm.models.AllUser.COARSE_TYPE;

/**
 * Created by Dell on 21-08-2017.
 */

public class MyFriendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<AllFriends> lists;
    DeleteMemberFromList   deletememberfromlist = null;
    Activity activity;


    public MyFriendsAdapter(RecyclerView recyclerView, ArrayList<AllFriends> lists,Activity activity) {
        this.lists = lists;
        this.activity = activity;
    }



    public void addData(ArrayList<AllFriends> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<AllFriends> lists){

        this.lists=lists;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {

                if(lists!=null && lists.size()>0){
                    lists.add(new AllFriends(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_myfriends, parent, false);
            return new CustomVholder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }




    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CustomVholder) {
            final AllFriends course = lists.get(position);
            final CustomVholder userViewHolder = (CustomVholder) holder;


            try {
                userViewHolder.tv_friendname.setText(lists.get(position).getField_firstname());
                userViewHolder.tv_friendlastname.setText(lists.get(position).getField_lastname());


                if (lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0) {
                    Glide.clear(userViewHolder.friendIcon);
                    Glide.with(activity).load(lists.get(position).getPicture())
                            .placeholder(R.mipmap.place_holder).dontAnimate()
                            .fitCenter().into(userViewHolder.friendIcon);

                } else {
                    userViewHolder.friendIcon.setImageResource(R.mipmap.place_holder);
                    Glide.clear(userViewHolder.friendIcon);
                }


                if (lists.get(position).getMainRole() != null && lists.get(position).getMainRole().length() > 0) {

                    if (lists.get(position).getMainRole().matches("13")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).CollageBranch);


                    } else if (lists.get(position).getMainRole().matches("14")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).getCollagedepartment());


                    } else if (lists.get(position).getMainRole().matches("15")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).CollageBranch);


                    } else if (lists.get(position).getMainRole().matches("16")) {
                        userViewHolder.tv_departmentrole.setText(lists.get(position).CollageRole);
                        userViewHolder.tv_department.setText(lists.get(position).CollageStudentName);

                    }


                } else {

                }


                userViewHolder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                                .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                                .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                        activity.startActivity(backIntent);


                    }
                });


                userViewHolder.friendIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                                .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                                .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                        activity.startActivity(backIntent);



                    }
                });

                userViewHolder.MainHead.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent backIntent = new Intent(activity, AnotherUserDisplay.class)
                                .putExtra("USERPROFILE_ID",lists.get(position).getUid())
                                .putExtra("COLLAGE_ID",lists.get(position).getCollageId());
                        activity.startActivity(backIntent);



                    }
                });





                userViewHolder.options.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        showPopupMenu();


                    }

                    private void showPopupMenu() {


                        PopupMenu popup = new PopupMenu(activity, userViewHolder.options);
                        MenuInflater inflater = popup.getMenuInflater();
                        inflater.inflate(R.menu.removepopupmenu, popup.getMenu());
                        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {

                                    case R.id.deleteoption:

                                        AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.app.AlertDialog.THEME_HOLO_DARK)
                                                .setTitle("Remove friend")
                                                .setMessage(R.string.Remove_Student)
                                                .setCancelable(false)
                                                .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {

                                                    /*deletememberfromlist = new DeleteMemberFromList();
                                                    deletememberfromlist.execute(course_id,lists.get(position).getUid());*/
                                                    }
                                                })
                                                .setNegativeButton("No", new Dialog.OnClickListener() {

                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        dialogInterface.dismiss();
                                                    }
                                                });
                                        builder.create().show();


                                        return true;
                                    default:
                                        return false;
                                }
                            }
                        });
                        popup.show();

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }


    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }



    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {
        ImageView friendIcon;
        RelativeLayout MainHead;
        private TextView tv_friendname,tv_friendlastname,tv_department,tv_departmentrole;
        ImageView options;

        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_friendlastname = itemView.findViewById(R.id.tv_friendlastname);
            friendIcon = itemView.findViewById(R.id.friendIcon);
            options = itemView.findViewById(R.id.options);
            tv_department = itemView.findViewById(R.id.tv_department);
            tv_departmentrole = itemView.findViewById(R.id.tv_departmentrole);
            MainHead =itemView.findViewById(R.id.MainHead);


        }


    }
    private class DeleteMemberFromList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(activity);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_TEACHERREMOVETEACHER +"/"+args[0]+"/"+args[1],body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletememberfromlist = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(activity);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.p_dialog_dismiss(activity);
                       // Utils.showAlertDialog(activity, "Delete", msg);
                        CourseModuleTeacherInfo.RefreshWorkedFaculty();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(activity);
                        Utils.showAlertDialog(activity, "Error", msg);


                    }
                } else {
                    Utils.p_dialog_dismiss(activity);
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {

            Utils.p_dialog_dismiss(activity);


        }
    }

}


