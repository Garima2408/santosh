package co.savm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.UserDisplayProfile;
import co.savm.models.CourseMemberLists;

/**
 * Created by Dell on 19-01-2018.
 */

public class GroupMemberAdapter extends RecyclerView.Adapter<GroupMemberAdapter.CustomVholder> {


    private ArrayList<CourseMemberLists> lists;

    private Context mcontext;

    public GroupMemberAdapter(Context mcontext, ArrayList<CourseMemberLists> lists) {
        this.lists = lists;
        this.mcontext = mcontext;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_groupmember, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_UserName.setText(lists.get(position).getField_first_name()+" "+lists.get(position).getField_last_name());




            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {
                Glide.clear(holder.circleView);
                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.circleView);

            }else {
                holder.circleView.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.circleView);
            }



            holder.intro4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getEtid());
                    mcontext.startActivity(backIntent);


                }
            });







        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView circleView,options;

        private TextView tv_UserName;
        RelativeLayout intro4;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_UserName = itemView.findViewById(R.id.tv_UserName);
            circleView = itemView.findViewById(R.id.circleView);
            intro4  = itemView.findViewById(R.id.intro4);

        }

        @Override
        public void onClick(View view) {

        }
    }



}


