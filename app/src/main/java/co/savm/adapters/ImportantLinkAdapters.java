package co.savm.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import co.savm.R;
import co.savm.models.ListImportanLink;

import static co.savm.models.ListImportanLink.COARSE_TYPE;
import static co.savm.models.ListImportanLink.PROGRESS_;

/**
 * Created by Dell on 16-08-2017.
 */

public class ImportantLinkAdapters extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<ListImportanLink> lists;
    private Activity activity;
    private boolean isLoading;



    public ImportantLinkAdapters( Activity activity, ArrayList<ListImportanLink> lists) {
        this.lists = lists;
        this.activity = activity;
    }

    public void addData(ArrayList<ListImportanLink> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }



    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<ListImportanLink> list){

        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new ListImportanLink(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);

    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_importantlink, parent, false);
            return new CustomVholder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CustomVholder) {
            final ListImportanLink contact = lists.get(position);
            final CustomVholder userViewHolder = (CustomVholder) holder;

            try {
                userViewHolder.tv_title.setText(lists.get(position).getTitle());
                userViewHolder.tv_site.setText(lists.get(position).getPath());

                userViewHolder.link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent viewIntent =
                                new Intent("android.intent.action.VIEW",
                                        Uri.parse(lists.get(position).getPath()));
                        activity.startActivity(viewIntent);

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }



    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }



    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {

        private TextView tv_title, tv_site;
        LinearLayout link;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(co.savm.R.id.tv_title);
            tv_site = itemView.findViewById(co.savm.R.id.tv_site);
            link = itemView.findViewById(co.savm.R.id.link);

        }

    }
}