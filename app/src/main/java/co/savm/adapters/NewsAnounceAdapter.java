package co.savm.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.NewsAnonocment;
import co.savm.activities.UpdateNotification;
import co.savm.models.AnouncementArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

import static co.savm.models.AnouncementArray.COARSE_TYPE;
import static co.savm.models.AnouncementArray.PROGRESS_;

/**
 * Created by Dell on 29-01-2018.
 */

public class NewsAnounceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<AnouncementArray> lists;
    private DeleteNews deleteNews=null;
    private boolean isLoading;
    private Activity activity;
    Dialog imageDialog;
    Bitmap thumbnail = null;
    String Extension;

    public NewsAnounceAdapter(RecyclerView recyclerView, ArrayList<AnouncementArray> lists, Activity activity) {
        this.lists = lists;
        this.activity = activity;


    }

    public void addData(ArrayList<AnouncementArray> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<AnouncementArray> list){
        lists.clear();
        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new AnouncementArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_anocment, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final AnouncementArray course = lists.get(position);
            final UserViewHolder userViewHolder = (UserViewHolder) holder;

            userViewHolder.tittle.setText(lists.get(position).getTitle());
            final String s = lists.get(position).getDescription();
            userViewHolder.DetailNotification.setText(Html.fromHtml(s));
            Log.d("TAG", "printthe string: " + s.length());

            String str = lists.get(position).getDate();
            String[] splited = str.split("\\s+");
            String split_one = splited[0];
            String split_second = splited[1];
            userViewHolder.monthDisplay.setText(split_one);
            userViewHolder.DateDisplay.setText(split_second);

            if(lists.get(position).getFile() != null && lists.get(position).getFile().length() > 0 ) {
                Extension = lists.get(position).getFile().substring(lists.get(position).getFile().lastIndexOf("."));
                Log.d("TAG", "Extension: " + Extension);

                if (Extension.matches(".jpg")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getFile())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);


                } else if (Extension.matches(".png")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getFile())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);

                }  else if (Extension.matches(".jpeg")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagePost.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.GONE);

                    Glide.clear(userViewHolder.imagePost);
                    Glide.with(activity).load(lists.get(position).getFile())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imagePost);

                }else if (Extension.matches(".docx")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_doc_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);




                }else if (Extension.matches(".xlsx")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_xls_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);




                } else if (Extension.matches(".xls")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_xls_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);




                } else if (Extension.matches(".pdf")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_pdf_download);

                    userViewHolder.imagePost.setVisibility(View.GONE);




                } else if (Extension.matches(".txt")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_text_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);



                }else if (Extension.matches(".doc")) {
                    userViewHolder.previewlayout.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setVisibility(View.VISIBLE);
                    userViewHolder.imagepreview.setImageResource(R.mipmap.ic_doc_download);
                    userViewHolder.imagePost.setVisibility(View.GONE);



                }else if (Extension.matches(".")) {

                    Toast.makeText(activity,"This Format doesn't Support",Toast.LENGTH_LONG).show();
                }

            }else {
                userViewHolder.previewlayout.setVisibility(View.GONE);
                userViewHolder.previewlayout.setVisibility(View.GONE);
                userViewHolder.imagePost.setImageDrawable(null);
                Glide.clear(userViewHolder.imagePost);
            }




            userViewHolder.imagepreview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  //  Utils.viewFile(activity,lists.get(position).getFile());


                    Utils.downloadFile(activity,lists.get(position).getFile());
                   // Utils.downloadFile(activity,lists.get(position).getFile());

                }
            });


                userViewHolder.imagePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Utils.ImageDialogOpen(activity,lists.get(position).getFile());

                }
            });


            if(lists.get(position).getUid().equals(SessionManager.getInstance(activity).getUser().getUserprofile_id())){
                userViewHolder.options.setVisibility(View.VISIBLE);

            }else {
                userViewHolder.options.setVisibility(View.INVISIBLE);

            }



            userViewHolder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopupMenu();
                }

                private void showPopupMenu() {

                    PopupMenu popup = new PopupMenu(activity, userViewHolder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            switch (item.getItemId()){

                                case R.id.Editoption:
                                    Intent backIntent = new Intent(activity, UpdateNotification.class)
                                            .putExtra("USER_ID", lists.get(position).getUid())
                                            .putExtra("POST_ID", lists.get(position).getTnid())
                                            .putExtra("POSTCONENT", lists.get(position).getTitle())
                                            .putExtra("BLOG_CONTENT", lists.get(position).getDescription())
                                            .putExtra("DATE", lists.get(position).getDate())
                                            .putExtra("IMAGE_URL", lists.get(position).getFile());


                                    activity.startActivity(backIntent);




                                    return true;
                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Delete News & Notification")
                                            .setMessage(R.string.Delete)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {


                                                    deleteNews = new DeleteNews();
                                                    deleteNews.execute(lists.get(position).getTnid());
                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();

                                    return true;
                                default:
                                    return false;

                            }



                        }
                    });

                    popup.show();
                }
            });




        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }




    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }
    /**
     * Viewholder for Adapter
     */
    public class UserViewHolder extends RecyclerView.ViewHolder {

        private TextView tittle,DateDisplay,monthDisplay,DetailNotification;
        ImageView imagePost,imagepreview,options;
        RelativeLayout previewlayout;

        public UserViewHolder(View itemView) {
            super(itemView);

            tittle = itemView.findViewById(R.id.tittle);
            monthDisplay = itemView.findViewById(R.id.monthDisplay);
            DateDisplay = itemView.findViewById(R.id.DateDisplay);
            DetailNotification = itemView.findViewById(R.id.DetailNotification);
            imagePost = itemView.findViewById(R.id.imagePost);
            imagepreview = itemView.findViewById(R.id.imagepreview);
            previewlayout = itemView.findViewById(R.id.previewlayout);
            options = itemView.findViewById(R.id.options);



        }

    }

    private class DeleteNews extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(activity);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETECOLLAGENEWSLIST+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deleteNews = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(activity);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.p_dialog_dismiss(activity);
                       // Utils.showAlertDialog(activity, "Delete", msg);
                        NewsAnonocment.FromNewsAndNotification();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(activity);
                        Utils.showAlertDialog(activity, "Error", msg);



                    }
                }else {
                    Utils.p_dialog_dismiss(activity);
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            deleteNews=null;
            Utils.p_dialog_dismiss(activity);


        }
    }



}


