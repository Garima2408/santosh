package co.savm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import co.savm.R;
import co.savm.models.MonthArrayAttandance;

/**
 * Created by AKASH on 12-Apr-18.
 */

public class MonthlyAttandanceAdapter extends RecyclerView.Adapter<MonthlyAttandanceAdapter.CustomVholder> {


    private ArrayList<MonthArrayAttandance> lists;
    String Service_id,time24format,time24format1;
    private Context mcontext;


    public MonthlyAttandanceAdapter(Context mcontext, ArrayList<MonthArrayAttandance> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_monthsattandance, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {

            String date = lists.get(position).getMonth();
            DateFormat inputFormat = new SimpleDateFormat("yyy-MM");
            Date d = null;
            try {
                d = inputFormat.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            DateFormat outputFormat = new SimpleDateFormat("MMM yyy");
            System.out.println(outputFormat.format(d));
            String CalenderMonth =outputFormat.format(d);





            holder.monthview.setText(CalenderMonth);
            holder.attandance.setText(lists.get(position).getPerc());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        private TextView monthview, attandance;


        public CustomVholder(View itemView) {
            super(itemView);

            monthview = itemView.findViewById(R.id.monthview);
            attandance = itemView.findViewById(R.id.attandance);


        }

    }




}


