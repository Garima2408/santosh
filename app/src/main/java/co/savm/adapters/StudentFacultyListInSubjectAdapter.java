package co.savm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.UserDisplayProfile;
import co.savm.models.SubjectFacultyArray;

/**
 * Created by Dell on 14-01-2018.
 */

public class StudentFacultyListInSubjectAdapter extends RecyclerView.Adapter<StudentFacultyListInSubjectAdapter.CustomVholder> {


    private ArrayList<SubjectFacultyArray> lists;
    String course_id;
    private Context mcontext;


    public StudentFacultyListInSubjectAdapter(Context mcontext, ArrayList<SubjectFacultyArray> lists , String course_id) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.course_id =course_id;


    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_teacherinsubject, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.Teacher.setText(lists.get(position).getName());

            holder.intro4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID", lists.get(position).getUid());
                    mcontext.startActivity(backIntent);


                }
            });

            holder.Department.setText(lists.get(position).getDepartment());
          //  holder.tv_department.setText(lists.get(position).getDepartment());


            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {

                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);

            }
            holder.options.setVisibility(View.GONE);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }





    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView friendIcon,options;
        private TextView Teacher, tv_email,Department;
        RelativeLayout intro4;

        public CustomVholder(View itemView) {
            super(itemView);

            Teacher = itemView.findViewById(R.id.Teacher);
            friendIcon = itemView.findViewById(R.id.friendIcon);
            Department =itemView.findViewById(R.id.Department);
            options =itemView.findViewById(R.id.options);
            options.setVisibility(View.GONE);
            intro4 =itemView.findViewById(R.id.intro4);

        }


    }





}





