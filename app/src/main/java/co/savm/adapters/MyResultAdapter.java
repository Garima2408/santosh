package co.savm.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import co.savm.R;
import co.savm.models.MyResultsItem;
import co.savm.models.ResultChlidListArray;

/**
 * Created by Dell on 07-09-2017.
 */

public class MyResultAdapter extends BaseExpandableListAdapter {

    private List<MyResultsItem> catList;
    private int itemLayoutId;
    private int groupLayoutId;
    private Context mContext;

    public MyResultAdapter(Context ctx, List<MyResultsItem> catList) {

        this.itemLayoutId = R.layout.list_of_resultchild;
        this.groupLayoutId = R.layout.list_of_resultparent;
        this.catList = catList;
        this.mContext = ctx;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return catList.get(groupPosition).childsTaskList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return catList.get(groupPosition).childsTaskList.get(childPosition).hashCode();
    }


    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_of_resultchild, parent, false);
        }

        TextView tittleChild = v.findViewById(R.id.tittlechild);
        TextView noChild = v.findViewById(R.id.nochild);
        TextView marks = v.findViewById(R.id.marks);

        ResultChlidListArray task = catList.get(groupPosition).childsTaskList.get(childPosition);
        tittleChild.setText(task.getTitle());
        noChild.setText(task.getMax_marks());
        marks.setText(task.getMarks());

        return v;

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int size = catList.get(groupPosition).childsTaskList.size();
        System.out.println("Child for group [" + groupPosition + "] is [" + size + "]");
        return size;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return catList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return catList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return catList.get(groupPosition).hashCode();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_of_resultparent, parent, false);
        }
       /* Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/Montserrat_Regular.ttf");*/
        TextView groupName = v.findViewById(R.id.tittle);


        if(android.os.Build.VERSION.SDK_INT >= 21) {
            if (isExpanded) {
                groupName.setCompoundDrawablesWithIntrinsicBounds(mContext.getDrawable(R.mipmap.faq_expanminus), null, null, null);
                groupName.setCompoundDrawablePadding(10);
            } else {
                groupName.setCompoundDrawablesWithIntrinsicBounds(mContext.getDrawable(R.mipmap.faq_expanplus), null, null, null);
                groupName.setCompoundDrawablePadding(10);
            }
            MyResultsItem cat = catList.get(groupPosition);

            if(catList.get(groupPosition).classType.matches("Exams")){
                groupName.setText(cat.className+"  "+cat.classType);
            } else if(catList.get(groupPosition).classType.matches("Assignments")){
                groupName.setText(cat.className+"  "+cat.classType);
            }


        }
        return v;

    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
