package co.savm.adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import co.savm.R;
import co.savm.models.FaqExpandableChildArray;
import co.savm.models.FaqExpandableparentArray;

/**
 * Created by Dell on 26-09-2017.
 */

public class FAQExpandableAdapter extends BaseExpandableListAdapter {

    private List<FaqExpandableparentArray> catList;
    private int itemLayoutId;
    private int groupLayoutId;
    private Context mContext;

    public FAQExpandableAdapter(Context ctx, List<FaqExpandableparentArray> catList) {

        this.itemLayoutId = R.layout.list_faqchild;
        this.groupLayoutId = R.layout.list_faqparent;
        this.catList = catList;
        this.mContext = ctx;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {

        return catList.get(groupPosition).childsTaskList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return catList.get(groupPosition).childsTaskList.get(childPosition).hashCode();
    }


    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_faqchild, parent, false);
        }

        TextView tittleChild = v.findViewById(R.id.expandedListItem);


        FaqExpandableChildArray task = catList.get(groupPosition).childsTaskList.get(childPosition);

        String s =task.getAnswer();
        tittleChild.setText(Html.fromHtml(s));


        return v;

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int size = catList.get(groupPosition).childsTaskList.size();
        System.out.println("Child for group [" + groupPosition + "] is [" + size + "]");
        return size;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return catList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return catList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return catList.get(groupPosition).hashCode();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_faqparent, parent, false);
        }

        TextView groupName = v.findViewById(R.id.listTitle);



        if (isExpanded) {
            groupName.setCompoundDrawablesWithIntrinsicBounds(mContext.getDrawable(R.mipmap.faq_expanminus), null, null, null);
            groupName.setCompoundDrawablePadding(15);
        } else {
            groupName.setCompoundDrawablesWithIntrinsicBounds(mContext.getDrawable(R.mipmap.faq_expanplus), null, null, null);
            groupName.setCompoundDrawablePadding(15);
        }
        FaqExpandableparentArray cat = catList.get(groupPosition);

        String Questin =cat.parentTitle;
        groupName.setText(Html.fromHtml(Questin));




        return v;

    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}







