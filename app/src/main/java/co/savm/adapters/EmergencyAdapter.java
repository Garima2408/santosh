package co.savm.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import co.savm.R;
import co.savm.models.EmergencyArray;

/**
 * Created by Dell on 29-01-2018.
 */

public class EmergencyAdapter extends RecyclerView.Adapter<EmergencyAdapter.CustomVholder> {

    private ArrayList<EmergencyArray> lists;
    private Context mcontext;
    String Group_id,GroupTittle,groupImage;


    public EmergencyAdapter(Context mcontext, ArrayList<EmergencyArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_emergeny, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {

            String s =lists.get(position).getDescription();

            final String Callno= String.valueOf(Html.fromHtml(s));

            holder.Emergency.setText(lists.get(position).getTitle()+"-"+Html.fromHtml(s));




            if(lists.get(position).getTnid().matches("254206"))
            {
                holder.imagecontact.setBackgroundResource(R.mipmap.emergency_phone_red);

            }else if(lists.get(position).getTnid().matches("254528")){

                holder.imagecontact.setBackgroundResource(R.mipmap.emergency_phone_red);


            }else if(lists.get(position).getTnid().matches("254512")){

                holder.imagecontact.setBackgroundResource(R.mipmap.emergency_phone_red);


            }else if(lists.get(position).getTnid().matches("254503")){

                holder.imagecontact.setBackgroundResource(R.mipmap.emergency_phone_red);


            }else{

                holder.imagecontact.setBackgroundResource(R.mipmap.emergency_phone_green);


            }







            holder.link.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {

                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", Callno, null));
                    mcontext.startActivity(intent);




                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {

        private TextView Emergency;
        RelativeLayout link;
        ImageView imagecontact;


        public CustomVholder(View itemView) {
            super(itemView);

            Emergency = itemView.findViewById(R.id.Emergency);
            link = itemView.findViewById(R.id.link);
            imagecontact = itemView.findViewById(R.id.imagecontact);
        }

    }


}



