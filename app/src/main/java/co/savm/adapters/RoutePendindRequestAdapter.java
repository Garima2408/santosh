package co.savm.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.tracker.RouteRequestPendingArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.tracker.RoutePendingRequest;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 23-10-2017.
 */

public class RoutePendindRequestAdapter extends RecyclerView.Adapter<RoutePendindRequestAdapter.CustomVholder> {


    private ArrayList<RouteRequestPendingArray> lists;
    private Context mcontext;
    private ProcessAcceptRequest acceptrequest = null;
    private ProcessRejectRequest rejectrequest = null;


    public RoutePendindRequestAdapter(Context mcontext, ArrayList<RouteRequestPendingArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_requestpending, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_friendname.setText(lists.get(position).getField_first_name()+" "+lists.get(position).getField_last_name());

            if(lists.get(position).getChild_field_first_name() != null && lists.get(position).getChild_field_last_name() != null){

                holder.tv_childname.setVisibility(View.VISIBLE);
                holder.tv_childname.setText(lists.get(position).getChild_field_first_name()+" "+lists.get(position).getChild_field_last_name());

            }




            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {

                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);

            }

            if(lists.get(position).getChild_picture() != null && lists.get(position).getChild_picture().length() > 0 ) {
                holder.childIcon.setVisibility(View.VISIBLE);

                Glide.with(mcontext).load(lists.get(position).getChild_picture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.childIcon);

            }else {
                holder.childIcon.setImageResource(R.mipmap.place_holder);

            }


         /*   holder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid());
                    mcontext.startActivity(backIntent);


                }
            });

            holder.friendIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getUid());
                    mcontext.startActivity(backIntent);


                }
            });
*/

            holder.requstAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    /*METHOD ACCEPTFRIEND REQUEST*/
                    acceptrequest = new ProcessAcceptRequest();
                    acceptrequest.execute(lists.get(position).getId());






                }
            });

            holder.requestreject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*FRIEND REQUEST REJECT*/

                    rejectrequest = new ProcessRejectRequest();
                    rejectrequest.execute(lists.get(position).getId());




                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }








    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        ImageView friendIcon,childIcon;
        private TextView tv_friendname,requestreject, requstAccept,tv_childname;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            requestreject = itemView.findViewById(R.id.requestreject);
            requstAccept = itemView.findViewById(R.id.requstAccept);
            friendIcon =  itemView.findViewById(R.id.friendIcon);
            childIcon =  itemView.findViewById(R.id.childIcon);
            tv_childname =  itemView.findViewById(R.id.tv_childname);
        }



    }

    /*
    ACCEPT FRIEND REQUEST*/

    private class ProcessAcceptRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();



            RequestBody body = new FormBody.Builder()
                    .add("state", "1")
                    .build();



            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_ROUTEJOINAXCCEPTREQUEST+"/"+args[0], body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            acceptrequest = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.showAlertDialog(mcontext, "Response", msg);
                        RoutePendingRequest.CalledFromPendingAdapter();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            acceptrequest = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }


 /*
    REJECT FRIEND REQUEST*/

    private class ProcessRejectRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();




            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_ROUTEJOINREJECTREQUEST+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");




            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            rejectrequest = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        Utils.showAlertDialog(mcontext, "Response", msg);
                        RoutePendingRequest.CalledFromPendingAdapter();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            rejectrequest = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }


}
