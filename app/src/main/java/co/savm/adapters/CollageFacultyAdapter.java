package co.savm.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.UserDisplayProfile;
import co.savm.models.FacultyArray;

import static co.savm.models.FacultyArray.COARSE_TYPE;
import static co.savm.models.FacultyArray.PROGRESS_;

/**
 * Created by Dell on 06-09-2017.
 */

public class CollageFacultyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<FacultyArray> lists;
    private Activity activity;
    private Dialog imageDialog;
    private Dialog profileDialog;


    public CollageFacultyAdapter(Activity activity, ArrayList<FacultyArray> lists) {
        this.lists = lists;
        this.activity = activity;
    }

    public void addData(ArrayList<FacultyArray> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }



    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<FacultyArray> list){

        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new FacultyArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_collagefaculty, parent, false);
            return new CustomVholder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof CustomVholder) {
            final FacultyArray contact = lists.get(position);
            final CustomVholder userViewHolder = (CustomVholder) holder;


            try {
                userViewHolder.facultyname.setText(lists.get(position).getFirst_name() + " " + lists.get(position).getLast_name());
                userViewHolder.Department.setText("Department: "+lists.get(position).getDepartment());

                if (lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0) {

                    Glide.with(activity).load(lists.get(position).getPicture())
                            .placeholder(R.mipmap.single).dontAnimate()

                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.circleView);

                } else {

                    userViewHolder.circleView.setImageResource(R.mipmap.single);

                }



                userViewHolder.link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                                .putExtra("USERPROFILE_ID", lists.get(position).getUid());
                        activity.startActivity(backIntent);


                    }
                });


                userViewHolder.circleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        showProfilepic(lists.get(position).getPicture(),lists.get(position).getFirst_name());

/*

                        Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                                .putExtra("USERPROFILE_ID", lists.get(position).getUid());
                        activity.startActivity(backIntent);

*/

                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        else if(holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }
    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }




    private void ImageDialogOpen(String url,String name1) {

        final ImageView backone;
        TextView senderName;


        imageDialog = new Dialog(activity);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.image_preview_show);
        // dialogLogin.setCancelable(false);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
        ImageView showimage = imageDialog.findViewById(R.id.showimage);
        senderName = imageDialog.findViewById(R.id.senderName);
        backone = imageDialog.findViewById(R.id.backone);
        Glide.clear(showimage);

        senderName.setText(name1);
        Glide.with(activity).load(url)
                .placeholder(R.mipmap.place_holder).dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(showimage);

        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageDialog.dismiss();
            }
        });




    }


    private void showProfilepic(final String url, final String name){

        final ImageView profilepic;
        TextView senderName;


        profileDialog = new Dialog(activity);
        profileDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        profileDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        profileDialog.setContentView(R.layout.show_user_profilepic);
        profileDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation2;

        profileDialog.show();

        //   message = profileDialog.findViewById(R.id.messsage);
        profilepic = profileDialog.findViewById(R.id.profilepic);
        // info = profileDialog.findViewById(R.id.info);
        senderName = profileDialog.findViewById(R.id.senderName);

        senderName.setText(name);

        Glide.clear(profilepic);
        Glide.with(activity).load(url)
                .placeholder(R.mipmap.place_holder).dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter().into(profilepic);




        profilepic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileDialog.dismiss();
                ImageDialogOpen(url,name);

            }
        });
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView facultyname,Department;
        ImageView circleView;
        RelativeLayout MainLayout;
        RelativeLayout link;

        public CustomVholder(View itemView) {
            super(itemView);

            facultyname = itemView.findViewById(R.id.facultyname);
            Department = itemView.findViewById(R.id.Department);
            circleView = itemView.findViewById(R.id.circleView);
            MainLayout =itemView.findViewById(R.id.MainLayout);
            link  =itemView.findViewById(R.id.link);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
