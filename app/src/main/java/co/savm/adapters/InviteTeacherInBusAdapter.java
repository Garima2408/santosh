package co.savm.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.UserDisplayProfile;
import co.savm.models.InviteTeacherArray;
import co.savm.models.routedetailResponse.Data;
import co.savm.models.routedetailResponse.RouteDetailResponseModel;
import co.savm.models.tracker.WayPointsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.tracker.InviteTeacherInBus;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 14-01-2018.
 */

public class InviteTeacherInBusAdapter extends RecyclerView.Adapter<InviteTeacherInBusAdapter.CustomVholder> {


    private ArrayList<InviteTeacherArray> lists;
    private SearchedInviteTeacher sendinvitetoteacher = null;
    String Route_id,UserId;
    private Activity mcontext;
    private ProgressJoinTheGroupRequest joingrouprequestList = null;

    /*dialog screens*/
    Dialog pickUpDialog;

    Dialog dialog ;
    RecyclerView rec_select_pic;
    private String routeId;
    private String routeName,sponsered;
    AdapterSelectInvitePick adapterSelectPick;
    private LinearLayout ll_cancel,llpickdrop;
    RelativeLayout ll_update2;
    TextView ll_update;
    public static int pickPos,dropPos;
    private RouteDetailResponseModel routeDetailResponseModel;
    private Gson gson;
    private ArrayList<WayPointsArray> wayPointsArrayList;
    WayPointsArray wayPointsPick=new WayPointsArray();
    WayPointsArray wayPointsDrop=new WayPointsArray();

    private ProgressBar progressBar;
    private boolean isChangePickDrop=false;

    public InviteTeacherInBusAdapter(Activity mcontext, ArrayList<InviteTeacherArray> lists, String Route_id) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.Route_id = Route_id;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_inviteteacher, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_friendname.setText(lists.get(position).getFirst_name() + " " + lists.get(position).getLast_name());
            Log.d("TAG", "Course_ID_adapter: " + Route_id);

            holder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID", lists.get(position).getUid());
                    mcontext.startActivity(backIntent);


                }
            });

             holder.tv_email.setText(lists.get(position).getEmail());
            holder.tv_department.setText(lists.get(position).getDepartment());


            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {
                Glide.clear(holder.friendIcon);
                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);
                Glide.clear(holder.friendIcon);
            }
            holder.InviteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    OpenPickUPDroupPointDialog(lists.get(position).getUid());
                    UserId =lists.get(position).getUid();

                  //  SendInviteToTeacher(Route_id, lists.get(position).getUid());

                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SendInviteToTeacher(String courseid, String Studentuid) {

        sendinvitetoteacher = new SearchedInviteTeacher();
        sendinvitetoteacher.execute(courseid, Studentuid);


    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView friendIcon;

        private TextView tv_friendname, tv_email,tv_department,InviteIcon;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            tv_email = itemView.findViewById(R.id.tv_email);
            friendIcon = itemView.findViewById(R.id.friendIcon);
            InviteIcon = itemView.findViewById(R.id.InviteIcon);
            tv_department =itemView.findViewById(R.id.tv_department);


        }


    }
    /*
    SEND  REQUEST TO Teacher TO ADD IN SUBJECT*/

    private class SearchedInviteTeacher extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type", "bus_route")
                    .add("role", "faculty")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ADDTEACHERINBUSROUTE + "/" + args[0] + "/" + args[1], body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData + args[1]);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitetoteacher = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                      //  Utils.showAlertDialog(mcontext, "Response", msg);
                        InviteTeacherInBus.RefreshedThepage();
                        mcontext.finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);
                        InviteTeacherInBus.RefreshedThepage();


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            sendinvitetoteacher = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }
    private void OpenPickUPDroupPointDialog(final String uid) {


        pickUpDialog = new Dialog(mcontext);
        pickUpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pickUpDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        pickUpDialog.setContentView(R.layout.joinroute_dailog_selectpick);

        pickUpDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        pickUpDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        llpickdrop =pickUpDialog.findViewById(R.id.llpickdrop);
        rec_select_pic=pickUpDialog.findViewById(R.id.rec_select_pic);
        ll_cancel=pickUpDialog.findViewById(R.id.ll_cancel);
        ll_update=pickUpDialog.findViewById(R.id.ll_update);
        llpickdrop=pickUpDialog.findViewById(R.id.llpickdrop);
        progressBar=pickUpDialog.findViewById(R.id.progress_bar_pick);

        getRouteDetail();


        ll_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetTheUpdatedValues(uid);
                pickUpDialog.dismiss();


            }
        });


        pickUpDialog.show();


    }

    private void GetTheUpdatedValues(String uid) {

        joingrouprequestList = new ProgressJoinTheGroupRequest();
        joingrouprequestList.execute(Route_id, uid);

    }





    private void getRouteDetail(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                llpickdrop.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                try {
                    String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.ROUTE_DETAILS + Route_id);


                    if (response!=null){
                        gson=new Gson();
                        routeDetailResponseModel=new RouteDetailResponseModel();
                        routeDetailResponseModel = gson.fromJson(response, RouteDetailResponseModel.class);

                        Log.e("response",response);

                        mcontext. runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                llpickdrop.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                                setUpdatePickDrop(routeDetailResponseModel.getData());

                            }
                        });

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private void setUpdatePickDrop(Data data) {

        wayPointsArrayList = new ArrayList<>();

        WayPointsArray WaypointInfoSrc = new WayPointsArray();

        WaypointInfoSrc.field_departure_time = data.getFieldCollectionWayPoints().getSource().getFieldDepartureTime();
        WaypointInfoSrc.field_photo = data.getFieldCollectionWayPoints().getSource().getFieldPhoto();
        WaypointInfoSrc.field_place_name = data.getFieldCollectionWayPoints().getSource().getField_place_name();
        WaypointInfoSrc.field_start_time = data.getFieldCollectionWayPoints().getSource().getFieldStartTime();
        WaypointInfoSrc.lat = data.getFieldCollectionWayPoints().getSource().getLat();
        WaypointInfoSrc.lng = data.getFieldCollectionWayPoints().getSource().getLng();

        wayPointsArrayList.add(WaypointInfoSrc);

        for (int i = 0; i < data.getFieldCollectionWayPoints().getWaypoints().size(); i++) {

            WayPointsArray WaypointInfo = new WayPointsArray();

            WaypointInfo.field_departure_time = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldDepartureTime();
            WaypointInfo.field_photo = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldPhoto();
            WaypointInfo.field_place_name = data.getFieldCollectionWayPoints().getWaypoints().get(i).getField_place_name();
            WaypointInfo.field_start_time = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldStartTime();
            WaypointInfo.lat = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldWayPoints().getLat();
            WaypointInfo.lng =data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldWayPoints().getLng();
            wayPointsArrayList.add(WaypointInfo);
        }

        WayPointsArray WaypointInfoDes = new WayPointsArray();

        WaypointInfoDes.field_departure_time = data.getFieldCollectionWayPoints().getDestination().getFieldDepartureTime();
        WaypointInfoDes.field_photo = data.getFieldCollectionWayPoints().getDestination().getFieldPhoto();
        WaypointInfoDes.field_place_name = data.getFieldCollectionWayPoints().getDestination().getField_place_name();
        WaypointInfoDes.field_start_time = data.getFieldCollectionWayPoints().getDestination().getFieldStartTime();
        WaypointInfoDes.lat = data.getFieldCollectionWayPoints().getDestination().getLat();
        WaypointInfoDes.lng = data.getFieldCollectionWayPoints().getDestination().getLng();

        wayPointsArrayList.add(WaypointInfoDes);
        updatePickDrop(data);

        if (data.getPickup_point().getLat()!=null && data.getDrop_point().getLat()!=null)
            isChangePickDrop=true;


       /* if (isChangePickDrop){
            ll_update.setVisibility(View.VISIBLE);
        }  else ll_update.setVisibility(View.GONE);
*/
        adapterSelectPick=new AdapterSelectInvitePick(mcontext,wayPointsArrayList,isChangePickDrop);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mcontext);
        rec_select_pic.setLayoutManager(mLayoutManager);
        rec_select_pic.setItemAnimator(new DefaultItemAnimator());
        rec_select_pic.setAdapter(adapterSelectPick);

    }

    private void updatePickDrop(Data data){
        if (data.getPickup_point()!=null && data.getDrop_point()!=null){
            String pickPoint =data.getPickup_point().getLat()+data.getPickup_point().getLng();
            String dropPoint =data.getDrop_point().getLat()+data.getDrop_point().getLng();

            for (int i=0;i<wayPointsArrayList.size();i++){
                String curLat=wayPointsArrayList.get(i).getLat()+wayPointsArrayList.get(i).getLng();
                if (curLat.equals(pickPoint)){
                    wayPointsArrayList.get(i).setPick(true);
                }else if (curLat.equals(dropPoint)){
                    wayPointsArrayList.get(i).setDrop(true);
                }
            }
        }
    }

    /*SEND REQUEST FOR JOINING ROUTES*/

    private class ProgressJoinTheGroupRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("etid",args[1])
                    .add("group_type","node")
                    .add("gid",args[0])
                    .add("state","2")
                    .add("membership type","og_membership_type_bus_route")
                    .add("type","og_membership_type_bus_route")
                    .add("field_name","field_bus_member")
                    .add("field_subscription_days","45")
                    .add("field_pickup_point[lat]",wayPointsArrayList.get(pickPos).getLat())
                    .add("field_pickup_point[lng]",wayPointsArrayList.get(pickPos).getLng())
                    .add("field_drop_point[lat]",wayPointsArrayList.get(dropPos).getLat())
                    .add("field_drop_point[lng]",wayPointsArrayList.get(dropPos).getLng())
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.ROUTE_JOINBUS ,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                mcontext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            joingrouprequestList = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject data = responce.getJSONObject("data");

                        if (data != null && data.length() > 0) {
                            String Membershipid = data.getString("membership_id");
                            String child_student=data.getString("child_student");
                            SendInviteToTeacher(Route_id, UserId);


                        } else {

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Toast.makeText(mcontext, msg, Toast.LENGTH_SHORT).show();


                    }
                }else {

                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");

            }
        }

        @Override
        protected void onCancelled() {
            joingrouprequestList = null;
            Utils.p_dialog_dismiss(mcontext);
            Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


        }
    }
}