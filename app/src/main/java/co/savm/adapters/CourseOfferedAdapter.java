package co.savm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import co.savm.models.CourseOfferedList;

/**
 * Created by Dell on 11-08-2017.
 */

public class CourseOfferedAdapter extends BaseAdapter {
    private ArrayList<CourseOfferedList> lists;
    Context contextm;
    int [] imageId;

    private static LayoutInflater inflater=null;

    public CourseOfferedAdapter(Context context, ArrayList<CourseOfferedList> lists, int[] osImages) {
        // TODO Auto-generated constructor stub

        this.lists = lists;
        contextm = context;
        imageId=osImages;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub

        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }



    public class Holder
    {
        TextView os_text;
        ImageView os_img;
        RelativeLayout course_Details;


    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
       Holder holder=new Holder();
        View rowView;
        View row=convertView;

        rowView = inflater.inflate(co.savm.R.layout.list_of_courseoffered, null);
        holder.os_text = rowView.findViewById(co.savm.R.id.os_texts);
        holder.os_img = rowView.findViewById(co.savm.R.id.os_images);
        holder.course_Details = rowView.findViewById(co.savm.R.id.course_Details);
        holder.os_text.setText(lists.get(position).getName());
        holder.os_img.setImageResource(imageId[position]);

        holder.course_Details.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

              /*  Intent intent=new Intent();
                intent.setClass(contextm,CourceModuleDetailPage.class);
                Bundle bundle=new Bundle();
                bundle.putString("COURCE_ID", lists.get(position).getTid());
                intent.putExtras(bundle);
                contextm.startActivity(intent);*/
            }
        });

        return rowView;
    }

}