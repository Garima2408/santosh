package co.savm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import co.savm.R;
import co.savm.models.ResultDetailArray;


/**
 * Created by Dell on 17-08-2017.
 */

public class ResultDetailDisplayAdapter extends RecyclerView.Adapter<ResultDetailDisplayAdapter.CustomVholder> {


    private ArrayList<ResultDetailArray> lists;
    private Context mcontext;

    public ResultDetailDisplayAdapter(Context mcontext, ArrayList<ResultDetailArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_result_details, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        final ResultDetailArray result = lists.get(position);

        try {
            holder.name.setText(result.getStudent());
            holder.Enrollment.setText(result.getEnrollment_id());
            if (result.getMarks()!=null &&result.getMarks().length()>0){
                holder.Marks.setText(result.getMarks());

            }else {
                holder.Marks.setText("Not Updated Yet");


            }

          if (result.getPicture() !=null && result.getPicture().length() >0){

              Glide.with(mcontext).load(lists.get(position).getPicture())
                      .placeholder(R.mipmap.place_holder).dontAnimate()
                      .diskCacheStrategy(DiskCacheStrategy.ALL)
                      .fitCenter().into(holder.circleView);

          }




        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {

        private TextView name,Enrollment,Marks;

        ImageView circleView;


        public CustomVholder(View itemView) {
            super(itemView);

            circleView = itemView.findViewById(R.id.circleView);
            name = itemView.findViewById(R.id.name);
            Enrollment = itemView.findViewById(R.id.Enrollment);
            Marks = itemView.findViewById(R.id.Marks);

        }

    }
}
