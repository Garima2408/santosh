package co.savm.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.StudentListsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class StudentListInClassAdapter extends RecyclerView.Adapter<StudentListInClassAdapter.CustomVholder> {

    private ArrayList<StudentListsArray> lists;
    private Context mcontext;
    String class_id;
    private AddStudentInClass addstudentinclass = null;


    public StudentListInClassAdapter(Context mcontext, ArrayList<StudentListsArray> lists, String class_id) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.class_id =class_id;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_student_inclass, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.student_name.setText(lists.get(position).getName());

            if(lists.get(position).getPicture().isEmpty()){
                holder.circleView.setImageResource(R.mipmap.place_holder);

            }else {


            }

            holder.studentstatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    addstudentinclass = new AddStudentInClass();
                    addstudentinclass.execute(lists.get(position).getUid(),class_id,"flag");



                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView circleView;
        private TextView student_name;
       RadioButton studentstatus;
        RelativeLayout toplayout;


        public CustomVholder(View itemView) {
            super(itemView);


            student_name = itemView.findViewById(R.id.student_name);
            circleView = itemView.findViewById(R.id.circleView);
            studentstatus =itemView.findViewById(R.id.studentstatus);
            //  present = (ImageButton) itemView.findViewById(R.id.present);
            toplayout = itemView.findViewById(R.id.toplayout);



        }

    }


    /*ADD LIKES ON COMMENTS */


    private class AddStudentInClass extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
              Utils.p_dialog(mcontext);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name","member")
                    .add("entity_id",args[1])
                    .add("action",args[2])
                    .add("uid",args[0])
                    .build();






            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LIKEDCOMMENTS,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                 Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Reload the page.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            addstudentinclass = null;
            try {
                if (responce != null) {
                     Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        // Utils.p_dialog_dismiss(mcontext);
                        //  CampusNewFeeds.RefreshWorked();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                       //  Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);



                    }
                }else {
                      Utils.p_dialog_dismiss(mcontext);
                    Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                  Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            addstudentinclass = null;
              Utils.p_dialog_dismiss(mcontext);


        }
    }





}
