package co.savm.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.savm.R;
import co.savm.models.MyEventsArray;
import co.savm.studentprofile.MyEventsDetails;

/**
 * Created by Dell on 06-12-2017.
 */

public class MyEventsAdapters extends RecyclerView.Adapter<MyEventsAdapters.CustomVholder> {


    private ArrayList<MyEventsArray> lists;

    private Context mcontext;


    public MyEventsAdapters(Context mcontext, ArrayList<MyEventsArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_myevents, null);

        return new CustomVholder(view);
    }

    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        final MyEventsArray mgroups = lists.get(position);


        try {
            holder.tv_title.setText(lists.get(position).getTitle());

            if(lists.get(position).getField_groups_logo() != null && lists.get(position).getField_groups_logo().length() > 0 ) {

                Glide.with(mcontext).load(lists.get(position).getField_groups_logo())
                        .placeholder(R.mipmap.eventback).dontAnimate()
                        .fitCenter().into(holder.eventicon);

            }else {
                holder.eventicon.setImageResource(R.mipmap.eventback);

            }




            holder.mainlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(mcontext,MyEventsDetails.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("EVENT_ID",lists.get(position).getNid());
                    bundle.putString("EVENT_LOGO",lists.get(position).getField_groups_logo());
                    intent.putExtras(bundle);
                    mcontext.startActivity(intent);



                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /* **/
    /* * Viewholder for Adapter*/

    public class CustomVholder extends RecyclerView.ViewHolder {
        private ImageView eventicon;
        private TextView tv_title;
        RelativeLayout mainlayout;

        public CustomVholder(View itemView) {
            super(itemView);
            eventicon = itemView.findViewById(R.id.eventicon);
            tv_title = itemView.findViewById(R.id.tv_title);
            mainlayout =  itemView.findViewById(R.id.mainlayout);


        }
    }


}