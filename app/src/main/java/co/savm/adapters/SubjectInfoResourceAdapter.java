package co.savm.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.InfoArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.teacher.CourseModuleTeacherInfo;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 31-10-2017.
 */
public class SubjectInfoResourceAdapter extends RecyclerView.Adapter<SubjectInfoResourceAdapter.CustomVholder> {

    private DeleteSubjectResource deletesubjectresource = null;
    private DeleteSubjectLinkResource deletesubjectlinkresource = null;
    private ArrayList<InfoArray> lists;
    private Activity mcontext;
    String course_idMain,Adminuser;
    String Extension;



    public SubjectInfoResourceAdapter(Activity mcontext, ArrayList<InfoArray> lists, String course_idMain,String Adminuser) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.course_idMain =course_idMain;
        this.Adminuser =Adminuser;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_subjectresourceinfo, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {

            if(lists.get(position).getTitle() != null && lists.get(position).getTitle().length() > 0 ) {
                holder.Tittle.setText(lists.get(position).getTitle());
            }else {

                holder.Tittle.setText("Resource Value");


            }




            holder.Values.setText(lists.get(position).getValue());

            if (Adminuser.equals("TRUE")) {


                holder.options.setVisibility(View.VISIBLE);


            } else if (Adminuser.equals("FALSE")) {
                holder.options.setVisibility(View.INVISIBLE);

            }





            if(lists.get(position).getValue() != null && lists.get(position).getValue().length() > 0 ) {
                Extension = lists.get(position).getValue().substring(lists.get(position).getValue().lastIndexOf("."));
                Log.d("TAG", "Extension: " + Extension);

                if (Extension.matches(".jpg")) {

                    Glide.clear(holder.extentionImage);
                    Glide.with(mcontext).load(lists.get(position).getValue())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(holder.extentionImage);


                } else if (Extension.matches(".jpeg")) {

                    Glide.clear(holder.extentionImage);
                    Glide.with(mcontext).load(lists.get(position).getValue())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(holder.extentionImage);


                } else if (Extension.matches(".png")) {

                    Glide.clear(holder.extentionImage);
                    Glide.with(mcontext).load(lists.get(position).getValue())
                            .dontAnimate()
                            .placeholder(R.color.black).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(holder.extentionImage);


                } else if (Extension.matches(".docx")) {

                    holder.extentionImage.setImageResource(R.mipmap.ic_doc_download);



                } else if (Extension.matches(".pdf")) {

                    holder.extentionImage.setImageResource(R.mipmap.ic_pdf_download);



                } else if (Extension.matches(".txt")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_text_download);



                } else if (Extension.matches(".zip")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_zip_download);



                } else if (Extension.matches(".doc")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_doc_download);



                }else if (Extension.matches(".xls")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_xls_download);



                } else if (Extension.matches(".xlsx")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_xls_download);



                } else if (Extension.matches(".xlsx")) {
                    holder.extentionImage.setImageResource(R.mipmap.ic_xls_download);



                }

            }else {
                holder.extentionImage.setImageDrawable(null);
            }


            holder.extentionImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                        Utils.downloadFile(mcontext,lists.get(position).getValue());
                    }

            });



            holder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Utils.downloadFile(mcontext,lists.get(position).getValue());

                }
            });

            holder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showPopupMenu();


                }

                private void showPopupMenu() {


                    PopupMenu popup = new PopupMenu(mcontext, holder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.removepopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {

                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Remove")
                                            .setMessage(R.string.Remove_Material)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {

                                                    if (lists.get(position).getPos().matches("True")){

                                                        deletesubjectlinkresource = new DeleteSubjectLinkResource();
                                                        deletesubjectlinkresource.execute(lists.get(position).getPos());


                                                    }else {

                                                        deletesubjectresource = new DeleteSubjectResource();
                                                        deletesubjectresource.execute(lists.get(position).getPos());

                                                    }


                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();



                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();




                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {


        private TextView Tittle,Values;
        LinearLayout link;


        ImageView options,extentionImage;

        public CustomVholder(View itemView) {
            super(itemView);

            Tittle = itemView.findViewById(R.id.Tittle);
            Values = itemView.findViewById(R.id.Values);
            link = itemView.findViewById(R.id.link);
            options = itemView.findViewById(R.id.options);
            extentionImage =itemView.findViewById(R.id.extentionImage);


        }

    }

  /*LISTS OF REMOVE RESOURCE ON SUBJECTS*/


    private class DeleteSubjectResource extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("field","field_attachments")
                    .add("position",args[0])
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_TEACHERREMOVEDOCUMENTS +"/"+course_idMain,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletesubjectresource = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        CourseModuleTeacherInfo.RefreshWorkedsubjectresource();


                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                } else {

                    Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            deletesubjectresource = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }


     /*LISTS OF DELETE LINK RESOURCE ON SUBJECTS*/


    private class DeleteSubjectLinkResource extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("field_course_resource[Title]","")
                    .add("field_course_resource[value]","")
                    .build();




            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_TEACHERREMOVELINK +"/"+course_idMain,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletesubjectlinkresource = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                       // Utils.showAlertDialog(mcontext, "Delete", msg);
                        CourseModuleTeacherInfo.RefreshWorkedsubjectresource();


                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                } else {

                    Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            deletesubjectlinkresource = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }


}


