package co.savm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import co.savm.R;
import co.savm.activities.GroupMemberList;
import co.savm.models.CourseMemberLists;


/**
 * Created by Dell on 12-09-2017.
 */

public class CourseMembersAdapter extends RecyclerView.Adapter<CourseMembersAdapter.RecyclerViewHolders> {
    private String TAG = getClass().getSimpleName();
    private int selectedItem = -1;
    private Context mContext;
    private int mRowLayout;
    String part1;
    private List<CourseMemberLists> itemList;


    public CourseMembersAdapter(Context context, int rowLayout, List<CourseMemberLists> itemList) {
        this.itemList = itemList;
        this.mContext = context;
        this.mRowLayout = rowLayout;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(mRowLayout, null);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {

        final CourseMemberLists photo = itemList.get(position);



        if(itemList.get(position).getPicture() != null && itemList.get(position).getPicture().length() > 0 ) {
            Glide.clear(holder.circleView);
            Glide.with(mContext).load(itemList.get(position).getPicture())
                    .placeholder(R.mipmap.place_holder).dontAnimate()
                    .fitCenter().into(holder.circleView);

        }else {
            holder.circleView.setImageResource(R.mipmap.place_holder);
            Glide.clear(holder.circleView);
        }



        holder.circleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(mContext, GroupMemberList.class)
                        .putExtra("COURSE_ID",itemList.get(position).getGid());
                mContext.startActivity(backIntent);


            }
        });




    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void setSelecteditem(int selecteditem) {
        this.selectedItem = selecteditem;
        notifyDataSetChanged();
    }



    // RecyclerViewHolders Inner Class
    public class RecyclerViewHolders extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        ImageView circleView;


        public RecyclerViewHolders(View convertView) {
            super(convertView);

            circleView =  convertView.findViewById(R.id.circleView);

            convertView.setTag(convertView);
            convertView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

        }
    }


}


