package co.savm.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import co.savm.R;
import co.savm.college.ClassesDetails;
import co.savm.models.SubjectsClassesArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.teacher.UpdateClasses;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

/**
 * Created by Dell on 21-09-2017.
 */

public class SubjectsClassesAdapters extends RecyclerView.Adapter<SubjectsClassesAdapters.CustomVholder> {


    private ArrayList<SubjectsClassesArray> lists;
    String Service_id,time24format,time24format1;
    private Context mcontext;
    private DeleteSubjectClasses deletesubjectclasses = null;
    String subject_idMain;

    public SubjectsClassesAdapters(Context mcontext, ArrayList<SubjectsClassesArray> lists, String subject_idMain) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.subject_idMain = subject_idMain;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_subjectsclass, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_title.setText(lists.get(position).getTitle());
            holder.tv_location.setText(lists.get(position).getRoom());

            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String CurrentDate = sdf.format(c.getTime());


            if (lists.get(position).getEnd_date() !=null && lists.get(position).getEnd_date().length() >0){

                if (CurrentDate.compareTo(lists.get(position).getEnd_date()) > 0){
                    holder.ExpiredClass.setVisibility(View.VISIBLE);
                }else {
                    holder.ExpiredClass.setVisibility(View.GONE);
                }


            }




            if(lists.get(position).getStart_time() != null && lists.get(position).getStart_time().length() > 0 ) {
                SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss",Locale.US);
                SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a",Locale.US);
                Date date;
                try {
                    date = parseFormat.parse(lists.get(position).getStart_time());
                    time24format = displayFormat.format(date);
                    System.out.println(parseFormat.format(date) + " = " + displayFormat.format(date));



                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }else if(lists.get(position).getStart_time() == null)
            {

            }







            if (lists.get(position).getEnd_time() != null && lists.get(position).getEnd_time().length() > 0 ) {
                SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
                SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm a",Locale.US);
                Date date1;
                try {

                    date1 = parseFormat.parse(lists.get(position).getEnd_time());

                    time24format1 = displayFormat.format(date1);

                } catch (ParseException e) {
                    e.printStackTrace();
                }





            }else if(lists.get(position).getStart_time() == null)
            {

            }


            holder.tv_date.setText(time24format+"-"+time24format1+" "+lists.get(position).getWeek() );
            holder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(mcontext,ClassesDetails.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("CLASSES_ID",lists.get(position).getTnid());
                    intent.putExtras(bundle);
                    mcontext.startActivity(intent);
                }
            });

            holder.options.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    showPopupMenuTheory();


                }

                private void showPopupMenuTheory() {


                    PopupMenu popup = new PopupMenu(mcontext, holder.options);
                    MenuInflater inflater = popup.getMenuInflater();
                    inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.Editoption:

                                    Intent i = new Intent(mcontext, UpdateClasses.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("CLASS_ID", lists.get(position).getTnid());
                                    bundle.putString("COURSE_ID", subject_idMain);

                                    i.putExtras(bundle);
                                    mcontext.startActivity(i);

                                    return true;
                                case R.id.deleteoption:

                                    AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                            .setTitle("Delete Class")
                                            .setMessage(R.string.Delete)
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    deletesubjectclasses = new DeleteSubjectClasses();
                                                    deletesubjectclasses.execute(lists.get(position).getTnid());
                                                    lists.remove(position);
                                                    notifyDataSetChanged();


                                                }
                                            })
                                            .setNegativeButton("No", new Dialog.OnClickListener() {

                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                    builder.create().show();


                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();

                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        private TextView tv_title, tv_location,tv_date,ExpiredClass;
        LinearLayout link;
        ImageView options;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_date =itemView.findViewById(R.id.tv_date);
            link = itemView.findViewById(R.id.link);
            options =itemView.findViewById(R.id.options);
            ExpiredClass =itemView.findViewById(R.id.ExpiredClass);


        }

    }

    /*DELETE  SUBJECTS*/

    private class DeleteSubjectClasses extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETESUBJECTS+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletesubjectclasses = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(mcontext, "Error", msg);



                    }
                }else {
                    Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            deletesubjectclasses = null;


        }
    }



}


