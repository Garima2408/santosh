package co.savm.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.UserDisplayProfile;
import co.savm.models.SubjectRequestArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.teacher.SubjectMemberrequestList;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by Dell on 10-01-2018.
 */

public class SubjectRequestMemberAdapter extends RecyclerView.Adapter<SubjectRequestMemberAdapter.CustomVholder> {


    private ArrayList<SubjectRequestArray> lists;
    private Context mcontext;
    private ProcessAcceptStudentRequest acceptstudentrequest = null;
    private ProcessRejectStudentRequest rejectstudentrequest = null;


    public SubjectRequestMemberAdapter(Context mcontext, ArrayList<SubjectRequestArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_requestpending, null);

        return new CustomVholder(view);
    }


    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.tv_friendname.setText(lists.get(position).getField_first_name()+" "+lists.get(position).getField_last_name());
            Log.d("TAG", "ridshow1: "+lists.get(position).getEtid());


            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {

                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.place_holder).dontAnimate()
                        .fitCenter().into(holder.friendIcon);

            }else {
                holder.friendIcon.setImageResource(R.mipmap.place_holder);

            }




            holder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getEtid());
                    mcontext.startActivity(backIntent);


                }
            });

            holder.friendIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getEtid());
                    mcontext.startActivity(backIntent);


                }
            });


            holder.requstAccept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    StudentRequestAccept(lists.get(position).getId());
                    Log.d("TAG", "ridshow2: "+lists.get(position).getId());


                }
            });

            holder.requestreject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    StudentRequestReject(lists.get(position).getId());


                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*METHOD ACCEPTFRIEND REQUEST*/

    private void StudentRequestAccept(String Studentuid) {
        acceptstudentrequest = new ProcessAcceptStudentRequest();
        acceptstudentrequest.execute(Studentuid);


    }



    /*FRIEND REQUEST REJECT*/

    private void StudentRequestReject(String Studentuid) {
        rejectstudentrequest = new ProcessRejectStudentRequest();
        rejectstudentrequest.execute(Studentuid);

    }


    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {

        ImageView friendIcon;

        private TextView tv_friendname,requestreject, requstAccept;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            requestreject = itemView.findViewById(R.id.requestreject);
            requstAccept = itemView.findViewById(R.id.requstAccept);
            friendIcon = itemView.findViewById(R.id.friendIcon);


        }


    }

    /*
    ACCEPT FRIEND REQUEST*/

    private class ProcessAcceptStudentRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("state", "1")
                    .build();


            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_STUDENTSUBJECTREQUESTEDAPPROVED+"/"+args[0], body);


                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            acceptstudentrequest = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // Utils.showAlertDialog(mcontext, "Response", msg);
                        SubjectMemberrequestList.CalledFromAcceptReject();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            acceptstudentrequest = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }


 /*
    REJECT FRIEND REQUEST*/

    private class ProcessRejectStudentRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();




            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_STUDENTSUBJECTREQUESTEDREJECT+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();

                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            rejectstudentrequest = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // Utils.showAlertDialog(mcontext, "Response", msg);
                        SubjectMemberrequestList.CalledFromAcceptReject();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            rejectstudentrequest = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }
}

