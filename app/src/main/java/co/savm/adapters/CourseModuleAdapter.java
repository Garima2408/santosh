package co.savm.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import co.savm.R;
import co.savm.college.CourceModuleDetailPage;
import co.savm.models.CoursemoduleArray;

import static co.savm.models.CoursemoduleArray.COARSE_TYPE;
import static co.savm.models.CoursemoduleArray.PROGRESS_;

/**
 * Created by Dell on 17-08-2017.
 */

public class CourseModuleAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private boolean isLoading;
    private Activity activity;
    private ArrayList<CoursemoduleArray> lists;
    String Student_Value;
    String Join_Value;

    public CourseModuleAdapter(RecyclerView recyclerView, ArrayList<CoursemoduleArray> lists, Activity activity) {
        this.lists = lists;
        this.activity = activity;


    }


    public void addData(List<CoursemoduleArray> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<CoursemoduleArray> list){
        lists.clear();
        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {

                if(lists!=null && lists.size()>0){
                    lists.add(new CoursemoduleArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_coursemodule, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final CoursemoduleArray course = lists.get(position);
            final UserViewHolder userViewHolder = (UserViewHolder) holder;

            userViewHolder.tv_title.setText(Html.fromHtml(course.getTitle()));
            userViewHolder.code.setText(course.getCode());
            userViewHolder.tv_teacher.setText(course.getTeacher());

            if(course.getLogo() != null && course.getLogo().length() > 0 ) {
                Glide.clear(userViewHolder.circleView);
                Glide.with(activity).load(course.getLogo())
                        .placeholder(R.mipmap.clasrrominfo).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(userViewHolder.circleView);

            }else {
                userViewHolder.circleView.setImageResource(R.mipmap.clasrrominfo);
                Glide.clear(userViewHolder.circleView);
            }





            if(lists.get(position).getIs_student().contains("TRUE")) {
                Student_Value ="TRUE";
                Log.d("TAG", "Student_Value" + Student_Value);


            }else if(lists.get(position).getIs_student().contains("FALSE")){
                Student_Value ="FALSE";
                Log.d("TAG", "Student_Value" + Student_Value);

            }


            if(lists.get(position).getIs_member().contains("member")) {
                Join_Value ="member";
                Log.d("TAG", "Join_Value" + Join_Value);


            }else if(lists.get(position).getIs_member().contains("pending")){
                Join_Value ="pending";
                Log.d("TAG", "Join_Value" + Join_Value);

            }else if(lists.get(position).getIs_member().contains("nomember")){
                Join_Value ="nomember";
                Log.d("TAG", "Join_Value" + Join_Value);

            }


            userViewHolder.link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Intent backIntent = new Intent(activity, CourceModuleDetailPage.class)
                            .putExtra("COURSE_ID",lists.get(position).getTnid())
                            .putExtra("TITTLE",lists.get(position).getTitle())
                            .putExtra("CHECKEDVALUETRUE",lists.get(position).getIs_student())
                            .putExtra("JOINREQUEST",lists.get(position).getIs_member());


                    activity.startActivity(backIntent);



                }
            });





        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title, tv_teacher, tv_cradits, code;
        ImageView circleView;
        FrameLayout link;

        public UserViewHolder(View view) {
            super(view);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_teacher = itemView.findViewById(R.id.tv_teacher);
            tv_cradits = itemView.findViewById(R.id.tv_cradits);
            code = itemView.findViewById(R.id.code);
            circleView = itemView.findViewById(R.id.circleView);
            link = itemView.findViewById(R.id.link);
        }
    }
}