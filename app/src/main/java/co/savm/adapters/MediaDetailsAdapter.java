package co.savm.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Random;

import co.savm.R;
import co.savm.imagezoom.ImageViewTouch;
import co.savm.imagezoom.ImageViewTouchBase;



/**
 * Created by Dell on 15-09-2017.
 */

public class MediaDetailsAdapter extends PagerAdapter {



    Context contextm;
    List<String> multipleimage;
    private static LayoutInflater inflater = null;
    ImageView view;
    Bitmap thumbnail = null;

    public MediaDetailsAdapter(Context context, List<String> multipleimage) {
        contextm = context;
        this.multipleimage = multipleimage;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return multipleimage.size();
    }


    @Override
    public boolean isViewFromObject(View view, Object object) {


        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        final   ImageViewTouch imggallary;
        final ImageView downloadimage;

        inflater = (LayoutInflater) contextm.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.multiple_display_image, container, false);

        imggallary = itemView.findViewById(R.id.imageView);
        downloadimage = itemView.findViewById(R.id.downloadimage);


        //  Picasso.with(contextm).load(multipleimage.get(position)).into(view);

        imggallary.setDisplayType(ImageViewTouchBase.DisplayType.FIT_TO_SCREEN);

        getBitmapFromURL(multipleimage.get(position));
        Glide.clear(imggallary);
        Glide.with(contextm).load(multipleimage.get(position))
                .placeholder(R.color.black).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imggallary);



        downloadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(contextm,"Download Image Successfully",Toast.LENGTH_LONG).show();
                saveImageToExternalStorage(thumbnail);
            }
        });



        imggallary.setSingleTapListener(
                new ImageViewTouch.OnImageViewTouchSingleTapListener() {

                    @Override
                    public void onSingleTapConfirmed() {

                        OpenSaveDialogBox(thumbnail);
                    }
                }
        );

        imggallary.setDoubleTapListener(
                new ImageViewTouch.OnImageViewTouchDoubleTapListener() {

                    @Override
                    public void onDoubleTap() {

                    }
                }
        );

        imggallary.setOnDrawableChangedListener(
                new ImageViewTouchBase.OnDrawableChangeListener() {

                    @Override
                    public void onDrawableChanged(Drawable drawable) {

                    }
                }
        );


        //  callthemethod();

        itemView.setTag(position);

        // Add viewpager_item.xml to ViewPager
        container.addView(itemView);


        return itemView;
    }

    public void getBitmapFromURL(final String src) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    URL url = new URL(src);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    thumbnail = BitmapFactory.decodeStream(input);
                    //return thumbnail;
                } catch (IOException e) {
                    e.printStackTrace();
                    // return null;
                }
            }
        }).start();


    }


    private void OpenSaveDialogBox(final Bitmap s) {
        final CharSequence[] items = { "Save Image",  "Cancel" };
        final AlertDialog.Builder builder = new AlertDialog.Builder(contextm);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Save Image")) {

                    //saveImage(s);
                    saveImageToExternalStorage(s);

                } else if (items[item].equals("Cancel")) {

                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }





    private void saveImageToExternalStorage(Bitmap myBitmap) {
        String root = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString();
        File myDir = new File(root + "/Questin Images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Image-" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists())
            file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }


        // Tell the media scanner about the new file so that it is
        // immediately available to the user.
        MediaScannerConnection.scanFile(contextm, new String[]{file.toString()}, null,
                new MediaScannerConnection.OnScanCompletedListener() {
                    public void onScanCompleted(String path, Uri uri) {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });

    }



    private String saveImage(Bitmap image) {
        String savedImagePath = null;

        String imageFileName = "JPEG_" + "FILE_NAME" + ".jpg";
        File storageDir = new File(            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                + "/Questin Images");
        boolean success = true;
        if (!storageDir.exists()) {
            success = storageDir.mkdirs();
        }
        if (success) {
            File imageFile = new File(storageDir, imageFileName);
            savedImagePath = imageFile.getAbsolutePath();
            try {
                OutputStream fOut = new FileOutputStream(imageFile);
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Add the image to the system gallery
            galleryAddPic(savedImagePath);
            Toast.makeText(contextm, "IMAGE SAVED", Toast.LENGTH_LONG).show();
        }
        return savedImagePath;
    }

    private void galleryAddPic(String imagePath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(imagePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        contextm.sendBroadcast(mediaScanIntent);
    }



    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        container.removeView((RelativeLayout) object);

    }

}
