package co.savm.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;

import co.savm.R;
import co.savm.models.StudentBatchDetailArray;

public class StudentBatchDetailsAdapter extends RecyclerView.Adapter<StudentBatchDetailsAdapter.CustomVholder> {

    private ArrayList<StudentBatchDetailArray> lists;
    private Context mcontext;


    public StudentBatchDetailsAdapter(Context mcontext, ArrayList<StudentBatchDetailArray> lists) {
        this.lists = lists;
        this.mcontext = mcontext;
    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_detailstudent_radio, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {

            if (lists.get(position).getBatch() !=null && lists.get(position).getBatch().length() >0){

                holder.Batch_name.setText(lists.get(position).getBatch());
                holder.Batch_name.setVisibility(View.VISIBLE);


            }

            holder.Batch_Radio.setVisibility(View.GONE);




        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        private TextView Batch_name;
        RadioButton Batch_Radio;
        public CustomVholder(View itemView) {
            super(itemView);


            Batch_name = itemView.findViewById(R.id.Batch_name);
            Batch_Radio = itemView.findViewById(R.id.Batch_Radio);



        }

    }







}
