package co.savm.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import co.savm.R;
import co.savm.college.GroupNSocityDetails;
import co.savm.models.ClubsNSocityArray;

import static co.savm.models.ClubsNSocityArray.COARSE_TYPE;
import static co.savm.models.ClubsNSocityArray.PROGRESS_;

/**
 * Created by Dell on 17-08-2017.
 */

public class GroupNSocityAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private boolean isLoading;
    private Activity activity;
    private ArrayList<ClubsNSocityArray> lists;
    String Student_Value;
    String Join_Value;

    public GroupNSocityAdapter(RecyclerView recyclerView, ArrayList<ClubsNSocityArray> lists, Activity activity) {
        this.lists = lists;
        this.activity = activity;


    }


    public void addData(ArrayList<ClubsNSocityArray> lists){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ lists.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(lists);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<ClubsNSocityArray> list){

        this.lists=list;
        notifyDataSetChanged();
    }

    public void addProgress(){

        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                if(lists!=null && lists.size()>0){
                    lists.add(new ClubsNSocityArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };
        handler.post(r);

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_clubs_n_socity, parent, false);
            return new UserViewHolder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserViewHolder) {
            final UserViewHolder userViewHolder = (UserViewHolder) holder;

            try {
                userViewHolder.tv_title.setText(lists.get(position).getTitle());


                if(lists.get(position).getImage() != null && lists.get(position).getImage().length() > 0 ) {

                    Glide.with(activity).load(lists.get(position).getImage())
                            .placeholder(R.mipmap.club).dontAnimate()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .fitCenter().into(userViewHolder.imageIcon);

                }else {
                    userViewHolder.imageIcon.setImageResource(R.mipmap.club);

                }



                userViewHolder.link.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent intent=new Intent(activity,GroupNSocityDetails.class);
                        Bundle bundle=new Bundle();
                        bundle.putString("GROUP_ID",lists.get(position).getTnid());
                        bundle.putString("TITTLE",lists.get(position).getTitle() );
                        bundle.putString("IMAGE",lists.get(position).getImage());
                        intent.putExtras(bundle);
                        activity.startActivity(intent);





                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }

    private class UserViewHolder extends RecyclerView.ViewHolder {
        ImageView imageIcon;
        TextView tv_title;
        RelativeLayout link;


        public UserViewHolder(View view) {
            super(view);

            tv_title = itemView.findViewById(R.id.tv_title);
            imageIcon = itemView.findViewById(R.id.imageIcon);

            link = itemView.findViewById(R.id.link);

        }
    }

}