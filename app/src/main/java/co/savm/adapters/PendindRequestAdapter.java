package co.savm.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.UserDisplayProfile;
import co.savm.models.RequestPendingArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.savm.models.AllUser.COARSE_TYPE;
import static co.savm.models.RequestPendingArray.PROGRESS_;

/**
 * Created by Dell on 23-10-2017.
 */

public class PendindRequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private ArrayList<RequestPendingArray> lists;
     static Activity activity;

    private ProcessAcceptFriendRequest acceptfriendrequest = null;
    private ProcessRejectFriendRequest rejectfriendrequest = null;


    public PendindRequestAdapter(RecyclerView recyclerView, ArrayList<RequestPendingArray> lists, Activity activity) {
        this.lists = lists;
        this.activity=activity;
    }


    public void addData(ArrayList<RequestPendingArray> list){

        int size= this.lists.size();
        Log.i("sizeing", "size of new: "+ list.size()+ " "+ "old: "+ this.lists.size());
        this.lists.addAll(list);             //add kara
        Log.i("sizeing", "size: "+ this.lists.size());
        notifyItemRangeInserted(size, this.lists.size());        //existing size, aur new size
    }

    public void removeProgress(){

        int size= lists!=null && lists.size()>0?lists.size():0;

        if(size>0){

            lists.remove(size-1);
            notifyItemRemoved(size-1);
        }

    }


    @Override
    public int getItemViewType(int position) {

        if(lists!=null && lists.size()>0)
            return lists.get(position).getType();
        return 0;
    }
    public void setInitialData( ArrayList<RequestPendingArray> lists){

        this.lists=lists;
        notifyDataSetChanged();
    }

    public void addProgress(){
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {

                if(lists!=null && lists.size()>0){
                    lists.add(new RequestPendingArray(1));
                    notifyItemInserted(lists.size()-1);
                }
            }
        };

        handler.post(r);
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == COARSE_TYPE) {
            View view = LayoutInflater.from(activity).inflate(R.layout.list_of_requestpending, parent, false);
            return new CustomVholder(view);
        } else if (viewType == PROGRESS_) {
            View view = LayoutInflater.from(activity).inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CustomVholder) {
            final RequestPendingArray course = lists.get(position);
            final CustomVholder userViewHolder = (CustomVholder) holder;


            try {
                userViewHolder.tv_friendname.setText(lists.get(position).getField_firstname() + " " + lists.get(position).getField_lastname());
                Log.d("TAG", "ridshow1: " + lists.get(position).getRid());


                if (lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0) {

                    Glide.with(activity).load(lists.get(position).getPicture())
                            .placeholder(R.mipmap.place_holder).dontAnimate()
                            .fitCenter().into(userViewHolder.friendIcon);

                } else {
                    userViewHolder.friendIcon.setImageResource(R.mipmap.place_holder);

                }




                userViewHolder.tv_friendname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getRid());
                    activity.startActivity(backIntent);


                }
            });

                userViewHolder.friendIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent backIntent = new Intent(activity, UserDisplayProfile.class)
                            .putExtra("USERPROFILE_ID",lists.get(position).getRid());
                    activity.startActivity(backIntent);


                }
            });

                userViewHolder.requstAccept.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                               /*METHOD ACCEPTFRIEND REQUEST*/
                        acceptfriendrequest = new ProcessAcceptFriendRequest();
                        acceptfriendrequest.execute(lists.get(position).getRid());
                        lists.remove(position);
                        notifyDataSetChanged();


                    }
                });

                userViewHolder.requestreject.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {



                        /*FRIEND REQUEST REJECT*/

                        rejectfriendrequest = new ProcessRejectFriendRequest();
                        rejectfriendrequest.execute(lists.get(position).getRid());
                        lists.remove(position);
                        notifyDataSetChanged();



                    }
                });


            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }


    }





    @Override
    public int getItemCount() {
        return lists == null ? 0 : lists.size();
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = view.findViewById(R.id.progressBar1);
        }
    }



    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder  {

        ImageView friendIcon;
        private TextView tv_friendname,requestreject, requstAccept;


        public CustomVholder(View itemView) {
            super(itemView);

            tv_friendname = itemView.findViewById(R.id.tv_friendname);
            requestreject = itemView.findViewById(R.id.requestreject);
            requstAccept = itemView.findViewById(R.id.requstAccept);
            friendIcon =  itemView.findViewById(R.id.friendIcon);


        }



    }

    /*
    ACCEPT FRIEND REQUEST*/

    private class ProcessAcceptFriendRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;

                OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


                RequestBody body = new FormBody.Builder()
                        .add("rid", args[0])
                        .build();


                try {
                    String responseData = ApiCall.POSTHEADER(client, URLS.URL_AXCCEPTREQUEST, body);
                    jsonObject = new JSONObject(responseData);
                    Log.d("TAG", "responseData: " + responseData);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                  //  Utils.p_dialog_dismiss(activity);
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
                return jsonObject;
            }


        @Override
        protected void onPostExecute(JSONObject responce) {
            acceptfriendrequest = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {




                       // PendingRequests.CalledFromPendingAdapter();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                      //  Utils.p_dialog_dismiss(activity);
                        Utils.showAlertDialog(activity, "Error", msg);


                    }
                }
            } catch (JSONException e) {
               // Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            acceptfriendrequest = null;
          //  Utils.p_dialog_dismiss(activity);


        }
    }


 /*
    REJECT FRIEND REQUEST*/

    private class ProcessRejectFriendRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;

                OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


                RequestBody body = new FormBody.Builder()
                        .add("rid", args[0])
                        .add("reason", "don't want to accept")
                        .build();


                try {
                    String responseData = ApiCall.POSTHEADER(client, URLS.URL_REJECTREQUEST, body);
                    jsonObject = new JSONObject(responseData);
                    Log.d("TAG", "responseData: " + responseData);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();

                  //  Utils.p_dialog_dismiss(activity);
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
                return jsonObject;
            }


        @Override
        protected void onPostExecute(JSONObject responce) {
            rejectfriendrequest = null;
            try {
                if (responce != null) {
                  //  Utils.p_dialog_dismiss(activity);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                      //  Utils.showAlertDialog(activity, "Response", msg);
                      //  PendingRequests.CalledFromPendingAdapter();





                    } else if (errorCode.equalsIgnoreCase("0")) {
                      //  Utils.p_dialog_dismiss(activity);
                        Utils.showAlertDialog(activity, "Error", msg);


                    }
                }
            } catch (JSONException e) {
              //  Utils.p_dialog_dismiss(activity);
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            rejectfriendrequest = null;


        }
    }


}


