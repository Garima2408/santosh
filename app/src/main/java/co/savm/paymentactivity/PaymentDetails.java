package co.savm.paymentactivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import co.savm.R;

public class PaymentDetails extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);
        ImageView doneGif =findViewById(R.id.doneGif);

        Glide.with(this).load(R.mipmap.checkcircle).asGif()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .crossFade(10)
                .into(doneGif);

    }

    @Override
    public void onBackPressed() {
        finish();

    }
}
