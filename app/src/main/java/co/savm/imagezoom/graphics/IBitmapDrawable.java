package co.savm.imagezoom.graphics;

import android.graphics.Bitmap;



public interface IBitmapDrawable {

    Bitmap getBitmap();
}
