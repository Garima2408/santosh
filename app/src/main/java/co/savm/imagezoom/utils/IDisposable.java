package co.savm.imagezoom.utils;

public interface IDisposable {

    void dispose();
}
