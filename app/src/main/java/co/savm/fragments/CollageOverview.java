package co.savm.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.savm.R;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;


public class CollageOverview extends Fragment {
    TextView facilities,ErrorText;
    private CollageFacilitesList collagefaciliteslist = null;
    private ProgressBar spinner;
    Activity activity;
    String Overview;
    public CollageOverview() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_collage_overview, container, false);
        activity = (Activity)view.getContext();
        facilities = view.findViewById(R.id.facilities);
        spinner= view.findViewById(R.id.progressBar);
        ErrorText = view.findViewById(R.id.ErrorText);
        collagefaciliteslist = new CollageFacilitesList();
        collagefaciliteslist.execute();

        ErrorText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collagefaciliteslist = new CollageFacilitesList();
                collagefaciliteslist.execute();

            }
        });

        return view;
    }
    private class CollageFacilitesList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEFACILITIES+"?"+"cid"+"="+ SessionManager.getInstance(getActivity()).getCollage().getTnid());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        ErrorText.setVisibility(View.VISIBLE);

                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            collagefaciliteslist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);
                    ErrorText.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        JSONArray mainData = responce.getJSONArray("data");

                        for (int i=0; i<mainData.length();i++) {

                            Overview = mainData.getJSONObject(0).getString("overview");

                        }
                        facilities.setText(Html.fromHtml(Overview));

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.GONE);
                        ErrorText.setVisibility(View.VISIBLE);



                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);
                ErrorText.setVisibility(View.VISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            collagefaciliteslist = null;
            spinner.setVisibility(View.GONE);


        }
    }

}
