package co.savm.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CourseOffered extends Fragment implements View.OnClickListener {

    public CourseOffered() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(co.savm.R.layout.fragment_profile, container, false);

    }


    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initLayout(view);

    }

    private void initLayout(View view) {



}

    @Override
    public void onClick(View v) {

    }
}