package co.savm.helperviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import co.savm.R;

/**
 * Created by farheen on 15/9/17
 */

public class TriangleView extends View {

    private Paint mPaint;
    private Path mPath;
    private boolean isLeft = false;
    private int triangleColor = Color.BLACK;

    public TriangleView(Context context) {
        super(context);
        init();
    }

    public TriangleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TriangleView, 0, 0);
        String direction = typedArray.getString(R.styleable.TriangleView_direction);
        triangleColor = typedArray.getColor(R.styleable.TriangleView_tri_color, Color.parseColor("#eeeeee"));
        if(direction != null){
            if(direction.equals("left")){
                isLeft = true;
            }
            else if(direction.equals("right")){
                isLeft = false;
            }
        }
        typedArray.recycle();
        init();
    }

    public TriangleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init(){
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(triangleColor);
        mPath = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(isLeft){
            mPath.moveTo(getRight(), getTop());
            mPath.lineTo(getRight()/2, getBottom()/2);
            mPath.lineTo(getRight(), getBottom());
            mPath.close();
        }
        else {
            mPath.moveTo(getLeft(), getTop());
            mPath.lineTo(getRight()/2, getBottom()/2);
            mPath.lineTo(getLeft(), getBottom());
            mPath.close();
        }
        canvas.drawPath(mPath, mPaint);
    }
}
