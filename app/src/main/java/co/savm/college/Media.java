package co.savm.college;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.MediaAdapter;
import co.savm.models.MediaArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.settings.FAQ;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class Media extends BaseAppCompactActivity {

    public ArrayList<MediaArray> mmediaList;
    private RecyclerView.LayoutManager layoutManager;
    private MediaAdapter mediaeadapter;
    RecyclerView rv_Collagemedia;
    FloatingActionButton fab;
    SwipeRefreshLayout swipeContainer;
    private static boolean isLoading = false;
    private static int PAGE_SIZE = 0;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;


    private CollageMediaShow collagemediashow = null;
    String gallery, video_url, media_type_demo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        ErrorText =findViewById(R.id.ErrorText);

        rv_Collagemedia = findViewById(R.id.rv_Collagemedia);
        swipeContainer =findViewById(R.id.swipeContainer);
        mmediaList = new ArrayList<>();
        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();
        mediaeadapter = new MediaAdapter(this, mmediaList);
        layoutManager = new LinearLayoutManager(this);
        fab = findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Media.this, FAQ.class));

            }
        });

        rv_Collagemedia.setHasFixedSize(true);
        rv_Collagemedia.setLayoutManager(layoutManager);
        rv_Collagemedia.setAdapter(mediaeadapter);


        collagemediashow = new CollageMediaShow();
        collagemediashow.execute();


        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        inItView();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

                        PAGE_SIZE = 0;
                        isLoading = false;
                        swipeContainer.setRefreshing(true);
                        collagemediashow = new CollageMediaShow();
                        collagemediashow.execute();

                    }
                }, 3000);
            }
        });




    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","media");

        super.onPause();
    }


    private void inItView() {

        swipeContainer.setRefreshing(false);

        rv_Collagemedia.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if (!isLoading) {


                    Log.i("loadinghua", "im here now");
                    isLoading = true;
                    PAGE_SIZE += 20;

                    mediaeadapter.addProgress();

                    collagemediashow = new CollageMediaShow();
                    collagemediashow.execute();
                } else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }


    private  class CollageMediaShow extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGMEDIA +"?cid=250905"+ "&offset=" + PAGE_SIZE + "&limit=20");

                jsonObject = new JSONObject(responseData);

                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                Media.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);

                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

                mediaeadapter.removeProgress();
                isLoading = false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            collagemediashow = null;
            try {
                if (responce != null) {
                    rv_Collagemedia.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);


                    // hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<MediaArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            MediaArray CourseInfo = new MediaArray(0);
                            arrayList.add(CourseInfo);
                            JSONObject jsonObject1 = jsonArrayData.optJSONObject(i);
                            String tnd = jsonObject1.optString("tnid");
                            String title = jsonObject1.optString("title");



                            JSONArray mediaType = jsonObject1.getJSONArray("media_type");
                            if(mediaType != null && mediaType.length() > 0 ) {
                                for (int j = 0; j < mediaType.length(); j++) {

                                    media_type_demo =mediaType.getString(j);


                                    Log.e("mediaType", mediaType.getString(j));

                                }
                            }





                            if (media_type_demo.equals("Image Gallery")){

                                JSONArray picture = jsonObject1.getJSONArray("gallery");
                                if(picture != null && picture.length() > 0 ) {
                                    for (int j = 0; j < picture.length(); j++) {
                                        Log.e("Media pictire", picture.getString(j));
                                        CourseInfo.setGallery(picture.getString(0));

                                    }
                                }


                            }else if (media_type_demo.equals("Embedded Video"))
                            {
                                if(jsonObject1.optString("video") !=null && jsonObject1.optString("video").length() > 0){
                                    video_url = jsonObject1.optString("video");
                                    CourseInfo.setVideo(video_url);
                                }



                            }

                            CourseInfo.setTitle(title);
                            CourseInfo.setMedia_type(media_type_demo);
                            CourseInfo.setTnid(tnd);









                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_Collagemedia.setVisibility(View.VISIBLE);
                                mediaeadapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                isLoading = false;
                                mediaeadapter.removeProgress();
                                swipeContainer.setRefreshing(false);
                                mediaeadapter.addData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }
                        }

                         else{
                            if (PAGE_SIZE==0){
                                rv_Collagemedia.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No media");
                                swipeContainer.setVisibility(View.GONE);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                mediaeadapter.removeProgress();

                        }

                        // view.setVisibility(View.GONE);

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        showAlertDialog(msg);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);


                    }

                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);

            }
        }
        @Override
        protected void onCancelled() {
            collagemediashow = null;
            swipeContainer.setRefreshing(false);


        }
    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}


