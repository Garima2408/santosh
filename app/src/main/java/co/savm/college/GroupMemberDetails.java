package co.savm.college;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.GroupMembersAdapter;
import co.savm.models.CourseMemberLists;
import co.savm.models.SubjectCommentArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class GroupMemberDetails extends BaseFragment {
    String Group_id;
    RecyclerView GroupMember_Lists,rv_subject_comment;
    Button JoinGroup;
    RecyclerView.LayoutManager mLayoutManager,layoutManager;
    GroupMembersAdapter mGroupMemberAdapter;
    public ArrayList<CourseMemberLists> memberList;
    public ArrayList<SubjectCommentArray> commentsList;
    private GroupMemberList groupmemberList = null;
    private ProgressJoinTheGroupRequest joingrouprequestList = null;
  /*  private GroupsAddComments addcommentingroups = null;*/
   // private SubjectsCommentsList subjectscommentsList = null;
    static Activity activity;
    private static ProgressBar spinner;





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_group_member_details, container, false);
        activity = (Activity) view.getContext();
        Group_id = getArguments().getString("GROUP_ID");
        spinner= view.findViewById(R.id.progressBar);
        JoinGroup = view.findViewById(R.id.JoinGroup);
        GroupMember_Lists = view.findViewById(R.id.GroupMember_Lists);
        rv_subject_comment = view.findViewById(R.id.rv_subject_comment);
        layoutManager = new LinearLayoutManager(activity);
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);

        GroupMember_Lists.setLayoutManager(mLayoutManager);
        rv_subject_comment.setHasFixedSize(true);
        rv_subject_comment.setLayoutManager(layoutManager);

        memberList = new ArrayList<CourseMemberLists>();
        commentsList = new ArrayList<SubjectCommentArray>();

        /*GROUP MEMBER LIST CALLING*/

        groupmemberList = new GroupMemberList();
        groupmemberList.execute();





        JoinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*JOIN ANY GROUP AND SOCITY CALLING*/

                joingrouprequestList = new ProgressJoinTheGroupRequest();
                joingrouprequestList.execute();
            }
        });


        return view;
    }





    /*GROUP MEMBER LIST CALLING METHODS*/

    private class GroupMemberList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_GROUPMEMBERLISTS+"/"+Group_id+"/"+"users"+"?"+"fields"+"="+"id,etid,gid");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);



                                if (CourseInfo.getEtid().contains(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()))
                                {
                                    Log.d("TAG", "myid: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());

                                    JoinGroup.setVisibility(View.GONE);
                                          /* CALLING LISTS OF COMMENTS*/
                                   /* subjectscommentsList = new SubjectsCommentsList();
                                    subjectscommentsList.execute();*/


                                }



                            }
                            mGroupMemberAdapter = new GroupMembersAdapter(activity, R.layout.list_of_groupmemberlists,memberList);
                            GroupMember_Lists.setAdapter(mGroupMemberAdapter);
                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    showAlertDialog(getString(R.string.error_responce));

                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }


      /*JOIN ANY GROUP AND SOCITY CALLING METHODS*/

    private class ProgressJoinTheGroupRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("type","subgroup")
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_JOINGROUP+"/"+Group_id+"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            joingrouprequestList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // showAlertFragmentDialog(activity,"Information", msg);


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                    Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            joingrouprequestList = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }





}

