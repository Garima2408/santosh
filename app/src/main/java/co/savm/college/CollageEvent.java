package co.savm.college;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.CollageEventAdapters;
import co.savm.models.CollageEventArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.settings.FAQ;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class  CollageEvent extends BaseAppCompactActivity {


    public ArrayList<CollageEventArray> mEventList;
    private RecyclerView.LayoutManager layoutManager;
    private CollageEventAdapters eventeadapter;
    RecyclerView rv_collageevent;
    FloatingActionButton fab;
    SwipeRefreshLayout swipeContainer;
    private CollageEventList collageeventlist = null;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static TextView ErrorText;
    private static ShimmerFrameLayout mShimmerViewContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_event);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        mEventList=new ArrayList<>();
        ErrorText =findViewById(R.id.ErrorText);
        swipeContainer =findViewById(R.id.swipeContainer);
        fab = findViewById(R.id.fab);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();
        rv_collageevent = findViewById(R.id.rv_collageevent);
        layoutManager = new LinearLayoutManager(this);
        rv_collageevent.setHasFixedSize(true);
        rv_collageevent.setLayoutManager(layoutManager);

        eventeadapter = new CollageEventAdapters(rv_collageevent,mEventList,CollageEvent.this);
        rv_collageevent.setAdapter(eventeadapter);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity (new Intent(CollageEvent.this, FAQ.class));

            }
        });


        inItView();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        PAGE_SIZE+=20;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);


                        // cancle the Visual indication of a refresh
                   /*     swipeContainer.setRefreshing(false);
                        mEventList.clear();*/
                        collageeventlist = new CollageEventList();
                        collageeventlist.execute();

                    }
                }, 3000);
            }
        });

        collageeventlist = new CollageEventList();
        collageeventlist.execute();


    }

    private void inItView() {


        swipeContainer.setRefreshing(false);

        rv_collageevent.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    eventeadapter.addProgress();

                    collageeventlist = new CollageEventList();
                    collageeventlist.execute();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });



    }


    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }


    private class CollageEventList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEEVENT+"?"+"cid=250905"+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                  Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CollageEvent.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
              collageeventlist = null;
            try {
                if (responce != null) {



                    rv_collageevent.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {



                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<CollageEventArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            CollageEventArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CollageEventArray.class);
                            arrayList.add(CourseInfo);

                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_collageevent.setVisibility(View.VISIBLE);
                                eventeadapter.setInitialData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            } else {
                                isLoading = false;
                                eventeadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                eventeadapter.addData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }
                        }

                        else{
                            if (PAGE_SIZE==0){
                                rv_collageevent.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Event");
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                                swipeContainer.setVisibility(View.GONE);
                            }else
                                eventeadapter.removeProgress();

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);




                    }
                }
            } catch (JSONException e) {

                swipeContainer.setRefreshing(false);

            }
        }

        @Override
        protected void onCancelled() {
            collageeventlist = null;

            swipeContainer.setRefreshing(false);


        }
    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }



}


