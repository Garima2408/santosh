package co.savm.college;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.CourseMembersAdapter;
import co.savm.adapters.SubjectsCommentsAdapter;
import co.savm.models.CourseMemberLists;
import co.savm.models.SubjectCommentArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CourseModuleGroup extends BaseFragment {
    String Course_id,comments;
    static String Course_idMain,Adminuser,Join_Value;
    RecyclerView GroupMember_Lists;
    static RecyclerView rv_subject_comment;
    static Button JoinGroup;
    CardView card_view1;
    RecyclerView.LayoutManager mLayoutManager,layoutManager;
    CourseMembersAdapter mGroupMemberAdapter;
    static SubjectsCommentsAdapter mcommentAdapter;
    public ArrayList<CourseMemberLists> memberList;
    public static ArrayList<SubjectCommentArray> commentsList;
    private GroupMemberList groupmemberList = null;
    private ProgressJoinTheGroupRequest joingrouprequestList = null;
    private static SubjectsCommentsList subjectscommentsList = null;
    static TextView ErrorText;
    static Activity activity;
    static String tittle,URLDoc;
    static SwipeRefreshLayout swipeContainer;
    private static ProgressBar spinner;
    static FloatingActionButton fab;
    private static boolean isLoading= true;
    private boolean _hasLoadedOnce= false; // your boolean field

    private static ShimmerFrameLayout mShimmerViewContainer;


    private static int PAGE_SIZE = 0;



    public CourseModuleGroup() {

        
        // Required empty public constructor

    }




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
//            PAGE_SIZE=0;
//        }
//    }


    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(isFragmentVisible_);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                Log.e("view", "CollegeNewFeed");
                PAGE_SIZE = 0;
                isLoading = false;
                swipeContainer.setRefreshing(true);
                subjectscommentsList = new SubjectsCommentsList();
                subjectscommentsList.execute(Course_idMain);
                _hasLoadedOnce = true;
            }
            groupmemberList = new GroupMemberList();
            groupmemberList.execute(Course_idMain);

        }
    }

    @Override
    public void onPause() {

        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");
        _hasLoadedOnce= false;
        super.onPause();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_course_module_group, container, false);
        activity = (Activity) view.getContext();
        Course_id = getArguments().getString("COURSE_ID");
        Course_idMain= getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        Join_Value = getArguments().getString("JOINED");
        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        Log.d("TAG", "Course_idMain: " + Course_idMain);
        JoinGroup = view.findViewById(R.id.JoinGroup);
        card_view1 = view.findViewById(R.id.card_view1);
        GroupMember_Lists = view.findViewById(R.id.GroupMember_Lists);
        spinner= view.findViewById(R.id.progressBar);
        rv_subject_comment = view.findViewById(R.id.rv_subject_comment);
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        layoutManager = new LinearLayoutManager(activity);
        GroupMember_Lists.setLayoutManager(mLayoutManager);
        rv_subject_comment.setHasFixedSize(true);
        rv_subject_comment.setLayoutManager(layoutManager);
        fab = view. findViewById(R.id.fab);
        ErrorText= view. findViewById(R.id.ErrorText);
        memberList = new ArrayList<CourseMemberLists>();
        commentsList = new ArrayList<SubjectCommentArray>();


        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();




        mcommentAdapter = new SubjectsCommentsAdapter(rv_subject_comment, commentsList ,activity);
        rv_subject_comment.setAdapter(mcommentAdapter);

      /*CALLING LISTS OF STUDENTS*/
        groupmemberList = new GroupMemberList();
        groupmemberList.execute(Course_idMain);



        JoinGroup.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                joingrouprequestList = new ProgressJoinTheGroupRequest();
                joingrouprequestList.execute(Course_idMain);
            }
        });

        if (Adminuser.equals("TRUE")) {

            if(SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                fab.setVisibility(View.GONE);
                JoinGroup.setVisibility(View.GONE);

            }else {

            }




        } else if (Adminuser.equals("FALSE")) {


        }

        if (Join_Value.equals("member")) {

            JoinGroup.setVisibility(View.GONE);
            rv_subject_comment.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (dy > 0) {
                        fab.hide();
                    } else if (dy < 0) {
                        fab.show();
                    }
                }
            });


            Animation makeInAnimation = AnimationUtils.makeInAnimation(activity,false);
            makeInAnimation.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationEnd(Animation animation) { }

                public void onAnimationRepeat(Animation animation) { }

                public void onAnimationStart(Animation animation) {
                    fab.setVisibility(View.VISIBLE);
                }
            });

            Animation makeOutAnimation = AnimationUtils.makeOutAnimation(activity,true);
            makeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationEnd(Animation animation) {
                    fab.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) { }

                @Override
                public void onAnimationStart(Animation animation) { }
            });


            if (fab.isShown()) {
                fab.startAnimation(makeOutAnimation);
            }

            if (!fab.isShown()) {
                fab.startAnimation(makeInAnimation);
            }


        } else if (Join_Value.equals("pending")) {

            swipeContainer.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
            JoinGroup.setText("Request Sent");
            JoinGroup.setOnClickListener(null);
            JoinGroup.setVisibility(View.VISIBLE);
        }
        else if (Join_Value.equals("nomember")) {

            if(SessionManager.getInstance(getActivity()).getCollage().getType() !=null){
                if(SessionManager.getInstance(getActivity()).getCollage().getType().matches("Schools")) {

                    swipeContainer.setVisibility(View.GONE);
                    JoinGroup.setText("Join");
                    JoinGroup.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.GONE);


                }else {
                    if(SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                        fab.setVisibility(View.GONE);
                        JoinGroup.setVisibility(View.GONE);

                    }else {

                        swipeContainer.setVisibility(View.GONE);
                        JoinGroup.setText("Join");
                        JoinGroup.setVisibility(View.VISIBLE);
                        fab.setVisibility(View.GONE);

                    }
                }

                }






        }



        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent backIntent = new Intent(activity, AddSubjectComment.class)
                        .putExtra("COURSE_ID",Course_id)
                        .putExtra("open","StudentCourceGroup");
                startActivity(backIntent);




            }
        });






        return view;
    }





    private void inItView(){



        swipeContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(false);
        CallTheSpin();
        rv_subject_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();

                    subjectscommentsList = new SubjectsCommentsList();
                    subjectscommentsList.execute(Course_idMain);
                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }








    public static void RefreshWorkedStudent() {
        PAGE_SIZE=0;
        isLoading=true;
        /* CALLING LISTS OF COMMENTS*/
        subjectscommentsList = new SubjectsCommentsList();
        subjectscommentsList.execute(Course_idMain);


    }

    /*STUDENTS ON THE SUBJECTS*/



    private class GroupMemberList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMEMBERLISTS+"/"+args[0]+"/"+"users"+"?"+"fields"+"="+"id, etid, gid, state&parameters=state=1");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                      //  Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {

                    memberList.clear();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);
                                if (CourseInfo.getEtid().equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()))
                                {
                                    Log.d("TAG", "myid: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());

                                    JoinGroup.setVisibility(View.GONE);
                             /*       fab.setVisibility(View.VISIBLE);*/
                                    inItView();



                                       /*    CALLING LISTS OF COMMENTS*/

//                                    subjectscommentsList = new SubjectsCommentsList();
//                                    subjectscommentsList.execute(Course_idMain);
                                }else {
                                    // JoinGroup.setVisibility(View.VISIBLE);

                                   /* JoinGroup.setVisibility(View.VISIBLE);
                                    rv_subject_comment.setVisibility(View.GONE);
                                    fab.setVisibility(View.GONE);
                                    swipeContainer.setRefreshing(false);
                                    swipeContainer.setVisibility(View.GONE);*/
                                }


                            }
                            mGroupMemberAdapter = new CourseMembersAdapter(activity, R.layout.list_of_groupmemberlists,memberList);
                            GroupMember_Lists.setAdapter(mGroupMemberAdapter);
                            card_view1.setVisibility(View.VISIBLE);

                        }else {

                           /* JoinGroup.setVisibility(View.VISIBLE);
                            rv_subject_comment.setVisibility(View.GONE);
                            fab.setVisibility(View.GONE);*/
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                    Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {

                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            spinner.setVisibility(View.INVISIBLE);



        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        setUserVisibleHint(true);
    }
    private void CallTheSpin()
    {
//        fab.setVisibility(View.VISIBLE);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {


                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh

//                        PAGE_SIZE=0;
//                        isLoading=false;
//
//                        swipeContainer.setRefreshing(false);
//                        subjectscommentsList = new SubjectsCommentsList();
//                        subjectscommentsList.execute(Course_idMain);

                        if (!isLoading){
                            RefreshWorkedStudent();
                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }




                    }
                }, 3000);
            }
        });


    }

/*LISTS OF COMMENTS ON SUBJECTS*/

    private static class SubjectsCommentsList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ErrorText.setVisibility(View.GONE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTSCOMMENTS+"?"+"sid"+"="+args[0]+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);


            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                      //  Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

                mcommentAdapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            try {
                if (responce != null) {
                    rv_subject_comment.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        ArrayList<SubjectCommentArray> arrayList = new ArrayList<>();
                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {



                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectCommentArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectCommentArray.class);

                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                if (jsonObject.has("field_comment_attachments")){
                                    JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                    if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                        URLDoc = AttacmentsFeilds.getString("value");
                                        tittle = AttacmentsFeilds.getString("title");
                                        subjects.title =tittle;
                                        subjects.url =URLDoc;

                                    }else {

                                    }
                                }
                                arrayList.add(subjects);



                            }

                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_subject_comment.setVisibility(View.VISIBLE);
                                mcommentAdapter.setInitialData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                mcommentAdapter.removeProgress();
                                swipeContainer.setRefreshing(false);
                                mcommentAdapter.addData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }

                            isLoading = false;

                        } else{
                            if (PAGE_SIZE==0){
                                rv_subject_comment.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Post");
                                swipeContainer.setVisibility(View.GONE);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                mcommentAdapter.removeProgress();

                        }









                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                      //  Utils.showAlertFragmentDialog(activity,"Information", msg);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);






                    }
                }else {
                    swipeContainer.setRefreshing(false);
                   // Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
               // Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            subjectscommentsList = null;
            swipeContainer.setRefreshing(false);



        }
    }







    /*SEND REQUEST FOR JOINING SUBJECTS*/





    private class ProgressJoinTheGroupRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("etid",SessionManager.getInstance(getActivity()).getUser().userprofile_id)
                    .add("group_type","node")
                    .add("gid",args[0])
                    .add("state","2")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_JOININSUBJECTS,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                       // Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            joingrouprequestList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        // showAlertFragmentDialog(activity,"Information", msg);
                       /* Intent i =new Intent(activity,CoursesModules.class);
                        activity.startActivity(i);*/
                        CoursesModules.CallFrromJoin();
                        activity.finish();
                        //  CallAboveMethods();

                        //  CallWorked();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                       // Utils.showAlertFragmentDialog(activity,"Information", msg);


                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                  //  Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            joingrouprequestList = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }

    private void CallWorked() {
        JoinGroup.setText("Request Sent");
        JoinGroup.setOnClickListener(null);
    }

    public static void CallAboveMethods() {

        JoinGroup.setVisibility(View.GONE);
        commentsList.clear();
        /* CALLING LISTS OF COMMENTS*/
        subjectscommentsList = new SubjectsCommentsList();
        subjectscommentsList.execute(Course_idMain);


    }

    @Override
    public void onDestroy() {
        isLoading=false;
        PAGE_SIZE=0;
        activity.finish();
        super.onDestroy();
    }


}

