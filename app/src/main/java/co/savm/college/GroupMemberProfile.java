package co.savm.college;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.savm.R;

public class GroupMemberProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member_profile);
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
