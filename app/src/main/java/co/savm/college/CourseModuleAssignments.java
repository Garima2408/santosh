package co.savm.college;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.StudentSubjectsAssignmentsAdapter;
import co.savm.models.SubjectsAssignmentsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import okhttp3.OkHttpClient;

public class CourseModuleAssignments extends BaseFragment {
    public ArrayList<SubjectsAssignmentsArray> mAssignmentList;
    private StudentSubjectsAssignmentsAdapter assignmentAdapters;
    RecyclerView rv_subject_assignments;
    private RecyclerView.LayoutManager layoutManager;
    private SubjectsAssignmentsDisplay  assignmentdisplay = null;
    String Subject_id,Adminuser;
    static Activity activity;
    private static ProgressBar spinner;
    TextView ErrorText;
    static ImageView ReloadProgress;
    static SwipeRefreshLayout swipeContainer;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_course_module_assignments, container, false);
        activity = (Activity) view.getContext();
        Subject_id = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        spinner= view.findViewById(R.id.progressBar);

        mAssignmentList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(activity);
        rv_subject_assignments =view.findViewById(R.id.rv_subject_assignments);
        rv_subject_assignments.setHasFixedSize(true);
        rv_subject_assignments.setLayoutManager(layoutManager);
        ErrorText =view.findViewById(R.id.ErrorText);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        swipeContainer =view.findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        if (Adminuser.equals("TRUE")) {
            assignmentdisplay = new SubjectsAssignmentsDisplay();
            assignmentdisplay.execute();



        } else if (Adminuser.equals("FALSE")) {

            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");
        }


        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAssignmentList.clear();
                assignmentdisplay = new SubjectsAssignmentsDisplay();
                assignmentdisplay.execute();


            }
        });

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        mAssignmentList.clear();
                        assignmentdisplay = new SubjectsAssignmentsDisplay();
                        assignmentdisplay.execute();


                    }
                }, 1000);
            }
        });



        return view;
    }

    private class SubjectsAssignmentsDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            swipeContainer.setRefreshing(true);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTASSIGNMENTS+"?"+"sid="+Subject_id+"&offset="+"0"+"&limit=50");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            assignmentdisplay = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectsAssignmentsArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectsAssignmentsArray.class);
                                mAssignmentList.add(CourseInfo);

                                assignmentAdapters = new StudentSubjectsAssignmentsAdapter(activity, mAssignmentList);
                                rv_subject_assignments.setAdapter(assignmentAdapters);


                            }
                        }else {


                            ErrorText.setVisibility(View.VISIBLE);


                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            assignmentdisplay = null;
            swipeContainer.setRefreshing(false);


        }
    }

}


