package co.savm.college;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class AcademicDivision extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.savm.R.layout.activity_academicdivision);
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
