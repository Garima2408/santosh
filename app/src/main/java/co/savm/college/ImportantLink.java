package co.savm.college;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.ImportantLinkAdapters;
import co.savm.models.ListImportanLink;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class ImportantLink extends BaseAppCompactActivity {
    RecyclerView  rv_ImportantLinks;
    SwipeRefreshLayout swipeContainer;

    private ImportantLinkDisplay importantlinkdisplay = null;
    public ArrayList<ListImportanLink> mLinkList;
    private RecyclerView.LayoutManager layoutManager;
    private ImportantLinkAdapters linkadapter;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private static ShimmerFrameLayout mShimmerViewContainer;
    static TextView ErrorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(co.savm.R.layout.activity_important_link);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        mLinkList=new ArrayList<>();

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        ErrorText = findViewById(R.id.ErrorText);
        layoutManager = new LinearLayoutManager(this);
        rv_ImportantLinks = findViewById(co.savm.R.id.rv_ImportantLinks);
        rv_ImportantLinks.setHasFixedSize(true);
        rv_ImportantLinks.setLayoutManager(layoutManager);

        linkadapter = new ImportantLinkAdapters(ImportantLink.this, mLinkList);
        rv_ImportantLinks.setAdapter(linkadapter);


        importantlinkdisplay = new ImportantLinkDisplay();
        importantlinkdisplay.execute();
        swipeContainer =findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        importantlinkdisplay = new ImportantLinkDisplay();
                        importantlinkdisplay.execute();

                    }
                }, 3000);
            }
        });

        inItView();
    }

    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_ImportantLinks.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    linkadapter.addProgress();

                    importantlinkdisplay = new ImportantLinkDisplay();
                    importantlinkdisplay.execute();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }




    private class ImportantLinkDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //   showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_IMPORTANTLINKS + "?cid=" + SessionManager.getInstance(getActivity()).getCollage().getTnid() + "&offset=" + PAGE_SIZE + "&limit=20");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData + PAGE_SIZE);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                ImportantLink.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        showAlertDialog(getString(co.savm.R.string.error_something_wrong));
                    }
                });

                linkadapter.removeProgress();
                isLoading = false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            importantlinkdisplay = null;
            try {
                if (responce != null) {

                    rv_ImportantLinks.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        ArrayList<ListImportanLink> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {


                            JSONArray currentDataLink = new JSONArray(jsonArrayData.getJSONObject(i).getString("link"));



                            for (int j = 0; j < currentDataLink.length(); j++) {
                                String t =currentDataLink.getJSONObject(j).getString("title");
                                String p =currentDataLink.getJSONObject(j).getString("path");
                                Log.d("TAG", "titlepath: " + t+p);

                                ListImportanLink LinkInfo = new ListImportanLink(0);
                                LinkInfo.title =currentDataLink.getJSONObject(j).getString("title");
                                LinkInfo.path =currentDataLink.getJSONObject(j).getString("path");

//                            mLinkList.add(CourseInfo);

//                            linkadapter = new ImportantLinkAdapters(ImportantLink.this, mLinkList);
//                            rv_ImportantLinks.setAdapter(linkadapter);
                                arrayList.add(LinkInfo);


                            }


                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_ImportantLinks.setVisibility(View.VISIBLE);
                                linkadapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                isLoading = false;
                                linkadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                linkadapter.addData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }
                        }

                        else{
                            if (PAGE_SIZE==0){
                                rv_ImportantLinks.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Important Link");
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                                swipeContainer.setVisibility(View.GONE);
                            }else
                                linkadapter.removeProgress();

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            importantlinkdisplay = null;


        }


    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
