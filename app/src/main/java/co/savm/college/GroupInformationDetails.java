package co.savm.college;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.savm.R;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class GroupInformationDetails extends BaseFragment implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener{
    private boolean isMapInitialized = false;
    private boolean isDetailsFetched = false;
    String Group_id,GroupTittle,groupImage,groupdetails,groupemail;
    TextView details,email;
    static Activity activity;
    private static ProgressBar spinner;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    //Our Map
    private GoogleMap mMap;
    MapView mapView;
    Double Lat,Long;
    private double longitude;
    private double latitude;
    private ProgressGroupInfo groupinfoAuthTask = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_group_information_details, container, false);
        activity = (Activity) view.getContext();
        Group_id = getArguments().getString("GROUP_ID");
        GroupTittle = getArguments().getString("TITTLE");
        groupdetails  = getArguments().getString("GROUP_DETAILS");
        groupemail  = getArguments().getString("GROUP_EMAIL");

        spinner= view.findViewById(R.id.progressBar);
        details = view.findViewById(R.id.details);
        email = view.findViewById(R.id.email);
        mapView =view.findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);


        mapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMapp) {
                mMap = mMapp;
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity
                        , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    mMap.getUiSettings().setScrollGesturesEnabled(false);


                } else {
                    Log.e("TAG", "onMapReady: ");
                    if(isDetailsFetched && !isMapInitialized){
                        isMapInitialized = true;

                    }
                }

            }
        });

        GetAllEventDetails();

        return view;
    }

    private void GetAllEventDetails() {
        groupinfoAuthTask = new ProgressGroupInfo();
        groupinfoAuthTask.execute();


    }

    private class ProgressGroupInfo extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGEGROUPINFO+"/"+Group_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupinfoAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");

                        String groupdetails =data.getString("body");
                        String groupdemail =data.getString("field_team_email");

                        try {
                            if (data.has("field_dept_location")) {
                                JSONObject locate = data.getJSONObject("field_dept_location");
                                if (locate != null && locate.length() > 0 ){
                                    Lat = Double.valueOf(locate.getString("lat"));
                                    Long = Double.valueOf(locate.getString("lng"));

                                    Log.d("TAG", "location: " + Lat + Long );


                                }else {

                                }
                            }
                        }catch (JSONException e){

                        }


                        isDetailsFetched = true;
                        details.setText(Html.fromHtml("<p><b>"+GroupTittle+" "+ "</b><br/><br/>"+groupdetails));
                        email.setText(groupdemail);
                        if(!isMapInitialized){
                            isMapInitialized = true;
                            moveMap();
                        }
                        //
                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            groupinfoAuthTask = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }




    @Override
    public void onStart() {
        googleApiClient.reconnect();

        super.onStart();
        if(mapView != null) mapView.onStart();


        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

        int response = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity);
        if (response != ConnectionResult.SUCCESS) {
            Log.d("TAG", "Google Play Service Not Available");
            GoogleApiAvailability.getInstance().getErrorDialog(activity, response, 1).show();
        } else {
            Log.d("TAG", "Google play service available");
        }



        if(mapView != null) mapView.onResume();
        Log.e("onResume","onResume");




    }

    @Override
    public void onPause() {
        if(mapView != null) mapView.onPause();
        super.onPause();
    }

    @Override
    public void onStop() {
        if(mapView != null) mapView.onStop();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);


            //moving the map to location
            moveMap();
        }
    }

    private void moveMap() {
        //String to display current latitude and longitude
        String msg = latitude + ", " + longitude;
        Log.d("TAG", "msg: " + msg);

        if (Lat !=null  && Long  !=null){

            LatLng latLng = new LatLng(Lat, Long);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(false) //Making the marker draggable
                    .title(GroupTittle)); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(22));
            mMap.getUiSettings().setAllGesturesEnabled(false);

        }else if (SessionManager.getInstance(getActivity()).getCollage().getLat() != null & SessionManager.getInstance(getActivity()).getCollage().getLng() != null) {
            Lat = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLat());
            Long = Double.valueOf(SessionManager.getInstance(getActivity()).getCollage().getLng());
            LatLng latLng = new LatLng(Lat, Long);


            //Adding marker to map
            mMap.addMarker(new MarkerOptions()
                    .position(latLng) //setting position
                    .draggable(false) //Making the marker draggable
                    .title(GroupTittle)); //Adding a title
            //Moving the camera
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

            //Animating the camera
            mMap.animateCamera(CameraUpdateFactory.zoomTo(22));
            mMap.getUiSettings().setAllGesturesEnabled(false);
            //Displaying current coordinates in toast
            //  Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        this.mMap = googleMap;
        mMap.getUiSettings().setScrollGesturesEnabled(false);

        getCurrentLocation();


    }




    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {


    }


    public void onSearch() {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

    }




}