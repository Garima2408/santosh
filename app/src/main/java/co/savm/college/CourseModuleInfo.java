package co.savm.college;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.StudentFacultyListInSubjectAdapter;
import co.savm.adapters.StudentSubjectInfoResourceAdapter;
import co.savm.models.InfoArray;
import co.savm.models.SubjectFacultyArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import okhttp3.OkHttpClient;

public class CourseModuleInfo extends BaseFragment {
    String Course_id,Adminuser,Course_idMain;
    private ProgressSubjectInfo subjectinfoAuthTask = null;
    ImageView subjectlogo;
    TextView subjectCode, Acadamic, Samester, subjectCredit, tittle, about, questionpaper, attachment, resource, syllabus, vedio;
    RecyclerView resourceList,TeacherList;
    private RecyclerView.LayoutManager layoutManager,mlayoutManager;
    public ArrayList<InfoArray> mInfoList;

    private StudentSubjectInfoResourceAdapter infoadapter;
    public ArrayList<SubjectFacultyArray> mfacultyList;
    private StudentFacultyListInSubjectAdapter facultyadapter;
    private SubjectTeacherDisplay facultylistdisplay = null;
    private ProgressDialog pDialog;
    ImageView my_image;
    static Activity activity;
    private static ProgressBar spinner;
    public static final int progress_bar_type = 0;
    String storeDir;

    public CourseModuleInfo() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_course_module_info, container, false);
        activity = (Activity) view.getContext();
        Course_id = getArguments().getString("COURSE_ID");
        Course_idMain = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");

        if (Course_id == null) {
        }


        mfacultyList = new ArrayList<>();
        mInfoList = new ArrayList<>();

        subjectCode = view.findViewById(R.id.subjectCode);
        subjectlogo = view.findViewById(R.id.subjectlogo);
        about = view.findViewById(R.id.about);
        Acadamic = view.findViewById(R.id.Acadamic);
        Samester = view.findViewById(R.id.Samester);
        subjectCredit = view.findViewById(R.id.subjectCredit);
        tittle = view.findViewById(R.id.tittle);
        layoutManager = new LinearLayoutManager(activity);
        mlayoutManager = new LinearLayoutManager(activity);
        TeacherList = view.findViewById(R.id.TeacherList);
        TeacherList.setHasFixedSize(true);
        TeacherList.setLayoutManager(mlayoutManager);

        spinner= view.findViewById(R.id.progressBar);
        resourceList = view.findViewById(R.id.resourceList);
        resourceList.setHasFixedSize(true);
        resourceList.setLayoutManager(layoutManager);



        if (Adminuser.equals("TRUE")) {
            GetAllSubjectDetails();
            GetAllSubjectFaculty();



        } else if (Adminuser.equals("FALSE")) {
            GetAllSubjectDetails();
            GetAllSubjectFaculty();
            resourceList.setVisibility(View.GONE);

        }


        return view;
    }

    private void GetAllSubjectFaculty() {
        facultylistdisplay = new SubjectTeacherDisplay();
        facultylistdisplay.execute();

    }


    private void GetAllSubjectDetails() {
        subjectinfoAuthTask = new ProgressSubjectInfo();
        subjectinfoAuthTask.execute();


    }


    private class SubjectTeacherDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDTEACHERINSUBJECTLIST+"/"+Course_id+"/faculty");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                       // Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            facultylistdisplay = null;
            try {
                if (responce != null) {

                    spinner.setVisibility(View.INVISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectFacultyArray TeacherInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectFacultyArray.class);
                                mfacultyList.add(TeacherInfo);
                                facultyadapter = new StudentFacultyListInSubjectAdapter(activity, mfacultyList,Course_id);
                                TeacherList.setAdapter(facultyadapter);



                            }

                        }else{

                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            builder.setTitle("Info");
                            builder.setMessage(getString(co.savm.R.string.nouserteacher))
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {

                        spinner.setVisibility(View.INVISIBLE);
                       // Utils.showAlertFragmentDialog(activity, "Information", msg);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            facultylistdisplay = null;
            spinner.setVisibility(View.INVISIBLE);

        }
    }


    private class ProgressSubjectInfo extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGESUBJECTINFO+"/"+Course_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                       // Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            subjectinfoAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject data = responce.getJSONObject("data");

                        String Tittle =data.getString("title");
                        String body =data.getString("body");
                        String field_job_title =data.getString("field_job_title");
                        String field_deparment =data.getString("field_deparment");
                        String field_academic_year =data.getString("field_academic_year");
                        String field_semester =data.getString("field_semester");
                        String field_teacher =data.getString("field_teacher");
                        String field_credit =data.getString("field_credit");

                        JSONArray picture = data.getJSONArray("field_logo");

                        if(picture != null && picture.length() > 0 ){
                            String strings[] = new String[picture.length()];
                            try{
                                for(int i=0;i<strings.length;i++) {
                                    strings[i] = picture.getString(i);
                                    Glide.with(activity).load(strings[i])
                                            .placeholder(R.mipmap.clasrrominfo).dontAnimate()
                                            .fitCenter().into(subjectlogo);

                                    Log.d("TAG", "photo: " +  strings[i]);
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            }else {
                            subjectlogo.setImageResource(R.mipmap.clasrrominfo);
                        }

                        JSONArray syllabus = data.getJSONArray("field_sub_syllabus");
                        if (syllabus != null && syllabus.length() > 0) {



                            for (int j = 0; j < syllabus.length(); j++) {
                                String t = syllabus.getJSONObject(j).getString("value");
                                String p = syllabus.getJSONObject(j).getString("title");
                                Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = syllabus.getJSONObject(j).getString("title");
                                LinkInfo.value = syllabus.getJSONObject(j).getString("value");

                                mInfoList.add(LinkInfo);


                            }
                        }

                        JSONArray QuestionPaperArray = data.getJSONArray("field_question_paper");
                        if (QuestionPaperArray != null && QuestionPaperArray.length() > 0) {



                            for (int j = 0; j < QuestionPaperArray.length(); j++) {
                                String t = QuestionPaperArray.getJSONObject(j).getString("value");
                                String p = QuestionPaperArray.getJSONObject(j).getString("title");
                                Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = QuestionPaperArray.getJSONObject(j).getString("title");
                                LinkInfo.value = QuestionPaperArray.getJSONObject(j).getString("value");

                                mInfoList.add(LinkInfo);


                            }
                        }





                        JSONArray AttachmentsArray = data.getJSONArray("field_attachments");
                        if (AttachmentsArray != null && AttachmentsArray.length() > 0) {



                            for (int j = 0; j < AttachmentsArray.length(); j++) {
                                String t = AttachmentsArray.getJSONObject(j).getString("value");
                                String p = AttachmentsArray.getJSONObject(j).getString("title");
                                Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = AttachmentsArray.getJSONObject(j).getString("title");
                                LinkInfo.value = AttachmentsArray.getJSONObject(j).getString("value");

                                mInfoList.add(LinkInfo);


                            }
                        }




                        JSONArray resourceArrays = data.getJSONArray("field_course_resource");
                        if (resourceArrays != null && resourceArrays.length() > 0) {



                            for (int j = 0; j < resourceArrays.length(); j++) {
                                String t = resourceArrays.getJSONObject(j).getString("value");
                                String p = resourceArrays.getJSONObject(j).getString("title");
                                Log.d("TAG", "recource: " + t + p);
                                InfoArray LinkInfo = new InfoArray();
                                LinkInfo.title = resourceArrays.getJSONObject(j).getString("title");
                                LinkInfo.value = resourceArrays.getJSONObject(j).getString("value");

                                mInfoList.add(LinkInfo);


                            }

                        }
                        infoadapter = new StudentSubjectInfoResourceAdapter(activity, mInfoList, Course_idMain);
                        resourceList.setAdapter(infoadapter);


                        subjectCode.setText(field_job_title);
                        Acadamic.setText(field_academic_year);
                        Samester.setText(field_semester);
                        subjectCredit.setText(field_credit);
                        tittle.setText(Tittle);
                        about.setText(body);
                       /* Teacher.setText(field_teacher);
                        Department.setText(field_deparment);
*/

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                       // Utils.showAlertFragmentDialog(activity,"Information", msg);

                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            subjectinfoAuthTask = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }

    private void StartDownlods(final String s) {

        syllabus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DownloadFileFromURL().execute(s);

            }
        });



    }

    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            spinner.setVisibility(View.INVISIBLE);

            // Displaying downloaded image into image view
            // Reading image path from sdcard
           /* String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
            // setting downloaded into image view
            my_image.setImageDrawable(Drawable.createFromPath(imagePath));*/
        }

    }

}






