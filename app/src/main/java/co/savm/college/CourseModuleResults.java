package co.savm.college;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import co.savm.R;
import co.savm.adapters.MyResultAdapter;
import co.savm.models.MyResultsItem;
import co.savm.models.ResultChlidListArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class CourseModuleResults extends Fragment {
    private ExpandableListView mExpandableListView;
    private MyResultAdapter mExpandableListAdapter;
    private List<String> mExpandableListTitle;
    private Map<String, List<String>> mExpandableListData;
    private ArrayList<ResultChlidListArray> parentList;
    private ArrayList<ResultChlidListArray> parentassignmentList;
    static Activity activity;
    private List<MyResultsItem> expandableList;
    private ProgressoftaskList progressoftasklistAuthTask = null;
    String Course_id, Adminuser, Course_idMain;
    private static ProgressBar spinner;
    TextView ErrorText;
    static ImageView ReloadProgress;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_course_module_results, container, false);
        activity = (Activity) view.getContext();
        Course_id = getArguments().getString("COURSE_ID");
        Course_idMain = getArguments().getString("COURSE_ID");
        Adminuser = getArguments().getString("ADMIN");
        spinner= view.findViewById(R.id.progressBar);
        ErrorText =view.findViewById(R.id.ErrorText);
        ReloadProgress =view.findViewById(R.id.ReloadProgress);
        mExpandableListView = view.findViewById(R.id.itemExpandableListView);
        expandableList = new ArrayList<MyResultsItem>();
        ArrayList<ResultChlidListArray> parentList = null;
        ArrayList<ResultChlidListArray> parentassignmentList = null;

        if (Adminuser.equals("TRUE")) {
            progressoftasklistAuthTask = new ProgressoftaskList();
            progressoftasklistAuthTask.execute();



        } else if (Adminuser.equals("FALSE")) {
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");

        }


        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandableList.clear();
                progressoftasklistAuthTask = new ProgressoftaskList();
                progressoftasklistAuthTask.execute();


            }
        });


        return view;
    }


    private class ProgressoftaskList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SUBJECTSRESULT +"/"+SessionManager.getInstance(getActivity()).getUser().userprofile_id+"?sid="+Course_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progressoftasklistAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.GONE);
                    ReloadProgress.setVisibility(View.GONE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if (jsonArrayData != null && jsonArrayData.length() > 0) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                JSONObject perResult = jsonArrayData.getJSONObject(i);

                                JSONArray ClassArray = perResult.getJSONArray("exam");

                                if (ClassArray != null && ClassArray.length() > 0) {
                                    ArrayList<ResultChlidListArray> parentList = new ArrayList<>();
                                    for (int j = 0; j < ClassArray.length(); j++) {

                                        ResultChlidListArray task = new Gson().fromJson(ClassArray.getJSONObject(j).toString(), ResultChlidListArray.class);
                                        parentList.add(task);

                                    }
                                    MyResultsItem ParentBean = new MyResultsItem();
                                    ParentBean.className = perResult.getString("subject");
                                    ParentBean.classType ="Exams";
                                    ParentBean.childsTaskList.addAll(parentList);
                                    expandableList.add(ParentBean);

                                }

                                JSONArray AssignmentArray = perResult.getJSONArray("assignment");

                                if (AssignmentArray != null && AssignmentArray.length() > 0) {

                                    ArrayList<ResultChlidListArray> parentassignmentList = new ArrayList<>();
                                    for (int j = 0; j < AssignmentArray.length(); j++) {

                                        ResultChlidListArray task = new Gson().fromJson(AssignmentArray.getJSONObject(j).toString(), ResultChlidListArray.class);
                                        parentassignmentList.add(task);


                                    }
                                    MyResultsItem ParentBean = new MyResultsItem();
                                    ParentBean.className = perResult.getString("subject");
                                    ParentBean.classType ="Assignments";
                                    ParentBean.childsTaskList.addAll(parentassignmentList);
                                    expandableList.add(ParentBean);

                                }


                                mExpandableListAdapter = new MyResultAdapter(getActivity(), expandableList);
                                mExpandableListView.setIndicatorBounds(0, 20);
                                mExpandableListView.setAdapter(mExpandableListAdapter);
                                mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
                                    @Override
                                    public void onGroupExpand(int groupPosition) {
                                        int len = mExpandableListAdapter.getGroupCount();
                                        for (int i = 0; i < len; i++) {
                                            if (i != groupPosition) {
                                                mExpandableListView.collapseGroup(i);
                                            }
                                        }
                                    }
                                });
                            }

                        } else {
                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText(R.string.MyResult);


                        }
                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.GONE);
                        ReloadProgress.setVisibility(View.VISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.GONE);
                ReloadProgress.setVisibility(View.VISIBLE);


            }
        }

        @Override
        protected void onCancelled() {
            progressoftasklistAuthTask = null;
            spinner.setVisibility(View.GONE);



        }
    }
}