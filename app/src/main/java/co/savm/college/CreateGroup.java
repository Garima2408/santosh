package co.savm.college;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.savm.R;

public class CreateGroup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
    }
    @Override
    public void onBackPressed() {
        finish();
    }
}
