package co.savm.college;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.youtube.player.YouTubeThumbnailView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import co.savm.R;
import co.savm.adapters.CollagemediaGrirdAdapter;
import co.savm.adapters.MediaDetailsAdapter;
import co.savm.models.MediaArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import in.srain.cube.views.GridViewWithHeaderAndFooter;
import okhttp3.OkHttpClient;

public class CollageMediaDetails extends BaseAppCompactActivity {
    String Media_id,MediaTittle,mediaDetails;
    private CollageMediaDetailsShow collagemediashow = null;
    TextView text_title,textDetails ;
    ImageView iv_vedio_icon;
    LinearLayout barlayout;
    YouTubeThumbnailView youTubeThumbnailView;
    Dialog imageDialog,vedioDialog;
    String img_url;
    int logos[];
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    ViewPager pagerImage;
    PagerAdapter Imageadapter;
    private GridViewWithHeaderAndFooter rv_mediagrid;
    //  GridView rv_mediagrid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collage_media_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        Bundle b = getIntent().getExtras();
        Media_id = b.getString("MEDIA_ID");
        MediaTittle = b.getString("MEDIA_TITLE");


        textDetails = findViewById(R.id.textDetails);
        text_title = findViewById(R.id.text_title);
        iv_vedio_icon = findViewById(R.id.iv_vedio_icon);
        youTubeThumbnailView = findViewById(R.id.thumbnail);
        rv_mediagrid = findViewById(R.id.rv_mediagrid);

       /* gridView = (GridViewWithHeaderAndFooter) findViewById(R.id.grid_view);*/




        collagemediashow = new CollageMediaDetailsShow();
        collagemediashow.execute();


    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    private void setGridViewHeaderAndFooter(String mediaTittle, String mediaDetails) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);

        View headerView = layoutInflater.inflate(R.layout.layout_header, null, false);

        //locate views
        TextView headerText1 = headerView.findViewById(R.id.text_title);
        TextView headerText2 = headerView.findViewById(R.id.textDetails);

        headerText1.setText(mediaTittle);
        headerText2.setText(Html.fromHtml(mediaDetails));
        rv_mediagrid.addHeaderView(headerView);
    }

    private class CollageMediaDetailsShow extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {


                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGMEDIADETAILS+"/"+Media_id);
                jsonObject = new JSONObject(responseData);

                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                CollageMediaDetails.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            collagemediashow = null;
            try {
                if (responce != null) {
                    hideLoading();

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONObject data = responce.getJSONObject("data");

                        String tnd = data.getString("nid");
                        MediaTittle = data.getString("title");
                        mediaDetails= data.getString("body");
                        String mediaType = data.getString("field_media_type");

                        text_title.setText(MediaTittle);
                        textDetails.setText(Html.fromHtml(mediaDetails));
                        setGridViewHeaderAndFooter(MediaTittle,mediaDetails);

                        final MediaArray medialists = new MediaArray(0);

                        if (mediaType.equals("image")){

                            final JSONArray picture = data.getJSONArray("field_forum_images");

                            for(int j=0; j<picture.length(); j++){
                                Log.e("Media pictire", picture.getString(j));

                                medialists.setField_forum_images(picture.getString(j));

                                SetValueTogird(picture);



                            }



                        }else if (mediaType.equals("video"))
                        {
                            rv_mediagrid.setVisibility(View.GONE);
                            JSONArray vedio = data.getJSONArray("field_ffile");
                            for(int j=0; j<vedio.length(); j++){

                                Log.e("Media vedio", vedio.getString(0));

                                try {
                                    final String videoId=extractYoutubeId(vedio.getString(0));
                                    Log.e("VideoId is->","" + videoId);
                                    img_url="http://img.youtube.com/vi/"+videoId+"/0.jpg"; // this is link which will give u thumnail image of that video


                                    Glide.with(CollageMediaDetails.this).load(img_url)
                                            .placeholder(R.color.black).dontAnimate()
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(youTubeThumbnailView);


                                    youTubeThumbnailView.setVisibility(View.VISIBLE);
                                    text_title.setVisibility(View.VISIBLE);
                                    textDetails.setVisibility(View.VISIBLE);
                                    iv_vedio_icon.setVisibility(View.VISIBLE);
                                    iv_vedio_icon.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            Intent intent = new Intent(CollageMediaDetails.this, CollageMediaDisplay.class);
                                            intent.putExtra("url",videoId);
                                            startActivity(intent);


                                        }
                                    });




                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }


                            }
                        }
                        else{
                            Toast.makeText(getApplicationContext(),"No Media is Availabe here",Toast.LENGTH_LONG).show();
                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            collagemediashow = null;
            hideLoading();


        }
    }






    private void SetValueTogird(final JSONArray picture) {


        List<String> list = new ArrayList<>();
        for(int i=0; i<picture.length(); i++){
            try {
                list.add(picture.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        rv_mediagrid.setAdapter(new CollagemediaGrirdAdapter(getActivity(), list));






    }

    public String extractYoutubeId(String url) throws MalformedURLException {
        String query = new URL(url).getQuery();
        String[] param = query.split("&");
        String id = null;
        for (String row : param) {
            String[] param1 = row.split("=");
            if (param1[0].equals("v")) {
                id = param1[1];
            }
        }
        return id;
    }








    private void imageDialogmultiple(JSONArray data){


        imageDialog = new Dialog(CollageMediaDetails.this);
        imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        imageDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        imageDialog.setContentView(R.layout.multiple_images_display);
        // dialogLogin.setCancelable(false);
        imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        imageDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        imageDialog.show();
        pagerImage = imageDialog. findViewById(R.id.pager);
        List<String> list = new ArrayList<>();
        for(int i=0; i<data.length(); i++){
            try {
                list.add(data.getString(i));
                pagerImage.setOnClickListener(onChagePageClickListener(i));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Imageadapter = new MediaDetailsAdapter(CollageMediaDetails.this, list);
        pagerImage.setAdapter(Imageadapter);

    }

    private View.OnClickListener onChagePageClickListener(final int i) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pagerImage.setCurrentItem(i);
            }
        };
    }



    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

               // PAGE_SIZE=0;
                // isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}








