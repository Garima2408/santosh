package co.savm.college;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.GroupNSocityAdapter;
import co.savm.models.ClubsNSocityArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.settings.FAQ;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class GroupNSocieties extends BaseAppCompactActivity {
    RecyclerView rv_ClubsNsocity;
    String Offsetvalue;
    public ArrayList<ClubsNSocityArray> mclubnsocityList;
    private RecyclerView.LayoutManager layoutManager;
    private GroupNSocityAdapter clunnsocityadapter;
    FloatingActionButton fab;
    SwipeRefreshLayout swipeContainer;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    private static ProgressBar progressBar2;
    private int currentPage = 1;
    static TextView ErrorText;
    private CollageClubsNSocityDisplay clubandsocitylist = null;
    private static ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_nsocieties);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        mclubnsocityList=new ArrayList<>();

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        layoutManager = new LinearLayoutManager(this);
        rv_ClubsNsocity = findViewById(R.id.rv_ClubsNsocity);
        rv_ClubsNsocity.setHasFixedSize(true);
        rv_ClubsNsocity.setLayoutManager(layoutManager);
        swipeContainer =findViewById(R.id.swipeContainer);
        ErrorText =findViewById(R.id.ErrorText);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));


        clunnsocityadapter = new GroupNSocityAdapter(rv_ClubsNsocity,mclubnsocityList,GroupNSocieties.this);
        rv_ClubsNsocity.setAdapter(clunnsocityadapter);

        fab = findViewById(R.id.fab);


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity (new Intent (GroupNSocieties.this, FAQ.class));

            }
        });



        inItView();


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {


                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        clubandsocitylist = new CollageClubsNSocityDisplay();
                        clubandsocitylist.execute();

                    }
                }, 3000);
            }
        });
        clubandsocitylist = new CollageClubsNSocityDisplay();
        clubandsocitylist.execute();


    }





    private void inItView(){

        swipeContainer.setRefreshing(false);

        rv_ClubsNsocity.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    clunnsocityadapter.addProgress();

                    clubandsocitylist = new CollageClubsNSocityDisplay();
                    clubandsocitylist.execute();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        mShimmerViewContainer.startShimmerAnimation();
    }


    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        Log.e("onPause","CampusNewFeeds");

        super.onPause();
    }





    private class CollageClubsNSocityDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECLUBS +"?cid=250905"+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);


            } catch (JSONException | IOException e) {

                e.printStackTrace();
                GroupNSocieties.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);

                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

                clunnsocityadapter.removeProgress();
                isLoading = false;


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {



            try {
                if (responce != null) {
                    rv_ClubsNsocity.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        // view.setVisibility(View.GONE);

                        ArrayList<ClubsNSocityArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            ClubsNSocityArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ClubsNSocityArray.class);
                            arrayList.add(CourseInfo);

                        }
                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_ClubsNsocity.setVisibility(View.VISIBLE);
                                clunnsocityadapter.setInitialData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            } else {
                                isLoading = false;
                                clunnsocityadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                clunnsocityadapter.addData(arrayList);


                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);




                            }
                        }
                                           else{
                            if (PAGE_SIZE==0){
                                rv_ClubsNsocity.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Societies");
                                swipeContainer.setVisibility(View.GONE);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            }else
                                clunnsocityadapter.removeProgress();

                        }
                        // view.setVisibility(View.GONE);



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        // spinner.setVisibility(View.INVISIBLE);
                        showAlertDialog(msg);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);


                        swipeContainer.setRefreshing(false);


                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);

                // spinner.setVisibility(View.INVISIBLE);
            }
        }
        @Override
        protected void onCancelled() {
            clubandsocitylist = null;
            //3   hideLoading();
            swipeContainer.setRefreshing(false);


        }

    }
    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                PAGE_SIZE=0;
                isLoading=false;
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
