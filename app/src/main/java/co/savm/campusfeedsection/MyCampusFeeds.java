package co.savm.campusfeedsection;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.MyCampusFeedsAdapters;
import co.savm.models.CampusFeedArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class MyCampusFeeds extends BaseAppCompactActivity {
    static RecyclerView rv_mycampus_news;
    public static ArrayList<CampusFeedArray> mcampusfeedList;
    private static MyCampusFeedsAdapters campusfeedadapter;
    private static MyCollageNewFeedsDisplay mycollagenewfeedslist = null;
    //  private static MyCollageNewFeedsDisplayRefresh mycollagenewfeedslistrefresh = null;
    private RecyclerView.LayoutManager layoutManager;
    static Activity activity;
    FloatingActionButton fab;
    static String tittle,URLDoc;
    private static boolean isLoading= false;
    private static int PAGE_SIZE = 0;
    static SwipeRefreshLayout swipeContainer;
    static TextView ErrorText;
    static ImageView ReloadProgress;
    private static ShimmerFrameLayout mShimmerViewContainer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_campus_feeds);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        mcampusfeedList = new ArrayList<>();
        activity =getActivity();
        swipeContainer = findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        ErrorText =findViewById(R.id.ErrorText);
        ReloadProgress =findViewById(R.id.ReloadProgress);
        layoutManager = new LinearLayoutManager(this);
        rv_mycampus_news = findViewById(R.id.rv_mycampus_news);
        rv_mycampus_news.setHasFixedSize(true);
        campusfeedadapter = new MyCampusFeedsAdapters(rv_mycampus_news, mcampusfeedList ,activity);
        rv_mycampus_news.setAdapter(campusfeedadapter);


        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        rv_mycampus_news.setLayoutManager(layoutManager);

        fab = findViewById(R.id.fab);

        inItView();

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RefreshWorkedFromMycampusAdapter();
            }
        });
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh


                        if (!isLoading){
                            RefreshWorkedFromMycampusAdapter();
                        }else{
                             isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 1000);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent backIntent = new Intent(activity, AddCampusFeeds.class)
                        .putExtra("open", "CameFromMyFeed");
                activity.startActivity(backIntent);


/*
                startActivity (new Intent(getActivity(), AddCampusFeeds.class));
*/

            }
        });


        rv_mycampus_news.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0) {
                    fab.hide();
                } else if (dy < 0) {
                    fab.show();
                }
            }
        });

    }


    @Override
    protected void onPause() {
        super.onPause();
        PAGE_SIZE=0;
        isLoading=false;
        mShimmerViewContainer.stopShimmerAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        swipeContainer.setRefreshing(false);

        mycollagenewfeedslist = new MyCollageNewFeedsDisplay();
        mycollagenewfeedslist.execute();

    }


    public static void RefreshWorkedFromMycampusAdapter() {
        Log.e("myfeed","refresh");
        PAGE_SIZE=0;
        isLoading=true;
        swipeContainer.setRefreshing(true);
        mycollagenewfeedslist = new MyCollageNewFeedsDisplay();
        mycollagenewfeedslist.execute();



    }


    private void inItView(){


        swipeContainer.setRefreshing(false);


        rv_mycampus_news.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager)layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    campusfeedadapter.addProgress();

                    mycollagenewfeedslist = new MyCollageNewFeedsDisplay();
                    mycollagenewfeedslist.execute();

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }




    private static class MyCollageNewFeedsDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;

                OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


                try {
                    String responseData = ApiCall.GETHEADER(client, URLS.URL_MYCAMPUSFEED+"?"+"cid=263214"+"&uid="+SessionManager.getInstance(activity).getUser().userprofile_id +"&offset="+PAGE_SIZE+"&limit=20");
                    jsonObject = new JSONObject(responseData);
                    Log.d("TAG", "responseData: " + responseData);

                } catch (JSONException | IOException e) {

                    e.printStackTrace();
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            swipeContainer.setRefreshing(false);
                            ReloadProgress.setVisibility(View.VISIBLE);

                        }
                    });

                    campusfeedadapter.removeProgress();
                    isLoading=false;
                }
                return jsonObject;

            }

        @Override
        protected void onPostExecute(JSONObject responce) {
             mycollagenewfeedslist = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    rv_mycampus_news.setVisibility(View.VISIBLE);
                    ErrorText.setVisibility(View.GONE);
                    ReloadProgress.setVisibility(View.GONE);



                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        ArrayList<CampusFeedArray> arrayList = new ArrayList<>();

                        for (int i = 0; i < jsonArrayData.length(); i++) {


                            CampusFeedArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CampusFeedArray.class);
                            arrayList.add(CourseInfo);

                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                            if (jsonObject.has("field_comment_attachments")){
                                JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                    URLDoc = AttacmentsFeilds.getString("value");
                                    tittle = AttacmentsFeilds.getString("title");
                                    Log.d("TAG", "titleurl: " + tittle +URLDoc);
                                    CourseInfo.title =tittle;
                                    CourseInfo.url =URLDoc;


                                }else {

                                }
                            }else {

                            }



                          /*  campusfeedadapter.notifyDataSetChanged();
                            rv_mycampus_news.setVisibility(View.VISIBLE);
                            campusfeedadapter.setInitialData(mcampusfeedList);*/


                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_mycampus_news.setVisibility(View.VISIBLE);
                                campusfeedadapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            } else {
                                campusfeedadapter.removeProgress();
                                swipeContainer.setRefreshing(false);

                                campusfeedadapter.addData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);



                            }

                            isLoading = false;

                        }else{
                            if (PAGE_SIZE==0){
                                rv_mycampus_news.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Post");
                                swipeContainer.setRefreshing(false);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }else
                                campusfeedadapter.removeProgress();

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);



                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                ReloadProgress.setVisibility(View.VISIBLE);


            }
        }

        @Override
        protected void onCancelled() {
            mycollagenewfeedslist = null;
            swipeContainer.setRefreshing(false);
            Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }
}
