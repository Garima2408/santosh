package co.savm.campusfeedsection;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import co.savm.R;
import co.savm.models.CampusNotificationArray;
import co.savm.utils.SessionManager;

public class CampusFeedNotifications extends AppCompatActivity {
    SessionManager sharedPreference;
    ListView listofnotifications;
    private ArrayList<CampusNotificationArray> notificationlist;
    private ArrayList<String> Displaylist;
    TextView ErrorText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_campus_feed_notifications);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        sharedPreference = new SessionManager(this);
        notificationlist = new ArrayList<>();
        Displaylist =new ArrayList<>();
        listofnotifications = findViewById(R.id.listofnotifications);
        ErrorText =findViewById(R.id.ErrorText);
        notificationlist = sharedPreference.getCampusNotification(this);
        /*Log.d("TAG", "onCreate: "+sharedPreference.getCampusNotification(this).size()+"  "
        +sharedPreference.getCampusNotification(this));
*/



        getNotifications();



    }

    private void getNotifications() {


        if ((notificationlist !=null)){

            Collections.reverse(notificationlist);


            for (int i=0; i<notificationlist.size();i++) {

                Displaylist.add(notificationlist.get(i).getMessage());
                Log.d("TAG", "Displaylist: "+i +"="+Displaylist.get(i));


            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    R.layout.list_of_notification,R.id.tv_title,
                    Displaylist);
            listofnotifications.setAdapter(adapter);


        }else {
            ErrorText.setVisibility(View.VISIBLE);
            listofnotifications.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {

        finish();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            case R.id.Clear:

                if(notificationlist != null){
                    sharedPreference.removeCampusnotification(this);
                    notificationlist.clear();
                    Displaylist.clear();


                }else {


                }
                getNotifications();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }



}
