package co.savm.campusfeedsection;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.UserDisplayProfile;
import co.savm.adapters.ReplyMyFeedAdapter;
import co.savm.buttonanimation.LikeButton;
import co.savm.buttonanimation.OnAnimationEndListener;
import co.savm.buttonanimation.OnLikeListener;
import co.savm.models.ReplayOnPostArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.studentprofile.AnotherUserDisplay;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ReplyOnMyFeeds extends BaseAppCompactActivity {
    String post_id,parentname,Pimage,Pdate,parentcomment,comments,TransferId,noOfComments,noOfLikes,IsLiked,Content_url;
    static String parent_id;
    ImageView circleView,comment,options,inaprooptions,imagePost,imagepreview,share;
    private TextView blog_content,publisher_name,dateTime,commentNo,likeNo,tv_comment;
    static RecyclerView rv_reply_comment;
    public static ArrayList<ReplayOnPostArray> commentsList;
    static ReplyMyFeedAdapter mcommentAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private static CollageCommentsList collagecommentsLis = null;
    private DeleteFeedComments deletefeedcomments = null;
    private LikeFeedComments likefeedcomments = null;
    private InappropriateFeedComments inappropriatefeedcomments = null;
    static Activity activity;
    static String tittle,URLDoc;
    RelativeLayout previewlayout;
    LinearLayout box1,box3;
    private static boolean isLoading= true;
    private static int PAGE_SIZE = 0;
    Bundle extras;
    String Extension;
    static ImageView ReloadProgress;
    LikeButton like;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply_on_campus_feeds);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.cross);
        actionBar.setDisplayShowHomeEnabled(true);
        activity =getActivity();
        Intent intent = getIntent();
        extras = intent.getExtras();
        post_id =intent.getStringExtra("POST_ID");
        parent_id= intent.getStringExtra("POST_ID");
        TransferId = intent.getStringExtra("USERSELECTED_ID");
        parentname = intent.getStringExtra("NAME");
        Pimage = intent.getStringExtra("IMAGE_URL");
        Pdate = intent.getStringExtra("DATE");
        parentcomment = intent.getStringExtra("BLOG_CONTENT");
        noOfComments = intent.getStringExtra("NOCOMMENT");
        noOfLikes = intent.getStringExtra("NOLIKED");
        IsLiked = intent.getStringExtra("IS_LIKED");
        Content_url = intent.getStringExtra("POSTCONENT");



        commentsList = new ArrayList<ReplayOnPostArray>();
        blog_content = findViewById(R.id.blog_content);
        tv_comment = findViewById(R.id.tv_comment);
        dateTime = findViewById(R.id.dateTime);
        publisher_name = findViewById(R.id.publisher_name);
        circleView = findViewById(R.id.circleView);
        rv_reply_comment =findViewById(R.id.rv_reply_comment);
        comment=findViewById(R.id.comment);
        like =findViewById(R.id.like);
        share  =findViewById(R.id.share);
        commentNo =findViewById(R.id.commentNo);
        likeNo =findViewById(R.id.likeNo);
        options =findViewById(R.id.options);
        inaprooptions =findViewById(R.id.inaprooptions);
        imagePost =findViewById(R.id.imagePost);
        imagepreview =findViewById(R.id.imagepreview);
        previewlayout =findViewById(R.id.previewlayout);
        box1=findViewById(R.id.box1);
        box3=findViewById(R.id.box3);
        layoutManager = new LinearLayoutManager(activity);
        rv_reply_comment.setHasFixedSize(true);
        rv_reply_comment.setLayoutManager(layoutManager);
        ReloadProgress = findViewById(R.id.ReloadProgress);
        mcommentAdapter = new ReplyMyFeedAdapter(rv_reply_comment, commentsList ,activity);
        rv_reply_comment.setAdapter(mcommentAdapter);
        rv_reply_comment.setNestedScrollingEnabled(false);



        blog_content. setText(Html.fromHtml(parentcomment));
        publisher_name.setText(parentname);
        dateTime.setText(Pdate);
        commentNo.setText(noOfComments);
        likeNo.setText(noOfLikes);


        if(Pimage.isEmpty()){
            circleView.setImageResource(R.mipmap.place_holder);

        }else {
            Glide.with(this).load(Pimage)
                    .placeholder(R.mipmap.place_holder).dontAnimate()
                    .fitCenter().into(circleView);
        }
        Log.d("TAG", "print: " + TransferId+","+SessionManager.getInstance(getActivity()).getUser().getParentUid());



        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
            if(TransferId.equals(SessionManager.getInstance(getActivity()).getUser().getParentUid())){
                options.setVisibility(View.VISIBLE);
                inaprooptions.setVisibility(View.INVISIBLE);
            }else {
                options.setVisibility(View.INVISIBLE);
                inaprooptions.setVisibility(View.VISIBLE);
            }


        }else {

            if(TransferId.equals(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())){
                options.setVisibility(View.VISIBLE);
                inaprooptions.setVisibility(View.INVISIBLE);
            }else {
                options.setVisibility(View.INVISIBLE);
                inaprooptions.setVisibility(View.VISIBLE);
            }
        }




        if(IsLiked.contains("False")){
            like.setLiked(false);
        }else if(IsLiked.contains("True"))  {
            like.setLiked(true);
        }




        if(Content_url != null  ) {

            Extension = Content_url.substring(Content_url.lastIndexOf("."));

            if (Extension.matches(".jpeg")) {

                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);
                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }else if (Extension.matches(".png"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);
                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }
            else if (Extension.matches(".jpg"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagePost.setVisibility(View.VISIBLE);

                Glide.clear(imagePost);
                Glide.with(this).load(Content_url)
                        .placeholder(R.mipmap.header_image).dontAnimate()
                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .fitCenter().into(imagePost);
            }else if (Extension.matches(".doc"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_doc_download);
            }
            else if (Extension.matches(".docx"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_doc_download);
            }
            else if (Extension.matches(".pdf"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_pdf_download);
            }
            else if (Extension.matches(".txt"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_text_download);
            }
            else if (Extension.matches(".xls"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_xls_download);
            }
            else if (Extension.matches(".xlsx"))
            {
                previewlayout.setVisibility(View.VISIBLE);
                imagepreview.setVisibility(View.VISIBLE);
                imagepreview.setImageResource(R.mipmap.ic_xls_download);
            }


        }else {
            previewlayout.setVisibility(View.GONE);



        }


        /*CALLING LISTS OF REPLY COMMENTS*/

        collagecommentsLis = new CollageCommentsList();
        collagecommentsLis.execute();
        inItView();

        tv_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ReplyOnMyFeeds.this, CommentScreen.class)
                        .putExtra("POST_ID", post_id)
                        .putExtra("open","CommentFromMyPostReply");
                startActivity(intent);
            }
        });



        like.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                if (!IsLiked.isEmpty()) {
                    if (IsLiked.contains("False")) {
                        like.setLiked(true);

                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) + 1));
                        String NewLikeNo = likeNo.getText().toString();

                        likefeedcomments = new LikeFeedComments();
                        likefeedcomments.execute(post_id,"flag");
                        IsLiked ="True";
                        noOfLikes =NewLikeNo;
                        //lists.get(position).setIs_liked("True");

                        /* lists.get(position).setLikes(NewLikeNo);
                         */
                    } else if (IsLiked.contains("True")) {
                        like.setLiked(false);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) - 1));
                        String NewLikeNo = likeNo.getText().toString();
                        likefeedcomments = new LikeFeedComments();
                        likefeedcomments.execute(post_id,"unflag");
                        IsLiked ="False";
                        noOfLikes =NewLikeNo;
                       /* lists.get(position).setIs_liked("False");
                        lists.get(position).setLikes(NewLikeNo);*/
                    }
                }
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                if (!IsLiked.isEmpty()) {
                    if (IsLiked.contains("False")) {
                        like.setLiked(true);

                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) + 1));
                        String NewLikeNo = likeNo.getText().toString();

                        likefeedcomments = new LikeFeedComments();
                        likefeedcomments.execute(post_id,"flag");
                        IsLiked ="True";
                        noOfLikes =NewLikeNo;
                        //lists.get(position).setIs_liked("True");

                        /* lists.get(position).setLikes(NewLikeNo);
                         */
                    } else if (IsLiked.contains("True")) {
                        like.setLiked(false);
                        likeNo.setText(Integer.toString(Integer.parseInt(noOfLikes) - 1));
                        String NewLikeNo = likeNo.getText().toString();
                        likefeedcomments = new LikeFeedComments();
                        likefeedcomments.execute(post_id,"unflag");
                        IsLiked ="False";
                        noOfLikes =NewLikeNo;
                       /* lists.get(position).setIs_liked("False");
                        lists.get(position).setLikes(NewLikeNo);*/
                    }
                }
            }
        });


        like.setOnAnimationEndListener(new OnAnimationEndListener() {
            @Override
            public void onAnimationEnd(LikeButton likeButton) {

            }
        });




        publisher_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(ReplyOnMyFeeds.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID",TransferId);
                startActivity(backIntent);


            }
        });
        box3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareBody = "https://play.google.com/store/apps/details?id=co.questin&ah=yzdEkzLESMuCtdkGNCmhSwTZazY";

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "APP NAME (Open it in Google Play Store to Download the Application)");

                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody+parentcomment);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));




            }
        });


        circleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent backIntent = new Intent(ReplyOnMyFeeds.this, UserDisplayProfile.class)
                        .putExtra("USERPROFILE_ID",TransferId);
                startActivity(backIntent);


            }
        });
        inaprooptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), android.app.AlertDialog.THEME_HOLO_DARK)
                        .setTitle("Inappropriate")
                        .setMessage(R.string.Inappropriate)
                        .setCancelable(false)
                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                                             /*  MARKED IT INAPPROPRIATE ON POST*/
                                inappropriatefeedcomments = new InappropriateFeedComments();
                                inappropriatefeedcomments.execute(post_id);



                            }
                        })
                        .setNegativeButton("No", new Dialog.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();



            }
        });

        options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopupMenu();


            }

            private void showPopupMenu() {


                PopupMenu popup = new PopupMenu(getActivity(), options);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.optionpopupmenu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.Editoption:


                                Intent backIntent = new Intent(ReplyOnMyFeeds.this, UpdateMyFeeds.class)
                                        .putExtra("POST_ID", post_id)
                                        .putExtra("NAME",parentname)
                                        .putExtra("BLOG_CONTENT", parentcomment)
                                        .putExtra("DATE", Pdate)
                                        .putExtra("IMAGE_URL",  Pimage)
                                        .putExtra("POSTCONENT", Content_url)
                                        .putExtra("open", "CameFromMyCampus");
                                startActivity(backIntent);
                                finish();
                                return true;
                            case R.id.deleteoption:

                                AlertDialog.Builder builder = new AlertDialog.Builder(ReplyOnMyFeeds.this, android.app.AlertDialog.THEME_HOLO_DARK)
                                        .setTitle("Delete My Feed")
                                        .setMessage(R.string.delete)
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                  /*  DELETE COMMENTS ON POST*/
                                                deletefeedcomments = new DeleteFeedComments();
                                                deletefeedcomments.execute(post_id);

                                            }
                                        })
                                        .setNegativeButton("No", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                builder.create().show();



                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popup.show();

            }
        });

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallAgainList();
            }
        });
    }


    private void inItView(){



        rv_reply_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();


                    collagecommentsLis = new CollageCommentsList();
                    collagecommentsLis.execute();
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }










    /*LISTS OF REPLY COMMENTS ON SUBJECTS*/


    private static class CollageCommentsList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_REPLYCOMMENTS+"?"+"&"+"cid"+"=263214"+"&"+"pid"+"="+parent_id+"&offset="+PAGE_SIZE+"limit=20");

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

                mcommentAdapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            collagecommentsLis = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        ArrayList<ReplayOnPostArray> list = new ArrayList<>();

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {



                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                ReplayOnPostArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ReplayOnPostArray.class);
                                list.add(subjects);
                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                if (jsonObject.has("field_comment_attachments")) {
                                    JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                    if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0) {
                                        URLDoc = AttacmentsFeilds.getString("value");
                                        tittle = AttacmentsFeilds.getString("title");
                                        subjects.title = tittle;
                                        subjects.url = URLDoc;

                                    } else {

                                    }
                                } else {

                                }

                            }

                        }
                        if (list!=null &&  list.size()>0){
                            if (PAGE_SIZE == 0) {


                                rv_reply_comment.setVisibility(View.VISIBLE);
                                mcommentAdapter.setInitialData(list);

                            } else {
                                isLoading = false;
                                mcommentAdapter.removeProgress();

                                mcommentAdapter.addData(list);


                            }
                        }else{
                            mcommentAdapter.removeProgress();
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertDialog(activity,"Error",msg );



                    }
                }else {
                    Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            collagecommentsLis = null;


        }
    }










    public static void CallAgainList() {

        commentsList.clear();
        collagecommentsLis = new CollageCommentsList();
        collagecommentsLis.execute();



    }

/*LISTS OF REPLY COMMENTS ON SUBJECTS*/


    private class DeleteFeedComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.DELETE_HEADER(client, URLS.URL_DELETECOMMENTS+"/"+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deletefeedcomments = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // showAlertDialog(msg);
                        Check();
                        finish();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);



                    }
                }else {
                    hideLoading();
                    showAlertDialog(getString(R.string.error_responce));


                }
            } catch (JSONException e) {
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
        }

        @Override
        protected void onCancelled() {
            deletefeedcomments = null;
            hideLoading();
            showAlertDialog(getString(R.string.error_responce));



        }
    }




      /*MARKED IN APPROPRIATE ON COMMENTS */


    private class InappropriateFeedComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name","comments_inappropriate")
                    .add("entity_id",args[0])
                    .add("action","flag")
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_MARKEDINAPPROPRIATECOMMENT,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            }catch (JSONException | IOException e) {

                e.printStackTrace();
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            inappropriatefeedcomments = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                       // showAlertDialog(msg);
                        Check();
                        finish();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);



                    }
                }else {
                    hideLoading();
                    showAlertDialog(getString(R.string.error_responce));


                }
            } catch (JSONException e) {
                hideLoading();
                showAlertDialog(getString(R.string.error_responce));


            }
        }

        @Override
        protected void onCancelled() {
            inappropriatefeedcomments = null;
            hideLoading();
            showAlertDialog(getString(R.string.error_responce));



        }
    }

    private void Check() {

        if (extras.containsKey("open")) {
            if (extras.getString("open").equals("CameFromMyCampusFeedAdapter")) {

                MyCampusFeeds.RefreshWorkedFromMycampusAdapter();



            }  else if (extras.getString("open").equals("CameFromUserprofileCampusFeed")) {

                UserDisplayProfile.RefreshWorkedFromMycampusAdapter();



            }
            else if(extras.getString("open").equals("CameFromanotherUserprofileCampusFeed")){

                AnotherUserDisplay.RefreshWorkedFromMycampusAdapter();




            }
            else if (extras.getString("open").equals("CameFromMyCampusFeedparentAdapter")) {

                ParentMyFeeds.RefreshWorkedFromMycampusAdapter();



            }

        }
    }

    private class LikeFeedComments extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("flag_name","comments_like")
                    .add("entity_id",args[0])
                    .add("action",args[1])
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_LIKEDCOMMENTS,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            }catch (JSONException | IOException e) {

                e.printStackTrace();
                showAlertDialog(getString(R.string.error_responce));


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            likefeedcomments = null;
            try {
                if (responce != null) {
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);



                    }
                }else {
                    showAlertDialog(getString(R.string.error_responce));


                }
            } catch (JSONException e) {
                showAlertDialog(getString(R.string.error_responce));


            }
        }

        @Override
        protected void onCancelled() {
            likefeedcomments = null;
            showAlertDialog(getString(R.string.error_responce));



        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


}
