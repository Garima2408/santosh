package co.savm.chat;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.savm.R;
import co.savm.adapters.MyServerChatListAdapter;
import co.savm.models.chat.SendServerMessage;
import co.savm.models.chat.ServerChatListArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class MyServerChatActivity extends BaseAppCompactActivity {
    private SendServerMessage sendMessageModel;
    String name,image,userid;
    ImageView attechments;
    private Gson gson;
    ImageView backone;
    private Context mContext;
    ImageView imageDp;
    TextView textName;
    ListView list_msg;
    ImageButton buttonSend;
    EditText editTextEnterMsg;
    private List<ServerChatListArray> ChatBubbles;
    private ArrayAdapter<ServerChatListArray> adapter;
    Handler delayhandler = new Handler();
    private static GetTheChatDataDisplay chatdisplay = null;
    boolean myMessage = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_server_chat);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        attechments =findViewById(R.id.attechments);
        backone =findViewById(R.id.backone);
        imageDp =findViewById(R.id.image_dp);
        textName =findViewById(R.id.text_name);
        list_msg =findViewById(R.id.list_msg);
        buttonSend =findViewById(R.id.button_send);
        editTextEnterMsg =findViewById(R.id.edit_text_enter_message);

        gson = new Gson();
        mContext = this;
        Intent intent = getIntent();
        ChatBubbles = new ArrayList<>();
        sendMessageModel = new SendServerMessage();
        name = intent.getStringExtra("USER_NAME");
        image = intent.getStringExtra("PICTURE");
        userid= intent.getStringExtra("USER_ID");


        if(image != null && image.length() > 0 ) {

            Glide.with(this).load(image)
                    .placeholder(R.mipmap.single).dontAnimate()
                    .fitCenter().into(imageDp);

        }else {
            imageDp.setImageResource(R.mipmap.single);

        }

        textName.setText(name);

        getTheperviousDataChat();
      //  mUpdateTimeTask.run();

        backone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delayhandler.removeCallbacks(mUpdateTimeTask);
                finish();
            }
        });
        editTextEnterMsg.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    Log.e("TAG", "onFocusChange: focused");
                } else {
                    Log.e("TAG", "onFocusChange: not focused");
                }
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = editTextEnterMsg.getText().toString();
                if (!text.isEmpty()) {
                    sendMessage(sendMessageModel
                            .setRecipients(userid)
                            .setSubject(text)
                            .setBody(text));
                    editTextEnterMsg.setText("");


                }
            }
        });


    }





    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {   // Todo
            long NOTIFY_INTERVAL = 29;
            long   millis =System.currentTimeMillis();
            int seconds = (int) (millis / 1000);
            seconds = seconds % 30;
            Log.e("Msggg", "Time:"+seconds);

            // This line is necessary for the next call
            delayhandler.postDelayed(this, NOTIFY_INTERVAL);

            if (seconds==NOTIFY_INTERVAL) {
                getTheperviousDataChat();
            }

        }

    };



    private void getTheperviousDataChat(){
        ChatBubbles.clear();
        chatdisplay = new GetTheChatDataDisplay();
        chatdisplay.execute();




    }




    private class GetTheChatDataDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
         //   showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()

                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.GET_CHAT_FROM_SERVER+"/"+userid,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData1: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                MyServerChatActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                      //  hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            chatdisplay = null;
            try {
                if (responce != null) {
                   // hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            ServerChatListArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), ServerChatListArray.class);
                            ChatBubbles.add(CourseInfo);
                            adapter = new MyServerChatListAdapter(MyServerChatActivity.this, R.layout.chat_text_left, ChatBubbles);
                            list_msg.setAdapter(adapter);
                           // recyclerChat.scrollToPosition(chatListAdapter.getItemCount() - 1);
                            list_msg.setSelection(list_msg.getAdapter().getCount()-1);


                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {
                      //  hideLoading();
                       // showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
              //  hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            chatdisplay = null;
           // hideLoading();


        }
    }








    private void sendMessage(final SendServerMessage sendMessageModel){
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = RequestBody.create(Constants.JSON,
                        gson.toJson(sendMessageModel, SendServerMessage.class));
                try {
                    String response = ApiCall.POSTHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.SEND_MESSAGE_TO_SERVER, requestBody);

                    if (response !=null){
                        getTheperviousDataChat();


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case android.R.id.home:
                delayhandler.removeCallbacks(mUpdateTimeTask);
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }


    @Override
    public void onBackPressed() {
        delayhandler.removeCallbacks(mUpdateTimeTask);
        finish();
    }


}