package co.savm.chat;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.UserDisplayProfile;
import co.savm.models.chat.GroupChatMemberArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

import static co.savm.chat.ChatGroupMemberProfile.RefershList;

/**
 * Created by Dell on 08-11-2017.
 */

public class GroupChatMemberAdapter extends RecyclerView.Adapter<GroupChatMemberAdapter.CustomVholder> {

    private ArrayList<GroupChatMemberArray> lists;
    private Context mcontext;
    String Group_id,GroupTittle,groupImage,is_admin,admin,memberId;
    private DeleteMember deleteMember = null;




    public GroupChatMemberAdapter(Context mcontext, ArrayList<GroupChatMemberArray> lists, String is_admin,String admin) {
        this.lists = lists;
        this.mcontext = mcontext;
        this.is_admin = is_admin;
        this.admin=admin;

    }


    @Override
    public CustomVholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ofchatmembers, null);

        return new CustomVholder(view);
    }



    @Override
    public void onBindViewHolder(final CustomVholder holder, final int position) {

        try {
            holder.memberName.setText(lists.get(position).getField_first_name()+" "+lists.get(position).getField_last_name());


            if(lists.get(position).getPicture() != null && lists.get(position).getPicture().length() > 0 ) {

                Glide.with(mcontext).load(lists.get(position).getPicture())
                        .placeholder(R.mipmap.single).dontAnimate()
                        .fitCenter().into(holder.circleView);

            }else {
                holder.circleView.setImageResource(R.mipmap.single);

            }



            holder.mainlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    final CharSequence  [] items= { "Remove this Member","Show Profile", "Cancel"};



                    AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);

                    builder.setTitle("Action:");
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (items[item].equals("Remove this Member")) {

                                AlertDialog.Builder builder = new AlertDialog.Builder(mcontext, android.app.AlertDialog.THEME_HOLO_DARK)
                                        .setTitle("Remove this member")
                                        .setMessage(R.string.delete_Chat_member)
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                /*  DELETE this member */

                                                if (lists.get(position).getEtid().equals(admin))

                                                {
                                                    deleteMemberfromlist(lists.get(position).getGid(), lists.get(position).getEtid());

                                                }else{

                                                    Toast.makeText(mcontext,"You are not authorized",Toast.LENGTH_SHORT).show();
                                                }



                                            }
                                        })
                                        .setNegativeButton("No", new Dialog.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                                dialogInterface.dismiss();
                                            }
                                        });
                                builder.create().show();



                            } else if (items[item].equals("Show Profile")) {

                                Intent backIntent = new Intent(mcontext, UserDisplayProfile.class)
                                        .putExtra("USERPROFILE_ID", lists.get(position).getEtid());
                                mcontext.startActivity(backIntent);

                            } else if (items[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();




                }
            });


            if (lists.get(position).getEtid().equals(admin))
            {
                holder.admin.setVisibility(View.VISIBLE);
            }

            else if(lists.get(position).getEtid().equals(is_admin))
            {
                holder.admin.setVisibility(View.VISIBLE);
            }





        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lists.size();
    }


    private void deleteMemberfromlist(String gid,String eid)
    {
        Group_id = gid;
        memberId = eid;
        deleteMember = new DeleteMember();
        deleteMember.execute();

    }


    /**
     * Viewholder for Adapter
     */
    public class CustomVholder extends RecyclerView.ViewHolder {
        ImageView circleView;
        private TextView memberName,admin;
        RelativeLayout mainlay;


        public CustomVholder(View itemView) {
            super(itemView);

            memberName = itemView.findViewById(R.id.memberName);
            circleView = itemView.findViewById(R.id.circleView);
            mainlay =itemView.findViewById(R.id.mainlay);
            admin =itemView.findViewById(R.id.admin);

        }

    }


    private class DeleteMember extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.p_dialog(mcontext);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .build();


            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_CHATGROUPMEMBEREXIXT+"/"+Group_id+"/"+memberId,body);
                jsonObject = new JSONObject(responseData);

                // https://www.questin.co/api/v1/og/leave/260405/3283

                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();

                Utils.p_dialog_dismiss(mcontext);
                Utils.showAlertDialog(mcontext, "Error", "There seems to be some problem with the Server. Try again later.");


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            deleteMember = null;
            try {
                if (responce != null) {
                    Utils.p_dialog_dismiss(mcontext);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        RefershList();
                        //  showAlertDialog(msg);
                        //  Toast.makeText(ChatGroupMemberProfile.this,msg,Toast.LENGTH_SHORT).show();



                        // finish();


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.p_dialog_dismiss(mcontext);
                        Utils.showAlertDialog(mcontext, "Error", msg);


                    }
                }
            } catch (JSONException e) {
                Utils.p_dialog_dismiss(mcontext);

            }
        }

        @Override
        protected void onCancelled() {
            deleteMember = null;
            Utils.p_dialog_dismiss(mcontext);


        }
    }




}


