package co.savm.chat;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.savm.R;
import co.savm.alumni.AluminiProfile;
import co.savm.calendersection.CalenderMain;
import co.savm.campusfeedsection.AllCampusFeeds;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.parent.ParentProfile;
import co.savm.studentprofile.ProfileDash;
import co.savm.teacher.TeacherProfile;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ChatTabbedActivity extends BaseAppCompactActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    BottomNavigationView mBottomNavigationView;
    TextView toolbar_title;
    String tokensend;
    ImageView cross,profileImage;
    String Course_id,CourseTittle,CourseImage;
    private ArrayAdapter<String> listAdapter;
    TextView textRegToken;
    ListView listMessages;
    Dialog imageDialog;
    private SharedPreferences preferences;
    private int Runfirst;
    private boolean hasgroup=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_tabbed);

        getIntentData();
        preferences = Utils.getSharedPreference(ChatTabbedActivity.this);
        Runfirst = preferences.getInt(Constants.RUNNIN_FIRST_CHAT, Constants.ROLE_RUNNING_FALSE_CHAT);
        FirstOneThis();


        textRegToken =findViewById(R.id.text_reg_token);
        listMessages =findViewById(R.id.list_messages);
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        if (hasgroup)
            viewPager.setCurrentItem(1);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


        Setuponbottombar();


        textRegToken.setText(FirebaseInstanceId.getInstance().getToken());
        Log.e("firebasetoken ",FirebaseInstanceId.getInstance().getToken());

//        if (FirebaseInstanceId.getInstance().getToken() != null)
//        {
//            UpdateToken();
//        }


        listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        listMessages.setAdapter(listAdapter);


    }

    private void FirstOneThis() {

        switch (Runfirst){
            case Constants.ROLE_RUNNING_TRUE :
                imageDialog = new Dialog(ChatTabbedActivity.this);
                imageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                imageDialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(Color.TRANSPARENT));
                imageDialog.setContentView(R.layout.image_overlay_screen);

                imageDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT);
                imageDialog.show();
                ImageView showimage = imageDialog.findViewById(R.id.DisplayOnbondingImage);
                showimage.setImageResource(R.mipmap.onboarding_chat);

                showimage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.getSharedPreference(ChatTabbedActivity.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_CHAT, Constants.ROLE_RUNNING_FALSE_CHAT).apply();
                        Utils.removeStringPreferences(ChatTabbedActivity.this,"0");

                        imageDialog.dismiss();

                    }
                });



                break;
            case Constants.ROLE_RUNNING_FALSE :
                break;


        }





    }

    private void getIntentData(){
        Intent intent = getIntent();

        hasgroup = intent.getBooleanExtra("isGroup",false);
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ConversationsActivity(), "Chat");
        adapter.addFragment(new MyChatsGroups(), "Group");

        viewPager.setAdapter(adapter);
    }






    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Constants.ACTION_MESSAGE_RECEIVED)){
                listAdapter.add(intent.getStringExtra(Constants.EXTRA_MESSAGE));
                listAdapter.notifyDataSetChanged();
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, new IntentFilter(Constants.ACTION_MESSAGE_RECEIVED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver);
    }






    private void Setuponbottombar() {

        TextView dashboard_child = findViewById(R.id.dashboard_child);
        final TextView calendar = findViewById(R.id.calendar);
        TextView campus_feeds = findViewById(R.id.campus_feeds);
        TextView message_feeds = findViewById(R.id.message_feeds);
        TextView profile = findViewById(R.id.profile);

        dashboard_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();


            }
        });

        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent p = new Intent(ChatTabbedActivity.this,CalenderMain.class);
                startActivity(p);
                finish();


            }
        });

        campus_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent y = new Intent(ChatTabbedActivity.this,AllCampusFeeds.class);
                startActivity(y);
                finish();


            }
        });

        message_feeds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent z = new Intent(ChatTabbedActivity.this,ChatTabbedActivity.class);
                startActivity(z);
                finish();
*/

            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionManager.getInstance(getActivity()).getUserClgRole()!=null) {

                    if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("13")) {
                        Intent j = new Intent(ChatTabbedActivity.this,ProfileDash.class);
                        startActivity(j);
                        finish();


                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
                        Intent j = new Intent(ChatTabbedActivity.this,TeacherProfile.class);
                        startActivity(j);
                        finish();


                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("15")) {
                        Intent j = new Intent(ChatTabbedActivity.this,AluminiProfile.class);
                        startActivity(j);
                        finish();


                    } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                        Intent j = new Intent(ChatTabbedActivity.this,ParentProfile.class);
                        startActivity(j);
                        finish();

                    }
                    else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("3")) {
                        Intent j = new Intent(ChatTabbedActivity.this,TeacherProfile.class);
                        startActivity(j);
                        finish();
                    }
                }
            }
        });


    }



    private void UpdateToken(){
        tokensend =  textRegToken.getText().toString();

        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = new FormBody.Builder()
                        .add("type", "android")
                        .add("token",tokensend)
                        .build();
                try {
                    String response = ApiCall.POSTHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.REG_TOKEN, requestBody);
                    JSONObject rootObject = new JSONObject(response);

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    ChatTabbedActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });


                }
            }
        }).start();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Utils.getSharedPreference(this).edit()
                .putInt(Constants.RUNNIN_FIRST_CHAT, Constants.ROLE_RUNNING_FALSE_CHAT).apply();
        Utils.removeStringPreferences(ChatTabbedActivity.this,"0");

        super.onBackPressed();
    }
}