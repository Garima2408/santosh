package co.savm.chat;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.ChatMemeberSelectAdapter;
import co.savm.models.AllFriends;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.studentprofile.FriendsSection;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class AddNewChatMember extends BaseAppCompactActivity  {
    EditText searchEmail;
    String SearchedName, uid, GroupMemberId,GroupId,Redirect;
    public ArrayList<AllFriends> mFriendsList;
    ChatMemeberSelectAdapter memberAdapter; /*all friends for selections*/
    FriendMembersAdapter friendmemberAdapter;
    //  public ArrayList<ChatSelectedArray> FriendSelectedList;
    RecyclerView SelectedName_Lists;/*list of all selected*/
    ListView rv_FriendList;/*list of friends*/
    ArrayAdapter<String> adapter;
    private ArrayList<String> frilist;
    private ArrayList<String> selectedstudentSend;
    RecyclerView.LayoutManager mLayoutManager;
    private MyFriendsListDisplay myfriendslist = null;
    private SearchedFriendDisplay searchfriendlist = null;
    private CreateChatGroupMember groupmemberchatelist = null;
    private ArrayList<String> UIDlists;
    TextView ErrorText;
    Button invitebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_chat_member);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        searchEmail =findViewById(R.id.searchEmail);
        SelectedName_Lists = findViewById(R.id.SelectedName_Lists);
        ErrorText =findViewById(R.id.ErrorText);
        invitebutton =findViewById(R.id.invitebutton);
        rv_FriendList = findViewById(R.id.rv_FriendList);
        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        SelectedName_Lists.setLayoutManager(mLayoutManager);
        SelectedName_Lists.setHasFixedSize(true);
        UIDlists = new ArrayList<>(); /*all friend list*/
        mFriendsList=new ArrayList<>(); /*all friend list*/
        frilist = new ArrayList<String>();/*search friend list*/

        Bundle b = getIntent().getExtras();
        GroupId = b.getString("GROUP_ID");
        Redirect =b.getString("ACTIVITY");
        myfriendslist = new MyFriendsListDisplay();
        myfriendslist.execute();
        searchEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {
                    addnewfriends();
                    return true;
                }
                return false;
            }
        });

        invitebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity (new Intent(AddNewChatMember.this, FriendsSection.class));
                finish();
            }
        });

    }


    private void addnewfriends() {
        mFriendsList.clear();
        searchEmail.setError(null);

        // Store values at the time of the login attempt.
        SearchedName = searchEmail.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(SearchedName)) {
            focusView = searchEmail;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            /* ADD COMMENTS ON SUBJECTS*/
            searchfriendlist = new SearchedFriendDisplay();
            searchfriendlist.execute();

        }
    }




    private class MyFriendsListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_MYFRIENDS+"?"+"email="+ SessionManager.getInstance(getActivity()).getUser().getEmail());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AddNewChatMember.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            myfriendslist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                AllFriends CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);
                                mFriendsList.add(CourseInfo);
                                frilist.add(CourseInfo.getField_firstname() + "" + CourseInfo.getField_lastname() + " " + CourseInfo.getPicture());
                                memberAdapter = new ChatMemeberSelectAdapter(AddNewChatMember.this,R.layout.list_groupchat_member, mFriendsList);
                                rv_FriendList.setAdapter(memberAdapter);
                                rv_FriendList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);



                                rv_FriendList.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                                    @Override
                                    public void onItemCheckedStateChanged(ActionMode mode,
                                                                          int position, long id, boolean checked) {
                                        // Capture total checked items
                                        final int checkedCount = rv_FriendList.getCheckedItemCount();

                                        Log.d("TAG", "checkedCount: " + checkedCount);
                                        mode.setTitle(checkedCount + " Selected");
                                        memberAdapter.toggleSelection(position);

                                        ArrayList<String> selectedstudent = new ArrayList<String>();
                                        SparseBooleanArray checkedd = rv_FriendList.getCheckedItemPositions();

                                        for (int i = 0; i < checkedd.size(); i++) {
                                            // Item position in adapter
                                            int positio = checkedd.keyAt(i);
                                            // Add sport if it is checked i.e.) == TRUE!
                                            if (checkedd.valueAt(i))
                                                /*  {0=true, 1=false, 2=true}*/

                                                selectedstudent.add(String.valueOf(memberAdapter.getItem(positio).getUid()+","+memberAdapter.getItem(positio).getField_firstname()+","+memberAdapter.getItem(positio).getField_lastname()+","+memberAdapter.getItem(positio).getPicture()));


                                        }

                                        String[] outputStrArr = new String[selectedstudent.size()];
                                        selectedstudentSend = new ArrayList<String>();
                                        for (int x = 0; x < selectedstudent.size(); x++) {
                                            outputStrArr[x] = selectedstudent.get(x);
                                            Log.d("TAG", " outputprint[x]: " + outputStrArr[x]);
                                            String[] parts =  outputStrArr[x] .split(",");
                                            uid = parts[0];
                                            String name = parts[1] + " " + parts[2];
                                            String picture = parts[3];
                                            GroupMemberId = uid;
                                            UIDlists.add(uid);

                                            selectedstudentSend.add(outputStrArr[x]);
                                            friendmemberAdapter = new FriendMembersAdapter(getApplicationContext(), R.layout.list_friend_member,selectedstudentSend);
                                            SelectedName_Lists.setAdapter(friendmemberAdapter);

                                        }

                                    }

                                    @Override
                                    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                                        switch (item.getItemId()) {
                                            case R.id.Submit:
                                                CallToAddMemberInGroup();
                                                return true;
                                            default:
                                                return false;
                                        }
                                    }

                                    @Override
                                    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                                        mode.getMenuInflater().inflate(R.menu.submit_menu, menu);
                                        return true;
                                    }

                                    @Override
                                    public void onDestroyActionMode(ActionMode mode) {
                                        // TODO Auto-generated method stub
                                        memberAdapter.removeSelection();

                                    }

                                    @Override
                                    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                                        // TODO Auto-generated method stub
                                        return false;
                                    }
                                });


                            }

                        }
                        else{
                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("you have no friends");
                            invitebutton.setVisibility(View.VISIBLE);

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            myfriendslist = null;
            hideLoading();


        }
    }

    private void CallToAddMemberInGroup() {
        for (int i = 0; i < UIDlists.size(); i++) {
            System.out.println(UIDlists.get(i));
            groupmemberchatelist = new CreateChatGroupMember();
            groupmemberchatelist.execute(UIDlists.get(i));

        }


    }


    private class SearchedFriendDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_SEARCHBYEMAIL+SearchedName);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AddNewChatMember.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            searchfriendlist = null;
            try {
                if (responce != null) {
                    hideLoading();


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");


                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                frilist.clear();
                                AllFriends CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), AllFriends.class);
                                mFriendsList.add(CourseInfo);
                                frilist.add(CourseInfo.getField_firstname()+""+CourseInfo.getField_lastname());

                                adapter = new ArrayAdapter<String>(AddNewChatMember.this,
                                        android.R.layout.simple_list_item_multiple_choice, frilist);
                                rv_FriendList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
                                rv_FriendList.setAdapter(adapter);

                                SparseBooleanArray checked = rv_FriendList.getCheckedItemPositions();
                                ArrayList<String> selectedItems = new ArrayList<String>();
                                for (int z = 0; z < checked.size(); z++) {
                                    // Item position in adapter
                                    int position = checked.keyAt(z);
                                    // Add sport if it is checked i.e.) == TRUE!
                                    if (checked.valueAt(z))
                                        selectedItems.add(adapter.getItem(position));
                                }

                                String[] outputStrArr = new String[selectedItems.size()];

                                for (int x = 0; x < selectedItems.size(); x++) {
                                    outputStrArr[x] = selectedItems.get(x);
                                }

                            }
                        }
                        else{
                            AlertDialog.Builder builder = new AlertDialog.Builder(AddNewChatMember.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(R.string.Friends))
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            searchfriendlist = null;
            hideLoading();


        }
    }

    private class CreateChatGroupMember extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .build();

            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_CREATEGROUPMEMBER +"/"+ GroupId +"/"+args[0], body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);
                Log.d("TAG", "args[0]: " + args[0]);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AddNewChatMember.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberchatelist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        Check();



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            groupmemberchatelist = null;
            hideLoading();


        }
    }

    private void Check() {

        if (Redirect.matches("CHATMEMBER_ACTIVITY")){
            ChatGroupMemberProfile.RefershList();
            finish();
        }else if(Redirect.matches("CHAT_ACTIVITY")){
            finish();
        }


    }

    @Override
    public void onBackPressed() {
        finish();

    }


}
