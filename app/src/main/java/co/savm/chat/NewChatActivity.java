package co.savm.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.database.QuestinSQLiteHelper;
import co.savm.models.chat.ConversationModel;
import co.savm.models.chat.NewChatModel;
import co.savm.models.userFriendsResponse.Datum;
import co.savm.models.userFriendsResponse.UserFriendsResponseModel;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.studentprofile.FriendsSection;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;

public class NewChatActivity extends AppCompatActivity {

    private Gson gson;
    private FriendsListAdapter friendsListAdapter;
    private QuestinSQLiteHelper questinSQLiteHelper;
     TextView ErrorText;
    ProgressBar progressBar;
     RecyclerView recyclerFriends;
     Button invitebutton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_chat);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        ErrorText =findViewById(R.id.ErrorText);
        progressBar =findViewById(R.id.progress_bar);
        recyclerFriends =findViewById(R.id.recycler_friends);
        invitebutton =findViewById(R.id.invitebutton);
        gson = new Gson();
        questinSQLiteHelper = new QuestinSQLiteHelper(this);
        friendsListAdapter = new FriendsListAdapter(new ArrayList<NewChatModel>(), this);
        recyclerFriends.setAdapter(friendsListAdapter);
        recyclerFriends.setLayoutManager(new LinearLayoutManager(this));
        getFriendsList();
        progressBar.setVisibility(View.VISIBLE);

        invitebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity (new Intent (NewChatActivity.this, FriendsSection.class));
                finish();
            }
        });

    }

    private void getFriendsList(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(), URLS.URL_MYFRIENDS+"?"+"email="+SessionManager.getInstance(NewChatActivity.this).getUser().getEmail());
                    final UserFriendsResponseModel userFriendsResponseModel =
                            gson.fromJson(response, UserFriendsResponseModel.class);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (userFriendsResponseModel!=null && userFriendsResponseModel.getData()!=null
                                    && userFriendsResponseModel.getData().size() >0){
                                for(Datum data : userFriendsResponseModel.getData()){
                                    friendsListAdapter.addData(
                                            new NewChatModel(data.getUid(),
                                                    data.getFieldFirstname() + " " + data.getFieldLastname(),
                                                    data.getPicture())
                                    );
                                }
                            }else {
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("you have no friends");
                                invitebutton.setVisibility(View.VISIBLE);
                            }




                            progressBar.setVisibility(View.GONE);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    class FriendsListAdapter extends RecyclerView.Adapter<FriendsListAdapter.FriendsListHolder>{

        private Context mContext;
        private LayoutInflater inflater;
        private ArrayList<NewChatModel> data;

        FriendsListAdapter(ArrayList<NewChatModel> data, Context context){
            this.data = data;
            mContext = context;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public FriendsListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new FriendsListHolder(inflater.inflate(R.layout.new_chat_list_adapter, parent, false));
        }

        @Override
        public void onBindViewHolder(final FriendsListHolder holder, int position) {
            holder.textName.setText(data.get(position).getName());
            Glide.with(mContext).load(data.get(position).getDpUrl())
                    .placeholder(R.mipmap.single).dontAnimate()
                    .fitCenter().into(holder.imageDp);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    ConversationModel conversationModel = new ConversationModel()
                            .setSenderId(data.get(holder.getAdapterPosition()).getId())
                            .setName(data.get(holder.getAdapterPosition()).getName())
                            .setDpUrl(data.get(holder.getAdapterPosition()).getDpUrl());

                    Intent backIntent = new Intent()
                            .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, !questinSQLiteHelper.doesConversationExist(conversationModel.getSenderId()))
                            .putExtra(Constants.CONVERSATION_MODEL, conversationModel);
                    NewChatActivity.this.setResult(RESULT_OK, backIntent);
                    NewChatActivity.this.finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        void addData(NewChatModel newChatModel){
            data.add(newChatModel);
            notifyItemInserted(data.size() - 1);
        }

        class FriendsListHolder extends RecyclerView.ViewHolder{

            View itemView;
            TextView textName;
            ImageView imageDp;
            FriendsListHolder(View itemView) {
                super(itemView);
                this.itemView = itemView;
                textName =itemView.findViewById(R.id.text_name);
                imageDp =itemView.findViewById(R.id.image_dp);

            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        finish();

    }
}
