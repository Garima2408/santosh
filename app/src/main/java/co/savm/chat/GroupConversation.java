package co.savm.chat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import co.savm.R;

public class GroupConversation extends AppCompatActivity {

   /* private Context mContext;
    private SharedPreferences dataFile;
    private GroupConversationListAdapter conversationListAdapter;
    private static final String TAG = "ConversationsActivity";

    private int RC_NEW_CHAT = 1;
    private int RC_NEW_GROUPCHAT = 2;



    @BindView(R.id.btn_new_chat)
    FloatingActionButton btnNewChat;
    @BindView(R.id.list_conversations)
    RecyclerView recyclerConversations;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_conversation);
    }
      /*  mContext = this;
        dataFile = Utils.getSharedPreference(this);
        conversationListAdapter = new GroupConversationListAdapter(new ArrayList<GroupConversationModel>(), this);
        recyclerConversations.setAdapter(conversationListAdapter);
        recyclerConversations.setLayoutManager(new LinearLayoutManager(this));

        if(!dataFile.getBoolean(Constants.IS_FCM_TOKEN_REGISTERED, false)){
            sendFcmToken();
        }


        QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(this);
        conversationListAdapter.addAllData(questinSQLiteHelper.getGroupConversationsList());




        Log.e(TAG, "onCreateId: " + SessionManager.getInstance(this).getUser().getUserprofile_id());
        Log.e(TAG, "onCreateToken: " + dataFile.getString(Constants.FCM_REG_TOKEN, null));

        btnNewChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newChatIntent = new Intent(mContext, AddGroupChatList.class);
                startActivityForResult(newChatIntent, RC_NEW_CHAT);
            }
        });



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_NEW_CHAT) {
            if (resultCode == RESULT_OK) {
                QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(this);
                conversationListAdapter.addAllData(questinSQLiteHelper.getGroupConversationsList());

                startActivity(new Intent(this, GroupChatActivity.class)
                        .putExtra(Constants.EXTRA_IS_FIRST_MESSAGE, data.getBooleanExtra(Constants.EXTRA_IS_FIRST_MESSAGE, false))
                        .putExtra(Constants.CONVERSATION_MODEL, data.getParcelableExtra(Constants.CONVERSATION_MODEL))
                );
            }
        }



    }

    private void sendFcmToken(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = new FormBody.Builder()
                        .add("type", "android")
                        .add("token", dataFile.getString(Constants.FCM_REG_TOKEN, ""))
                        .build();
                try {
                    String response = ApiCall.POSTHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.REG_TOKEN, requestBody);
                    JSONObject rootObject = new JSONObject(response);
                    if(rootObject.getJSONObject("data").getString("success").equals("1")){
                        dataFile.edit().putBoolean(Constants.IS_FCM_TOKEN_REGISTERED, true).apply();
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void deleteFcmToken(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String response = ApiCall.DELETE_HEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.REG_TOKEN + "/" + dataFile.getString(Constants.FCM_REG_TOKEN, "null"));
                    Log.e(TAG, "run: " + response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }*/

}
