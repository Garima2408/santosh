package co.savm.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import co.savm.R;


/**
 * Created by Dell on 12-09-2017.
 */

public class FriendMembersAdapter extends RecyclerView.Adapter<FriendMembersAdapter.RecyclerViewHolders> {
    private String TAG = getClass().getSimpleName();
    private int selectedItem = -1;
    private Context mContext;
    private int mRowLayout;
    String part1;
    private ArrayList<String> itemList;


    public FriendMembersAdapter(Context context, int rowLayout, ArrayList<String> itemList) {
        this.itemList = itemList;
        this.mContext = context;
        this.mRowLayout = rowLayout;
    }



    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(mRowLayout, null);
        return new RecyclerViewHolders(layoutView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {

           String Friends =itemList.get(position);

        String[] parts = Friends.split(",");
        String uid = parts[0];
        String name = parts[1]+" "+ parts[2];
        String picture = parts[3];

        Log.d("TAG", "outpuadapr: " + uid+","+name+","+picture);
        Log.d("TAG", "parts: "+ parts);


        holder.friend_name.setText(name);

        if(picture != null && picture.length() > 0 ) {

            Glide.with(mContext).load(picture)
                    .placeholder(R.mipmap.single).dontAnimate()
                    .fitCenter().into(holder.circleView);

        }else {
            holder.circleView.setImageResource(R.mipmap.single);

        }

    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void setSelecteditem(int selecteditem) {
        this.selectedItem = selecteditem;
        notifyDataSetChanged();
    }



    // RecyclerViewHolders Inner Class
    public class RecyclerViewHolders extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        ImageView circleView;
        TextView  friend_name;


        public RecyclerViewHolders(View convertView) {
            super(convertView);

            circleView = convertView.findViewById(R.id.circleView);
            friend_name = convertView.findViewById(R.id.friend_name);

            convertView.setTag(convertView);
            convertView.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

        }
    }


}


