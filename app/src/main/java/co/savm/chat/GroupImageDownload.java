package co.savm.chat;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

import co.savm.database.QuestinSQLiteHelper;
import co.savm.models.chat.GroupChatModel;
import co.savm.utils.Constants;
import co.savm.utils.Utils;

/**
 * Created by HP on 4/5/2018.
 */

public class GroupImageDownload extends IntentService {

    private static final String TAG = "GroupImageDownload";

    public GroupImageDownload(){
        super("GroupImageDownload");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        assert intent != null;
        final GroupChatModel chatModel = intent.getParcelableExtra(Constants.GROUP_MODEL);
        final String senderId = intent.getStringExtra(Constants.GROUP_ID);
        final QuestinSQLiteHelper questinSQLiteHelper = new QuestinSQLiteHelper(this);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference reference = storage.getReferenceFromUrl(chatModel.getLink());
        Log.e("link download before",chatModel.getLink());

        if(Utils.isExternalStorageWritable()){
            final File imageFile = new File(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    Utils.createNewImageFileName());

            boolean isCreated = false;
            try {
                isCreated = imageFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.e("link download",chatModel.getLink());

            if(isCreated){
                reference.getFile(imageFile)
                        .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                chatModel.setLink(Uri.fromFile(imageFile).toString());


                                Log.e("link download",chatModel.getLink());
                                questinSQLiteHelper.updateGroupChatMessageLink(chatModel, senderId);
                                LocalBroadcastManager.getInstance(GroupImageDownload.this)
                                        .sendBroadcast(new Intent(Constants.ACTION_IMAGE_DOWNLOADED)
                                                .putExtra(Constants.GROUP_MODEL, chatModel)
                                                .putExtra(Constants.GROUP_ID, senderId)
                                        );
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                    }
                });
            }
            else {
                Log.e(TAG, "onHandleIntent: file not created");
            }
        }
    }
}
