package co.savm.alumni;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.SelectYourType;
import co.savm.models.CollageLists;
import co.savm.models.CollageRoleArray;
import co.savm.models.CourseMainList;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class AluminiRegistration extends BaseAppCompactActivity {
    EditText edt_enrolment,edt_bach,edt_branch,edt_selectsub;
    Button btn_go_dashboard;
    TextView collageName;
    Spinner edt_selectsubSpinner;
    String enrollment,batch,branch,subject,Membershipid;
    private RegistrationAluminiAuthTask registrationaluminAuthTask = null;
    private ArrayList<String> courselist;
    ArrayList<CourseMainList> listofcourse;
    private ProgressSearchOfCourseList progresssearchofcourselist = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alumini_registration);
        GetTheListOfCourse();

        edt_enrolment = findViewById(R.id.edt_enrolment);
        edt_bach = findViewById(R.id.edt_bach);
        edt_branch = findViewById(R.id.edt_branch);
        edt_selectsub = findViewById(R.id.edt_selectsub);
        btn_go_dashboard = findViewById(R.id.btn_go_dashboard);
        edt_selectsubSpinner = findViewById(co.savm.R.id.edt_selectsubSpinner);
        courselist = new ArrayList<String>();
        listofcourse = new ArrayList<CourseMainList>();


        collageName = findViewById(R.id.collageName);
        String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        SpannableString spString = new SpannableString(Topheading);
        AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
        spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        collageName.setText(spString);



        btn_go_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptToAluminiRegistration();


            }
        });

        edt_selectsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

    }

    private void GetTheListOfCourse() {
        progresssearchofcourselist = new ProgressSearchOfCourseList();
        progresssearchofcourselist.execute();

    }






    private void attemptToAluminiRegistration() {
        edt_enrolment.setError(null);
        edt_bach.setError(null);
        edt_branch.setError(null);
        edt_selectsub.setError(null);



        // Store values at the time of the login attempt.
        enrollment= edt_enrolment.getText().toString().trim();
        batch = edt_bach.getText().toString().trim();
        branch = edt_branch.getText().toString().trim();
        subject = edt_selectsub.getText().toString().trim();


        CollageRoleArray userRoleDetail = new CollageRoleArray();
        userRoleDetail.role = "15";
        userRoleDetail.tnid = SessionManager.getInstance(getActivity()).getCollage().getTnid();
        userRoleDetail.title = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        userRoleDetail.field_group_image = SessionManager.getInstance(getActivity()).getCollage().getField_group_image();
        userRoleDetail.CollageLogo =SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo();
        userRoleDetail.CollageRole ="alumni";
        userRoleDetail.CollageBranch = branch;
        userRoleDetail.CollageBatch =batch;
        userRoleDetail.CollageEnrollment =enrollment;
        SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);



        boolean cancel = false;
        View focusView = null;

         if (co.savm.utils.TextUtils.isNullOrEmpty(enrollment)) {
            // check for First Name
            focusView = edt_enrolment;
            cancel = true;
             showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(batch)) {
            // check for First Name
            focusView = edt_bach;
            cancel = true;
             showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(branch)) {
            // check for First Name
            focusView = edt_branch;
            cancel = true;
             showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(subject)) {
            // check for First Name
            focusView = edt_selectsub;
            cancel = true;
             showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            registrationaluminAuthTask = new RegistrationAluminiAuthTask();
            registrationaluminAuthTask.execute();



        }
    }


    private class RegistrationAluminiAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("group_type","node")
                    .add("membership type","og_membership_type_college")
                    .add("field_i_am_a","alumni")
                    .add("state","1")
                    .add("etid",SessionManager.getInstance(getActivity()).getUser().userprofile_id)
                    .add("gid",SessionManager.getInstance(getActivity()).getCollage().getTnid())
                    .add("field_branch",branch)
                    .add("field_enrollment_number",enrollment)
                    .add("field_member_course",subject)
                    .add("field_batch_year",batch)
                    .add("field_name","field_college_member")
                    .add("roles[15]","alumni")
                    .build();





            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_STUDENETREGISTRATION,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AluminiRegistration.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            registrationaluminAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject data = responce.getJSONObject("data");

                        if (data != null && data.length() > 0) {
                            Membershipid = data.getString("membership_id");

                            Log.d("TAG", "membership_id: " + Membershipid );

                        } else {

                        }

                        CollageLists lists = new CollageLists();
                        lists.setTitle(SessionManager.getInstance(getActivity()).getCollage().getTitle());
                        lists.setTnid(SessionManager.getInstance(getActivity()).getCollage().getTnid());
                        lists.setField_group_image(SessionManager.getInstance(getActivity()).getCollage().getField_group_image());
                        lists.setField_groups_logo(SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo());
                        lists.setLat(SessionManager.getInstance(getActivity()).getCollage().getLat());
                        lists.setLng(SessionManager.getInstance(getActivity()).getCollage().getLng());
                        lists.setMultiple(SessionManager.getInstance(getActivity()).getCollage().getMultiple());
                        lists.setType(SessionManager.getInstance(getActivity()).getCollage().getType());

                        lists.setCollageMemberShipId(Membershipid);
                        SessionManager.getInstance(getActivity()).saveCollage(lists);

                        Intent upanel = new Intent(AluminiRegistration.this, AluminiMain.class);
                        Utils.getSharedPreference(AluminiRegistration.this).edit()
                                .putInt(Constants.USER_ROLE, Constants.ROLE_ALUMNI).apply();

                        Utils.getSharedPreference(AluminiRegistration.this).edit()
                                .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_TRUE).apply();

                        Utils.getSharedPreference(AluminiRegistration.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_TRUE_CAMPUSFEED).apply();

                        Utils.getSharedPreference(AluminiRegistration.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_TRUE_PROFILE).apply();

                        Utils.getSharedPreference(AluminiRegistration.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_CHAT, Constants.ROLE_RUNNING_TRUE_CHAT).apply();



                        startActivity(upanel);

                        finish();





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            registrationaluminAuthTask = null;
            hideLoading();


        }
    }


    private class ProgressSearchOfCourseList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();




            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COURCELIST+"?cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid());

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                AluminiRegistration.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.savm.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofcourselist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i=0; i<data.length();i++) {
                            CourseMainList GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), CourseMainList.class);
                            courselist.add(GoalInfo.getName());
                            listofcourse.add(GoalInfo);

                            edt_selectsubSpinner.setAdapter(new ArrayAdapter<String>(AluminiRegistration.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    courselist));


                            edt_selectsubSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    edt_selectsub.setText(listofcourse.get(position).getTid());

                                    Log.d("TAG", "edt_selectsub: " + edt_selectsub);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {}

                            });

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofcourselist = null;
            hideLoading();


        }
    }


    @Override
    public void onBackPressed() {
        Intent i = new Intent(AluminiRegistration.this, SelectYourType.class);
        startActivity(i);

        finish();
    }
}

