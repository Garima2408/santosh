package co.savm.alumni;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import co.savm.R;
import co.savm.college.CampusServices;
import co.savm.college.CollageEvent;
import co.savm.college.CollageMaps;
import co.savm.college.Facilities;
import co.savm.college.Faculties;
import co.savm.college.GroupNSocieties;
import co.savm.college.ImportantLink;
import co.savm.college.Media;
import co.savm.college.OldPapers;
import co.savm.college.Projects;
import co.savm.college.StudyNotes;
import co.savm.college.Syllabus;

/**
 * Created by HP on 4/14/2018.
 */

public class Adapterdashboard extends RecyclerView.Adapter<Adapterdashboard.ViewHolder> {
    private Activity context;
     String typeDash;
    private final int[] gridViewImageId;
    public Adapterdashboard(Activity context, String type, int[] gridViewImageId) {
        this.context = context;
        this.typeDash=type;
        this.gridViewImageId=gridViewImageId;
    }

    @Override
    public Adapterdashboard.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_dashboard, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Adapterdashboard.ViewHolder viewHolder, final int i) {

      //  viewHolder.img_android.setBackground(context.getResources().getDrawable(dashboardArrayList.get(i).getImg()));

        Log.e("image",""+gridViewImageId[i]);
        if (typeDash.equals("collegeInfo")){
            viewHolder.img_android.setImageResource(gridViewImageId[i]);
        }else  if (typeDash.equals("studentInfo")){
            viewHolder.img_android.setImageResource(gridViewImageId[i]);
        } if (typeDash.equals("acFinance")){
            viewHolder.img_android.setImageResource(gridViewImageId[i]);
        } if (typeDash.equals("mResource")){
            viewHolder.img_android.setImageResource(gridViewImageId[i]);
        }
        viewHolder.linear_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        if (typeDash.equals("collegeInfo")){

                            switch (i){
                                case 0:

                                   context. startActivity (new Intent (context, Facilities.class));

                                    break;
                                case 1:
                                    context. startActivity (new Intent (context, CollageMaps.class));

                                    break;
                                case 2:

                                    context.  startActivity (new Intent (context, ImportantLink.class));

                                    break;
                                case 3:
                                    context.  startActivity (new Intent (context, CampusServices.class));

                                    break;
                                case 4:
                                    //  startActivity (new Intent (getActivity(), Facilities.class));
                                    //  startActivity (new Intent (getActivity(), AcademicDivision.class));
                                    Toast.makeText(context, "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                                    break;

                                case 5:
                                    //startActivity (new Intent (getActivity(), AcademicDivision.class));
                                    // startActivity (new Intent (getActivity(), Accommodation.class));
                                    Toast.makeText(context, "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();


                                    break;


                            }
                         }else  if (typeDash.equals("studentInfo")){



                            switch (i){
                                case 0:

                                    context.startActivity (new Intent (context, GroupNSocieties.class));
                                     // Toast.makeText(context, "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                                    break;
                                case 1:
                                    context.  startActivity (new Intent (context, Media.class));

                                    break;
                                case 2:

                                  //  context .startActivity (new Intent (context, CoursesModules.class));

                                    context. startActivity (new Intent (context, CollageEvent.class));


                                    break;
                                case 3:

                                    context.startActivity (new Intent (context, Faculties.class));
                               //     context. startActivity (new Intent (context, CollageEvent.class));
                                    //Toast.makeText(getActivity(), "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                                    break;
                                case 4:
                                 //   context.startActivity (new Intent (context, Faculties.class));
                                    Toast.makeText(context, "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                                    break;
                                case 5:
                                    // startActivity (new Intent (getActivity(), Alumni.class));
                                    Toast.makeText(context, "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                                    break;
                                case 6:
                                 //   Toast.makeText(context, "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                                    //context.startActivity(new Intent(context, AllRoutesDisplay.class));
                                    break;
                                case 7:
                                    Toast.makeText(context, "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();

                                    //  startActivity(new Intent(getActivity(), AdminMapsActivity.class));
                                    break;
                            }

                        } if (typeDash.equals("acFinance")){
                    switch (i){
                        case 0:

                            context.startActivity (new Intent (context, Projects.class));

                            break;
                        case 1:
                            context.startActivity (new Intent (context, StudyNotes.class));



                            break;
                        case 2:

                            context. startActivity (new Intent (context, OldPapers.class));


                            break;
                        case 3:
                            context.   startActivity (new Intent (context, Syllabus.class));

                            break;


                    }

                           } if (typeDash.equals("mResource")){

                    switch (i){
                        case 0:

                           //context. startActivity (new Intent (context, DealsNDiscount.class));
                            Toast.makeText(context, "Your Upcoming Feature!", Toast.LENGTH_SHORT).show();
                            break;

                    }
                }

            }
        });

    }

    @Override
    public int getItemCount() {


//        if (typeDash.equals("collegeInfo")){
//            return gridViewImageId.length;
//        }else  if (typeDash.equals("studentInfo")){
//            return gridViewImageId.length;
//        } if (typeDash.equals("acFinance")){
//            return gridViewImageId.length;
//        } if (typeDash.equals("mResource")){
//            return gridViewImageId.length;
//        }


        return gridViewImageId.length;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView img_android;
        private LinearLayout linear_;
        public ViewHolder(View view) {
            super(view);

            img_android = view.findViewById(R.id.img_android);

            linear_= view.findViewById(R.id.linear_);
        }
    }
}
