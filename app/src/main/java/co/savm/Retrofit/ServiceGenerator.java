package co.savm.Retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;




public class ServiceGenerator {

    //base url

    public static  String BASE_URL= "http://beusalons.com/";

//    public static  String BASE_URL= "http://13.126.45.78/";        //pre live but live database

//    public static  String BASE_URL= "http://13.126.90.129/";        //test
//    public static final String BASE_URL= "http://10.0.3.8/";        //local nikita

    //google distance matrix api

    private static Retrofit retrofit = null;





//    private static final String BASE_DISTANCE_URL= "https://maps.googleapis.com/maps/";
    private static final String BASE_DISTANCE_URL= "http://maps.googleapis.com/maps/api/distancematrix/";


    public static Retrofit getClient() {

        if (retrofit==null) {


            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);



            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
             httpClient.connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)

            .networkInterceptors().add(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    final Request request = chain.request().newBuilder()
                            .addHeader("CUSTOM_HEADER_NAME_1", "CUSTOM_HEADER_VALUE_1")
                            .build();
                    return chain.proceed(request);
                }
            });


//////////////////////// add logging as last intesrceptor
//            if (BuildConfig.DEBUG) {
////                BASE_URL= "http://beusalons.com/";        //test
//                BASE_URL= "http://13.126.45.78/";         //pre live
//                httpClient.addInterceptor(logging);
//            }else BASE_URL= "http://beusalons.com/";

////            // <-- this is the important line!




            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(httpClient.build())
                    .build();
        }
        return retrofit;
    }



    public static Retrofit getDistance() {

        if (retrofit==null) {

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_DISTANCE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }


}
