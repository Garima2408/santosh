package co.savm.Retrofit;


import co.savm.models.GeoCode.GeoCodingResponse;
import co.savm.models.etaResponse.DistanceModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;


/**
 * Created by Ajay Rajput on 30/3/2018.
 */

public interface ApiInterface {


//    @GET("user")
//    Call<ParlorUser> getSuccessDetails();


    @GET
        //this is to get address by lat long
    Call<GeoCodingResponse> getAddress(@Url String url);
//    @GET
//    Call<DistanceModel> getDistanceDuration(@Query("units") String units, @Query("origin") String origin, @Query("destination") String destination);

    @GET
    Call<DistanceModel> distanceMatrix(@Url String url);
//    @GET("api/directions/json?key=AIzaSyC22GfkHu9FdgT9SwdCWMwKX1a4aohGifM")
//    Call<Example> getDistanceDuration(@Query("units") String units, @Query("origin") String origin, @Query("destination") String destination, @Query("mode") String mode)



}
