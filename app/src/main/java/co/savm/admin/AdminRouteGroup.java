package co.savm.admin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.savm.Event.ServerMemberUpdate;
import co.savm.R;
import co.savm.adapters.BusRouteCommentsAdapter;
import co.savm.adapters.CourseMembersAdapter;
import co.savm.models.CourseMemberLists;
import co.savm.models.tracker.BusCommentArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.tracker.BusAddComment;
import co.savm.utils.BaseFragment;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class AdminRouteGroup extends BaseFragment {
    static TextView ErrorText;
    static Activity activity;
    static String Route_id,droplat;
    String RouteTittle,Adminuser,Join_Value,AdminCreater;
    RecyclerView.LayoutManager mLayoutManager,layoutManager;
    RecyclerView GroupMember_Lists;
    static RecyclerView rv_route_comment;
    static SwipeRefreshLayout swipeContainer;
    private static ProgressBar spinner;
    static FloatingActionButton fab;
    static Button JoinGroup;
    CourseMembersAdapter mGroupMemberAdapter;
    static BusRouteCommentsAdapter mcommentAdapter;
    public static ArrayList<BusCommentArray> commentsList;
    public ArrayList<CourseMemberLists> memberList;
    private GroupMemberList groupmemberList = null;
    private static RouteCommentsList routecommentsList = null;
    private static boolean isLoading= true;
    private boolean _hasLoadedOnce= true; // your boolean field
    Dialog pickUpDialog;
    private static int PAGE_SIZE = 0;
    static String tittle,URLDoc;
    public static List<String> groupMember;
    private boolean isComment=false;
    public String field_place_name,field_start_time,field_departure_time,field_way_points,lat,lng,field_photo,driver_name,driver_no,co_drivername,co_driverno;


    public AdminRouteGroup() {
        // Required empty public constructor
    }

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(isFragmentVisible_);
        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                Log.e("view", "routeFeeds");
                PAGE_SIZE = 0;
                isLoading = false;
                swipeContainer.setRefreshing(true);
                routecommentsList = new RouteCommentsList();
                routecommentsList.execute(Route_id);

                _hasLoadedOnce = false;
            }
        }
    }

    @Override
    public void onPause() {

        Log.e("onPause","routeFeeds");
        _hasLoadedOnce= true;
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_admin_route_group, container, false);
        activity = (Activity) view.getContext();
        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle= getArguments().getString("ROUTE_NAME");
        Adminuser = getArguments().getString("IS_FACULTY");
        Join_Value = getArguments().getString("IS_MEMBER");
        AdminCreater = getArguments().getString("IS_CREATER");

        JoinGroup = view.findViewById(R.id.JoinGroup);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        GroupMember_Lists = view.findViewById(R.id.GroupMember_Lists);
        spinner= view.findViewById(R.id.progressBar);
        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        layoutManager = new LinearLayoutManager(activity);
        GroupMember_Lists.setLayoutManager(mLayoutManager);
        rv_route_comment = view.findViewById(R.id.rv_route_comment);
        rv_route_comment.setHasFixedSize(true);
        rv_route_comment.setLayoutManager(layoutManager);
        fab = view. findViewById(R.id.fab);
        ErrorText= view. findViewById(R.id.ErrorText);
        memberList = new ArrayList<CourseMemberLists>();
        commentsList = new ArrayList<BusCommentArray>();
        groupMember=new ArrayList<>();

        mcommentAdapter = new BusRouteCommentsAdapter(rv_route_comment, commentsList ,activity);
        rv_route_comment.setAdapter(mcommentAdapter);
        rv_route_comment.setVisibility(View.GONE);




        if (Join_Value.equals("member")) {

            JoinGroup.setVisibility(View.GONE);


            }




        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent backIntent = new Intent(activity, BusAddComment.class)
                        .putExtra("ROUTE_ID",Route_id)
                        .putExtra("open","AdminBusRouteGroup");
                startActivity(backIntent);




            }
        });


        /*CALLING LISTS OF ROUTE MEMBER */
        groupmemberList = new GroupMemberList();
        groupmemberList.execute(Route_id);


        return view;
    }
    public static void RefreshWorkedBusRoute() {
        PAGE_SIZE=0;
        isLoading=true;

        swipeContainer.setRefreshing(true);

        /* CALLING LISTS OF COMMENTS*/
        routecommentsList = new RouteCommentsList();
        routecommentsList.execute(Route_id);


    }
    private void inItView(){

        swipeContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(true);

        rv_route_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();

                    routecommentsList = new RouteCommentsList();
                    routecommentsList.execute(Route_id);

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }

    /*STUDENTS ON THE SUBJECTS*/

    private class GroupMemberList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEMEMBERLIST+"/"+args[0]+"/users?fields=id, etid, gid");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    memberList.clear();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);
                                groupMember.add(CourseInfo.getEtid());
                                if (CourseInfo.getEtid().contains(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()))
                                {
                                    Log.d("TAG", "myid: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());

                                    JoinGroup.setVisibility(View.GONE);

                                    inItView();



                                }else {

                                }


                            }
                            mGroupMemberAdapter = new CourseMembersAdapter(activity, R.layout.list_of_groupmemberlists,memberList);
                            GroupMember_Lists.setAdapter(mGroupMemberAdapter);


                            if(SessionManager.getInstance(activity).getUser().getParentUid()==null){
                                if (groupMember.contains(SessionManager.getInstance(activity)
                                        .getUser().getUserprofile_id())){
                                    Utils.init(activity);
                                    Utils.clearDataJoin();
                                    Utils.saveJoinGroup(true);
                                    EventBus.getDefault().post(new ServerMemberUpdate(1));

                                }else{
                                    Utils.init(activity);
                                    Utils.clearDataJoin();
                                    Utils.saveJoinGroup(false);


                                }

                            }else {
                                if (groupMember.contains(SessionManager.getInstance(activity)
                                        .getUser().getParentUid())){
                                    Utils.init(activity);
                                    Utils.clearDataJoin();
                                    Utils.saveJoinGroup(true);
                                    EventBus.getDefault().post(new ServerMemberUpdate(1));



                                }else{
                                    Utils.init(activity);
                                    Utils.clearDataJoin();
                                    Utils.saveJoinGroup(false);


                                }
                            }

                            Utils.init(activity);

                            if (Utils.isJoinGroup(activity)){
//                                routecommentsList = new RouteCommentsList();
//                                routecommentsList.execute(Route_id);
                                _hasLoadedOnce=false;
                                setUserVisibleHint(true);
                            }

                        }else {

                           /* JoinGroup.setVisibility(View.VISIBLE);
                            rv_subject_comment.setVisibility(View.GONE);
                            fab.setVisibility(View.GONE);*/
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                    showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
                showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            spinner.setVisibility(View.INVISIBLE);



        }
    }



    /*LISTS OF COMMENTS ON SUBJECTS*/

    private static class RouteCommentsList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTECOMMENTSLIST+"?"+"rid"+"="+args[0]+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);


            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

                mcommentAdapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    rv_route_comment.setVisibility(View.VISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        ArrayList<BusCommentArray> arrayList = new ArrayList<>();
                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {



                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                BusCommentArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), BusCommentArray.class);

                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                if (jsonObject.has("field_comment_attachments")){
                                    JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                    if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                        URLDoc = AttacmentsFeilds.getString("value");
                                        tittle = AttacmentsFeilds.getString("title");
                                        subjects.title =tittle;
                                        subjects.url =URLDoc;

                                    }else {

                                    }
                                }
                                arrayList.add(subjects);



                            }

                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            rv_route_comment.setVisibility(View.VISIBLE);
                            if (PAGE_SIZE == 0) {


                                mcommentAdapter.setInitialData(arrayList);

                            } else {
                                mcommentAdapter.removeProgress();
                                swipeContainer.setRefreshing(false);
                                mcommentAdapter.addData(arrayList);


                            }

                            isLoading = false;

                        } else{
                            if (PAGE_SIZE==0){
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Post");
                                swipeContainer.setVisibility(View.GONE);
                            }else
                                mcommentAdapter.removeProgress();

                        }


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        showAlertFragmentDialog(activity,"Information", msg);


                    }
                }else {
                    swipeContainer.setRefreshing(false);
                    showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            routecommentsList = null;
            swipeContainer.setRefreshing(false);



        }
    }



    public static void showAlertFragmentDialog(Context context, String title, String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        alertDialog.setTitle(context.getResources().getString(R.string.app_name));

        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    @Override
    public void onDestroy() {
        isLoading=false;
        PAGE_SIZE=0;
        activity.finish();
        super.onDestroy();
    }









}
