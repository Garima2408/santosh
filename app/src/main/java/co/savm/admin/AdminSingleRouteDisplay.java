package co.savm.admin;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import co.savm.Event.ServerMemberUpdate;
import co.savm.R;
import co.savm.Retrofit.ApiInterface;
import co.savm.Retrofit.ServiceGenerator;
import co.savm.activities.SpalashScreen;
import co.savm.models.etaResponse.DistanceModel;
import co.savm.models.routedetailResponse.RouteDetailResponseModel;
import co.savm.models.routedetailResponse.Waypoint;
import co.savm.models.tracker.GeofenceModel;
import co.savm.models.tracker.LocationModel;
import co.savm.models.tracker.StopsModel;
import co.savm.models.tracker.WayPointResponse;
import co.savm.models.tracker.WayPointsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.tracker.AllRoutesDisplay;
import co.savm.tracker.HttpConnection;
import co.savm.tracker.PathJSONParser;
import co.savm.utils.BaseFragment;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;


public class AdminSingleRouteDisplay  extends BaseFragment implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "RideTrackDialogFragment";
    private static final String TAG2 = "Geo";

    private boolean isMapInitialized = false;
    private boolean isRouteDetailsFetched = false;
    private GoogleMap mGoogleMap;
    private Polyline polyline;
    private PolylineOptions polylineOptions;
    private LocationModel locationModel;
    private HashMap<Marker, StopsModel> mMarkersHashMap;
    private int RC_TRACKER_INTENT = 0;
    MapView mapView;
    ImageView imageLocateBus;
    FloatingActionButton fab;
    boolean isPressed = false;
    private String myStop;
    private String routeId;
    private String routeName,sponsered;
    private String collegeId;
    private Marker vehicleMarker;
    private GeofenceModel geofenceModel;
    static Activity activity;
    private DatabaseReference etaDatabase;
    private DatabaseReference locationDatabase;
    private DatabaseReference geofenceDatabase;
    private ValueEventListener etaEventListener;
    private ValueEventListener geofenceEventListener;
    private ValueEventListener locationEventListener;
    private Gson gson;
    private StopsModel sourceStop;
    private StopsModel destStop;
    private ArrayList<StopsModel> mStopsList;
    private RouteDetailResponseModel routeDetailResponseModel;
    private Handler uiHandler;
    DistanceModel.Duration durationModel;
    View rootView;
    private LinearLayout update_pick;
    private ArrayList<WayPointsArray> wayPointsArrayList;
    private TextView txtEta;
    JSONObject jsonObjectSource;
    public String field_place_name, field_start_time, field_departure_time, field_way_points, lat, lng, field_photo;
    private String pickLocation="",dropLocation="";
    WayPointResponse wayPickDropJson=new WayPointResponse();
    ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
    WayPointsArray wayPointsPick=new WayPointsArray();
    WayPointsArray wayPointsDrop=new WayPointsArray();
    private Location location;
    private static final int REQUEST_LOCATION_PERMISSION_CODE = 101;
    int speed;
    ImageView image_locate_bus,traficEnable;
    private boolean firstRender = true;
    private GeofencingRequest geofencingRequest;
    private GoogleApiClient googleApiClient;

    private boolean isMonitoring = true;

    private MarkerOptions markerOptions;

    private Marker currentLocationMarker;
    private PendingIntent pendingIntent;





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_admin_single_route_display, container, false);
        activity = (Activity) rootView.getContext();
        mapView =rootView.findViewById(R.id.map_view);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();
        Bundle argsBundle=getArguments();
        routeId = argsBundle.getString("ROUTE_ID");
        routeName = argsBundle.getString("ROUTE_NAME");
        sponsered=argsBundle.getString("sponsered");
        mMarkersHashMap=new HashMap<>();
        uiHandler = new Handler();
        txtEta=rootView.findViewById(R.id.txtEta);
        collegeId = SessionManager.getInstance(activity).getCollage().getTnid();
        image_locate_bus =rootView.findViewById(R.id.image_locate_bus);
        traficEnable =rootView.findViewById(R.id.traficEnable);
        imageLocateBus =rootView.findViewById(R.id.image_locate_bus);
        fab =rootView.findViewById(R.id.fab);



        googleApiClient = new GoogleApiClient.Builder(activity)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE);
        }





        init();
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;

                //  startLocationMonitor();

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity
                        , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.


                } else {
                  /*  mGoogleMap.setMyLocationEnabled(true);
                    mMap.setMyLocationEnabled(true);
                    mMap.getUiSettings().setMapToolbarEnabled(true);
*/
                    //  mGoogleMap.setTrafficEnabled(true);

                    Log.e(TAG, "onMapReady: ");
                    if(isRouteDetailsFetched && !isMapInitialized){
                        isMapInitialized = true;
                        initMap();
                    }

                }
            }
        });

        getRouteDetail();
        image_locate_bus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (locationModel != null) {
                    CameraUpdate current = CameraUpdateFactory.newLatLngZoom(new LatLng(locationModel.getLat(), locationModel.getLng()), 20);
                    if (firstRender) {
                        mGoogleMap.animateCamera(current);
                        firstRender = false;
                    } else {
                        mGoogleMap.moveCamera(current);
                    }

                }
            }
        });

        traficEnable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isPressed){
                    mGoogleMap.setTrafficEnabled(true);

                }else if(!isPressed){

                    mGoogleMap.setTrafficEnabled(false);

                }

                isPressed = !isPressed; // reverse





            }
        });


        return rootView;
    }


    private void init(){
        gson = new Gson();

        etaDatabase = FirebaseDatabase.getInstance().getReference()
                .child( collegeId +"/routes/"+ routeId + "/routes_name/" + routeName + "/eta");
        locationDatabase = FirebaseDatabase.getInstance().getReference()
                .child( collegeId +"/routes/"+ routeId+"/routes_name/" + routeName + "/location");
        geofenceDatabase = FirebaseDatabase.getInstance().getReference()
                .child( collegeId +"/routes/"+ routeId + "/routes_name/" + routeName + "/geofence");



        etaEventListener = getEtaEventListener();
        locationEventListener = getLocationEventListener();
        geofenceEventListener = getGeofenceEventListener();

        geofenceModel = new GeofenceModel();
        polylineOptions = new PolylineOptions().geodesic(true).width(5).color(Color.BLUE);

        mStopsList = new ArrayList<>();

    }
    @Override
    public void onStart() {
        googleApiClient.reconnect();

        super.onStart();
        if(mapView != null) mapView.onStart();

    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ServerMemberUpdate event) {

        /* Do something */

        if (Utils.isJoinGroup(getActivity())) {

            locationDatabase = FirebaseDatabase.getInstance().getReference()
                    .child( collegeId +"/routes/"+ routeId + "/routes_name/" + routeName + "/location");

            locationEventListener = getLocationEventListener();

        }

    }
    @Override
    public void onResume() {
        super.onResume();

        int response = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(activity);
        if (response != ConnectionResult.SUCCESS) {
            Log.d(TAG, "Google Play Service Not Available");
            GoogleApiAvailability.getInstance().getErrorDialog(activity, response, 1).show();
        } else {
            Log.d(TAG, "Google play service available");
        }



        if(mapView != null) mapView.onResume();
        Log.e("onResume","onResume");




    }

    @Override
    public void onPause() {
        if(mapView != null) mapView.onPause();
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public void onStop() {
        if(mapView != null) mapView.onStop();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if(mapView != null) mapView.onDestroy();

        if(locationEventListener != null){
            Log.e(TAG, "onStop: location Listener removed");
            locationDatabase.removeEventListener(locationEventListener);
        }
        if(geofenceEventListener != null){
            Log.e(TAG, "onStop: geofence Listener removed");
            geofenceDatabase.removeEventListener(geofenceEventListener);
        }
        if(etaEventListener != null){
            Log.e(TAG, "onStop: eta Listener removed");
            etaDatabase.removeEventListener(etaEventListener);
        }
        googleApiClient.disconnect();

        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(mapView != null) mapView.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        if(mapView != null) mapView.onLowMemory();
        super.onLowMemory();
    }

    private ValueEventListener getLocationEventListener(){
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() == null) return;
                Log.e(TAG, "onDataChange: " + dataSnapshot.toString());
                if(locationModel == null){
                    locationModel = new LocationModel();
                    locationModel.updateModel(dataSnapshot.getValue(LocationModel.class));
                }
                locationModel.updateModel(dataSnapshot.getValue(LocationModel.class));
                vehicleMarker.setPosition(new LatLng(locationModel.getLat(), locationModel.getLng()));
                vehicleMarker.setRotation(locationModel.getBearing());
                Double floatspeed =locationModel.getSpeed()*3.6;
                speed = Integer.valueOf(floatspeed.intValue()) ;
                vehicleMarker.setSnippet("Speed: " +speed +"Km/h");
                Log.e(TAG, "Speed: "+locationModel.getSpeed());

                animateMarker(locationModel,vehicleMarker);

                Log.d(TAG2, "First Lat Long"+ locationModel.getLat()+".."+locationModel.getLng() );

                Utils.init(activity);


                WayPointResponse wayPointResponse = Utils.getsavePickAndDrop(activity);
                if (wayPointResponse!=null && wayPointResponse.getWayPointsArrays().get(0).getLat()!=null){

                    Log.e(TAG, "Waypoints curent: "+locationModel.getLat()+"..."+locationModel.getLng());
                    Log.e(TAG, "Waypoints curent server: "+wayPointResponse.getWayPointsArrays().get(0).getLat()+"..."
                            +locationModel.getLng() +","+wayPointResponse.getWayPointsArrays().get(0).getLng() +
                            ".."+ wayPointResponse.getWayPointsArrays().get(0).getField_place_name());

                    try{
                        getETA(new LatLng(locationModel.getLat(), locationModel.getLng()),
                                new LatLng(Double.valueOf(wayPointResponse.getWayPointsArrays().get(0).getLat()),
                                        Double.valueOf(wayPointResponse.getWayPointsArrays().get(0).getLng())),
                                wayPointResponse.getWayPointsArrays().get(0).getField_place_name());


                    }catch (IndexOutOfBoundsException e){

                    }

                }

                //{"lat":"28.50931012964809","lng":"77.37459729171587"}
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        };
    }



    private ValueEventListener getGeofenceEventListener(){
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, "onDataChange: " + dataSnapshot.toString());
                if(dataSnapshot.getValue() != null){
                    geofenceModel.updateModel(dataSnapshot.getValue(GeofenceModel.class));


                    //  Toast.makeText(activity, geofenceModel.toString(), Toast.LENGTH_LONG).show();
                }
               /* else {
                    if (getActivity()!=null)
                        Toast.makeText(activity, "null geofence object",
                                Toast.LENGTH_SHORT).show();
                }*/
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        };
    }

    private ValueEventListener getEtaEventListener(){
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() != null){

                    //  Toast.makeText(activity, "Eta "+dataSnapshot.getValue().toString(), Toast.LENGTH_SHORT).show();

                    Integer seconds = dataSnapshot.getValue(Integer.class);
                    long hours = TimeUnit.SECONDS.toHours(seconds);
                    long minutes = TimeUnit.SECONDS.toMinutes(seconds);
                    if(hours == 0){
//                        textMyEta.setText(String.format(Locale.ENGLISH ,
//                                "ETA: %d minutes", minutes));



                    }
                    else {
//                        textMyEta.setText(String.format(Locale.ENGLISH ,
//                                "ETA: %d hours %d minutes", hours, minutes));
                    }
                }
                else {
                    //  Toast.makeText(activity, "No Eta yet", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled: " + databaseError.getMessage());
            }
        };
    }



    private void getRouteDetail(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.ROUTE_DETAILS + routeId);
                    Log.e(TAG, "run: " + response);


                    if (response!=null){
                        routeDetailResponseModel=new RouteDetailResponseModel();
                        routeDetailResponseModel = gson.fromJson(response, RouteDetailResponseModel.class);

                        sourceStop = new StopsModel(Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLat()),
                                Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getLng()),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getField_place_name(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldStartTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldDepartureTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getSource().getFieldPhoto());

                        destStop = new StopsModel(Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLat()),
                                Double.valueOf(routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getLng()),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getField_place_name(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldStartTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldDepartureTime(),
                                routeDetailResponseModel.getData().getFieldCollectionWayPoints().getDestination().getFieldPhoto());


                        for(Waypoint waypoint : routeDetailResponseModel.getData().getFieldCollectionWayPoints().getWaypoints()){

                            mStopsList.add(new StopsModel(Double.valueOf(waypoint.getFieldWayPoints().getLat()),
                                    Double.valueOf(waypoint.getFieldWayPoints().getLng()),
                                    waypoint.getField_place_name(),
                                    waypoint.getFieldStartTime(),
                                    waypoint.getFieldDepartureTime(),
                                    waypoint.getFieldPhoto()));


                        }



                        if(routeDetailResponseModel.getData().getPickup_point()!=null && routeDetailResponseModel.getData().getDrop_point()!=null){
                            pickLocation=routeDetailResponseModel.getData().getPickup_point().getLat()+routeDetailResponseModel.getData().getPickup_point().getLng();
                            dropLocation=routeDetailResponseModel.getData().getDrop_point().getLat()+routeDetailResponseModel.getData().getDrop_point().getLng();

                        }



                        isRouteDetailsFetched = true;
                        isMapInitialized=false;

                        uiHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                if(!isMapInitialized){
                                    isMapInitialized = true;
                                    initMap();
                                }
                            }
                        });
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }




    private void initMap(){
        for(int i=0; i<mStopsList.size(); i++){
            if(i == mStopsList.size()-1){
                String url = getDirectionsUrl(mStopsList.get(i), destStop);
                ReadTask downloadTask = new ReadTask();
                downloadTask.execute(url);

                //  addStopMarker(mStopsList.get(i));


                continue;
            }
            if(i==0){
                String url = getDirectionsUrl(sourceStop,mStopsList.get(0));
                ReadTask downloadTask = new ReadTask();
                downloadTask.execute(url);
                // addStopMarker(mStopsList.get(0));

            }
            String url = getDirectionsUrl(mStopsList.get(i), mStopsList.get(i+1));
            ReadTask downloadTask = new ReadTask();
            downloadTask.execute(url);

            //  addStopMarker(mStopsList.get(i+1));
        }
        String ltLngSrc=String.valueOf(sourceStop.getLat())+sourceStop.getLng();
        String ltLngDes=String.valueOf(destStop.getLat())+destStop.getLng();

      /*  wayPointsPick.setLat(""+sourceStop.getLat());
        wayPointsPick.setLng(""+sourceStop.getLng());
        wayPointsPick.setField_place_name(sourceStop.getPlanceName());
        wayPointsDrop.setPick(true);
*/
        if(routeDetailResponseModel.getData().getPickup_point()!=null && routeDetailResponseModel.getData().getDrop_point()!=null){
            Utils.clearPickDropData();

        }



        wayPointsDrop.setLat(""+destStop.getLat());
        wayPointsDrop.setLng(""+destStop.getLng());
        wayPointsDrop.setField_place_name(destStop.getPlanceName());
        wayPointsDrop.setDrop(true);

        // wayPointsArrays.add(wayPointsPick);
        wayPointsArrays.add(wayPointsDrop);
        wayPickDropJson.setWayPointsArrays(wayPointsArrays);


        Utils.savePickAndDrop(activity,wayPickDropJson);







        mGoogleMap.addMarker(new MarkerOptions()
                .title(sourceStop.getPlanceName())
                //  .snippet("Your pick")
                .position(new LatLng(sourceStop.getLat(), sourceStop.getLng())));



        mGoogleMap.addMarker(new MarkerOptions()
                .title(destStop.getPlanceName())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .position(new LatLng(destStop.getLat(), destStop.getLng())));



        Utils.init(getActivity());
        if (Utils.isJoinGroup(activity)){

            vehicleMarker = mGoogleMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.bus))
                    .title(routeName)
                    .flat(true).position(new LatLng(sourceStop.getLat(), sourceStop.getLng())));




            locationDatabase.addValueEventListener(locationEventListener);
            geofenceDatabase.addValueEventListener(geofenceEventListener);
           /* Log.d(TAG2, "First Lat Long"+ sourceStop.getLat()+".."+sourceStop.getLng() );
            startGeofencing(sourceStop.getLat(),sourceStop.getLng());
*/

        }

        //etaDatabase.child(myStop).addValueEventListener(etaEventListener);

        polyline = mGoogleMap.addPolyline(polylineOptions);
        mGoogleMap.moveCamera(CameraUpdateFactory
                .newLatLngZoom(new LatLng(sourceStop.getLat(), sourceStop.getLng()), 14));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.e(TAG, "Connection Failed:" + connectionResult.getErrorMessage());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        this.mGoogleMap = googleMap;
        // googleMap.setMyLocationEnabled(true);




    }

    class ReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                HttpConnection http = new HttpConnection();
                data = http.readUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task","test"+ e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("onPostExecute Task","test"+ result);

            new ParserTask().execute(result);

        }
    }

    private void addStopMarker(StopsModel stopModel){
        String ltLng=String.valueOf(stopModel.getLat())+stopModel.getLng();

        // condition here for geofencing

        if (ltLng.equals(pickLocation)){
            mGoogleMap.addMarker(new MarkerOptions()
                    .title(stopModel.getPlanceName())
                    .snippet("Your pick")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_pick_location))
                    .position(new LatLng(stopModel.getLat(), stopModel.getLng())));
            wayPointsPick.setLat(""+stopModel.getLat());
            wayPointsPick.setLng(""+stopModel.getLng());
            wayPointsPick.setField_place_name(stopModel.getPlanceName());
            // wayPointsArrays.add(0,wayPointsPick);
            wayPointsDrop.setPick(true);




        }else  if ( ltLng.equals(dropLocation) ){
            mGoogleMap.addMarker(new MarkerOptions()
                    .title(stopModel.getPlanceName())
                    .snippet("Your drop")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_drop_loc))
                    .position(new LatLng(stopModel.getLat(), stopModel.getLng())));


            wayPointsDrop.setLat(""+stopModel.getLat());
            wayPointsDrop.setLng(""+stopModel.getLng());
            wayPointsDrop.setField_place_name(stopModel.getPlanceName());
            wayPointsDrop.setDrop(true);
            //wayPointsArrays.add(1,wayPointsDrop);

        }else{
            mGoogleMap.addMarker(new MarkerOptions()
                    .title(stopModel.getPlanceName())
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.stop))
                    .position(new LatLng(stopModel.getLat(), stopModel.getLng())));
        }

    }

    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {
        Activity activity;
        StopsModel stopsModel;
        public MarkerInfoWindowAdapter(Activity activity,StopsModel  stopsModel) {
            this.activity=activity;
            this.stopsModel=stopsModel;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            View v =activity.getLayoutInflater().inflate(R.layout.googlemapinfo_window, null);



            StopsModel myMarker = mMarkersHashMap.get(marker);

            TextView tittle = v.findViewById(R.id.tittle);
            TextView pick = v.findViewById(R.id.Arivaltime);

            TextView drop = v.findViewById(R.id.DepatureTime);
            ImageView location = v.findViewById(R.id.pinlocation);
            // location.setImageResource(R.mipmap.stoplocation);
            if (myMarker!=null){
                tittle.setText(myMarker.getPlanceName());
                pick.setText(myMarker.getStartTime());
                drop.setText(myMarker.getDepartTime());
            }

            return v;
        }
    }
    class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                PathJSONParser parser = new PathJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("parserTask fail","test");
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> routes) {
            ArrayList<LatLng> points = null;
            PolylineOptions polyLineOptions = null;

            // traversing through routes
            // Log.e("onPostExecute success","test"+routes.size());

            for (int i = 0; i < routes.size(); i++) {

                points = new ArrayList<LatLng>();
                polyLineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = routes.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                polyLineOptions.addAll(points);
                polyLineOptions.width(5);
                polyLineOptions.color(Color.BLUE);
            }

            if (polyLineOptions!=null)
                mGoogleMap.addPolyline(polyLineOptions);
        }
    }
    private String getDirectionsUrl(StopsModel origin, StopsModel dest) {

        // Origin of route
        String str_origin = "origin=" + origin.getLat() + "," + origin.getLng();

        // Destination of route
        String str_dest = "destination=" + dest.getLat() + "," + dest.getLng()
                ;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters+"&key="+Constants.MAPS_API_KEY;


        return url;
    }



    private void getNotification(String eta,String pickLoc){
        Intent trackerIntent = new Intent(activity, AllRoutesDisplay.class);
        PendingIntent trackerPendingIntent = PendingIntent.getActivity(activity, RC_TRACKER_INTENT,
                trackerIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(activity, Constants.CH_TRACKING_NOTIFICATION)
                        .setContentIntent(trackerPendingIntent)
                        .setContentText(eta)
                        .setContentTitle("Your pick location "+pickLoc)
                        .setSmallIcon(R.mipmap.appicon)
                        .setAutoCancel(false)

                        .setSmallIcon(R.mipmap.ic_launcher);
        NotificationManager notificationManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(Constants.ID_TRACKING_NOTIFICATION, builder.build());


    }




    private void getETA(LatLng origin, LatLng dest,String placeName) {
        Retrofit distance= ServiceGenerator.getDistance();
        ApiInterface service= distance.create(ApiInterface.class);


        Call<DistanceModel> call= service.distanceMatrix("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+origin.latitude+","+origin.longitude+"&destinations="+dest.latitude+","+dest.longitude+
                //           "&departure_time=now&traffic_model=best_guess&key="+Constants.MAPS_API_KEY);
                "&departure_time=now&traffic_model=best_guess"+"&key="+Constants.MAPS_API_KEY);

        call.enqueue(new Callback<DistanceModel>() {
            @Override
            public void onResponse(Call<DistanceModel> call, retrofit2.Response<DistanceModel> response) {

                if(response!=null){
                    try {

                        Utils.init(activity);

                        WayPointResponse wayPointResponse = Utils.getsavePickAndDrop(activity);
                        if (response.body().getRows()!=null && response.body().getRows().size()>0){
                            String distance=response.body().getRows().get(0).getElements().get(0).getDistance().getText();



                            Log.e("distance",response.body().getDestinationAddresses()+""
                                    +distance+"time"
                                    +response.body().getRows().get(0).getElements().get(0).getDuration().getText());
                            durationModel= response.body().getRows().get(0).getElements().get(0).getDuration();


                            String str = distance;
                            String[] splited = str.split("\\s+");

                            String DistanceSplit =  splited[0];

                            int CalculateRange= new Double(DistanceSplit).intValue();

                            // int CalculateRange = Double.valueOf(DistanceSplit).intValue();

                            Log.e("distance Split", String.valueOf(CalculateRange) +""+ splited[0]);



                            //  Toast.makeText(activity, "Main Eta Distance " + distance.toString() + response.body().getRows().get(0).getElements().get(0).getDuration().getText(), Toast.LENGTH_SHORT).show();


                            String title="Speed: " + speed +" "+"Km/h "+ " "+"ETA: " + distance +" " +response.body().getRows().get(0).getElements().get(0).getDuration().getText();


                            vehicleMarker.setSnippet(title);


                            //  Toast.makeText(activity, "Eta Distance " + distance.toString() + response.body().getRows().get(0).getElements().get(0).getDuration().getText(), Toast.LENGTH_SHORT).show();




                           /* getNotification(response.body().getRows().get(0).getElements().get(0).getDuration().getText()
                                    +" ETA ("+ wayPointResponse.getWayPointsArrays().get(0).getField_place_name()+")"+distance,wayPointResponse.getWayPointsArrays().get(0).getField_place_name());
*/



                            if (distance!=null){

                            }
                        }

                    }
                    catch (NullPointerException e){
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<DistanceModel> call, Throwable t) {

            }
        });
    }







    private void animateMarker(final LocationModel locationModel, final Marker vehicleMarker) {

        if (this.vehicleMarker != null) {
            final LatLng startPosition = this.vehicleMarker.getPosition();
            final LatLng endPosition = new LatLng(locationModel.getLat(), locationModel.getLng());
            // Location  mLocation =lat+lng;


            final float startRotation = this.vehicleMarker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000); // duration 1 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        AdminSingleRouteDisplay.this.vehicleMarker.setPosition(newPosition);
                        vehicleMarker.setRotation(computeRotation(v, startRotation,locationModel.getBearing()));
                    } catch (Exception ex) {
                        // I don't care atm..
                    }
                }
            });

            valueAnimator.start();
        }


    }



    private static float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }
    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    private void handleNotification(String messageBody, String bigText) {
        Intent intent = new Intent(activity,SpalashScreen.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        int uniqueId = (int) System.currentTimeMillis();

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationManager notificationManager =
                (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Creates the PendingIntent
            PendingIntent notifyPendingIntent =
                    PendingIntent.getActivity(
                            activity,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            String channelID = "com.myapp.ind.push.ServiceListener";// The id of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelID, "MyApp", importance);
            // Create a notification and set the notification channel.
            Notification notification = getNotificationBuilder(messageBody, notifyPendingIntent, defaultSoundUri)
                    .setChannelId(channelID)
                    .build();

            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mChannel);
                notificationManager.notify(uniqueId, notification);
            }
        } else if (notificationManager != null) {

            PendingIntent pendingIntent =
                    PendingIntent.getActivity(
                            activity,
                            0,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                    );

            NotificationCompat.Builder notificationBuilder = getNotificationBuilder(messageBody, pendingIntent, defaultSoundUri);
            notificationManager.notify(uniqueId /* ID of notification */,
                    notificationBuilder.build());
        }
    }

    private NotificationCompat.Builder getNotificationBuilder(String messageBody, PendingIntent pendingIntent, Uri defaultSoundUri) {
        return new NotificationCompat.Builder(activity)
                .setSmallIcon(R.mipmap.appicon)
                .setContentTitle("Santosh Riyan Badi")
                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


    }

}
