package co.savm.Event;

/**
 * Created by HP on 6/1/2018.
 */

public class PickLocationEvent {


    private int pickPos;
    private  int dropPos;

    public PickLocationEvent(int pickPos, int dropPos) {
        this.pickPos = pickPos;
        this.dropPos = dropPos;
    }

    public int getPickPos() {
        return pickPos;
    }

    public void setPickPos(int pickPos) {
        this.pickPos = pickPos;
    }

    public int getDropPos() {
        return dropPos;
    }

    public void setDropPos(int dropPos) {
        this.dropPos = dropPos;
    }
}
