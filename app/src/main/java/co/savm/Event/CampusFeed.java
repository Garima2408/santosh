package co.savm.Event;

/**
 * Created by HP on 5/5/2018.
 */

public class CampusFeed {
    private int pos;

    public CampusFeed(int pos) {
        this.pos = pos;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
