package co.savm.Event;

/**
 * Created by HP on 3/30/2018.
 */

public class ImageUpload {

    private boolean isUpload=false;
    private int pos;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public ImageUpload(boolean isUpload, int pos) {
        this.isUpload = isUpload;
        this.pos = pos;
    }


    public boolean isUpload() {
        return isUpload;
    }

    public void setUpload(boolean upload) {
        isUpload = upload;
    }
}
