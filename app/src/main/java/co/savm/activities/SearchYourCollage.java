package co.savm.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.CollageLists;
import co.savm.models.MultipleLocn;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class SearchYourCollage extends BaseAppCompactActivity {

    public ArrayList<CollageLists> collageListMain;
    public ArrayList<String> collageList;
    private ProgressSearchOfCollageList progressSearchofcollagelist = null;
    EditText Autoentercollage;
    TextView output;
    Button ButtonSelected_collage;
    String selectedText, selectedClgId, matchText, SelectedClgImageId;
    ListView choose_class;
    String keyword,latiti,longiti;
    ArrayList<MultipleLocn> multiple ;
    Spinner CollegeTypeList;
    String CollageTpyeSelected,TypeSelected,typee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_your_collage);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        collageListMain = new ArrayList<>();
        collageList = new ArrayList<>();
        TypeSelected ="allcollegesbyname";

        final String[] collageType = { "All", "College", "School", "Vocational-ITI", "Polytechnic" , "University",};

        //Initializing views and adding onclick listeners

        CollegeTypeList = findViewById(R.id.CollegeTypeList);
        Autoentercollage = findViewById(R.id.entercollage);
        ButtonSelected_collage = findViewById(R.id.Selected_collage);
        output = findViewById(R.id.txt);
        choose_class = findViewById(R.id.choose_class);

        progressSearchofcollagelist = new ProgressSearchOfCollageList();
        progressSearchofcollagelist.execute("santosh");



        Autoentercollage.addTextChangedListener(mTextEditorWatcher);


        Autoentercollage.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {

                    selectedText = Autoentercollage.getText().toString();
                    keyword =selectedText;




                    return true;
                }
                return false;
            }
        });

        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,collageType);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        CollegeTypeList.setAdapter(aa);

        CollegeTypeList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                CollageTpyeSelected = collageType[position].toString();

                if(CollageTpyeSelected.contains("All")){


                }else if(CollageTpyeSelected.contains("College")){
                    TypeSelected ="collegebyname";

                }else if(CollageTpyeSelected.contains("Schools")){
                    TypeSelected ="schoolsbyname";


                }else if(CollageTpyeSelected.contains("Vocational-ITI")){
                    TypeSelected ="vocationalbyname";

                }else if(CollageTpyeSelected.contains("Polytechnic")){
                    TypeSelected ="polytechnicbyname";

                }
                else if(CollageTpyeSelected.contains("Universities")){
                    TypeSelected ="universitiesbyname";

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }


    TextWatcher mTextEditorWatcher = new TextWatcher(){

        @Override
        public void afterTextChanged(Editable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                      int arg3) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onTextChanged(CharSequence s, int a, int b, int c) {
            // TODO Auto-generated method stub

            output.setText("");

            // output.setText(s);

            if (c == 3) {
                keyword =s.toString();

                Log.d("TAG", "keyword: " + keyword);

                progressSearchofcollagelist = new ProgressSearchOfCollageList();
                progressSearchofcollagelist.execute(keyword);

            }



        }};





    private class ProgressSearchOfCollageList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }


        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGELISTBYTYPE+TypeSelected+"?title="+args[0]+"&offset="+"0"+"&limit=1000" );
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);
                Log.d("TAG", "keyword2: " + args[0]);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                SearchYourCollage.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.savm.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }


        @Override
        protected void onPostExecute(JSONObject responce) {
            progressSearchofcollagelist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    collageListMain.clear();
                    collageList.clear();
                    choose_class.setVisibility(View.VISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        if(data != null && data.length() > 0 ) {
                            for (int i = 0; i < 2; i++) {

                                final CollageLists CollageInfo = new CollageLists();

                                String  tnid = data.getJSONObject(i).getString("tnid");
                                String  title = data.getJSONObject(i).getString("title");
                                String  field_groups_logo = data.getJSONObject(i).getString("field_groups_logo");
                                String  field_group_image = data.getJSONObject(i).getString("field_group_image");
                                String CollageAffiliation = data.getJSONObject(i).getString("affiliation");



                                try{
                                    final JSONArray locate = data.getJSONObject(0).getJSONArray("location");
                                    if (locate != null && locate.length() > 0 ){
                                        multiple = new ArrayList<>();
                                        for (int j = 0; j < locate.length(); j++) {

                                            latiti = locate.getJSONObject(j).getString("lat");
                                            longiti = locate.getJSONObject(j).getString("lng");

                                            String latiti1 = locate.getJSONObject(0).getString("lat");
                                            String longiti2 = locate.getJSONObject(0).getString("lng");

                                            MultipleLocn lon = new MultipleLocn();
                                            lon.lat =latiti;
                                            lon.lng =longiti;
                                            multiple.add(lon);

                                            CollageInfo.lat = latiti1;
                                            CollageInfo.lng = longiti2;
                                            CollageInfo.multiple=multiple;


                                        }

                                    }

                                }catch (Exception e){

                                }


                                CollageInfo.tnid=tnid;
                                CollageInfo.title =title;
                                CollageInfo.field_group_image =field_group_image;
                                CollageInfo.field_groups_logo =field_groups_logo;
                                CollageInfo.affiliation = CollageAffiliation;
                                CollageInfo.type =CollageTpyeSelected;
                                collageListMain.add(CollageInfo);
                                collageList.add(CollageInfo.getTitle());


                                choose_class.setAdapter(new ArrayAdapter<String>(SearchYourCollage.this,
                                        android.R.layout.simple_spinner_dropdown_item,
                                        collageList));

                                choose_class.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    public void onItemClick(AdapterView<?> parent, View view,
                                                            int position, long id) {

                                        if (position==0){

                                            typee ="College";

                                        }else if (position==1){

                                            typee ="Schools";

                                        }
                                        String  Collageid = collageListMain.get(position).getTnid();
                                        String colaagename = collageListMain.get(position).getTitle();
                                        String CollageImageurl =collageListMain.get(position).getField_group_image();
                                        String Collagelogourl =collageListMain.get(position).getField_groups_logo();
                                        String lagtt =collageListMain.get(position).getLat();
                                        String longgg =collageListMain.get(position).getLng();
                                        String affila =collageListMain.get(position).getAffiliation();

                                        CollageLists lists = new CollageLists();
                                        lists.setTitle(colaagename);
                                        lists.setTnid(Collageid);
                                        lists.setField_group_image(CollageImageurl);
                                        lists.setField_groups_logo(Collagelogourl);
                                        lists.setLat(lagtt);
                                        lists.setLng(longgg);
                                        lists.setAffiliation(affila);
                                        lists.setMultiple(collageListMain.get(position).getMultiple());
                                        lists.setType(typee);
                                        SessionManager.getInstance(getActivity()).saveCollage(lists);

                                        Utils.getSharedPreference(SearchYourCollage.this).edit()
                                                .putBoolean(Constants.IS_COLLEGE_ADDED, true).apply();

                                        Intent i = new Intent(SearchYourCollage.this, SelectYourType.class);
                                        startActivity(i);
                                        finish();

                                    }
                                });




                            }



                        }else {

                            output.setText(R.string.no_matching_data);
                            choose_class.setVisibility(View.GONE);
                            output.setVisibility(View.VISIBLE);
                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);


                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progressSearchofcollagelist = null;
            hideLoading();


        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {


            case android.R.id.home:
                Intent m = new Intent(SearchYourCollage.this, SignIn.class);
                startActivity(m);
                finish();

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }

    }

}