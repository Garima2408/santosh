package co.savm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AlignmentSpan;
import android.view.View;
import android.widget.TextView;

import co.savm.R;
import co.savm.alumni.AluminiRegistration;
import co.savm.models.CollageRoleArray;
import co.savm.parent.ParentRegistration;
import co.savm.student.StudentRegistration;
import co.savm.teacher.TeacherRegistration;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import co.savm.visitor.VisitorMain;

public class SelectYourType extends BaseAppCompactActivity implements View.OnClickListener {
    TextView tv_student_register, tv_faculty, vister,alumini,parent,tv_college_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_your_type);
        tv_college_name = findViewById(R.id.tv_college_name);
        tv_student_register = findViewById(R.id.tv_student_register);
        tv_faculty = findViewById(R.id.tv_faculty);
        vister = findViewById(R.id.vister);
        alumini = findViewById(R.id.alumini);
        parent = findViewById(R.id.parent);


        if (SessionManager.getInstance(getActivity()).getCollage().getTitle()!=null){
            String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
            SpannableString spString = new SpannableString(Topheading);
            AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
            spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv_college_name.setText(spString);

        }


        tv_student_register.setOnClickListener(this);
        tv_faculty.setOnClickListener(this);
        vister.setOnClickListener(this);
        alumini.setOnClickListener(this);
        parent.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.tv_student_register:

                CollageRoleArray userRoleDetail = new CollageRoleArray();
                userRoleDetail.CollageRole ="student";
                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);

                Intent i = new Intent(SelectYourType.this, StudentRegistration.class);
                startActivity(i);


                break;

            case R.id.tv_faculty:
                CollageRoleArray userRoleDetail1 = new CollageRoleArray();
                userRoleDetail1.CollageRole ="faculty";
                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail1);


                Intent j = new Intent(SelectYourType.this, TeacherRegistration.class);
                startActivity(j);

                break;

            case R.id.vister:

                Intent k = new Intent(SelectYourType.this, VisitorMain.class);
                Utils.getSharedPreference(SelectYourType.this).edit()
                        .putInt(Constants.USER_ROLE, Constants.ROLE_VISITOR).apply();
                startActivity(k);

                break;
            case R.id.alumini:

                CollageRoleArray userRoleDetail3 = new CollageRoleArray();
                userRoleDetail3.CollageRole ="alumni";
                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail3);

                Intent l = new Intent(SelectYourType.this, AluminiRegistration.class);
                startActivity(l);

                break;

            case R.id.parent:

                CollageRoleArray userRoleDetail4 = new CollageRoleArray();
                userRoleDetail4.CollageRole ="parents";
                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail4);
                Intent m = new Intent(SelectYourType.this, ParentRegistration.class);
                startActivity(m);

                break;

        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}