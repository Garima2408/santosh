package co.savm.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.models.CollageLists;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class SelectCollage extends BaseAppCompactActivity   implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener {

    public ArrayList<CollageLists> collageListMain;
    public ArrayList<String> collageList;
    private  ProgressSearchOfCollageList progressSearchofcollagelist = null;
    AutoCompleteTextView Autoentercollage;
    Button ButtonSelected_collage;
    String selectedText,selectedClgId,matchText,SelectedClgImageId;

    //Our Map
    private GoogleMap mMap;

    //To store longitude and latitude from map
    private double longitude;
    private double latitude;
    String longi;
    String lati;
    String CollageImageurl;

    //Google ApiClient
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_collage);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        collageListMain = new ArrayList<CollageLists>();
        collageListMain = new ArrayList<>();
        final ArrayList<CollageLists> collageListMain = null;
        collageList = new ArrayList<>();
        progressSearchofcollagelist = new ProgressSearchOfCollageList();
        progressSearchofcollagelist.execute();

        //Initializing views and adding onclick listeners




        Autoentercollage = findViewById(R.id.entercollage);
        ButtonSelected_collage = findViewById(R.id.Selected_collage);

        Autoentercollage.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {

                    selectedText = Autoentercollage.getText().toString();

                    if (collageList.contains(selectedText)){
                        String[] collageName = selectedText.split("-");
                        CollageLists lists = new CollageLists();
                        lists.setTitle(collageName[0]);
                        lists.setTnid( collageName[1]);
                        lists.setField_group_image(CollageImageurl);
                        lists.setLat(lati);
                        lists.setLng(longi);
                        Log.d("TAG", "lati: " + lati+longi);

                        SessionManager.getInstance(getActivity()).saveCollage(lists);

                        Utils.getSharedPreference(SelectCollage.this).edit()
                                .putBoolean(Constants.IS_COLLEGE_ADDED, true).apply();
                        Log.d("TAG", "list: " + lists+ collageName[0]+ collageName[1]);

                        Intent i = new Intent(SelectCollage.this,SelectYourType.class);
                        startActivity(i);
                        finish();

                    }else {
                        Toast.makeText(getBaseContext(), "please select correct college",
                                Toast.LENGTH_SHORT).show();
                    }

                    return true;
                }
                return false;
            }
        });


        ButtonSelected_collage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedText = Autoentercollage.getText().toString();
                if (collageList.contains(selectedText)){
                    String[] collageName = selectedText.split("-");
                    CollageLists lists = new CollageLists();
                    lists.setTitle(collageName[0]);
                    lists.setTnid( collageName[1]);
                    lists.setField_group_image(CollageImageurl);
                    lists.setLat(lati);
                    lists.setLng(longi);
                    SessionManager.getInstance(getActivity()).saveCollage(lists);

                    Utils.getSharedPreference(SelectCollage.this).edit()
                            .putBoolean(Constants.IS_COLLEGE_ADDED, true).apply();
                    Log.d("TAG", "list: " + lists+ collageName[0]+ collageName[1]);

                    Intent i = new Intent(SelectCollage.this,SelectYourType.class);
                    startActivity(i);
                    finish();

                }else {
                    Toast.makeText(getBaseContext(), "please select correct college",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onStart() {
        googleApiClient.connect();

        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        //Creating a location object
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            Log.d("TAG", "latlong: " + longitude+ " "+latitude);


            //moving the map to location
            moveMap();
        }
    }

    //Function to move the map
    private void moveMap() {
        //String to display current latitude and longitude
        String msg = latitude + ", "+longitude;
        Log.d("TAG", "msg: " + msg);


        //Creating a LatLng Object to store Coordinates
        LatLng latLng = new LatLng(latitude, longitude);

        //Adding marker to map
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
                .title("Current Location")); //Adding a title

        //Moving the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        //Animating the camera
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        //Displaying current coordinates in toast
      //  Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);
        getCurrentLocation();
        moveMap();

    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //Clearing all the markers
        mMap.clear();

        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;

        //Moving the map
        moveMap();
    }

    @Override
    public void onClick(View v) {

    }


    private class ProgressSearchOfCollageList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGELIST);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                SelectCollage.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progressSearchofcollagelist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");



                        for (int i=0; i<data.length();i++) {

                            JSONObject jsonObject = data.getJSONObject(i);
                            if (jsonObject.has("location")){
                                JSONObject locate = jsonObject.getJSONObject("location");
                                lati = locate.getString("lat");
                                longi = locate.getString("lng");


                            }


                            CollageLists GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), CollageLists.class);
                            collageListMain.add(GoalInfo);
                            collageList.add(GoalInfo.getTitle()+"-"+GoalInfo.getTnid());
                            CollageImageurl =(GoalInfo.getField_group_image());

                            Log.d("TAG", "CollageImageurl: " + CollageImageurl);





                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (SelectCollage.this, android.R.layout.select_dialog_item,  collageList);
                            Log.d("TAG", "arrPackage: " + collageList);
                            Autoentercollage.setThreshold(1);
                            Autoentercollage.setAdapter(adapter);





                            Autoentercollage.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                @Override
                                public void onItemClick(AdapterView<?> parent, View arg1, int pos,
                                                        long id) {

                                    hideKeyBoard(Autoentercollage);





                                }
                            });


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progressSearchofcollagelist = null;
            hideLoading();


        }
    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}


