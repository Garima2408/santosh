package co.savm.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import co.savm.R;
import co.savm.fragments.WelcomeScreenFragment;

public class AppIntroScreen extends AppCompatActivity  {
    static final int TOTAL_PAGES = 4;
    private static final String TAG = SignIn.class.getSimpleName();
    private static final int FIRST_PAGE = 0;

    ViewPager pager;
    PagerAdapter pagerAdapter;
    LinearLayout pageIndicator;
    Button btnSkip;
    Button btnDone;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_intro_screen);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        initbtnSkip();
        initbtnDone();

        initViewPager();

    }

    private void initViewPager() {
        pager = findViewById(R.id.pager);
        pagerAdapter = new ScreenSlideAdapter(getSupportFragmentManager());
        pager.setAdapter(pagerAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setIndicator(position);
                if (position == 3) {
                    btnSkip.setVisibility(View.GONE);
                    btnDone.setVisibility(View.VISIBLE);
                } else {
                    btnSkip.setVisibility(View.VISIBLE);
                    btnDone.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        buildCircles();


    }

    private void initbtnDone() {

        btnDone = Button.class.cast(findViewById(R.id.done));
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTutorial();
            }
        });
    }

    private void initbtnSkip() {
        btnSkip = Button.class.cast(findViewById(R.id.btn_skip));
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                endTutorial();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pager != null) {
            pager.clearOnPageChangeListeners();
        }
    }

    private void buildCircles() {
        pageIndicator = findViewById(R.id.circles);
        setIndicator(FIRST_PAGE);
    }

    private void setIndicator(int index) {

        for (int i = 0; i < TOTAL_PAGES; i++) {
            ImageView circle = (ImageView) pageIndicator.getChildAt(i);
            if (i == index) {

                circle.setImageResource(R.drawable.ic_white_dot);
                circle.setAlpha(1.0f);

            } else {

                circle.setImageResource(R.drawable.ic_dot_grey);
                circle.setAlpha(0.6f);

            }

        }
    }

    private void endTutorial() {

        Intent intent = new Intent(AppIntroScreen.this, SignIn.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.animator.introfadein, R.animator.introfadeout);
    }

    @Override
    public void onBackPressed() {
        if (pager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
        finish();
    }

    private class ScreenSlideAdapter extends FragmentStatePagerAdapter {

        public ScreenSlideAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            WelcomeScreenFragment welcomeScreenFragment = null;
            switch (position) {
                case 0:
                    welcomeScreenFragment = WelcomeScreenFragment.newInstance(R.layout.app_intro_one);
                    break;
                case 1:
                    welcomeScreenFragment = WelcomeScreenFragment.newInstance(R.layout.app_intro_two);
                    break;
                case 2:
                    welcomeScreenFragment = WelcomeScreenFragment.newInstance(R.layout.app_intro_three);
                    break;
                case 3:
                    welcomeScreenFragment = WelcomeScreenFragment.newInstance(R.layout.app_intro_four);
                    break;

            }

            return welcomeScreenFragment;
        }

        @Override
        public int getCount() {
            return TOTAL_PAGES;
        }
    }


}