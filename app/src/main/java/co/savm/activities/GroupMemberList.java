package co.savm.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.Event.CampusFeed;
import co.savm.R;
import co.savm.adapters.GroupMemberAdapter;
import co.savm.models.CourseMemberLists;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class GroupMemberList extends BaseAppCompactActivity {
    RecyclerView CourseMemberList;
    String Subject_Id;
    GroupMemberAdapter mGroupMemberAdapter;
    public ArrayList<CourseMemberLists> memberList;
    private GroupMemberListDisplay groupmemberList = null;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_member_list);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);

        Bundle b = getActivity().getIntent().getExtras();
        Subject_Id = b.getString("COURSE_ID");

        memberList=new ArrayList<>();

        layoutManager = new LinearLayoutManager(this);
        CourseMemberList = findViewById(co.savm.R.id.CourseMemberList);
        CourseMemberList.setHasFixedSize(true);
        CourseMemberList.setLayoutManager(layoutManager);

        if(Utils.isInternetConnected(GroupMemberList.this)){
            groupmemberList = new GroupMemberListDisplay();
            groupmemberList.execute(Subject_Id);

        }else {
            showAlertDialog(getString(R.string.error_responce_internet));

        }



    }


    private class GroupMemberListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMEMBERLISTS+"/"+args[0]+"/users?fields=id, etid, gid, state&parameters=state=1&page=0&pagesize=100");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                GroupMemberList.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.savm.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);



                            }
                            mGroupMemberAdapter = new GroupMemberAdapter(GroupMemberList.this, memberList);
                            CourseMemberList.setAdapter(mGroupMemberAdapter);

                        }else {


                        }





                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            hideLoading();


        }


    }
    @Override
    public void onBackPressed() {
        EventBus.getDefault().post(new CampusFeed(1));
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                EventBus.getDefault().post(new CampusFeed(1));
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
