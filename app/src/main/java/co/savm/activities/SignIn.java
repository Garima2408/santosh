package co.savm.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.alumni.AluminiMain;
import co.savm.driver.DriverDash;
import co.savm.models.AccessDeviceToken;
import co.savm.models.CollageLists;
import co.savm.models.CollageRoleArray;
import co.savm.models.MultipleLocn;
import co.savm.models.UserDetail;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.parent.ParentMain;
import co.savm.permissions.MarshmallowPermission;
import co.savm.teacher.TeacherMain;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import co.savm.visitor.VisitorMain;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class SignIn extends BaseAppCompactActivity implements View.OnClickListener {
    EditText editText_email;
    Button button_enter;
    String email,password,urlsofphoto,urlsofWallpic, MainRole,CollageId,CollageName,CollageImage,lati,longi,locationName,CollageMemberShipId,CollageBatch,CollageBranch,Collagedepartment,CollageRole,CollageLogo,CollageEmail,
            CollageDesignation,CollageEnrollment,CollageStudentName,ParentUserChildId,ParentUserChildImage,CollageAffiliation,CollageType;
    TextView tv_signup,tv_forgot_password;
    ImageView show_password;
    ShowHidePasswordEditText editText_password;
    boolean flag = true;
    private ProgressSignIn SignintAuthTask = null;
    public ArrayList<CollageLists> collageListMain;
    public ArrayList<String> collageList;
    ArrayList<MultipleLocn> multiple ;
    JSONArray locate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

             if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (!MarshmallowPermission.checkAllPermissions(SignIn.this)) {
                //  showToast("All Permission are required for app");
                MarshmallowPermission.checkAllPermissions(this);
            }
        }

        collageListMain = new ArrayList<>();
        collageList = new ArrayList<>();





        editText_email = findViewById(R.id.editText_email);
        editText_password = findViewById(R.id.editText_password);

        //editText_password = findViewById(R.id.editText_password);
        tv_signup = findViewById(R.id.tv_signup);
        tv_forgot_password = findViewById(R.id.tv_forgot_password);
        button_enter = findViewById(R.id.button_enter);
        // show_password =findViewById(R.id.show_password);
        button_enter.setOnClickListener(this);
        tv_signup.setOnClickListener(this);
        tv_forgot_password.setOnClickListener(this);
        // show_password.setOnClickListener(this);


    }





    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you want to close this application ?");
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finishAffinity();
            }
        });
        builder.setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert=builder.create();
        alert.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.button_enter:
                hideKeyBoard(v);

                attemptLogin();
                break;

            case R.id.tv_signup:
                hideKeyBoard(v);

                Intent i = new Intent(SignIn.this,SignUp.class);
                startActivity(i);
                break;

            case R.id.tv_forgot_password:
                hideKeyBoard(v);

                Intent j = new Intent(SignIn.this,ForgetsPassword.class);
                startActivity(j);
                break;


        }
    }

    private void attemptLogin() {
        // Reset errors.
        editText_email.setError(null);
        editText_password.setError(null);

        // Store values at the time of the login attempt.
        email = editText_email.getText().toString().trim();
        password = editText_password.getText().toString().trim();
        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            focusView = editText_email;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }/* else if (!Utils.isValidEmailAddress(email)) {
            focusView = editText_email;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_invalid_email));
            // Check for a valid password, if the user entered one.
        }*/ else if (TextUtils.isEmpty(password)) {
            focusView = editText_password;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            SignintAuthTask = new ProgressSignIn();
            SignintAuthTask.execute();



        }
    }


    private class ProgressSignIn extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("username", email)
                    .add("password", password)
                    .build();





            try {
                String responseData = ApiCall.POST(client, URLS.URL_LOGIN,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                SignIn.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_responce));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            SignintAuthTask = null;
            try {
                if (responce != null) {

                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putBoolean("isExit", true);
                        editor.commit();


                        JSONObject data = responce.getJSONObject("data");
                        String toke = data.getString("token");
                        AccessDeviceToken token = new AccessDeviceToken();
                        token.sessionID = data.getString("sessid");
                        token.sessionName = data.getString("session_name");
                        token.accesstoken = data.getString("token");
                        SessionManager.getInstance(getActivity()).saveAccesstoken(token);

                        JSONObject user = data.getJSONObject("user");
                        String userid = user.getString("uid");
                        String username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                        String mail = user.getString("mail");
                        JSONObject picture = user.getJSONObject("picture");
                        if (picture != null && picture.length() > 0) {
                            urlsofphoto = picture.getString("url");
                            Log.d("TAG", "urlsofphoto: " + urlsofphoto);

                        } else {

                        }


                        JSONArray Userwallpic = user.getJSONArray("field_wall_picture");
                        if (Userwallpic != null && Userwallpic.length() > 0) {
                            for (int j = 0; j < Userwallpic.length(); j++) {
                                Log.e("Mediapictire", Userwallpic.getString(j));

                                urlsofWallpic =Userwallpic.getString(j);
                            }




                        }



                        JSONArray CampusData = user.getJSONArray("colleges");


                        if (CampusData != null && CampusData.length() > 0) {


                            MainRole = CampusData.getJSONObject(0).getString("role");


                            for (int i=0; i<CampusData.length();i++) {
                                try{

                                    locate = CampusData.getJSONObject(0).getJSONArray("field_grp_location");



                                    if (locate != null && locate.length() > 0 ){
                                        multiple = new ArrayList<>();
                                        for (int j = 0; j < locate.length(); j++) {

                                            lati = locate.getJSONObject(j).getString("lat");
                                            longi = locate.getJSONObject(j).getString("lng");
                                            locationName = locate.getJSONObject(j).getString("name");

                                            MultipleLocn  lon = new MultipleLocn();

                                            lon.lat =lati;
                                            lon.lng =longi;
                                            lon.name =locationName;
                                            multiple.add(lon);


                                        }




                                    }

                                    JSONObject jsonObject = CampusData.getJSONObject(0);

                                    JSONObject UserChild = jsonObject.getJSONObject("child_student");
                                    if (UserChild != null && UserChild.length() > 0) {

                                        ParentUserChildId = UserChild.getString("uid");
                                        ParentUserChildImage = UserChild.getString("picture");
                                        Log.e("UserChild", ParentUserChildId+".."+ParentUserChildImage);

                                    }

                                    /* JSONArray UserChild = jsonObject.getJSONArray("child_student");
                                    if (UserChild != null && UserChild.length() > 0) {
                                        for (int j = 0; j < UserChild.length(); j++) {

                                            ParentUserChildId =UserChild.getString(0);
                                            ParentUserChildImage =UserChild.getString(1);

                                            Log.e("UserChild", ParentUserChildId+".."+ParentUserChildImage);

                                        }*/


                                }catch (JSONException e){
                                    e.printStackTrace();
                                }



                            }









                            if (MainRole.matches("13") ) {

                                CollageId = CampusData.getJSONObject(0).getString("tnid");
                                CollageName = CampusData.getJSONObject(0).getString("title");
                                CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                CollageBranch = CampusData.getJSONObject(0).getString("field_branch");
                                CollageEnrollment  = CampusData.getJSONObject(0).getString("field_enrollment_number");
                                CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                CollageType = CampusData.getJSONObject(0).getString("type");
                                Log.d("TAG", "recource: " + MainRole + CollageId + CollageName + CollageImage +"-"+lati+"-"+longi);


                                CollageRoleArray userRoleDetail = new CollageRoleArray();
                                userRoleDetail.role = MainRole;
                                userRoleDetail.tnid = CollageId;
                                userRoleDetail.title = CollageName;
                                userRoleDetail.field_group_image = CollageImage;
                                userRoleDetail.CollageLogo =CollageLogo;
                                userRoleDetail.CollageRole =CollageRole;
                                userRoleDetail.CollageEmail =CollageEmail;
                                userRoleDetail.CollageBatch =CollageBatch;
                                userRoleDetail.CollageBranch =CollageBranch;
                                userRoleDetail.CollageEnrollment =CollageEnrollment;


                                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                CollageLists lists = new CollageLists();
                                lists.setTitle(CollageName);
                                lists.setTnid(CollageId);
                                lists.setField_group_image(CollageImage);
                                lists.setField_groups_logo(CollageLogo);
                                lists.setCollageMemberShipId(CollageMemberShipId);
                                lists.setMultiple(multiple);
                                lists.setLat(lati);
                                lists.setLng(longi);
                                lists.setAffiliation(CollageAffiliation);
                                lists.setType(CollageType);
                                SessionManager.getInstance(getActivity()).saveCollage(lists);

                                UserDetail userDetail = new UserDetail();
                                userDetail.userprofile_id = user.getString("uid");
                                userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                userDetail.email = user.getString("mail");
                                userDetail.FirstName = user.getString("field_first_name");
                                userDetail.LastName = user.getString("field_last_name");
                                userDetail.photo = urlsofphoto;
                                userDetail.Userwallpic = urlsofWallpic;

                                SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                Log.d("TAG", "only name: " + username);



                                Utils.setStringPreferences(SignIn.this, "login", "0");

                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putInt(Constants.USER_ROLE, Constants.ROLE_STUDENT).apply();
                                Intent upanel = new Intent(SignIn.this, MainActivity.class);
                                startActivity(upanel);
                                finish();



                            } else if (MainRole.matches("14")) {


                                CollageId = CampusData.getJSONObject(0).getString("tnid");
                                CollageName = CampusData.getJSONObject(0).getString("title");
                                CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                CollageDesignation = CampusData.getJSONObject(0).getString("field_faculty_designation");
                                Collagedepartment = CampusData.getJSONObject(0).getString("field_department");
                                CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                CollageType = CampusData.getJSONObject(0).getString("type");





                                CollageRoleArray userRoleDetail = new CollageRoleArray();
                                userRoleDetail.role = MainRole;
                                userRoleDetail.tnid = CollageId;
                                userRoleDetail.title = CollageName;
                                userRoleDetail.field_group_image = CollageImage;
                                userRoleDetail.CollageLogo = CollageLogo;
                                userRoleDetail.CollageRole = CollageRole;
                                userRoleDetail.CollageEmail = CollageEmail;
                                userRoleDetail.CollageDesignation = CollageDesignation;
                                userRoleDetail.Collagedepartment = Collagedepartment;



                                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                CollageLists lists = new CollageLists();
                                lists.setTitle(CollageName);
                                lists.setTnid(CollageId);
                                lists.setField_group_image(CollageImage);
                                lists.setField_groups_logo(CollageLogo);
                                lists.setCollageMemberShipId(CollageMemberShipId);
                                lists.setLat(lati);
                                lists.setLng(longi);
                                lists.setMultiple(multiple);
                                lists.setAffiliation(CollageAffiliation);
                                lists.setType(CollageType);
                                SessionManager.getInstance(getActivity()).saveCollage(lists);
                                UserDetail userDetail = new UserDetail();
                                userDetail.userprofile_id = user.getString("uid");
                                userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                userDetail.email = user.getString("mail");
                                userDetail.FirstName = user.getString("field_first_name");
                                userDetail.LastName = user.getString("field_last_name");
                                userDetail.photo = urlsofphoto;
                                userDetail.Userwallpic = urlsofWallpic;
                                //  userDetail.colleges = user.getJSONArray("colleges");
                                Log.d("TAG", "only name: " + username);
                                SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                Utils.setStringPreferences(SignIn.this, "login", "0");
                                Intent upanel = new Intent(SignIn.this, TeacherMain.class);
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putInt(Constants.USER_ROLE, Constants.ROLE_FACULTY).apply();
                                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(upanel);
                                finish();




                            } else if (MainRole.matches("15")) {

                                CollageId = CampusData.getJSONObject(0).getString("tnid");
                                CollageName = CampusData.getJSONObject(0).getString("title");
                                CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                //  CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                CollageBranch = CampusData.getJSONObject(0).getString("field_branch");
                                CollageEnrollment  = CampusData.getJSONObject(0).getString("field_enrollment_number");
                                CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                CollageType = CampusData.getJSONObject(0).getString("type");




                                CollageRoleArray userRoleDetail = new CollageRoleArray();
                                userRoleDetail.role = MainRole;
                                userRoleDetail.tnid = CollageId;
                                userRoleDetail.title = CollageName;
                                userRoleDetail.field_group_image = CollageImage;
                                userRoleDetail.CollageLogo = CollageLogo;
                                userRoleDetail.CollageRole = CollageRole;
                                userRoleDetail.CollageBranch = CollageBranch;
                                userRoleDetail.CollageBatch =CollageBatch;
                                //  userRoleDetail.CollageEmail =CollageEmail;
                                userRoleDetail.CollageEnrollment =CollageEnrollment;


                                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                CollageLists lists = new CollageLists();
                                lists.setTitle(CollageName);
                                lists.setTnid(CollageId);
                                lists.setField_group_image(CollageImage);
                                lists.setField_groups_logo(CollageLogo);
                                lists.setLat(lati);
                                lists.setLng(longi);
                                lists.setMultiple(multiple);
                                lists.setAffiliation(CollageAffiliation);
                                lists.setCollageMemberShipId(CollageMemberShipId);
                                lists.setType(CollageType);
                                SessionManager.getInstance(getActivity()).saveCollage(lists);
                                UserDetail userDetail = new UserDetail();
                                userDetail.userprofile_id = user.getString("uid");
                                userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                userDetail.email = user.getString("mail");
                                userDetail.FirstName = user.getString("field_first_name");
                                userDetail.LastName = user.getString("field_last_name");
                                userDetail.photo = urlsofphoto;
                                userDetail.Userwallpic = urlsofWallpic;
                                //  userDetail.colleges = user.getJSONArray("colleges");
                                Log.d("TAG", "only name: " + username);
                                SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                Utils.setStringPreferences(SignIn.this, "login", "0");
                                Intent upanel = new Intent(SignIn.this, AluminiMain.class);
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putInt(Constants.USER_ROLE, Constants.ROLE_ALUMNI).apply();

                                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(upanel);
                                finish();



                            } else if (MainRole.matches("16")) {




                                CollageId = CampusData.getJSONObject(0).getString("tnid");
                                CollageName = CampusData.getJSONObject(0).getString("title");
                                CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                CollageStudentName = CampusData.getJSONObject(0).getString("field_student_name");
                                CollageEnrollment  = CampusData.getJSONObject(0).getString("field_enrollment_number");
                                CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                CollageType = CampusData.getJSONObject(0).getString("type");



                                CollageRoleArray userRoleDetail = new CollageRoleArray();
                                userRoleDetail.role = MainRole;
                                userRoleDetail.tnid = CollageId;
                                userRoleDetail.title = CollageName;
                                userRoleDetail.field_group_image = CollageImage;
                                userRoleDetail.CollageLogo = CollageLogo;
                                userRoleDetail.CollageRole = CollageRole;
                                userRoleDetail.CollageBatch = CollageBatch;
                                userRoleDetail.CollageStudentNamev = CollageStudentName;
                                userRoleDetail.CollageEnrollment = CollageEnrollment;



                                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                CollageLists lists = new CollageLists();
                                lists.setTitle(CollageName);
                                lists.setTnid(CollageId);
                                lists.setField_group_image(CollageImage);
                                lists.setField_groups_logo(CollageLogo);
                                lists.setLat(lati);
                                lists.setLng(longi);
                                lists.setMultiple(multiple);
                                lists.setAffiliation(CollageAffiliation);
                                lists.setCollageMemberShipId(CollageMemberShipId);
                                lists.setType(CollageType);
                                SessionManager.getInstance(getActivity()).saveCollage(lists);
                                UserDetail userDetail = new UserDetail();
                                userDetail.userprofile_id = ParentUserChildId;
                                userDetail.ParentUid = user.getString("uid");
                                userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                userDetail.email = user.getString("mail");
                                userDetail.FirstName = user.getString("field_first_name");
                                userDetail.LastName = user.getString("field_last_name");
                                userDetail.photo = urlsofphoto;
                                UserDetail.childImage =ParentUserChildImage;
                                userDetail.Userwallpic = urlsofWallpic;
                                //  userDetail.colleges = user.getJSONArray("colleges");
                                Log.d("TAG", "onlynameParentUserChildId " + ParentUserChildId+".."+user.getString("uid"));
                                SessionManager.getInstance(getActivity()).saveUser(userDetail);
                                Utils.setStringPreferences(SignIn.this, "login", "0");

                                Intent upanel = new Intent(SignIn.this, ParentMain.class);
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putInt(Constants.USER_ROLE, Constants.ROLE_PARENT).apply();

                                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(upanel);
                                finish();


                            } else if (MainRole.matches("2")) {
                                CollageRoleArray userRoleDetail = new CollageRoleArray();
                                userRoleDetail.role = MainRole;
                                userRoleDetail.tnid = CollageId;
                                userRoleDetail.title = CollageName;
                                userRoleDetail.field_group_image = CollageImage;
                                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                CollageLists lists = new CollageLists();
                                lists.setTitle(CollageName);
                                lists.setTnid(CollageId);
                                lists.setField_group_image(CollageImage);
                                lists.setField_groups_logo(CollageLogo);
                                lists.setLat(lati);
                                lists.setLng(longi);
                                lists.setMultiple(multiple);
                                lists.setAffiliation(CollageAffiliation);
                                lists.setType(CollageType);
                                SessionManager.getInstance(getActivity()).saveCollage(lists);
                                UserDetail userDetail = new UserDetail();
                                userDetail.userprofile_id = user.getString("uid");
                                userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                userDetail.email = user.getString("mail");
                                userDetail.FirstName = user.getString("field_first_name");
                                userDetail.LastName = user.getString("field_last_name");
                                userDetail.photo = urlsofphoto;
                                userDetail.Userwallpic = urlsofWallpic;
                                //  userDetail.colleges = user.getJSONArray("colleges");
                                Log.d("TAG", "only name: " + username);
                                SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                Utils.setStringPreferences(SignIn.this, "login", "0");
                                Intent upanel = new Intent(SignIn.this, VisitorMain.class);
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putInt(Constants.USER_ROLE, Constants.ROLE_VISITOR).apply();

                                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(upanel);
                                finish();



                            } else if (MainRole.matches("")) {
                                CollageRoleArray userRoleDetail = new CollageRoleArray();
                                userRoleDetail.role = MainRole;
                                userRoleDetail.tnid = CollageId;
                                userRoleDetail.title = CollageName;
                                userRoleDetail.field_group_image = CollageImage;
                                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                CollageLists lists = new CollageLists();
                                lists.setTitle(CollageName);
                                lists.setTnid(CollageId);
                                lists.setField_group_image(CollageImage);
                                lists.setField_groups_logo(CollageLogo);
                                lists.setLat(lati);
                                lists.setLng(longi);
                                lists.setMultiple(multiple);
                                lists.setAffiliation(CollageAffiliation);
                                lists.setType(CollageType);
                                SessionManager.getInstance(getActivity()).saveCollage(lists);
                                UserDetail userDetail = new UserDetail();
                                userDetail.userprofile_id = user.getString("uid");
                                userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                userDetail.email = user.getString("mail");
                                userDetail.FirstName = user.getString("field_first_name");
                                userDetail.LastName = user.getString("field_last_name");
                                userDetail.photo = urlsofphoto;
                                userDetail.Userwallpic = urlsofWallpic;
                                //  userDetail.colleges = user.getJSONArray("colleges");
                                Log.d("TAG", "only name: " + username);
                                SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                Utils.setStringPreferences(SignIn.this, "login", "0");
                                Intent upanel = new Intent(SignIn.this, VisitorMain.class);
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putInt(Constants.USER_ROLE, Constants.ROLE_VISITOR).apply();

                                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(upanel);
                                finish();

                              /*  AlertDialog.Builder builder = new AlertDialog.Builder(SignIn.this);
                                builder.setTitle("Successful login");
                                builder.setMessage(msg)
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {

                                                Utils.setStringPreferences(SignIn.this, "login", "0");
                                                Intent upanel = new Intent(SignIn.this, VisitorMain.class);
                                                Utils.getSharedPreference(SignIn.this).edit()
                                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                                Utils.getSharedPreference(SignIn.this).edit()
                                                        .putInt(Constants.USER_ROLE, Constants.ROLE_VISITOR).apply();

                                                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(upanel);
                                                dialog.dismiss();
                                                finish();

                                            }
                                        });
                                AlertDialog alert = builder.create();
                                alert.show();*/


                            }
                            else if (MainRole.matches("3")) {

                                CollageId = CampusData.getJSONObject(0).getString("tnid");
                                CollageName = CampusData.getJSONObject(0).getString("title");
                                CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                                CollageRole = CampusData.getJSONObject(0).getString("field_i_am_a");
                                CollageBatch = CampusData.getJSONObject(0).getString("field_batch_year");
                                CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                CollageDesignation = CampusData.getJSONObject(0).getString("field_faculty_designation");
                                Collagedepartment = CampusData.getJSONObject(0).getString("field_department");
                                CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                CollageType = CampusData.getJSONObject(0).getString("type");





                                CollageRoleArray userRoleDetail = new CollageRoleArray();
                                userRoleDetail.role = MainRole;
                                userRoleDetail.tnid = CollageId;
                                userRoleDetail.title = CollageName;
                                userRoleDetail.field_group_image = CollageImage;
                                userRoleDetail.CollageLogo = CollageLogo;
                                userRoleDetail.CollageRole = CollageRole;
                                userRoleDetail.CollageEmail = CollageEmail;
                                userRoleDetail.CollageDesignation = CollageDesignation;
                                userRoleDetail.Collagedepartment = Collagedepartment;




                                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                CollageLists lists = new CollageLists();
                                lists.setTitle(CollageName);
                                lists.setTnid(CollageId);
                                lists.setField_group_image(CollageImage);
                                lists.setField_groups_logo(CollageLogo);
                                lists.setLat(lati);
                                lists.setLng(longi);
                                lists.setMultiple(multiple);
                                lists.setAffiliation(CollageAffiliation);
                                lists.setCollageMemberShipId(CollageMemberShipId);
                                lists.setType(CollageType);
                                SessionManager.getInstance(getActivity()).saveCollage(lists);
                                UserDetail userDetail = new UserDetail();
                                userDetail.userprofile_id = user.getString("uid");
                                userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                userDetail.email = user.getString("mail");
                                userDetail.FirstName = user.getString("field_first_name");
                                userDetail.LastName = user.getString("field_last_name");
                                userDetail.photo = urlsofphoto;
                                userDetail.Userwallpic = urlsofWallpic;
                                //  userDetail.colleges = user.getJSONArray("colleges");
                                Log.d("TAG", "only name: " + username);
                                SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                Utils.setStringPreferences(SignIn.this, "login", "0");
                                Intent upanel = new Intent(SignIn.this, TeacherMain.class);
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putInt(Constants.USER_ROLE, Constants.ROLE_FACULTY).apply();
                                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(upanel);
                                finish();


                            }else if(MainRole.matches("27")){


                                CollageId = CampusData.getJSONObject(0).getString("tnid");
                                CollageName = CampusData.getJSONObject(0).getString("title");
                                CollageImage = CampusData.getJSONObject(0).getString("field_group_image");
                                CollageLogo  = CampusData.getJSONObject(0).getString("field_groups_logo");
                               // CollageEmail = CampusData.getJSONObject(0).getString("field_college_email");
                                CollageMemberShipId= CampusData.getJSONObject(0).getString("membership_id");
                                CollageAffiliation = CampusData.getJSONObject(0).getString("affiliation");
                                CollageType = CampusData.getJSONObject(0).getString("type");


                                CollageRoleArray userRoleDetail = new CollageRoleArray();
                                userRoleDetail.role = MainRole;
                                userRoleDetail.tnid = CollageId;
                                userRoleDetail.title = CollageName;
                                userRoleDetail.field_group_image = CollageImage;
                                userRoleDetail.CollageLogo = CollageLogo;

                                SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);
                                CollageLists lists = new CollageLists();
                                lists.setTitle(CollageName);
                                lists.setTnid(CollageId);
                                lists.setField_group_image(CollageImage);
                                lists.setField_groups_logo(CollageLogo);
                                lists.setLat(lati);
                                lists.setLng(longi);
                                lists.setMultiple(multiple);
                                lists.setAffiliation(CollageAffiliation);
                                lists.setCollageMemberShipId(CollageMemberShipId);
                                lists.setType(CollageType);
                                SessionManager.getInstance(getActivity()).saveCollage(lists);
                                UserDetail userDetail = new UserDetail();
                                userDetail.userprofile_id = user.getString("uid");
                                userDetail.username = user.getString("field_first_name") + " " + user.getString("field_last_name");
                                userDetail.email = user.getString("mail");
                                userDetail.FirstName = user.getString("field_first_name");
                                userDetail.LastName = user.getString("field_last_name");
                                userDetail.photo = urlsofphoto;
                                userDetail.Userwallpic = urlsofWallpic;
                                //  userDetail.colleges = user.getJSONArray("colleges");
                                Log.d("TAG", "only name: " + username);
                                SessionManager.getInstance(getActivity()).saveUser(userDetail);

                                Utils.setStringPreferences(SignIn.this, "login", "0");
                                Intent upanel = new Intent(SignIn.this, DriverDash.class);
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                Utils.getSharedPreference(SignIn.this).edit()
                                        .putInt(Constants.USER_ROLE, Constants.ROLE_DRIVER).apply();
                                upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(upanel);
                                finish();



                            }







                        }

                        else {

                            UserDetail userDetail = new UserDetail();
                            userDetail.userprofile_id = user.getString("uid");
                            userDetail.username = user.getString("field_first_name")+" "+user.getString("field_last_name");
                            userDetail.email = user.getString("mail");
                            userDetail.FirstName = user.getString("field_first_name");
                            userDetail.LastName = user.getString("field_last_name");
                            userDetail.photo = urlsofphoto;
                            userDetail.Userwallpic = urlsofWallpic;
                            //  userDetail.colleges = user.getJSONArray("colleges");
                            Log.d("TAG", "only name: " + username);
                            SessionManager.getInstance(getActivity()).saveUser(userDetail);

                            Utils.setStringPreferences(SignIn.this,"login","0");
                            Intent upanel = new Intent(SignIn.this, SearchYourCollage.class);
                            Utils.getSharedPreference(SignIn.this).edit()
                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(upanel);
                            // finish();


                           /* AlertDialog.Builder builder = new AlertDialog.Builder(SignIn.this);
                            builder.setTitle("Successful login");
                            builder.setMessage(msg)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Utils.setStringPreferences(SignIn.this,"login","0");
                                            Intent upanel = new Intent(SignIn.this, SearchYourCollage.class);
                                            Utils.getSharedPreference(SignIn.this).edit()
                                                    .putBoolean(Constants.IS_LOGGED_IN, true).apply();
                                            upanel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(upanel);
                                            dialog.dismiss();
                                            finish();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();
*/


                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {



                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
                showAlertDialog(getString(R.string.error_responce));
                hideLoading();

            }
        }



        @Override
        protected void onCancelled() {
            showAlertDialog(getString(R.string.error_responce));
            SignintAuthTask = null;
            hideLoading();


        }
    }

}










