package co.savm.tracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import co.savm.Event.JoinRoute;
import co.savm.R;

public class TransportDetailsStudent extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    String Route_id,RouteTittle,Join_Value,Adminuser,AdminCreater,sponsered,transportImages;
    private Bundle bundle;
    TextView toolbar_title;
    ImageView backone;
    int viewPos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studenttransport_details);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();


        if (extras.containsKey("IS_STUDENT")) {

            if (extras.getString("IS_STUDENT").equals("TRUE")) {
                Route_id = intent.getStringExtra("ROUTE_ID");
                RouteTittle = intent.getStringExtra("ROUTE_NAME");
                sponsered=intent.getStringExtra("sponsered");
                transportImages = intent.getStringExtra("TRANSPORT_IMAGE");

                Adminuser = "TRUE";


            } else if (extras.getString("IS_STUDENT").equals("FALSE")) {

                Route_id = intent.getStringExtra("ROUTE_ID");
                RouteTittle = intent.getStringExtra("ROUTE_NAME");
                sponsered=intent.getStringExtra("sponsered");
                transportImages = intent.getStringExtra("TRANSPORT_IMAGE");
                Adminuser = "FALSE";
            }
        }


        if (extras.containsKey("IS_CREATER")) {

            if (extras.getString("IS_CREATER").equals("TRUE")) {

                AdminCreater = "TRUE";


            } else if (extras.getString("IS_CREATER").equals("FALSE")) {


                AdminCreater = "FALSE";
            }
        }




        if (extras.containsKey("IS_MEMBER")) {

            if (extras.getString("IS_MEMBER").equals("member")) {

                Join_Value = "member";


            } else if (extras.getString("IS_MEMBER").equals("pending")) {


                Join_Value = "pending";
            }
            else if (extras.getString("IS_MEMBER").equals("nomember")) {


                Join_Value = "nomember";
            }
        }





        actionBar.setTitle(RouteTittle);
        viewPager = findViewById(R.id.viewpager);


        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupViewPager(viewPager);
        viewPager.setOffscreenPageLimit(6);


    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        bundle = new Bundle();
        bundle.putString("ROUTE_ID", Route_id);
        bundle.putString("ROUTE_NAME", RouteTittle);
        bundle.putString("IS_FACULTY", Adminuser);
        bundle.putString("IS_MEMBER", Join_Value);
        bundle.putString("IS_CREATER", AdminCreater);
        bundle.putString("sponsered",sponsered);




        RouteInfo info = new RouteInfo();
        info.setArguments(bundle);


        RideTrackDialogFragment map = new RideTrackDialogFragment();//Get Fragment Instance
        map.setArguments(bundle);

        RouteGroup group = new RouteGroup();
        group.setArguments(bundle);


        RouteMedia media = new RouteMedia();
        media.setArguments(bundle);

        StudentMyBusAttandance list = new StudentMyBusAttandance();
        list.setArguments(bundle);


        SelectPickupDialogFragment selectPickupDialogFragment=new SelectPickupDialogFragment();
        selectPickupDialogFragment.setArguments(bundle);




        adapter.addFragment(info, "Info");
        adapter.addFragment(group, "Group");
        adapter.addFragment(map, "Map");
        adapter.addFragment(media, "Media");
        adapter.addFragment(list, "Attendance");
        adapter.addFragment(selectPickupDialogFragment, "My Way");

        viewPager.setAdapter(adapter);


    }





    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }





    @Override
    public void onBackPressed() {
        finish();

    }

    @Override
    protected void onResume() {
        super.onResume();


        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        //FirstOneThis();


       /* tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(1);
*/


        Log.e("onResumecapus","app");
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(JoinRoute event) {
        /* Do something */
        Log.e("pos",""+event.getIndex());
        viewPos=event.getIndex();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.blank_menu, menu);//Menu Resource, Menu
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
