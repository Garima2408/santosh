package co.savm.tracker;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.RoutelistAdapter;
import co.savm.models.tracker.RoutelistArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class AllRoutesDisplay extends BaseAppCompactActivity {


    public static ArrayList<RoutelistArray> mrouteList;
    private RecyclerView.LayoutManager layoutManager;
    private static RoutelistAdapter routeadapter;
    static RecyclerView rv_Route_list;
    static SwipeRefreshLayout swipeContainer;
    static TextView ErrorText;
    static Activity activity;
    static ImageView ReloadProgress;
    private static CollageRoutesDisplay collageroutedisplay = null;
    private static ShimmerFrameLayout mShimmerViewContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_routes_display);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);


        activity = getActivity();
        mrouteList=new ArrayList<>();
        routeadapter=new RoutelistAdapter(this,mrouteList);
        ErrorText =findViewById(R.id.ErrorText);
        ReloadProgress =findViewById(R.id.ReloadProgress);
        layoutManager = new LinearLayoutManager(this);
        swipeContainer =findViewById(R.id.swipeContainer);
        swipeContainer.setColorSchemeColors(getResources().getColor(R.color.questin_dark),getResources().getColor(R.color.questin_base_light), getResources().getColor(R.color.questin_dark));

        mShimmerViewContainer = findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();

        rv_Route_list = findViewById(co.savm.R.id.rv_Route_list);
        rv_Route_list.setHasFixedSize(true);
        rv_Route_list.setLayoutManager(layoutManager);
        rv_Route_list.setAdapter(routeadapter);

        collageroutedisplay = new CollageRoutesDisplay();
        collageroutedisplay.execute();

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        swipeContainer.setRefreshing(true);
                        mrouteList.clear();
                        collageroutedisplay = new CollageRoutesDisplay();
                        collageroutedisplay.execute();

                    }
                }, 3000);
            }
        });

        ReloadProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CallFromRouteGroup();
            }
        });



    }

    public static void CallFromRouteGroup() {
        mrouteList.clear();
        collageroutedisplay = new CollageRoutesDisplay();
        collageroutedisplay.execute();



    }



    @Override
    protected void onPause() {
        super.onPause();

        mShimmerViewContainer.stopShimmerAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mShimmerViewContainer.startShimmerAnimation();
        swipeContainer.setRefreshing(false);

    }

    private static class CollageRoutesDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swipeContainer.setRefreshing(true);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.ROUTES_LIST+"?"+"cid="+ SessionManager.getInstance(activity).getCollage().getTnid());
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            collageroutedisplay = null;
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    ReloadProgress.setVisibility(View.GONE);

                    mrouteList.clear();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                RoutelistArray RouteInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), RoutelistArray.class);
                                mrouteList.add(RouteInfo);
                                routeadapter = new RoutelistAdapter(activity, mrouteList);
                                rv_Route_list.setAdapter(routeadapter);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            }
                        }else {
                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Route is added");

                            mShimmerViewContainer.stopShimmerAnimation();
                            mShimmerViewContainer.setVisibility(View.GONE);
                        }

                        } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        ReloadProgress.setVisibility(View.VISIBLE);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);


                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                ReloadProgress.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            collageroutedisplay = null;
            swipeContainer.setRefreshing(false);


        }
    }
    @Override
    public void onBackPressed() {
        finish();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.blank_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                finish();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }


}






