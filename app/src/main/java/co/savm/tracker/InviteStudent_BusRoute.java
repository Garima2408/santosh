package co.savm.tracker;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.AddStudentInRouteAdapter;
import co.savm.adapters.StudentRoutememberAdapter;
import co.savm.models.CollageStudentArray;
import co.savm.models.CourseMemberLists;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class InviteStudent_BusRoute extends BaseFragment {
    static Activity activity;
    static String Route_id,RouteTittle,Adminuser,Join_Value,AdminCreater;
    String SearchedName;
    static TextView ErrorText;
    static EditText searchEmail;
    private static ProgressBar spinner;
    private SendInviteStudentDisplay sendinvitelist = null;
    public static ArrayList<CollageStudentArray> mSearchList;
    private AddStudentInRouteAdapter searchedadapter;
    RecyclerView SearchedStudentList;
    static RecyclerView CourseMemberList;
    private RecyclerView.LayoutManager layoutManager ,mlayoutManager;
    static StudentRoutememberAdapter mGroupMemberAdapter;
    public static ArrayList<CourseMemberLists> memberList;
    private static GroupMemberListDisplay groupmemberList = null;
    RelativeLayout MainLayout;
    public InviteStudent_BusRoute() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_invite_student__bus_route, container, false);
        activity = (Activity) view.getContext();
        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle = getArguments().getString("ROUTE_NAME");

        Adminuser = getArguments().getString("IS_FACULTY");
        Join_Value = getArguments().getString("IS_MEMBER");
        AdminCreater = getArguments().getString("IS_CREATER");





        ErrorText =view.findViewById(R.id.ErrorText);
        searchEmail = view.findViewById(R.id.searchEmail);
        spinner= view.findViewById(R.id.progressBar);
        layoutManager = new LinearLayoutManager(getContext());
        mlayoutManager  = new LinearLayoutManager(getContext());
        SearchedStudentList = view.findViewById(R.id.SearchedStudentList);
        SearchedStudentList.setHasFixedSize(true);
        SearchedStudentList.setLayoutManager(layoutManager);
        CourseMemberList = view.findViewById(R.id.CourseMemberList);
        CourseMemberList.setHasFixedSize(true);
        CourseMemberList.setLayoutManager(mlayoutManager);
        MainLayout = view.findViewById(R.id.MainLayout);

        memberList=new ArrayList<>();

        if (Adminuser.equals("TRUE") && AdminCreater.equals("TRUE")) {


            searchEmail.setVisibility(View.VISIBLE);
            CourseMemberList.setVisibility(View.VISIBLE);
            memberList.clear();
            groupmemberList = new GroupMemberListDisplay();
            groupmemberList.execute(Route_id);




        } else  {

            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");

        }




        searchEmail.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER) {

                    addnewStudentinsubject();


                    return true;
                }
                return false;
            }
        });






        return view;
    }

    public static void hideKeyboard(Context mContext) {
        InputMethodManager imm = (InputMethodManager) mContext
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(((Activity) mContext).getWindow()
                .getCurrentFocus().getWindowToken(), 0);
    }

    private void addnewStudentinsubject() {

        searchEmail.setError(null);

        // Store values at the time of the login attempt.
        SearchedName = searchEmail.getText().toString().trim();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(SearchedName)) {
            focusView = searchEmail;
            cancel = true;
            //showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();

        } else {
            /* ADD COMMENTS ON SUBJECTS*/
            sendinvitelist = new SendInviteStudentDisplay();
            sendinvitelist.execute(SearchedName);

        }
    }






    private class SendInviteStudentDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {

                String responseData = ApiCall.GETHEADER(client, URLS.URL_ADDSTUDENTINSUBJECTBYEMAIL+"?"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&"+"search="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            sendinvitelist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        mSearchList = new ArrayList<>();

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CollageStudentArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CollageStudentArray.class);
                                mSearchList.add(CourseInfo);

                                searchedadapter = new AddStudentInRouteAdapter(activity, mSearchList, Route_id);
                                SearchedStudentList.setAdapter(searchedadapter);
                                SearchedStudentList.setVisibility(View.VISIBLE);
                                spinner.setVisibility(View.INVISIBLE);
                                CourseMemberList.setVisibility(View.GONE);
                                ErrorText.setVisibility(View.INVISIBLE);


                            }





                        }else {
                            spinner.setVisibility(View.INVISIBLE);
                            CourseMemberList.setVisibility(View.GONE);
                            ErrorText.setText(R.string.noStudent);
                            ErrorText.setVisibility(View.VISIBLE);
                            hideKeyboard(activity);
                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.showAlertFragmentDialog(activity, "Error", msg);
                        spinner.setVisibility(View.INVISIBLE);



                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            sendinvitelist = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }




    public static void RefreshedThepage() {
        searchEmail.clearFocus();
        searchEmail.getText().clear();
        mSearchList.clear();
        memberList.clear();
        hideKeyboard(activity);
        CourseMemberList.setVisibility(View.VISIBLE);
        groupmemberList = new GroupMemberListDisplay();
        groupmemberList.execute(Route_id);
        ErrorText.setVisibility(View.GONE);


    }


    public static void  RefreshedThepagetheAdapter(){
        memberList.clear();
        groupmemberList = new GroupMemberListDisplay();
        groupmemberList.execute(Route_id);


    }



    private static class GroupMemberListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEMEMBERLIST+"/"+args[0]+"/users?fields=id, etid, gid&parameters=state=1");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {

                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);



                            }
                            mGroupMemberAdapter = new StudentRoutememberAdapter(activity, memberList,Route_id);
                            CourseMemberList.setAdapter(mGroupMemberAdapter);

                        }else {


                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                    Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            spinner.setVisibility(View.INVISIBLE);



        }
    }




}
