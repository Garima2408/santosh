package co.savm.tracker;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.AdapterSelectPickDisplay;
import co.savm.models.routedetailResponse.Data;
import co.savm.models.routedetailResponse.RouteDetailResponseModel;
import co.savm.models.tracker.WayPointsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;

/**
 * Created by HP on 5/31/2018.
 */

public class SelectPickupDialogFragment extends BaseFragment {

    Dialog dialog ;
    RecyclerView rec_select_pic;
    private String routeId;
    private String routeName,sponsered,Adminuser,Join_Value,  pickLocation,dropLocation;
    AdapterSelectPickDisplay adapterSelectPick;
    private LinearLayout ll_cancel,llpickdrop;
    TextView ll_update;
    public static int pickPos,dropPos;
    private RouteDetailResponseModel routeDetailResponseModel;
    private Gson gson;
    private ArrayList<WayPointsArray> wayPointsArrayList;
    private ProgressBar progressBar;
    private boolean isChangePickDrop=false;
    TextView ErrorText;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dailog_selectpick, container, false);

        Bundle bundle= getArguments();
        if(bundle!=null){


            Bundle argsBundle=getArguments();

            routeId = argsBundle.getString("ROUTE_ID");
            routeName = argsBundle.getString("ROUTE_NAME");
            sponsered=argsBundle.getString("sponsered");
            Adminuser = getArguments().getString("IS_FACULTY");
            Join_Value = getArguments().getString("IS_MEMBER");


            }
        rec_select_pic=rootView.findViewById(R.id.rec_select_pic);
        ll_cancel=rootView.findViewById(R.id.ll_cancel);
        ll_update=rootView.findViewById(R.id.ll_update);
        ll_update.setVisibility(View.GONE);
        ErrorText = rootView.findViewById(R.id.ErrorText);
        llpickdrop=rootView.findViewById(R.id.llpickdrop);
        progressBar=rootView.findViewById(R.id.progress_bar_pick);

        if (Adminuser.equals("TRUE") && Join_Value.equals("member")) {
            llpickdrop.setVisibility(View.VISIBLE);
            getRouteDetail();
        }else  {

            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");
            progressBar.setVisibility(View.GONE);


        }



      /*  ll_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                editPickDropData();


            }
        });*/
        return rootView;
    }




    private void getRouteDetail(){
        new Thread(new Runnable() {
            @Override
            public void run() {
               // progressBar.setVisibility(View.VISIBLE);
                try {
                    String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.ROUTE_DETAILS + routeId);


                    if (response!=null){
                        gson=new Gson();
                        routeDetailResponseModel=new RouteDetailResponseModel();
                        routeDetailResponseModel = gson.fromJson(response, RouteDetailResponseModel.class);

                        Log.e("response",response);

                        getActivity(). runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                llpickdrop.setVisibility(View.VISIBLE);
                               // progressBar.setVisibility(View.GONE);
                                setUpdatePickDrop(routeDetailResponseModel.getData());

                            }
                        });

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private void setUpdatePickDrop(Data data) {

        wayPointsArrayList = new ArrayList<>();

        WayPointsArray WaypointInfoSrc = new WayPointsArray();

        WaypointInfoSrc.field_departure_time = data.getFieldCollectionWayPoints().getSource().getFieldDepartureTime();
        WaypointInfoSrc.field_photo = data.getFieldCollectionWayPoints().getSource().getFieldPhoto();
        WaypointInfoSrc.field_place_name = data.getFieldCollectionWayPoints().getSource().getField_place_name();
        WaypointInfoSrc.field_start_time = data.getFieldCollectionWayPoints().getSource().getFieldStartTime();
        WaypointInfoSrc.lat = data.getFieldCollectionWayPoints().getSource().getLat();
        WaypointInfoSrc.lng = data.getFieldCollectionWayPoints().getSource().getLng();

        wayPointsArrayList.add(WaypointInfoSrc);

        for (int i = 0; i < data.getFieldCollectionWayPoints().getWaypoints().size(); i++) {

            WayPointsArray WaypointInfo = new WayPointsArray();

            WaypointInfo.field_departure_time = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldDepartureTime();
            WaypointInfo.field_photo = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldPhoto();
            WaypointInfo.field_place_name = data.getFieldCollectionWayPoints().getWaypoints().get(i).getField_place_name();
            WaypointInfo.field_start_time = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldStartTime();
            WaypointInfo.lat = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldWayPoints().getLat();
            WaypointInfo.lng =data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldWayPoints().getLng();
            wayPointsArrayList.add(WaypointInfo);
        }

        WayPointsArray WaypointInfoDes = new WayPointsArray();

        WaypointInfoDes.field_departure_time = data.getFieldCollectionWayPoints().getDestination().getFieldDepartureTime();
        WaypointInfoDes.field_photo = data.getFieldCollectionWayPoints().getDestination().getFieldPhoto();
        WaypointInfoDes.field_place_name = data.getFieldCollectionWayPoints().getDestination().getField_place_name();
        WaypointInfoDes.field_start_time = data.getFieldCollectionWayPoints().getDestination().getFieldStartTime();
        WaypointInfoDes.lat = data.getFieldCollectionWayPoints().getDestination().getLat();
        WaypointInfoDes.lng = data.getFieldCollectionWayPoints().getDestination().getLng();

        wayPointsArrayList.add(WaypointInfoDes);
        updatePickDrop(data);






        if (data.getPickup_point().getLat()!=null && data.getDrop_point().getLat()!=null)
            isChangePickDrop=true;


        if (isChangePickDrop){
            ll_update.setVisibility(View.GONE);
        }  else ll_update.setVisibility(View.GONE);

        adapterSelectPick=new AdapterSelectPickDisplay(getActivity(),wayPointsArrayList,isChangePickDrop);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rec_select_pic.setLayoutManager(mLayoutManager);
        rec_select_pic.setItemAnimator(new DefaultItemAnimator());
        rec_select_pic.setAdapter(adapterSelectPick);

    }

    private void updatePickDrop(Data data){
        if (data.getPickup_point()!=null && data.getDrop_point()!=null) {


            if (routeDetailResponseModel.getData().getPickup_point() != null && routeDetailResponseModel.getData().getDrop_point() != null) {
                pickLocation = routeDetailResponseModel.getData().getPickup_point().getLat() + routeDetailResponseModel.getData().getPickup_point().getLng();
                dropLocation = routeDetailResponseModel.getData().getDrop_point().getLat() + routeDetailResponseModel.getData().getDrop_point().getLng();

                Log.d("TAG", "updatePickDrop: " + pickLocation + "..." + dropLocation);


            }


            for (int i = 0; i < wayPointsArrayList.size(); i++) {
                String curLat = wayPointsArrayList.get(i).getLat() + wayPointsArrayList.get(i).getLng();

                Log.d("TAG", "curLat: " + curLat);
                if (curLat.equals(pickLocation)) {
                    wayPointsArrayList.get(i).setPick(true);
                }

                if (curLat.equals(dropLocation)) {
                    wayPointsArrayList.get(i).setDrop(true);
                }
            }
        }




    }

   /* private class  EditBusRoute extends AsyncTask<String ,JSONObject,JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();




        }
        @Override
        protected JSONObject doInBackground(String... strings) {

            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();
            RequestBody body = new FormBody.Builder()
                    .add("membership type", "og_membership_type_bus_route")
                    .add("field_name" , "field_bus_member")
                    .add("type","og_membership_type_bus_route")
                    .add("field_subscription_days","45")
                    .add("field_pickup_point[lat]",wayPointsArrayList.get(pickPos).getLat())
                    .add("field_pickup_point[lng]",wayPointsArrayList.get(pickPos).getLng())
                    .add("field_drop_point[lat]",wayPointsArrayList.get(dropPos).getLat())
                    .add("field_drop_point[lng]",wayPointsArrayList.get(dropPos).getLng())
                    .build();

            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_ROUTEJOINAXCCEPTREQUEST+"/"+strings[0] ,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        showAlertFragmentDialog(getActivity(), "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }



        @Override
        protected void onPostExecute(JSONObject response) {
            super.onPostExecute(response);


            try {
                if (response != null) {

                    String errorCode = response.getString("status");
                    final String msg = response.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        JSONObject data = response.getJSONObject("data");

                        if (data != null && data.length() > 0) {
                            String Membershipid = data.getString("membership_id");
                            String child_student=data.getString("child_student");
                            Utils.init(getActivity());

                            Utils.clearPickDropData();
                            WayPointResponse wayPointResponse=new WayPointResponse();
                            wayPointResponse.setChild_student(child_student);
                            wayPointResponse.setMembership_id(Membershipid);


                            ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
                            wayPointsArrays.add(wayPointsArrayList.get(pickPos));
                            wayPointsArrays.add(wayPointsArrayList.get(dropPos));
                            wayPointResponse.setWayPointsArrays(wayPointsArrays);
                            Utils.savePickAndDrop(getActivity(),wayPointResponse);
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            Log.d("TAG", "membership_id: " + Membershipid );

                        } else {
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {
                        Utils.init(getActivity());

                         WayPointResponse wayPointResponse=new WayPointResponse();
                        ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
                        wayPointsArrays.add(wayPointsArrayList.get(pickPos));
                        wayPointsArrays.add(wayPointsArrayList.get(dropPos));
                        wayPointResponse.setWayPointsArrays(wayPointsArrays);
                        Utils.savePickAndDrop(getActivity(),wayPointResponse);

                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (JSONException e) {

            }
        }
    }


    private void editPickDropData(){



        Utils.init(getActivity());
        SelectBusRoute selectBusRoute=new SelectBusRoute();
        selectBusRoute.execute();

    }



    private class SelectBusRoute extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();



            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("etid", SessionManager.getInstance(getActivity()).getUser().getUserprofile_id())
                    .add("gid", routeId)
                    .add("state", "2")
                    .add("group_type", "node")
                    .add("membership type", "og_membership_type_bus_route")
                    .add("field_name" , "field_bus_member")
                    .add("type","og_membership_type_bus_route")
                    .add("field_subscription_days","45")
                    .add("field_pickup_point[lat]",wayPointsArrayList.get(pickPos).getLat())
                    .add("field_pickup_point[lng]",wayPointsArrayList.get(pickPos).getLng())
                    .add("field_drop_point[lat]",wayPointsArrayList.get(dropPos).getLat())
                    .add("field_drop_point[lng]",wayPointsArrayList.get(dropPos).getLng())
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ROUTEJOINAXCCEPTREQUEST,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {

            try {
                if (responce != null) {
                    progressBar.setVisibility(View.GONE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        JSONObject data = responce.getJSONObject("data");

                        if (data != null && data.length() > 0) {
                            String Membershipid = data.getString("membership_id");
                            String child_student=data.getString("child_student");
                            Utils.init(getActivity());


                            WayPointResponse wayPointResponse=new WayPointResponse();
                            wayPointResponse.setChild_student(child_student);
                            wayPointResponse.setMembership_id(Membershipid);
                            ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
                            wayPointsArrays.add(wayPointsArrayList.get(pickPos));
                            wayPointsArrays.add(wayPointsArrayList.get(dropPos));
                            wayPointResponse.setWayPointsArrays(wayPointsArrays);
                            Utils.savePickAndDrop(getActivity(),wayPointResponse);
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            Log.d("TAG", "membership_id: " + Membershipid );

                        } else {
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                        }
                        pickPos=0;
                        dropPos=0;





                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                    }
                }
            } catch (JSONException e) {

            }
        }

        @Override
        protected void onCancelled() {



        }
    }
*/

}
