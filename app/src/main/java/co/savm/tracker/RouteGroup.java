package co.savm.tracker;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.savm.Event.ServerMemberUpdate;
import co.savm.R;
import co.savm.adapters.AdapterSelectPick;
import co.savm.adapters.BusRouteCommentsAdapter;
import co.savm.adapters.CourseMembersAdapter;
import co.savm.models.CourseMemberLists;
import co.savm.models.routedetailResponse.Data;
import co.savm.models.routedetailResponse.RouteDetailResponseModel;
import co.savm.models.tracker.BusCommentArray;
import co.savm.models.tracker.WayPointResponse;
import co.savm.models.tracker.WayPointsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class RouteGroup extends BaseFragment {
    static TextView ErrorText;
    static Activity activity;
    static String Route_id,droplat;
    String RouteTittle,Adminuser,Join_Value,AdminCreater;
    RecyclerView.LayoutManager mLayoutManager,layoutManager;
    RecyclerView GroupMember_Lists;
    static RecyclerView rv_route_comment;
    static SwipeRefreshLayout swipeContainer;
    private static ProgressBar spinner;
    static FloatingActionButton fab;
    static Button JoinGroup;
    CardView card_view1;
    CourseMembersAdapter mGroupMemberAdapter;
    static BusRouteCommentsAdapter mcommentAdapter;
    public static ArrayList<BusCommentArray> commentsList;
    public ArrayList<CourseMemberLists> memberList;
    private GroupMemberList groupmemberList = null;
    private ProgressJoinTheGroupRequest joingrouprequestList = null;
    private static RouteCommentsList routecommentsList = null;
    private static boolean isLoading= true;
    private boolean _hasLoadedOnce= false; // your boolean field
    Dialog pickUpDialog;
    private static int PAGE_SIZE = 0;
    static String tittle,URLDoc;
    public static List<String> groupMember;
    private boolean isComment=false;
    public String field_place_name,field_start_time,field_departure_time,field_way_points,lat,lng,field_photo,driver_name,driver_no,co_drivername,co_driverno;

    /*dialog screens*/
    Dialog dialog ;
    RecyclerView rec_select_pic;
    private String routeId;
    private String routeName,sponsered;
    AdapterSelectPick adapterSelectPick;
    private LinearLayout ll_cancel,llpickdrop;
    RelativeLayout ll_update2;
    TextView ll_update;
    public static int pickPos,dropPos;
    private RouteDetailResponseModel routeDetailResponseModel;
    private Gson gson;
    private ArrayList<WayPointsArray> wayPointsArrayList;
    WayPointsArray wayPointsPick=new WayPointsArray();
    WayPointsArray wayPointsDrop=new WayPointsArray();
    private static ShimmerFrameLayout mShimmerViewContainer;
    private ProgressBar progressBar;
    private boolean isChangePickDrop=false;
    //  private static ProgressBusRouteInfo busrouteinfoAuthTask = null;
    public static String wayPointData,SourceStartLat,SourceStartLng,DestinationLat,DestinationLng,routetittle,routediscription,
            busroutecode,busno,PickUpLat,PickUpLng,DropLat,DropLng;

    public RouteGroup() {
        // Required empty public constructor
    }




    @Override
    public void onPause() {

        Log.e("onPause","routeFeeds");
        _hasLoadedOnce= false;
        mShimmerViewContainer.stopShimmerAnimation();
        super.onPause();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route_group, container, false);
        activity = (Activity) view.getContext();
        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle= getArguments().getString("ROUTE_NAME");
        Adminuser = getArguments().getString("IS_FACULTY");
        Join_Value = getArguments().getString("IS_MEMBER");
        AdminCreater = getArguments().getString("IS_CREATER");

        wayPointsArrayList = new ArrayList<>();
        JoinGroup = view.findViewById(R.id.JoinGroup);
        swipeContainer = view.findViewById(R.id.swipeContainer);
        GroupMember_Lists = view.findViewById(R.id.GroupMember_Lists);
        spinner= view.findViewById(R.id.progressBar);

        mShimmerViewContainer = view.findViewById(R.id.shimmer_view_container);
        mShimmerViewContainer.startShimmerAnimation();


        mLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        layoutManager = new LinearLayoutManager(activity);
        GroupMember_Lists.setLayoutManager(mLayoutManager);
        rv_route_comment = view.findViewById(R.id.rv_route_comment);
        rv_route_comment.setHasFixedSize(true);
        rv_route_comment.setLayoutManager(layoutManager);
        card_view1 = view.findViewById(R.id.card_view1);
        fab = view. findViewById(R.id.fab);
        ErrorText= view. findViewById(R.id.ErrorText);
        memberList = new ArrayList<CourseMemberLists>();
        commentsList = new ArrayList<BusCommentArray>();
        groupMember=new ArrayList<>();

        mcommentAdapter = new BusRouteCommentsAdapter(rv_route_comment, commentsList ,activity);
        rv_route_comment.setAdapter(mcommentAdapter);
        rv_route_comment.setVisibility(View.GONE);
        routecommentsList = new RouteCommentsList();
        routecommentsList.execute(Route_id);



        if (Join_Value.equals("member")) {
            rv_route_comment.setVisibility(View.VISIBLE);
            JoinGroup.setVisibility(View.GONE);
            Utils.init(getActivity());
            Utils.clearDataJoin();
            Utils.saveJoinGroup(true);
            swipeContainer.setRefreshing(false);
            inItView();


        } else if (Join_Value.equals("pending")) {
            rv_route_comment.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.GONE);
            swipeContainer.setRefreshing(false);
            fab.setVisibility(View.GONE);
            JoinGroup.setText("Request Sent");
            JoinGroup.setOnClickListener(null);
            JoinGroup.setVisibility(View.VISIBLE);
        }

        else if (Join_Value.equals("nomember")) {
            if(SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                rv_route_comment.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);
                JoinGroup.setText("Join");
                JoinGroup.setVisibility(View.VISIBLE);
                fab.setVisibility(View.GONE);

            }

            else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
                rv_route_comment.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);
                fab.setVisibility(View.GONE);
                JoinGroup.setVisibility(View.GONE);
                ErrorText.setVisibility(View.VISIBLE);
                ErrorText.setText("You Are Not Authorised");

            }
            else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("13"))   {
                rv_route_comment.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);
                JoinGroup.setText("Join");
                JoinGroup.setVisibility(View.VISIBLE);
                fab.setVisibility(View.GONE);

            }

            else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("15"))   {
                rv_route_comment.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);
                JoinGroup.setText("Join");
                JoinGroup.setVisibility(View.VISIBLE);
                fab.setVisibility(View.GONE);

            }


        }



        JoinGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                OpenPickUPDroupPointDialog();

            }

        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent backIntent = new Intent(activity, BusAddComment.class)
                        .putExtra("ROUTE_ID",Route_id)
                        .putExtra("open","BusRouteGroup");
                startActivity(backIntent);




            }
        });


        /*CALLING LISTS OF ROUTE MEMBER */
        groupmemberList = new GroupMemberList();
        groupmemberList.execute(Route_id);


        return view;
    }




    private void getValue() {

        if (Join_Value.equals("member")) {
            rv_route_comment.setVisibility(View.VISIBLE);
            JoinGroup.setVisibility(View.GONE);
            Utils.init(getActivity());
            Utils.clearDataJoin();
            Utils.saveJoinGroup(true);
            inItView();


        } else if (Join_Value.equals("pending")) {
            rv_route_comment.setVisibility(View.GONE);
            swipeContainer.setVisibility(View.GONE);
            swipeContainer.setRefreshing(false);
            fab.setVisibility(View.GONE);
            JoinGroup.setText("Request Sent");
            JoinGroup.setOnClickListener(null);
            JoinGroup.setVisibility(View.VISIBLE);
        }
        else if (Join_Value.equals("nomember")) {
            if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {
                rv_route_comment.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);
                JoinGroup.setText("Join");
                JoinGroup.setVisibility(View.VISIBLE);
                fab.setVisibility(View.GONE);

            } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("14")) {
                rv_route_comment.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);

                fab.setVisibility(View.GONE);
                JoinGroup.setVisibility(View.GONE);
                ErrorText.setVisibility(View.VISIBLE);
                ErrorText.setText("You Are Not Authorised");

            } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("13")) {
                rv_route_comment.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);

                JoinGroup.setText("Join");
                JoinGroup.setVisibility(View.VISIBLE);
                fab.setVisibility(View.GONE);

            } else if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("15")) {
                rv_route_comment.setVisibility(View.GONE);
                swipeContainer.setVisibility(View.GONE);
                swipeContainer.setRefreshing(false);

                JoinGroup.setText("Join");
                JoinGroup.setVisibility(View.VISIBLE);
                fab.setVisibility(View.GONE);

            }
        }


        }



    public static void RefreshWorkedBusRoute() {
        PAGE_SIZE=0;

        // isLoading=true;

      // swipeContainer.setRefreshing(true);

        /* CALLING LISTS OF COMMENTS*/
        routecommentsList = new RouteCommentsList();
        routecommentsList.execute(Route_id);


    }



    private void inItView(){

        swipeContainer.setVisibility(View.VISIBLE);
        swipeContainer.setRefreshing(false);
            CallTheSpin();
        rv_route_comment.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading   ){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    mcommentAdapter.addProgress();

                    routecommentsList = new RouteCommentsList();
                    routecommentsList.execute(Route_id);

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }


    /*STUDENTS ON THE SUBJECTS*/



    private class GroupMemberList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEMEMBERLIST+"/"+args[0]+"/users?fields=id, etid, gid");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            groupmemberList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    memberList.clear();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {


                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CourseMemberLists CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CourseMemberLists.class);
                                memberList.add(CourseInfo);
                                groupMember.add(CourseInfo.getEtid());
                                if (CourseInfo.getEtid().contains(SessionManager.getInstance(getActivity()).getUser().getUserprofile_id()))
                                {
                                    Log.d("TAG", "myid: " + SessionManager.getInstance(getActivity()).getUser().getUserprofile_id());

                                  //  JoinGroup.setVisibility(View.GONE);
                                    /*       fab.setVisibility(View.VISIBLE);*/




                                    /*    CALLING LISTS OF COMMENTS*/



                                }else {
                                    // JoinGroup.setVisibility(View.VISIBLE);

                                   /* JoinGroup.setVisibility(View.VISIBLE);
                                    rv_subject_comment.setVisibility(View.GONE);
                                    fab.setVisibility(View.GONE);
                                    swipeContainer.setRefreshing(false);
                                    swipeContainer.setVisibility(View.GONE);*/
                                }


                            }


                            mGroupMemberAdapter = new CourseMembersAdapter(activity, R.layout.list_of_groupmemberlists,memberList);
                            GroupMember_Lists.setAdapter(mGroupMemberAdapter);



                            if(SessionManager.getInstance(activity).getUser().getParentUid()==null){
                                if (groupMember.contains(SessionManager.getInstance(activity)
                                        .getUser().getUserprofile_id())){
                                    Utils.init(activity);
                                    Utils.clearDataJoin();
                                    Utils.saveJoinGroup(true);
                                    EventBus.getDefault().post(new ServerMemberUpdate(1));

                                }else{
                                    Utils.init(activity);
                                    Utils.clearDataJoin();
                                    Utils.saveJoinGroup(false);


                                }

                            }else {
                                if (groupMember.contains(SessionManager.getInstance(activity)
                                        .getUser().getParentUid())){
                                    Utils.init(activity);
                                    Utils.clearDataJoin();
                                    Utils.saveJoinGroup(true);
                                    EventBus.getDefault().post(new ServerMemberUpdate(1));



                                }else{
                                    Utils.init(activity);
                                    Utils.clearDataJoin();
                                    Utils.saveJoinGroup(false);


                                }
                            }

                            Utils.init(activity);

                            if (Utils.isJoinGroup(activity)){
//                                routecommentsList = new RouteCommentsList();
//                                routecommentsList.execute(Route_id);
                                _hasLoadedOnce=false;
                                setUserVisibleHint(true);
                            }

                            card_view1.setVisibility(View.VISIBLE);

                        }else {

                           /* JoinGroup.setVisibility(View.VISIBLE);
                            rv_subject_comment.setVisibility(View.GONE);
                            fab.setVisibility(View.GONE);*/
                        }



                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);



                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                    Utils. showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            groupmemberList = null;
            spinner.setVisibility(View.INVISIBLE);



        }
    }





    /*SEND REQUEST FOR JOINING ROUTES*/

    private class ProgressJoinTheGroupRequest extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("etid",args[1])
                    .add("group_type","node")
                    .add("gid",args[0])
                    .add("state","2")
                    .add("membership type","og_membership_type_bus_route")
                    .add("type","og_membership_type_bus_route")
                    .add("field_name","field_bus_member")
                    .add("field_subscription_days","45")
                    .add("field_pickup_point[lat]",wayPointsArrayList.get(pickPos).getLat())
                    .add("field_pickup_point[lng]",wayPointsArrayList.get(pickPos).getLng())
                    .add("field_drop_point[lat]",wayPointsArrayList.get(dropPos).getLat())
                    .add("field_drop_point[lng]",wayPointsArrayList.get(dropPos).getLng())
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.ROUTE_JOINBUS ,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            joingrouprequestList = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject data = responce.getJSONObject("data");

                        if (data != null && data.length() > 0) {
                            String Membershipid = data.getString("membership_id");
                            String child_student=data.getString("child_student");
                            // Utils.init(getActivity());

                            Utils.clearPickDropData();
                            WayPointResponse wayPointResponse=new WayPointResponse();
                            wayPointResponse.setChild_student(child_student);
                            wayPointResponse.setMembership_id(Membershipid);


                            ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
                            wayPointsArrays.add(wayPointsArrayList.get(pickPos));
                            wayPointsArrays.add(wayPointsArrayList.get(dropPos));
                            wayPointResponse.setWayPointsArrays(wayPointsArrays);
                            Utils.savePickAndDrop(getActivity(),wayPointResponse);
                            // Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            Log.d("TAG", "membership_id: " + Membershipid );

                        } else {
                            Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                        }
                       /* Utils.clearDataJoin();
                        Utils.saveJoinGroup(true);*/
                        // pickUpDialog.dismiss();
                        AllRoutesDisplay.CallFromRouteGroup();
                        activity.finish();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);

                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);

                        //  Utils.init(getActivity());
                      /*  Utils.init(getActivity());
                        WayPointResponse wayPointResponse=new WayPointResponse();
                        ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
                        wayPointsArrays.add(wayPointsArrayList.get(pickPos));
                        wayPointsArrays.add(wayPointsArrayList.get(dropPos));
                        wayPointResponse.setWayPointsArrays(wayPointsArrays);
                        Utils.savePickAndDrop(getActivity(),wayPointResponse);*/
                        Utils.showAlertFragmentDialog(activity,"Information", msg);
                        // pickUpDialog.dismiss();

                    }
                }else {
                    spinner.setVisibility(View.INVISIBLE);
                    Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            joingrouprequestList = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }





    /*LISTS OF COMMENTS ON SUBJECTS*/

    private static class RouteCommentsList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTECOMMENTSLIST+"?"+"rid"+"="+args[0]+"&offset="+PAGE_SIZE+"&limit=20");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);


            } catch (JSONException | IOException e) {
                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Reload the page.");

                    }
                });

                mcommentAdapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            try {
                if (responce != null) {
                    swipeContainer.setRefreshing(false);
                    rv_route_comment.setVisibility(View.VISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        ArrayList<BusCommentArray> arrayList = new ArrayList<>();
                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {



                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                BusCommentArray subjects = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), BusCommentArray.class);

                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                                if (jsonObject.has("field_comment_attachments")){
                                    JSONObject AttacmentsFeilds = jsonObject.getJSONObject("field_comment_attachments");

                                    if (AttacmentsFeilds != null && AttacmentsFeilds.length() > 0 ){
                                        URLDoc = AttacmentsFeilds.getString("value");
                                        tittle = AttacmentsFeilds.getString("title");
                                        subjects.title =tittle;
                                        subjects.url =URLDoc;

                                    }else {

                                    }
                                }


                                arrayList.add(subjects);
                            }

                        }

                        if (arrayList!=null &&  arrayList.size()>0){
                            rv_route_comment.setVisibility(View.VISIBLE);
                            if (PAGE_SIZE == 0) {


                                mcommentAdapter.setInitialData(arrayList);

                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);

                            } else {
                                mcommentAdapter.removeProgress();
                                swipeContainer.setRefreshing(false);
                                mcommentAdapter.addData(arrayList);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);


                            }

                            isLoading = false;

                        } else{
                            if (PAGE_SIZE==0){
                                ErrorText.setVisibility(View.VISIBLE);
                                ErrorText.setText("No Post");
                                swipeContainer.setVisibility(View.GONE);
                                mShimmerViewContainer.stopShimmerAnimation();
                                mShimmerViewContainer.setVisibility(View.GONE);
                            }else
                                mcommentAdapter.removeProgress();

                        }









                    } else if (errorCode.equalsIgnoreCase("0")) {
                        swipeContainer.setRefreshing(false);
                        Utils.showAlertFragmentDialog(activity,"Information", msg);
                        mShimmerViewContainer.stopShimmerAnimation();
                        mShimmerViewContainer.setVisibility(View.GONE);

                    }
                }else {
                    swipeContainer.setRefreshing(false);
                    Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);
                Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");


            }
        }

        @Override
        protected void onCancelled() {
            routecommentsList = null;
            swipeContainer.setRefreshing(false);



        }
    }

    @Override
    public void onResume() {
        setUserVisibleHint(true);
        mShimmerViewContainer.startShimmerAnimation();
        super.onResume();
    }

    private void CallTheSpin()
    {
//        fab.setVisibility(View.VISIBLE);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {


                    @Override
                    public void run() {

                        if (isLoading){
                            RefreshWorkedBusRoute();
                        }else{
                            isLoading=false;
                            swipeContainer.setRefreshing(false);
                        }


                    }
                }, 3000);
            }
        });


    }


    @Override
    public void onDestroy() {
        isLoading=true;
        PAGE_SIZE=0;

        super.onDestroy();
    }


    private void OpenPickUPDroupPointDialog() {


        pickUpDialog = new Dialog(activity);
        pickUpDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pickUpDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(Color.BLACK));
        pickUpDialog.setContentView(R.layout.joinroute_dailog_selectpick);

        pickUpDialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        pickUpDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;


        llpickdrop =pickUpDialog.findViewById(R.id.llpickdrop);
        rec_select_pic=pickUpDialog.findViewById(R.id.rec_select_pic);
        ll_cancel=pickUpDialog.findViewById(R.id.ll_cancel);
        ll_update=pickUpDialog.findViewById(R.id.ll_update);
        llpickdrop=pickUpDialog.findViewById(R.id.llpickdrop);
        progressBar=pickUpDialog.findViewById(R.id.progress_bar_pick);

        getRouteDetail();


        ll_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetTheUpdatedValues();
                pickUpDialog.dismiss();


            }
        });


        pickUpDialog.show();


    }

    private void GetTheUpdatedValues() {

        if (SessionManager.getInstance(getActivity()).getUserClgRole().getRole().matches("16")) {

            joingrouprequestList = new ProgressJoinTheGroupRequest();
            joingrouprequestList.execute(Route_id, SessionManager.getInstance(getActivity()).getUser().getParentUid());

        } else {
            joingrouprequestList = new ProgressJoinTheGroupRequest();
            joingrouprequestList.execute(Route_id, SessionManager.getInstance(getActivity()).getUser().userprofile_id);

        }

    }





    private void getRouteDetail(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                llpickdrop.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                try {
                    String response = ApiCall.GETHEADER(OkHttpClientObject.getOkHttpClientObject(),
                            URLS.ROUTE_DETAILS + Route_id);


                    if (response!=null){
                        gson=new Gson();
                        routeDetailResponseModel=new RouteDetailResponseModel();
                        routeDetailResponseModel = gson.fromJson(response, RouteDetailResponseModel.class);

                        Log.e("response",response);

                        getActivity(). runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                llpickdrop.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);
                                setUpdatePickDrop(routeDetailResponseModel.getData());

                            }
                        });

                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private void setUpdatePickDrop(Data data) {

        wayPointsArrayList = new ArrayList<>();

        WayPointsArray WaypointInfoSrc = new WayPointsArray();

        WaypointInfoSrc.field_departure_time = data.getFieldCollectionWayPoints().getSource().getFieldDepartureTime();
        WaypointInfoSrc.field_photo = data.getFieldCollectionWayPoints().getSource().getFieldPhoto();
        WaypointInfoSrc.field_place_name = data.getFieldCollectionWayPoints().getSource().getField_place_name();
        WaypointInfoSrc.field_start_time = data.getFieldCollectionWayPoints().getSource().getFieldStartTime();
        WaypointInfoSrc.lat = data.getFieldCollectionWayPoints().getSource().getLat();
        WaypointInfoSrc.lng = data.getFieldCollectionWayPoints().getSource().getLng();

        wayPointsArrayList.add(WaypointInfoSrc);

        for (int i = 0; i < data.getFieldCollectionWayPoints().getWaypoints().size(); i++) {

            WayPointsArray WaypointInfo = new WayPointsArray();

            WaypointInfo.field_departure_time = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldDepartureTime();
            WaypointInfo.field_photo = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldPhoto();
            WaypointInfo.field_place_name = data.getFieldCollectionWayPoints().getWaypoints().get(i).getField_place_name();
            WaypointInfo.field_start_time = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldStartTime();
            WaypointInfo.lat = data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldWayPoints().getLat();
            WaypointInfo.lng =data.getFieldCollectionWayPoints().getWaypoints().get(i).getFieldWayPoints().getLng();
            wayPointsArrayList.add(WaypointInfo);
        }

        WayPointsArray WaypointInfoDes = new WayPointsArray();

        WaypointInfoDes.field_departure_time = data.getFieldCollectionWayPoints().getDestination().getFieldDepartureTime();
        WaypointInfoDes.field_photo = data.getFieldCollectionWayPoints().getDestination().getFieldPhoto();
        WaypointInfoDes.field_place_name = data.getFieldCollectionWayPoints().getDestination().getField_place_name();
        WaypointInfoDes.field_start_time = data.getFieldCollectionWayPoints().getDestination().getFieldStartTime();
        WaypointInfoDes.lat = data.getFieldCollectionWayPoints().getDestination().getLat();
        WaypointInfoDes.lng = data.getFieldCollectionWayPoints().getDestination().getLng();

        wayPointsArrayList.add(WaypointInfoDes);
        updatePickDrop(data);

        if (data.getPickup_point().getLat()!=null && data.getDrop_point().getLat()!=null)
            isChangePickDrop=true;


       /* if (isChangePickDrop){
            ll_update.setVisibility(View.VISIBLE);
        }  else ll_update.setVisibility(View.GONE);
*/
        adapterSelectPick=new AdapterSelectPick(getActivity(),wayPointsArrayList,isChangePickDrop);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rec_select_pic.setLayoutManager(mLayoutManager);
        rec_select_pic.setItemAnimator(new DefaultItemAnimator());
        rec_select_pic.setAdapter(adapterSelectPick);

    }

    private void updatePickDrop(Data data){
        if (data.getPickup_point()!=null && data.getDrop_point()!=null){
            String pickPoint =data.getPickup_point().getLat()+data.getPickup_point().getLng();
            String dropPoint =data.getDrop_point().getLat()+data.getDrop_point().getLng();

            for (int i=0;i<wayPointsArrayList.size();i++){
                String curLat=wayPointsArrayList.get(i).getLat()+wayPointsArrayList.get(i).getLng();
                if (curLat.equals(pickPoint)){
                    wayPointsArrayList.get(i).setPick(true);
                }else if (curLat.equals(dropPoint)){
                    wayPointsArrayList.get(i).setDrop(true);
                }
            }
        }
    }







}
