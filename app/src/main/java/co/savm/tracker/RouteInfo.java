package co.savm.tracker;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import co.savm.R;
import co.savm.adapters.FacultyListInBusRouteAdapter;
import co.savm.adapters.RoutewaypointsAdapter;
import co.savm.models.SubjectFacultyArray;
import co.savm.models.tracker.WayPointsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class RouteInfo extends BaseFragment {
    TextView routeCode,Vehicle_no,tittle,about,driverName,driverNo,co_driver,co_driverNo;
    String busroutecode;
    String busno,transportImages;
    String routetittle;
    String routediscription;
    static String Route_id;
    String RouteTittle;
    static String Adminuser;
    String Join_Value;
    static String AdminCreater;
    public String field_place_name,field_start_time,field_departure_time,field_way_points,lat,lng,field_photo,driver_name,driver_no,co_drivername,co_driverno;
    ImageView AddTeacher,routelogo;
    private RecyclerView.LayoutManager layoutManager,mlayoutManager;
    RecyclerView busrouteList;
    static RecyclerView TeacherList;
    private static ProgressBar spinner;
    static Activity activity;
    private static ProgressBusRouteInfo busrouteinfoAuthTask = null;
    public static ArrayList<WayPointsArray> mwaypointsList;
    private static RoutewaypointsAdapter routewaypointadapter;
    public static ArrayList<SubjectFacultyArray> mfacultyList;
    private static FacultyListInBusRouteAdapter facultyadapter;
    private static RouteTeacherDisplay facultylistdisplay = null;
    public static String wayPointData,SourceStartLat,SourceStartLng,DestinationLat,DestinationLng;
    private SparseIntArray sparseIntArray;
    String  pickuplat,pickuplng,droplat,droplng;

    ArrayList<WayPointsArray> wayPointsArrays=new ArrayList<>();
    WayPointsArray wayPointsPick=new WayPointsArray();
    WayPointsArray wayPointsDrop=new WayPointsArray();



    int selectStopIndex;
    public RouteInfo() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_route_info, container, false);
        activity = (Activity) view.getContext();
        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle = getArguments().getString("ROUTE_NAME");
        Adminuser = getArguments().getString("IS_FACULTY");
        Join_Value = getArguments().getString("IS_MEMBER");
        AdminCreater = getArguments().getString("IS_CREATER");
        transportImages =getArguments().getString("TRANSPORT_IMAGE");



        routelogo = view.findViewById(R.id.routelogo);
        routeCode = view.findViewById(R.id.routeCode);
        Vehicle_no = view.findViewById(R.id.Vehicle_no);
        tittle = view.findViewById(R.id.tittle);
        about = view.findViewById(R.id.about);
        driverName = view.findViewById(R.id.driverName);
        driverNo = view.findViewById(R.id.driverNo);
        co_driver = view.findViewById(R.id.co_driver);
        co_driverNo = view.findViewById(R.id.co_driverNo);
        busrouteList = view.findViewById(R.id.busrouteList);
        spinner = view.findViewById(R.id.progressBar);
        layoutManager = new LinearLayoutManager(getContext());
        mwaypointsList = new ArrayList<>();
        mfacultyList = new ArrayList<>();
        busrouteList.setHasFixedSize(true);
        busrouteList.setLayoutManager(layoutManager);

        AddTeacher =view.findViewById(R.id.AddTeacher);

        mlayoutManager = new LinearLayoutManager(activity);
        spinner= view.findViewById(R.id.progressBar);

        TeacherList = view.findViewById(R.id.TeacherList);
        TeacherList.setHasFixedSize(true);
        TeacherList.setLayoutManager(mlayoutManager);

        sparseIntArray = new SparseIntArray(5);
        sparseIntArray.put(0, R.mipmap.transa);
        sparseIntArray.put(1, R.mipmap.transb);
        sparseIntArray.put(2, R.mipmap.transc);
        sparseIntArray.put(3, R.mipmap.transd);
        sparseIntArray.put(4, R.mipmap.transe);

        Random rand = new Random();

        for(int j=0; j<sparseIntArray.size(); j++) {

            int index = rand.nextInt(sparseIntArray.size());

            routelogo.setBackground(ContextCompat
                    .getDrawable(activity, sparseIntArray.get(index)));
        }

     /*   Glide.with(this).load(transportImages)
                .placeholder(R.mipmap.header_image).dontAnimate()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .fitCenter().into(routelogo);*/

        if (Adminuser.equals("TRUE") && AdminCreater.equals("TRUE")) {

            AddTeacher.setVisibility(View.VISIBLE);


        }else {
            AddTeacher.setVisibility(View.GONE);

        }




        AddTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent newIntent = new Intent(activity, InviteTeacherInBus.class);
                Bundle bundle=new Bundle();
                bundle.putString("ROUTE_ID",Route_id);
                newIntent.putExtras(bundle);
                startActivity(newIntent);

            }
        });

        GetAllDetailsOfRoutes();
        GetAllSubjectFaculty();

        return view;

    }

    private void GetAllDetailsOfRoutes() {
        mfacultyList.clear();
        busrouteinfoAuthTask = new ProgressBusRouteInfo();
        busrouteinfoAuthTask.execute();

    }


    public static void GetAllSubjectFaculty() {
        mfacultyList.clear();
        facultylistdisplay = new RouteTeacherDisplay();
        facultylistdisplay.execute();

    }


    public static void RefreshWorkedFacultyInFaculty() {

        mfacultyList.clear();
        facultylistdisplay = new RouteTeacherDisplay();
        facultylistdisplay.execute();
    }

    private static class RouteTeacherDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEFACULTYLISTING+"/"+Route_id+"/faculty");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            facultylistdisplay = null;
            try {
                if (responce != null) {

                    spinner.setVisibility(View.INVISIBLE);

                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");
                        mfacultyList.clear();
                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                SubjectFacultyArray TeacherInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), SubjectFacultyArray.class);
                                mfacultyList.add(TeacherInfo);
                                facultyadapter = new FacultyListInBusRouteAdapter(activity, mfacultyList,Route_id,Adminuser,AdminCreater);
                                TeacherList.setAdapter(facultyadapter);
                                TeacherList.setNestedScrollingEnabled(false);



                            }

                        }else{

                            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                            // builder.setTitle("Info");
                            builder.setMessage(R.string.nouserteacher)
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {


                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {

                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Information", msg);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected void onCancelled() {
            facultylistdisplay = null;
            spinner.setVisibility(View.INVISIBLE);

        }
    }







    private  class ProgressBusRouteInfo extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEINFO +"/"+Route_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            busrouteinfoAuthTask = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        WayPointsArray WaypointInfo1 = new WayPointsArray();
                        WayPointsArray WaypointInfo2 = new WayPointsArray();

                        JSONObject data = responce.getJSONObject("data");
                        routetittle = data.getString("title");
                        routediscription = data.getString("body");
                        busroutecode = data.getString("field_route_number");
                        busno = data.getString("field_device_id");
                        driver_name = data.getString("field_driver_name");
                        driver_no = data.getString("field_driver_phone_number");
                        co_drivername = data.getString("field_attendent_name");
                        co_driverno = data.getString("field_attendant_phone_number");


                        JSONObject field_collection_way_points = data.getJSONObject("field_collection_way_points");
                        JSONObject Source = field_collection_way_points.getJSONObject("source");



                        String   field_place_name1 = Source.getString("field_place_name");
                        String   field_start_time1 = Source.getString("field_start_time");
                        String  field_departure_time1  = Source.getString("field_departure_time");
                        String   field_photo1 = Source.getString("field_photo");
                        String  lat1 = Source.getString("lat");
                        String lng1 = Source.getString("lng");

                        SourceStartLat =lat1;
                        SourceStartLng =lng1;



                        WaypointInfo1.field_place_name = field_place_name1;
                        WaypointInfo1.field_start_time = field_start_time1;
                        WaypointInfo1.field_departure_time = field_departure_time1;
                        WaypointInfo1.field_photo = field_photo1;
                        WaypointInfo1.lat =lat1;
                        WaypointInfo1.lng =lng1;

                        mwaypointsList.add(WaypointInfo1);



                        JSONObject destination = field_collection_way_points.getJSONObject("destination");

                        String   field_place_name2 = destination.getString("field_place_name");
                        String   field_start_time2 = destination.getString("field_start_time");
                        String  field_departure_time2  = destination.getString("field_departure_time");
                        String   field_photo2 = destination.getString("field_photo");
                        String  lat2 = destination.getString("lat");
                        String lng2 = destination.getString("lng");


                        DestinationLat =lat2;
                        DestinationLng =lng2;




                        JSONArray waypoint = field_collection_way_points.getJSONArray("waypoints");
                        if(waypoint != null && waypoint.length() > 0 ) {

                            for (int i = 0; i < waypoint.length(); i++) {
                                WayPointsArray WaypointInfo = new WayPointsArray();

                                JSONObject jsonObject = waypoint.getJSONObject(i);
                                if (jsonObject.has("field_way_points")) {
                                    JSONObject locate = jsonObject.getJSONObject("field_way_points");
                                    if (locate != null && locate.length() > 0) {
                                        lat = locate.getString("lat");
                                        lng = locate.getString("lng");

                                        WaypointInfo.lat =lat;
                                        WaypointInfo.lng =lng;



                                    } else {

                                    }
                                }


                                field_place_name = waypoint.getJSONObject(i).getString("field_place_name");
                                field_start_time = waypoint.getJSONObject(i).getString("field_start_time");
                                field_departure_time  = waypoint.getJSONObject(i).getString("field_departure_time");
                                field_photo = waypoint.getJSONObject(i).getString("field_photo");




                                WaypointInfo.field_place_name = field_place_name;
                                WaypointInfo.field_start_time = field_start_time;
                                WaypointInfo.field_departure_time = field_departure_time;
                                WaypointInfo.field_photo = field_photo;
                                mwaypointsList.add(WaypointInfo);



                                Log.d("TAG", "location: " + lat + lng +field_place_name +"..."+field_start_time);


                                JSONObject pickuppoint = data.getJSONObject("pickup_point");
                                if (pickuppoint != null && pickuppoint.length() > 0) {

                                    pickuplat = pickuppoint.getString("lat");
                                    pickuplng = pickuppoint.getString("lng");



                                }

                                JSONObject droppoint = data.getJSONObject("drop_point");
                                if (pickuppoint != null && pickuppoint.length() > 0) {

                                    droplat = droppoint.getString("lat");
                                     droplng = droppoint.getString("lng");

                                }



                            }

                            WaypointInfo2.field_place_name = field_place_name2;
                            WaypointInfo2.field_start_time = field_start_time2;
                            WaypointInfo2.field_departure_time = field_departure_time2;
                            WaypointInfo2.field_photo = field_photo2;
                            WaypointInfo2.lat =lat2;
                            WaypointInfo2.lng =lng2;
                            WaypointInfo2.pickuplng =pickuplat;
                            WaypointInfo2.pickuplng =pickuplng;
                            WaypointInfo2.droplat =droplat;
                            WaypointInfo2.droplng =droplng;



                            mwaypointsList.add(WaypointInfo2);

                            routewaypointadapter = new RoutewaypointsAdapter(activity, mwaypointsList,SourceStartLat,SourceStartLng,DestinationLat,DestinationLng);
                            busrouteList.setAdapter(routewaypointadapter);
                            busrouteList.setNestedScrollingEnabled(false);

                        }




                        Vehicle_no.setText(busno);
                        routeCode.setText(busroutecode);
                        tittle.setText(routetittle);
                        about.setText(routediscription);
                        driverName.setText(driver_name);
                        driverNo.setText(driver_no);
                        co_driver.setText(co_drivername);
                        co_driverNo.setText(co_driverno);




                        wayPointData=data.toString();

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Information", msg);

                    }
                }

            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }


        @Override
        protected void onCancelled() {
            busrouteinfoAuthTask = null;
            spinner.setVisibility(View.INVISIBLE);


        }



    }


}
