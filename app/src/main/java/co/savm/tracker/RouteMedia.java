package co.savm.tracker;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import co.savm.R;
import co.savm.adapters.CollagemediaGrirdAdapter;
import co.savm.models.tracker.WayPointsArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.Utils;
import in.srain.cube.views.GridViewWithHeaderAndFooter;
import okhttp3.OkHttpClient;


public class RouteMedia extends Fragment {
    private GridViewWithHeaderAndFooter rv_mediagrid;
    TextView text_title,textDetails,ErrorText;
    static Activity activity;
    private static ProgressBusRouteInfo busrouteinfoAuthTask = null;
    private static ProgressBar spinner;
    String routetittle;
    String routediscription;
    static String Route_id;
    String RouteTittle;
    static String Adminuser;
    String Join_Value;
    static String AdminCreater;
    public String field_place_name,field_start_time,field_departure_time,field_way_points,lat,lng,field_photo,driver_name,driver_no,co_drivername,co_driverno;
    String busroutecode;
    String busno;
    public static ArrayList<WayPointsArray> mwaypointsList;



    public RouteMedia() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_route_media, container, false);
        activity = (Activity) view.getContext();
        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle = getArguments().getString("ROUTE_NAME");
        Adminuser = getArguments().getString("IS_FACULTY");
        Join_Value = getArguments().getString("IS_MEMBER");
        AdminCreater = getArguments().getString("IS_CREATER");

        ErrorText =view.findViewById(R.id.ErrorText);
        spinner= view.findViewById(R.id.progressBar);
        rv_mediagrid = view.findViewById(R.id.rv_mediagrid);
        textDetails = view.findViewById(R.id.textDetails);
        text_title = view.findViewById(R.id.text_title);
        mwaypointsList = new ArrayList<>();

        if (Adminuser.equals("TRUE") && Join_Value.equals("member")) {
            GetAllDetailsOfRoutes();

        }else {

            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");


        }



        return view;
    }

    private void GetAllDetailsOfRoutes() {
        busrouteinfoAuthTask = new ProgressBusRouteInfo();
        busrouteinfoAuthTask.execute();

    }

    private  class ProgressBusRouteInfo extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // spinner.setVisibility(View.VISIBLE);


        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEINFO +"/"+Route_id);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //  spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            busrouteinfoAuthTask = null;
            try {
                if (responce != null) {
                    //  spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {
                        WayPointsArray WaypointInfo1 = new WayPointsArray();
                        WayPointsArray WaypointInfo2 = new WayPointsArray();

                        JSONObject data = responce.getJSONObject("data");

                        JSONObject field_collection_way_points = data.getJSONObject("field_collection_way_points");
                        JSONObject Source = field_collection_way_points.getJSONObject("source");

                        String   field_photo1 = Source.getString("field_photo");


                        WaypointInfo1.field_photo = field_photo1;

                        mwaypointsList.add(WaypointInfo1);


                        JSONObject destination = field_collection_way_points.getJSONObject("destination");

                        String   field_photo2 = destination.getString("field_photo");

                        JSONArray waypoint = field_collection_way_points.getJSONArray("waypoints");
                        if(waypoint != null && waypoint.length() > 0 ) {

                            for (int i = 0; i < waypoint.length(); i++) {
                                WayPointsArray WaypointInfo = new WayPointsArray();

                                field_photo = waypoint.getJSONObject(i).getString("field_photo");

                                WaypointInfo.field_photo = field_photo;
                                mwaypointsList.add(WaypointInfo);


                            }


                            WaypointInfo2.field_photo = field_photo2;

                            mwaypointsList.add(WaypointInfo2);



                            if (mwaypointsList != null) {
                                SetValueTogird(mwaypointsList);
                                rv_mediagrid.setVisibility(View.VISIBLE);




                            }else {
                                ErrorText.setText(" No Media");
                                ErrorText.setVisibility(View.VISIBLE);

                            }








                        }






                    } else if (errorCode.equalsIgnoreCase("0")) {
                        // spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Information", msg);

                    }
                }

            } catch (JSONException e) {
                // spinner.setVisibility(View.INVISIBLE);

            }
        }


        @Override
        protected void onCancelled() {
            busrouteinfoAuthTask = null;
            //  spinner.setVisibility(View.INVISIBLE);


        }



    }

    private void SetValueTogird(ArrayList<WayPointsArray> mwaypointsList) {


        List<String> list = new ArrayList<>();



        for (int i = 0; i < mwaypointsList.size(); i++) {


            list.add(mwaypointsList.get(i).getField_photo());

        }

        rv_mediagrid.setAdapter(new CollagemediaGrirdAdapter(activity, list));




    }



}
