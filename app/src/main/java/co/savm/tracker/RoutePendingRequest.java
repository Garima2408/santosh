package co.savm.tracker;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.RoutePendindRequestAdapter;
import co.savm.models.tracker.RouteRequestPendingArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.Utils;
import okhttp3.OkHttpClient;

public class RoutePendingRequest extends BaseFragment {
    public static ArrayList<RouteRequestPendingArray> mRequestpendingList;
    private static RoutePendindRequestAdapter requestadapter;
    static RecyclerView PendingStudents;
    static TextView ErrorText;
    static String Route_id,RouteTittle,Adminuser,Join_Value,AdminCreater;
     private RecyclerView.LayoutManager layoutManager;
    private static RequestPendingListDisplay requestpendinglist = null;
     static Activity activity;
    private static ProgressBar spinner;


    public RoutePendingRequest() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_route_pending_request, container, false);
        activity = (Activity) view.getContext();
        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle = getArguments().getString("ROUTE_NAME");

        Adminuser = getArguments().getString("IS_FACULTY");
        Join_Value = getArguments().getString("IS_MEMBER");
        AdminCreater = getArguments().getString("IS_CREATER");


        mRequestpendingList=new ArrayList<>();
        layoutManager = new LinearLayoutManager(getContext());
        PendingStudents = view.findViewById(R.id.PendingStudents);
        PendingStudents.setHasFixedSize(true);
        PendingStudents.setLayoutManager(layoutManager);
        spinner= view.findViewById(R.id.progressBar);
        ErrorText =view.findViewById(R.id.ErrorText);

        if (Adminuser.equals("TRUE") && AdminCreater.equals("TRUE")) {

            requestpendinglist = new RequestPendingListDisplay();
            requestpendinglist.execute();


        }else if (Adminuser.equals("FALSE")) {

            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");



        }



        return view;

    }


    public static void CalledFromPendingAdapter() {
        mRequestpendingList.clear();

        requestpendinglist = new RequestPendingListDisplay();
        requestpendinglist.execute();


    }



    private static class RequestPendingListDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //  Utils.p_dialog(activity);
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_ROUTEJOINPENDINGREQUEST+"/"+Route_id+"/users?fields=id, etid, gid, state&parameters=state=2");
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseDataroute: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            requestpendinglist = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    mRequestpendingList.clear();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if(jsonArrayData != null && jsonArrayData.length() > 0 ) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {
                                JSONObject jsonObject = jsonArrayData.getJSONObject(i);

                                RouteRequestPendingArray requestInfo = new RouteRequestPendingArray();


                                RouteRequestPendingArray Request = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), RouteRequestPendingArray.class);

                                JSONObject ChildArray = jsonObject.getJSONObject("child");
                                if (ChildArray != null && ChildArray.length() > 0) {

                                    String childId = ChildArray.getString("child_uid");
                                    String childFirst = ChildArray.getString("field_first_name");
                                    String childLast = ChildArray.getString("field_last_name");
                                    String childPicture = ChildArray.getString("picture");




                                    Request.child_uid = childId;
                                    Request.child_field_first_name = childFirst;
                                    Request.child_field_last_name = childLast;
                                    Request.child_picture = childPicture;


                                }


                                mRequestpendingList.add(Request);
                                requestadapter = new RoutePendindRequestAdapter(activity, mRequestpendingList);
                                PendingStudents.setAdapter(requestadapter);


                            }
                        }else{


                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Request Is Pending");
                        }




                    } else if (errorCode.equalsIgnoreCase("0")) {

                        Utils.showAlertDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");
                        spinner.setVisibility(View.INVISIBLE);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            requestpendinglist = null;
            spinner.setVisibility(View.INVISIBLE);

        }
    }



}
