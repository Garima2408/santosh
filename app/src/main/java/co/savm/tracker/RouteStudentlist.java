package co.savm.tracker;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import co.savm.R;
import co.savm.adapters.TeacherViewStudentAttandanceAdapter;
import co.savm.models.ShowAttandanceDates;
import co.savm.models.TeacherViewStudentArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseFragment;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class RouteStudentlist extends BaseFragment {

    static Activity activity;
    private static ProgressBar spinner;
    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1;
    String Route_id,RouteTittle,Adminuser,AdminCreater,Join_Value;
     LinearLayout Mainheader;

    String weekSdate, weekLdate,CalenderMonth,ShiftId;
    public ArrayList<TeacherViewStudentArray> mviewattandanceList;
    public ArrayList<ShowAttandanceDates> mdateattandanceList;
    RelativeLayout MainHead;

    String date0,attendance0,date1,attendance1,date2,attendance2,date3,attendance3,date4,attendance4,date5,attendance5,date6,attendance6,date7,attendance7;

    private RecyclerView.LayoutManager layoutManager,mLayoutManager;
    private TeacherViewStudentAttandanceAdapter viewStudentAdapter;
    RecyclerView rv_studentList,attandance_Lists;
    private ClassStudentAttandanceDisplay classstudentdisplay = null;
    TextView weekstart, weekend, dayone, daytwo, daythree, dayfour, dayfive, daysix, dayseven, dayeight, weekeight, weekseven, weeksix, weekfive, weekfour, weekthree, weektwo, weekone,curentmonth,ErrorText;
    private int CalendarHour, CalendarMinute, CalendarSecon;
    private Calendar mcalender;
    private int startDay, startMonth, startYear;
    FrameLayout weekdate,top;
    RelativeLayout displaydates;
    private int mYear, mMonth, mDay, mHour, mMinute;
    MenuItem item;
    Spinner routes;
    View view1,view2;


    public RouteStudentlist() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_route_studentlist, container, false);
        activity = (Activity) view.getContext();

        Route_id = getArguments().getString("ROUTE_ID");
        RouteTittle= getArguments().getString("ROUTE_NAME");
        Adminuser = getArguments().getString("IS_FACULTY");
        Join_Value = getArguments().getString("IS_MEMBER");
        AdminCreater = getArguments().getString("IS_CREATER");

        ErrorText =view.findViewById(R.id.ErrorText);
        spinner= view.findViewById(R.id.progressBar);
        routes = view.findViewById(R.id.routes);
        materialDesignFAM = view.findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionButton1 = view.findViewById(R.id.material_design_floating_action_menu_item4);
        Mainheader =view.findViewById(R.id.Mainheader);
        view1 =view.findViewById(R.id.view1);
        view2 =view.findViewById(R.id.view2);


        layoutManager = new LinearLayoutManager(activity);
        mLayoutManager = new LinearLayoutManager(activity);
        // mLayoutManager = new LinearLayoutManager(AttandanceViewStudent.this, LinearLayoutManager.HORIZONTAL, false);
        mdateattandanceList = new ArrayList<>();

        weekstart = view.findViewById(R.id.weekstart);
        weekend = view.findViewById(R.id.weekend);
        displaydates= view.findViewById(R.id.displaydates);
        dayone = view.findViewById(R.id.dayone);
        daytwo = view.findViewById(R.id.daytwo);
        daythree = view.findViewById(R.id.daythree);
        dayfour = view.findViewById(R.id.dayfour);
        dayfive = view.findViewById(R.id.dayfive);
        daysix = view.findViewById(R.id.daysix);
        dayseven = view.findViewById(R.id.dayseven);
        dayeight = view.findViewById(R.id.dayeight);
        MainHead = view.findViewById(R.id.MainHead);
        weekeight = view.findViewById(R.id.weekeight);
        weekseven = view.findViewById(R.id.weekseven);
        weeksix = view.findViewById(R.id.weeksix);
        weekfive = view.findViewById(R.id.weekfive);
        weekfour = view.findViewById(R.id.weekfour);
        weekthree = view.findViewById(R.id.weekthree);
        weektwo = view.findViewById(R.id.weektwo);
        weekone = view.findViewById(R.id.weekone);
        curentmonth =view. findViewById(R.id.curentmonth);
        ErrorText  = view.findViewById(R.id.ErrorText);
        weekdate =view.findViewById(R.id.weekdate);
        top =view.findViewById(R.id.top);
        rv_studentList = view.findViewById(co.savm.R.id.rv_studentList);
        rv_studentList.setHasFixedSize(true);
        rv_studentList.setLayoutManager(layoutManager);
        rv_studentList.setAdapter(viewStudentAdapter);

        attandance_Lists = view.findViewById(co.savm.R.id.attandance_Lists);
        attandance_Lists.setHasFixedSize(true);
        attandance_Lists.setLayoutManager(mLayoutManager);
        // attandance_Lists.setAdapter(mDisplayDateAdapter);


        mcalender = Calendar.getInstance();
        CalendarHour = mcalender.get(Calendar.HOUR_OF_DAY);
        CalendarMinute = mcalender.get(Calendar.MINUTE);


        final Calendar datepicker = Calendar.getInstance();
        mYear = datepicker.get(Calendar.YEAR);
        mMonth = datepicker.get(Calendar.MONTH);
        mDay = datepicker.get(Calendar.DAY_OF_MONTH);




        Calendar c = Calendar.getInstance();

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = sdf.format(c.getTime());

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -6);
        System.out.println("Date = " + cal.getTime());

        SimpleDateFormat sdf3 = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDatee = sdf3.format(cal.getTime());


        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate2 = sdf2.format(mcalender.getTime());

        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate4 = sdf4.format(cal.getTime());


        weekstart.setText(formattedDate);
        weekend.setText(formattedDatee);
        //  CalenderMonth =formattedDate;

        weekSdate = formattedDate2;
        weekLdate = formattedDate4;






        weekstart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartdateDialog();

            }
        });
        weekend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   StartEnddateDialog();

            }
        });

        if (Adminuser.equals("TRUE")) {

            if(weekSdate !=null &&  weekLdate!=null){

                classstudentdisplay = new ClassStudentAttandanceDisplay();
                classstudentdisplay.execute(weekLdate,weekSdate,"morning");



            }


        }else if (Adminuser.equals("FALSE")) {
            materialDesignFAM.setVisibility(View.GONE);
            Mainheader.setVisibility(View.GONE);
            weekdate.setVisibility(View.GONE);
            top.setVisibility(View.GONE);
            view1.setVisibility(View.GONE);
            view2.setVisibility(View.GONE);
            ErrorText.setVisibility(View.VISIBLE);
            ErrorText.setText("You Are Not Authorised");
            setHasOptionsMenu(false);


        }

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(activity, BusAttandanceAdd.class);
                Bundle bundle = new Bundle();
                bundle.putString("ROUTE_ID", Route_id);
                bundle.putString("ROUTE_NAME", RouteTittle);
                intent.putExtras(bundle);
                startActivity(intent);



            }
        });
        // Spinner Drop down elements
        List<String> Shift = new ArrayList<String>();
        Shift.add("morning");
        Shift.add("evening");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, Shift);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        routes.setAdapter(dataAdapter);
        routes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                ShiftId = parent.getItemAtPosition(position).toString();

                Log.d("TAG", "Shift: " + ShiftId);

                if(ShiftId.contains("morning")){

                    classstudentdisplay = new ClassStudentAttandanceDisplay();
                    classstudentdisplay.execute(weekLdate,weekSdate,"morning");


                }else if(ShiftId.contains("evening")) {

                    classstudentdisplay = new ClassStudentAttandanceDisplay();
                    classstudentdisplay.execute(weekLdate,weekSdate,"evening");


                }





            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }


    private void StartdateDialog() {


        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int mYear, int mMonth, int mDay) {



                startYear = mYear;
                startMonth = mMonth;
                startDay = mDay;

                showDateStart(mYear, mMonth + 1, mDay);


            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(getActivity(), android.app.AlertDialog.THEME_HOLO_LIGHT, listener, mYear, mMonth, mDay);
        dpDialog.getDatePicker().setMaxDate(System.currentTimeMillis() +1000);
        dpDialog.show();

    }

    private void showDateStart(int year, int month, int day) {
        Log.d("TAG", "Start Date: " + day + "-" + month + "-" + year);
        weekSdate = String.valueOf(new StringBuilder().append(year).append("-")
                .append(month).append("-").append(day));
        weekstart.setText(new StringBuilder().append(day).append("-")
                .append(month).append("-").append(year));



        // Start date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(weekSdate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, -6);  // number of days to add
        String weekyyyy = sdf.format(c.getTime());
        weekLdate =weekyyyy;

        Log.d("TAG", "weekyyyy " +weekyyyy);

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");

        Date date = null;
        try {
            date = inputFormat.parse(weekyyyy);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);

        Log.d("Splited String ", "Splited String" + outputDateStr+outputDateStr);
        weekend.setText(outputDateStr.toString());
        CalenderMonth =outputDateStr;

    }




    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.submit_menu, menu);
        item = menu.findItem(R.id.Submit);
       // item.setVisible(false);

        super.onCreateOptionsMenu(menu, menuInflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.Submit:
                mdateattandanceList.clear();
                classstudentdisplay = new ClassStudentAttandanceDisplay();
                classstudentdisplay.execute(weekLdate,weekSdate,ShiftId);

                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }







    private class ClassStudentAttandanceDisplay extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            spinner.setVisibility(View.VISIBLE);

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();

            RequestBody body = new FormBody.Builder()
                    .add("from_date", args[0])
                    .add("to_date", args[1])
                    .add("type",args[2])
                    .build();



            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_ROUTEATTANDANCEDISPLAY+"/"+Route_id+"?offset=0&limit=130", body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Error", "There seems to be some problem with the Server. Try again later.");

                    }
                });


            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            classstudentdisplay = null;
            try {
                if (responce != null) {
                    spinner.setVisibility(View.INVISIBLE);
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        mviewattandanceList = new ArrayList<>();
                        //   mviewattandanceList.clear();

                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {


                                TeacherViewStudentArray CourseInfo = new TeacherViewStudentArray();
                                String student = jsonArrayData.getJSONObject(i).getString("student");
                                String Enrollment = jsonArrayData.getJSONObject(i).getString("enrollment_id");
                                String picture = jsonArrayData.getJSONObject(i).getString("picture");
                                String perc = jsonArrayData.getJSONObject(i).getString("perc");

                                JSONArray jsonArrayDates = new JSONArray(jsonArrayData.getJSONObject(i).getString("dates"));



                                //  mdateattandanceList.clear();
                                for (int j = 0; j < jsonArrayDates.length(); j++) {

                                    ShowAttandanceDates attandancedate = new ShowAttandanceDates();

                                    String date = jsonArrayDates.getJSONObject(j).getString("date");
                                    String attendance = jsonArrayDates.getJSONObject(j).getString("attendance");


                                    date0 = jsonArrayDates.getJSONObject(0).getString("date");
                                    attendance0 = jsonArrayDates.getJSONObject(0).getString("attendance");
                                    date1 = jsonArrayDates.getJSONObject(1).getString("date");
                                    attendance1 = jsonArrayDates.getJSONObject(1).getString("attendance");
                                    date2 = jsonArrayDates.getJSONObject(2).getString("date");
                                    attendance2 = jsonArrayDates.getJSONObject(2).getString("attendance");
                                    date3 = jsonArrayDates.getJSONObject(3).getString("date");
                                    attendance3 = jsonArrayDates.getJSONObject(3).getString("attendance");
                                    date4 = jsonArrayDates.getJSONObject(4).getString("date");
                                    attendance4 = jsonArrayDates.getJSONObject(4).getString("attendance");
                                    date5 = jsonArrayDates.getJSONObject(5).getString("date");
                                    attendance5 = jsonArrayDates.getJSONObject(5).getString("attendance");
                                    date6 = jsonArrayDates.getJSONObject(6).getString("date");
                                    attendance6 = jsonArrayDates.getJSONObject(6).getString("attendance");
                                   /* date7 = jsonArrayDates.getJSONObject(7).getString("date");
                                    attendance7 = jsonArrayDates.getJSONObject(7).getString("attendance");
*/




                                    attandancedate.date = date;
                                    attandancedate.attendance = attendance;
                                    mdateattandanceList.add(attandancedate);



                                }

                                CourseInfo.student = student;
                                CourseInfo.Enrollment =Enrollment;
                                CourseInfo.picture = picture;
                                CourseInfo.perc = perc;
                                CourseInfo.date0 =date0;
                                CourseInfo.date1 =date1;
                                CourseInfo.date2 =date2;
                                CourseInfo.date3 =date3;
                                CourseInfo.date4 =date4;
                                CourseInfo.date5 =date5;
                                CourseInfo.date6 =date6;
                               // CourseInfo.date7 =date7;
                                CourseInfo.attendance0 =attendance0;
                                CourseInfo.attendance1 =attendance1;
                                CourseInfo.attendance2 =attendance2;
                                CourseInfo.attendance3 =attendance3;
                                CourseInfo.attendance4 =attendance4;
                                CourseInfo.attendance5 =attendance5;
                                CourseInfo.attendance6 =attendance6;
                               // CourseInfo.attendance7 =attendance7;




                                mviewattandanceList.add(CourseInfo);


                                AddAllValues(mdateattandanceList);

                                viewStudentAdapter = new TeacherViewStudentAttandanceAdapter(activity, mviewattandanceList, mdateattandanceList);
                                rv_studentList.setAdapter(viewStudentAdapter);

                            }




                        } else {

                            ErrorText.setVisibility(View.VISIBLE);
                            ErrorText.setText("No Attandance");
                            weekdate.setVisibility(View.GONE);
                            top.setVisibility(View.GONE);
                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        spinner.setVisibility(View.INVISIBLE);
                        Utils.showAlertFragmentDialog(activity, "Information", msg);


                    }
                }
            } catch (JSONException e) {
                spinner.setVisibility(View.INVISIBLE);

            }
        }

        @Override
        protected void onCancelled() {
            classstudentdisplay = null;
            spinner.setVisibility(View.INVISIBLE);


        }
    }

    private void AddAllValues(ArrayList<ShowAttandanceDates> mdateattandanceList) {
        displaydates.setVisibility(View.VISIBLE);

        for (int i = 0; i < mdateattandanceList.size(); i++) {

            System.out.println(mdateattandanceList.get(i).getAttendance() + "....." + mdateattandanceList.get(i).getDate()); //prints element i

         /*   mDisplayDateAdapter = new DisplayDemoDatesAdapter(AttandanceViewStudent.this,  mdateattandanceList);
            attandance_Lists.setAdapter(mDisplayDateAdapter);*/
          /*  mDisplayDateAdapter = new DisplayAttandanceDateAdapter(AttandanceViewStudent.this, R.layout.list_ofdisplatdateattandance, mdateattandanceList);
            attandance_Lists.setAdapter(mDisplayDateAdapter);*/
        }



        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat outputFormat = new SimpleDateFormat("dd-MMM-yyyy");

        Date date = null;
        try {
            date = inputFormat.parse(mdateattandanceList.get(0).getDate());
            String outputDateStr = outputFormat.format(date);
            String[] outputx = outputDateStr.split("-");
            /* String[] Datevaluex = outputx[0].split(" ");*/
            curentmonth.setText(outputx[1]);

        } catch (ParseException e) {
            e.printStackTrace();
        }








        String one = mdateattandanceList.get(0).getDate();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt1 = null;
        try {
            dt1 = format1.parse(one);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format2 = new SimpleDateFormat("EEE");
        String finalDay = format2.format(dt1);
        Log.d("TAG", "finalDay: " + finalDay);

        String[] output = mdateattandanceList.get(0).getDate().split("-");
        String[] Datevalue = output[2].split(" ");
        dayone.setText(Datevalue[0]);
        weekone.setText(finalDay);


        String two = mdateattandanceList.get(1).getDate();
        SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt2 = null;
        try {
            dt2 = format3.parse(two);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format4 = new SimpleDateFormat("EEE");
        String finalDay2 = format4.format(dt2);
        Log.d("TAG", "finalDay2: " + finalDay2);
        String[] output1 = mdateattandanceList.get(1).getDate().split("-");
        String[] Datevalue1 = output1[2].split(" ");
        daytwo.setText(Datevalue1[0]);
        weektwo.setText(finalDay2);


        String three = mdateattandanceList.get(2).getDate();
        SimpleDateFormat format5 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt3 = null;
        try {
            dt3 = format5.parse(three);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format6 = new SimpleDateFormat("EEE");
        String finalDay3 = format6.format(dt3);
        Log.d("TAG", "finalDay3: " + finalDay3);
        String[] output2 = mdateattandanceList.get(2).getDate().split("-");
        String[] Datevalue2 = output2[2].split(" ");
        daythree.setText(Datevalue2[0]);
        weekthree.setText(finalDay3);


        String four = mdateattandanceList.get(3).getDate();
        SimpleDateFormat format7 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt4 = null;
        try {
            dt4 = format7.parse(four);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format8 = new SimpleDateFormat("EEE");
        String finalDay4 = format8.format(dt4);
        Log.d("TAG", "finalDay4: " + finalDay4);
        String[] output4 = mdateattandanceList.get(3).getDate().split("-");
        String[] Datevalue4 = output4[2].split(" ");
        dayfour.setText(Datevalue4[0]);
        weekfour.setText(finalDay4);


        String five = mdateattandanceList.get(4).getDate();
        SimpleDateFormat format9 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt5 = null;
        try {
            dt5 = format9.parse(five);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format10 = new SimpleDateFormat("EEE");
        String finalDay5 = format10.format(dt5);
        Log.d("TAG", "finalDay5: " + finalDay5);
        String[] output5 = mdateattandanceList.get(4).getDate().split("-");
        String[] Datevalu5e = output5[2].split(" ");
        dayfive.setText(Datevalu5e[0]);
        weekfive.setText(finalDay5);

        String six = mdateattandanceList.get(5).getDate();
        SimpleDateFormat format11 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt6 = null;
        try {
            dt6 = format11.parse(six);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format12 = new SimpleDateFormat("EEE");
        String finalDay6 = format12.format(dt6);
        Log.d("TAG", "finalDay6: " + finalDay6);
        String[] output6 = mdateattandanceList.get(5).getDate().split("-");
        String[] Datevalue6 = output6[2].split(" ");
        daysix.setText(Datevalue6[0]);
        weeksix.setText(finalDay6);

        String seven = mdateattandanceList.get(6).getDate();
        SimpleDateFormat format13 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt7 = null;
        try {
            dt7 = format13.parse(seven);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format14 = new SimpleDateFormat("EEE");
        String finalDaydt7 = format14.format(dt7);
        Log.d("TAG", "finalDaydt7: " + finalDaydt7);
        String[] output7 = mdateattandanceList.get(6).getDate().split("-");
        String[] Datevalue7 = output7[2].split(" ");
        dayseven.setText(Datevalue7[0]);
        weekseven.setText(finalDaydt7);



       /* String eight = mdateattandanceList.get(7).getDate();
        SimpleDateFormat format15 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dt8 = null;
        try {
            dt8 = format15.parse(eight);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat format16 = new SimpleDateFormat("EEE");
        String finalDay8 = format16.format(dt8);
        Log.d("TAG", "finalDay: " + finalDay8);
        String[] output8 = mdateattandanceList.get(7).getDate().split("-");
        String[] Datevalue8 = output8[2].split(" ");
        dayeight.setText(Datevalue8[0]);
        weekeight.setText(finalDay8);
*/

    }


}


