package co.savm.buttonanimation;

public interface OnAnimationEndListener {
    void onAnimationEnd(LikeButton likeButton);
}
