package co.savm.parent;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import co.savm.R;
import co.savm.college.CourseModuleAssignments;
import co.savm.college.CourseModuleAttandance;
import co.savm.college.CourseModuleClasses;
import co.savm.college.CourseModuleExams;
import co.savm.college.CourseModuleGroup;
import co.savm.college.CourseModuleInfo;
import co.savm.college.CourseModuleResults;

public class ParentCourseModuleDetails extends AppCompatActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    String Course_id,CourseTittle,CourseImage,Adminuser,Join_Value;
    private Bundle bundle;
    TextView toolbar_title;
    ImageView backone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_course_module_details);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.mipmap.backarrow);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();

        if (extras.containsKey("CHECKEDVALUETRUE")) {

            if (extras.getString("CHECKEDVALUETRUE").equals("TRUE")) {
                Course_id = intent.getStringExtra("COURSE_ID");
                CourseTittle = intent.getStringExtra("TITTLE");
                Adminuser = "TRUE";


            } else if (extras.getString("CHECKEDVALUETRUE").equals("FALSE")) {

                Course_id = intent.getStringExtra("COURSE_ID");
                CourseTittle = intent.getStringExtra("TITTLE");
                Adminuser = "FALSE";
            }
        }

        if (extras.containsKey("JOINREQUEST")) {

            if (extras.getString("JOINREQUEST").equals("member")) {

                Join_Value = "member";


            } else if (extras.getString("JOINREQUEST").equals("pending")) {


                Join_Value = "pending";
            }
            else if (extras.getString("JOINREQUEST").equals("nomember")) {


                Join_Value = "nomember";
            }

            else if (extras.getString("JOINREQUEST").equals("blocked")) {


                Join_Value = "nomember";
            }


        }



        actionBar.setTitle(CourseTittle);
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(6);


    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        bundle = new Bundle();
        bundle.putString("COURSE_ID", Course_id);
        bundle.putString("TITTLE", CourseTittle);
        bundle.putString("ADMIN", Adminuser);
        bundle.putString("JOINED", Join_Value);

        CourseModuleInfo info = new CourseModuleInfo();
        info.setArguments(bundle);

        CourseModuleGroup group = new CourseModuleGroup();
        group.setArguments(bundle);

        CourseModuleClasses classlist = new CourseModuleClasses();
        classlist.setArguments(bundle);


        CourseModuleExams exam = new CourseModuleExams();
        exam.setArguments(bundle);

        CourseModuleAssignments assignment = new CourseModuleAssignments();
        assignment.setArguments(bundle);

        CourseModuleResults result = new CourseModuleResults();
        result.setArguments(bundle);

        CourseModuleAttandance attendance = new CourseModuleAttandance();
        attendance.setArguments(bundle);





        adapter.addFragment(info, "Info");
        adapter.addFragment(group, "Group");
        adapter.addFragment(classlist, "Class");
        adapter.addFragment(exam, "Exam");
        adapter.addFragment(assignment, "Assignment");
        adapter.addFragment(result, "Result");
        adapter.addFragment(attendance, "Attendance");


        viewPager.setAdapter(adapter);
    }





    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }





    @Override
    public void onBackPressed() {
        Intent i = new Intent(ParentCourseModuleDetails.this, ParentCourseModule.class);
        finish();

    }

}
