package co.savm.parent;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import co.savm.R;
import co.savm.activities.SelectYourType;
import co.savm.models.CollageLists;
import co.savm.models.CollageRoleArray;
import co.savm.models.UserDetail;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.Constants;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ParentRegistration extends BaseAppCompactActivity {
    EditText edt_enrolment,edt_studentname,edt_email_id;
    Button btn_go_dashboard;
    TextView collageName;
    String enrollment,name,email, Membershipid ,child_student;
    private RegistrationParentAuthTask registrationparentAuthTask = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_registration);
        edt_enrolment = findViewById(R.id.edt_enrolment);
        edt_studentname = findViewById(R.id.edt_studentname);
        edt_email_id = findViewById(R.id.edt_email_id);
        btn_go_dashboard = findViewById(R.id.btn_go_dashboard);
        collageName = findViewById(R.id.collageName);
        String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        SpannableString spString = new SpannableString(Topheading);
        AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
        spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        collageName.setText(spString);



        btn_go_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                attemptToParentRegistration();


            }
        });
    }

    private void attemptToParentRegistration() {
        edt_enrolment.setError(null);
        edt_studentname.setError(null);
        edt_email_id.setError(null);


        // Store values at the time of the login attempt.
        enrollment= edt_enrolment.getText().toString().trim();
        name = edt_studentname.getText().toString().trim();
        email = edt_email_id.getText().toString().trim();

        CollageRoleArray userRoleDetail = new CollageRoleArray();
        userRoleDetail.role = "16";
        userRoleDetail.tnid = SessionManager.getInstance(getActivity()).getCollage().getTnid();
        userRoleDetail.title = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        userRoleDetail.field_group_image = SessionManager.getInstance(getActivity()).getCollage().getField_group_image();
        userRoleDetail.CollageLogo =SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo();
        userRoleDetail.CollageRole ="parent";
        userRoleDetail.CollageEmail =email;
        userRoleDetail.CollageStudentNamev = name;
        userRoleDetail.CollageEnrollment = enrollment;

        SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);





        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            focusView = edt_email_id;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));

            // Check for a valid email address.
        } else if (!Utils.isValidEmailAddress(email)) {
            focusView = edt_email_id;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_invalid_email));
            // Check for a valid password, if the user entered one.
        }else if (co.savm.utils.TextUtils.isNullOrEmpty(enrollment)) {
            // check for First Name
            focusView = edt_enrolment;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(name)) {
            // check for First Name
            focusView = edt_studentname;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            registrationparentAuthTask = new RegistrationParentAuthTask();
            registrationparentAuthTask.execute();



        }
    }


    private class RegistrationParentAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("group_type","node")
                    .add("membership type","og_membership_type_college")
                    .add("field_i_am_a","parents")
                    .add("state","1")
                    .add("etid",SessionManager.getInstance(getActivity()).getUser().userprofile_id)
                    .add("gid",SessionManager.getInstance(getActivity()).getCollage().getTnid())
                    .add("field_college_email",email)
                    .add("field_enrollment_number",enrollment)
                    .add("field_name","field_college_member")
                    .add("field_student_name",name)
                    .add("roles[16]","parents")

                    .build();




            try {
                String responseData = ApiCall.POSTHEADER(client, URLS.URL_STUDENETREGISTRATION,body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                ParentRegistration.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            registrationparentAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONObject data = responce.getJSONObject("data");

                        if (data != null && data.length() > 0) {
                             Membershipid = data.getString("membership_id");
                             child_student = data.getString("child_student");

                            Log.d("TAG", "membership_id: " + Membershipid +child_student);

                        } else {

                        }


                        CollageLists lists = new CollageLists();
                        lists.setTitle(SessionManager.getInstance(getActivity()).getCollage().getTitle());
                        lists.setTnid(SessionManager.getInstance(getActivity()).getCollage().getTnid());
                        lists.setField_group_image(SessionManager.getInstance(getActivity()).getCollage().getField_group_image());
                        lists.setField_groups_logo(SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo());
                        lists.setLat(SessionManager.getInstance(getActivity()).getCollage().getLat());
                        lists.setLng(SessionManager.getInstance(getActivity()).getCollage().getLng());
                        lists.setMultiple(SessionManager.getInstance(getActivity()).getCollage().getMultiple());
                        lists.setType(SessionManager.getInstance(getActivity()).getCollage().getType());

                        lists.setCollageMemberShipId(Membershipid);
                        SessionManager.getInstance(getActivity()).saveCollage(lists);

                        UserDetail userDetail = new UserDetail();
                        userDetail.userprofile_id = child_student;
                        userDetail.ParentUid =SessionManager.getInstance(getActivity()).getUser().getUserprofile_id();
                        userDetail.username = SessionManager.getInstance(getActivity()).getUser().getUsername();
                        userDetail.email =SessionManager.getInstance(getActivity()).getUser().getEmail();
                        userDetail.FirstName = SessionManager.getInstance(getActivity()).getUser().getFirstName();
                        userDetail.LastName = SessionManager.getInstance(getActivity()).getUser().getLastName();
                        userDetail.photo = SessionManager.getInstance(getActivity()).getUser().getPhoto();
                        userDetail.Userwallpic = SessionManager.getInstance(getActivity()).getUser().getUserwallpic();
                        //  userDetail.colleges = user.getJSONArray("colleges");

                        SessionManager.getInstance(getActivity()).saveUser(userDetail);

                        Intent upanel = new Intent(ParentRegistration.this, ParentMain.class);
                        Utils.getSharedPreference(ParentRegistration.this).edit()
                                .putInt(Constants.USER_ROLE, Constants.ROLE_PARENT).apply();
                        Utils.getSharedPreference(ParentRegistration.this).edit()
                                .putInt(Constants.RUNNIN_FIRST, Constants.ROLE_RUNNING_TRUE).apply();

                        Utils.getSharedPreference(ParentRegistration.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_CAMPUSFEED, Constants.ROLE_RUNNING_TRUE_CAMPUSFEED).apply();

                        Utils.getSharedPreference(ParentRegistration.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_PROFILE, Constants.ROLE_RUNNING_TRUE_PROFILE).apply();

                        Utils.getSharedPreference(ParentRegistration.this).edit()
                                .putInt(Constants.RUNNIN_FIRST_CHAT, Constants.ROLE_RUNNING_TRUE_CHAT).apply();


                        startActivity(upanel);

                        finish();







                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            registrationparentAuthTask = null;
            hideLoading();


        }
    }
    @Override
    public void onBackPressed() {
        Intent i = new Intent(ParentRegistration.this, SelectYourType.class);
        startActivity(i);

        finish();
    }
}

