package co.savm.parent;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.adapters.CourseModuleAdapter;
import co.savm.models.CoursemoduleArray;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.PaginationScrollListener;
import co.savm.utils.SessionManager;
import okhttp3.OkHttpClient;

public class ParentCourseModule extends BaseAppCompactActivity implements SearchView.OnQueryTextListener {

    String Offsetvalue;
    public ArrayList<CoursemoduleArray> mCouseList;
    private CourseModuleAdapter coursemoduleadapter;
    RecyclerView rv_coursemodule;
    SwipeRefreshLayout swipeContainer;
    private RecyclerView.LayoutManager layoutManager;
    private CourceModuleList courcemoduleList = null;
    private boolean isLoading= false;
    private int PAGE_SIZE = 0;
    private static CourceModuleSearchedList courcemodulesearchList = null;
    // private CourceModuleListRefreshed courcemoduleListRefresh = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parent_course_module);
        mCouseList=new ArrayList<>();
        swipeContainer =findViewById(R.id.swipeContainer);

        layoutManager = new LinearLayoutManager(this);

        rv_coursemodule = findViewById(R.id.rv_coursemodule);
        rv_coursemodule.setHasFixedSize(true);
        rv_coursemodule.setLayoutManager(layoutManager);



        coursemoduleadapter = new CourseModuleAdapter(rv_coursemodule, mCouseList ,ParentCourseModule.this);
        rv_coursemodule.setAdapter(coursemoduleadapter);
        swipeContainer =findViewById(R.id.swipeContainer);

        courcemoduleList = new CourceModuleList();
        courcemoduleList.execute();
        inItView();
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                // implement Handler to wait for 3 seconds and then update UI means update value of TextView
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // cancle the Visual indication of a refresh
                        PAGE_SIZE=0;
                        isLoading=false;
                        swipeContainer.setRefreshing(true);
                        rv_coursemodule.setVisibility(View.INVISIBLE);

                        courcemoduleList = new CourceModuleList();
                        courcemoduleList.execute();

                    }
                }, 1000);
            }
        });




    }



    private void inItView(){
        swipeContainer.setRefreshing(true);

        rv_coursemodule.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            protected void loadMoreItems() {

                if(!isLoading){


                    Log.i("loadinghua", "im here now");
                    isLoading= true;
                    PAGE_SIZE+=20;

                    coursemoduleadapter.addProgress();

                    courcemoduleList = new CourceModuleList();
                    courcemoduleList.execute();

                    //fetchData(fYear,,);
                }else
                    Log.i("loadinghua", "im else now");

            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return false;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });
    }



    private class CourceModuleList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMODULE+"?"+"cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid() +"&offset="+PAGE_SIZE+"&limit=20");
                Log.d("TAG", "responseData: " + responseData);
                jsonObject = new JSONObject(responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                ParentCourseModule.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swipeContainer.setRefreshing(false);
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

                coursemoduleadapter.removeProgress();
                isLoading=false;

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
              courcemoduleList = null;

            try {
                if (responce != null) {
                    rv_coursemodule.setVisibility(View.VISIBLE);
                    swipeContainer.setRefreshing(false);


                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if (PAGE_SIZE==0){
                            if (mCouseList.size()>0){
                                mCouseList.clear();
                            }
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                mCouseList.add(CourseInfo);

                            }

                            rv_coursemodule.setVisibility(View.VISIBLE);

                            coursemoduleadapter.setInitialData(mCouseList);

                        }else{

                            swipeContainer.setRefreshing(false);
                            coursemoduleadapter.removeProgress();
                            ArrayList<CoursemoduleArray> arrayList=new ArrayList<>();
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                arrayList.add(CourseInfo);

                            }
                            coursemoduleadapter.addData(arrayList);

                            isLoading= false;


                        }



                        // view.setVisibility(View.GONE);


                    } else if (errorCode.equalsIgnoreCase("0")) {
                        // spinner.setVisibility(View.INVISIBLE);
                        showAlertDialog(msg);

                        swipeContainer.setRefreshing(false);


                    }
                }
            } catch (JSONException e) {
                swipeContainer.setRefreshing(false);

            }
        }
        @Override
        protected void onCancelled() {
            courcemoduleList = null;
            swipeContainer.setRefreshing(false);


        }
    }


    private class CourceModuleSearchedList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COLLAGECOURCEMODULE+"?"+"cid="+SessionManager.getInstance(getActivity()).getCollage().getTnid()+"&"+"search="+args[0]);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                ParentCourseModule.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            courcemodulesearchList = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {


                        JSONArray jsonArrayData = responce.getJSONArray("data");

                        if (jsonArrayData != null && jsonArrayData.length() > 0) {
                            for (int i = 0; i < jsonArrayData.length(); i++) {

                                CoursemoduleArray CourseInfo = new Gson().fromJson(jsonArrayData.getJSONObject(i).toString(), CoursemoduleArray.class);
                                mCouseList.add(CourseInfo);
                                coursemoduleadapter = new CourseModuleAdapter(rv_coursemodule, mCouseList ,ParentCourseModule.this);
                                rv_coursemodule.setAdapter(coursemoduleadapter);

                            }
                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ParentCourseModule.this);
                            builder.setTitle("Info");
                            builder.setMessage(getString(R.string.No_course))
                                    .setCancelable(false)
                                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();

                                        }
                                    });
                            AlertDialog alert = builder.create();
                            alert.show();

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            courcemodulesearchList = null;
            hideLoading();


        }
    }







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_menu, menu);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));

        searchView.setOnQueryTextListener(this);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        Log.d("TAG", "query:"+ query);
        mCouseList.clear();
        courcemodulesearchList = new CourceModuleSearchedList();
        courcemodulesearchList.execute(query);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {



        return true;
    }






    @Override
    public void onBackPressed() {
        PAGE_SIZE=0;
        isLoading=false;
        finish();
    }
}



