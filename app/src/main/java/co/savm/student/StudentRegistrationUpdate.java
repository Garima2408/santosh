package co.savm.student;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import co.savm.R;
import co.savm.activities.SignIn;
import co.savm.models.CollageRoleArray;
import co.savm.models.CourseMainList;
import co.savm.network.ApiCall;
import co.savm.network.OkHttpClientObject;
import co.savm.network.URLS;
import co.savm.utils.BaseAppCompactActivity;
import co.savm.utils.SessionManager;
import co.savm.utils.Utils;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class StudentRegistrationUpdate  extends BaseAppCompactActivity {

    EditText edt_enrolment,edt_bach,edt_branch,edt_selectsub,edt_email_id;
    Button btn_go_dashboard;
    Spinner edt_selectsubSpinner;
    TextView collageName;
    String enrollment,batch,branch,subject,email;
    private ArrayList<String> courselist;
    ArrayList<CourseMainList> listofcourse;
    private RegistrationStudentAuthTask registrationstudentAuthTask = null;
    private ProgressSearchOfCourseList progresssearchofcourselist = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_registration_update);
        GetTheListOfCourse();

        edt_enrolment = findViewById(co.savm.R.id.edt_enrolment);
        edt_bach = findViewById(co.savm.R.id.edt_bach);
        edt_branch = findViewById(co.savm.R.id.edt_branch);
        edt_selectsub = findViewById(co.savm.R.id.edt_selectsub);
        edt_email_id = findViewById(co.savm.R.id.edt_email_id);
        btn_go_dashboard = findViewById(co.savm.R.id.btn_go_dashboard);
        collageName = findViewById(co.savm.R.id.collageName);
        courselist = new ArrayList<String>();
        listofcourse = new ArrayList<CourseMainList>();
        edt_selectsubSpinner = findViewById(co.savm.R.id.edt_selectsubSpinner);

        String Topheading = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        Log.d("TAG", "getTnid: " + SessionManager.getInstance(getActivity()).getCollage().getTnid());
        SpannableString spString = new SpannableString(Topheading);
        AlignmentSpan.Standard aligmentSpan = new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER);
        spString.setSpan(aligmentSpan, 0, spString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        collageName.setText(spString);

        displayUserData();


        btn_go_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                attemptToStudentRegistration();

            }
        });
    }

    private void displayUserData() {

            if (!co.savm.utils.TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageEnrollment())) {
                edt_enrolment.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageEnrollment());
            }
            if (!co.savm.utils.TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBatch())) {
                edt_bach.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBatch());
            }
            if (!co.savm.utils.TextUtils.isNullOrEmpty(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBranch())) {
                edt_branch.setText(SessionManager.getInstance(getActivity()).getUserClgRole().getCollageBranch());
            }
        if (!co.savm.utils.TextUtils.isNullOrEmpty(getUser().email)) {
            edt_email_id.setText(getUser().email);
        }


    }

    private void GetTheListOfCourse() {
        progresssearchofcourselist = new ProgressSearchOfCourseList();
        progresssearchofcourselist.execute();

    }


    private void attemptToStudentRegistration() {

        edt_enrolment.setError(null);
        edt_bach.setError(null);
        edt_branch.setError(null);
        edt_selectsub.setError(null);
        edt_email_id.setError(null);


        // Store values at the time of the login attempt.
        enrollment= edt_enrolment.getText().toString().trim();
        batch = edt_bach.getText().toString().trim();
        branch = edt_branch.getText().toString().trim();
        subject = edt_selectsub.getText().toString().trim();
        email = edt_email_id.getText().toString().trim();


        CollageRoleArray userRoleDetail = new CollageRoleArray();
        userRoleDetail.role = "13";
        userRoleDetail.tnid = SessionManager.getInstance(getActivity()).getCollage().getTnid();
        userRoleDetail.title = SessionManager.getInstance(getActivity()).getCollage().getTitle();
        userRoleDetail.field_group_image = SessionManager.getInstance(getActivity()).getCollage().getField_group_image();
        userRoleDetail.CollageLogo =SessionManager.getInstance(getActivity()).getCollage().getField_groups_logo();
        userRoleDetail.CollageRole ="student";
        userRoleDetail.CollageEmail =email;
        userRoleDetail.CollageBatch =batch;
        userRoleDetail.CollageBranch =branch;
        userRoleDetail.CollageEnrollment =enrollment;

        SessionManager.getInstance(getActivity()).saveUserClgRole(userRoleDetail);




        Log.d("TAG", "subject: " + subject);
        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            focusView = edt_email_id;
            cancel = true;
            showSnackbarMessage(getString(co.savm.R.string.error_field_required));

            // Check for a valid email address.
        } else if (!Utils.isValidEmailAddress(email)) {
            focusView = edt_email_id;
            cancel = true;
            showSnackbarMessage(getString(co.savm.R.string.error_invalid_email));
            // Check for a valid password, if the user entered one.
        }else if (co.savm.utils.TextUtils.isNullOrEmpty(enrollment)) {
            // check for First Name
            focusView = edt_enrolment;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(batch)) {
            // check for First Name
            focusView = edt_bach;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(branch)) {
            // check for First Name
            focusView = edt_branch;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        else if (co.savm.utils.TextUtils.isNullOrEmpty(subject)) {
            // check for First Name
            focusView = edt_selectsub;
            cancel = true;
            showSnackbarMessage(getString(R.string.error_field_required));
        }
        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick toggle_off a background task to
            // perform the user login attempt.
            registrationstudentAuthTask = new RegistrationStudentAuthTask();
            registrationstudentAuthTask.execute();



        }
    }




    private class RegistrationStudentAuthTask extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();


            RequestBody body = new FormBody.Builder()
                    .add("entity_type","user")
                    .add("etid",SessionManager.getInstance(getActivity()).getUser().userprofile_id)
                    .add("gid",SessionManager.getInstance(getActivity()).getCollage().getTnid())
                    .add("group_type","node")
                    .add("state","1")
                    .add("field_i_am_a","student")
                    .add("field_branch",branch)
                    .add("field_enrollment_number",enrollment)
                    .add("field_member_course",subject)
                    .add("field_college_email",email)
                    .add("field_batch_year",batch)
                    .add("roles[13]","student")
                    .build();





            try {
                String responseData = ApiCall.PUTHEADER(client, URLS.URL_STUDENETREGISTRATION+"/"+SessionManager.getInstance(getActivity()).getCollage().getCollageMemberShipId(),body);
                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);
                Log.d("TAG", "mid: " + SessionManager.getInstance(getActivity()).getCollage().getCollageMemberShipId());

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                StudentRegistrationUpdate.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.savm.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            registrationstudentAuthTask = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        AlertDialog.Builder builder = new AlertDialog.Builder(StudentRegistrationUpdate.this);
                        builder.setMessage("Role Change Successfully.Login Again")
                                .setCancelable(false)
                                .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        Intent upanel = new Intent(StudentRegistrationUpdate.this, SignIn.class);
                                        startActivity(upanel);
                                        dialog.dismiss();
                                        finish();

                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();




                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            registrationstudentAuthTask = null;
            hideLoading();


        }
    }


    private class ProgressSearchOfCourseList extends AsyncTask<String, JSONObject, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showLoading();

        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject jsonObject = null;
            OkHttpClient client = OkHttpClientObject.getOkHttpClientObject();




            try {
                String responseData = ApiCall.GETHEADER(client, URLS.URL_COURCELIST+"?cid="+ SessionManager.getInstance(getActivity()).getCollage().getTnid());

                jsonObject = new JSONObject(responseData);
                Log.d("TAG", "responseData: " + responseData);

            } catch (JSONException | IOException e) {

                e.printStackTrace();
                StudentRegistrationUpdate.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoading();
                        showAlertDialog(getString(co.savm.R.string.error_something_wrong));
                    }
                });

            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject responce) {
            progresssearchofcourselist = null;
            try {
                if (responce != null) {
                    hideLoading();
                    String errorCode = responce.getString("status");
                    final String msg = responce.getString("message");

                    if (errorCode.equalsIgnoreCase("1")) {

                        JSONArray data = responce.getJSONArray("data");

                        for (int i=0; i<data.length();i++) {
                            CourseMainList GoalInfo = new Gson().fromJson(data.getJSONObject(i).toString(), CourseMainList.class);
                            courselist.add(GoalInfo.getName());
                            listofcourse.add(GoalInfo);

                            edt_selectsubSpinner.setAdapter(new ArrayAdapter<String>(StudentRegistrationUpdate.this,
                                    android.R.layout.simple_spinner_dropdown_item,
                                    courselist));


                            edt_selectsubSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    edt_selectsub.setText(listofcourse.get(position).getTid());

                                    Log.d("TAG", "edt_selectsub: " + edt_selectsub);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {}

                            });

                        }

                    } else if (errorCode.equalsIgnoreCase("0")) {
                        hideLoading();
                        showAlertDialog(msg);



                    }
                }
            } catch (JSONException e) {
                hideLoading();

            }
        }

        @Override
        protected void onCancelled() {
            progresssearchofcourselist = null;
            hideLoading();


        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}




